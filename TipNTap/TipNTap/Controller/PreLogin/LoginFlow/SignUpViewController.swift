//
//  SignUpViewController.swift
//  TipNTap
//
//  Created by TecOrb on 22/01/18.
//  Copyright © 2018 Nakul Sharma. All rights reserved.
//

import UIKit
import CountryPickerView


class SignUpViewController: UIViewController {
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var countryPickerView: UIView!

    @IBOutlet weak var phoneNumberButton: UIButton!
    @IBOutlet weak var phoneNumberTextField: FloatLabelTextField!

    @IBOutlet weak var userNameButton: UIButton!
    @IBOutlet weak var userNameTextField: FloatLabelTextField!

    @IBOutlet weak var passwordButton: UIButton!
    @IBOutlet weak var passwordTextField: FloatLabelTextField!

    @IBOutlet weak var signUpButton: UIButton!

    var cpv : CountryPickerView!
    var selectedCountry: Country!

    var appAction : appAction = .signUp

    override func viewDidLoad() {
        super.viewDidLoad()
        self.textFieldSetUp()
        self.countryPickerSetUp()
        self.containerView.setNeedsLayout()
        self.containerView.layoutIfNeeded()
    }
    func textFieldSetUp() {
        self.phoneNumberTextField.addTarget(self, action:#selector(refreshLoginEditingFields(textField:)), for: .editingChanged)
        self.passwordTextField.addTarget(self, action:#selector(refreshLoginEditingFields(textField:)), for: .editingChanged)
        self.userNameTextField.addTarget(self, action:#selector(refreshLoginEditingFields(textField:)), for: .editingChanged)
        self.phoneNumberTextField.titleFont = fonts.Raleway.semiBold.font(.small)
        self.passwordTextField.titleFont = fonts.Raleway.semiBold.font(.small)
        self.userNameTextField.titleFont = fonts.Raleway.semiBold.font(.small)

        self.phoneNumberTextField.delegate = self
        self.passwordTextField.delegate = self
        self.userNameTextField.delegate = self
    }

    @IBAction func onClickBackButton(_ sender: UIBarButtonItem){
        self.navigationController?.pop(true)
    }

    @IBAction func onClickLogIn(_ sender: UIButton){
        self.navigationController?.pop(false)
    }

    @IBAction func onClickOfTermsAndCondition(_ sender: UIButton){
        let navigationController = AppStoryboard.Settings.viewController(PrivacyPolicyNavigationController.self)
        for vc in navigationController.viewControllers{
            if let pvc = vc as? PrivacyPolicyViewController{
                pvc.isPrivacyPolicy = false
                pvc.fromMenu = false
            }
        }
        self.present(navigationController, animated: true, completion: nil)
    }

    @IBAction func onClickOfPrivacyPolicy(_ sender: UIButton){
        let navigationController = AppStoryboard.Settings.viewController(PrivacyPolicyNavigationController.self)
        for vc in navigationController.viewControllers{
            if let pvc = vc as? PrivacyPolicyViewController{
                pvc.isPrivacyPolicy = true
                pvc.fromMenu = false
            }
        }
        self.present(navigationController, animated: true, completion: nil)
    }
    @IBAction func onClickSignUpButton(_ sender: UIButton){
        guard let fullName = userNameTextField.text else {
            NKToastHelper.sharedInstance.showAlert(self, title: warningMessage.title, message: "Please enter your full name", completionBlock: {})
            return
        }

        let countryCode = self.selectedCountry.phoneCode
        guard let phoneNumber = phoneNumberTextField.text else {
            NKToastHelper.sharedInstance.showAlert(self, title: warningMessage.title, message: "Please enter your contact", completionBlock: {})
            return
        }
        if !phoneNumber.contains(countryCode){
            NKToastHelper.sharedInstance.showAlert(self, title: warningMessage.title, message: "Please reenter your phone number by selecting country first", completionBlock: {})
            return
        }
        guard let password = passwordTextField.text else {
            NKToastHelper.sharedInstance.showAlert(self, title: warningMessage.title, message: "Please enter your password", completionBlock: {})
            return
        }
        
        let withoutCountryCode = phoneNumber.replacingOccurrences(of: countryCode, with: "")
        let actuallPhoneNumber = withoutCountryCode.removingWhitespaces()

        let validation = self.validateParams(fullName: fullName, countryCode: countryCode, phoneNumber: actuallPhoneNumber, password: password)
        if !validation.success{
            AppSettings.shared.interestedInLoginWithTouchID = true
            AppSettings.shared.isFirstTimeLogin = true
            NKToastHelper.sharedInstance.showAlert(self, title: warningMessage.title, message: validation.message, completionBlock: {})
            return
        }
        self.checkUserStatus(fullName: fullName, phoneNumber: actuallPhoneNumber, countryCode: countryCode, password: password)

    }

    func checkUserStatus(fullName:String,phoneNumber:String,countryCode:String,password:String){
        if !AppSettings.isConnectedToNetwork{
            NKToastHelper.sharedInstance.showAlert(self, title: warningMessage.title, message: warningMessage.networkIsNotConnected.rawValue)
            return
        }

        LoginService.sharedInstance.checkUserStatus(countryCode, phoneNumber: phoneNumber,fullName: fullName) {[fullName,phoneNumber,countryCode,password] (isExist, message) in
            if isExist{
                NKToastHelper.sharedInstance.showAlert(self, title: warningMessage.title, message: message)
            }else{
                self.goForSecurityQuestions(fullName: fullName, phoneNumber: phoneNumber, countryCode: countryCode, password: password)
            }
        }
    }

    func goForSecurityQuestions(fullName:String,phoneNumber:String,countryCode:String,password:String){
        let userSecurityQuestionVC = AppStoryboard.Main.viewController(SelectedSecurityQuestionsController.self)
        userSecurityQuestionVC.phoneNumber = phoneNumber
        userSecurityQuestionVC.fullNmae = fullName
        userSecurityQuestionVC.countryCode = countryCode
        userSecurityQuestionVC.password = password
        userSecurityQuestionVC.appAction = self.appAction
        self.navigationController?.pushViewController(userSecurityQuestionVC, animated: true)
    }




    func validateParams(fullName:String,countryCode:String,phoneNumber: String,password:String) -> (success:Bool,message:String){
        if fullName.trimmingCharacters(in: .whitespaces).count == 0{
            return (false,"Please enter your full name")
        }

        if countryCode.trimmingCharacters(in: .whitespaces).count == 0{
            return (false,"Please enter country code")
        }

        if phoneNumber.trimmingCharacters(in: .whitespaces).count == 0{
            return (false,"Please enter your contact")
        }
        let phoneValidation = CommonClass.validatePhoneNumber(phoneNumber.trimmingCharacters(in: .whitespaces))
        if !phoneValidation{
            return (false,"Please enter a valid contact")
        }

        if (phoneNumber.trimmingCharacters(in: .whitespaces).count < self.selectedCountry.minDigit){
            return (false,"Contact should not be less than \(self.selectedCountry.minDigit) in length")
        }
        if  (phoneNumber.trimmingCharacters(in: .whitespaces).count > self.selectedCountry.maxDigit){
            return (false,"Contact should not be more than \(self.selectedCountry.maxDigit) in length")
        }
        let passwordValidation = CommonClass.validatePassword(password)
        if !passwordValidation{
            return (false,"Please enter a valid password")
        }

        return (true,"")
    }




    func countryPickerSetUp() {
        if cpv != nil{
            cpv.removeFromSuperview()
        }
        cpv = CountryPickerView(frame: self.countryPickerView.frame)
        self.countryPickerView.addSubview(cpv)
        cpv.countryDetailsLabel.font = fonts.Raleway.medium.font(.medium)
        cpv.showCountryFlagInView = false
        cpv.showPhoneCodeInView = true
        cpv.showCountryCodeInView = true
        cpv.dataSource = self
        cpv.delegate = self
        cpv.translatesAutoresizingMaskIntoConstraints = false
        let topConstraints = NSLayoutConstraint(item: cpv, attribute: .top, relatedBy: .equal, toItem: self.countryPickerView, attribute: .top, multiplier: 1, constant: 0)
        let bottomConstraints = NSLayoutConstraint(item: cpv, attribute: .bottom, relatedBy: .equal, toItem: self.countryPickerView, attribute: .bottom, multiplier: 1, constant: 0)
        let leadingConstraints = NSLayoutConstraint(item: cpv, attribute: .leading, relatedBy: .equal, toItem: self.countryPickerView, attribute: .leading, multiplier: 1, constant: 0)
        let trailingConstraints = NSLayoutConstraint(item: cpv, attribute: .trailing, relatedBy: .equal, toItem: self.countryPickerView, attribute: .trailing, multiplier: 1, constant: 0)
        self.countryPickerView.addConstraints([topConstraints,leadingConstraints,trailingConstraints,bottomConstraints])
        self.selectedCountry = cpv.selectedCountry
    }

    override func viewDidLayoutSubviews() {
        CommonClass.makeViewCircularWithCornerRadius(self.containerView, borderColor: appColor.gray , borderWidth: 1, cornerRadius: 3)
        self.containerView.addshadow(top: true, left: true, bottom: true, right: true, shadowRadius: 12, shadowOpacity: 0.3,shadowColor: appColor.gray)

        CommonClass.makeViewCircularWithCornerRadius(self.signUpButton, borderColor: UIColor.clear, borderWidth: 0, cornerRadius: self.signUpButton.frame.size.height/4)

        self.signUpButton.applyGradient(withColours: [appColor.gradientStart,appColor.gradientEnd], gradientOrientation: .topLeftBottomRight)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}


extension SignUpViewController: CountryPickerViewDataSource,CountryPickerViewDelegate{
    func sectionTitleForPreferredCountries(in countryPickerView: CountryPickerView) -> String? {
        return (AppSettings.shared.currentCountryCode.count != 0) ? "Current" : nil
    }

    func showOnlyPreferredSection(in countryPickerView: CountryPickerView) -> Bool? {
        return false
    }

    func navigationTitle(in countryPickerView: CountryPickerView) -> String? {
        return "Select Country"
    }

    func closeButtonNavigationItem(in countryPickerView: CountryPickerView) -> UIBarButtonItem? {
        let barButton = UIBarButtonItem(image: #imageLiteral(resourceName: "close_icon"), style: .plain, target: nil, action: nil)
        barButton.tintColor = appColor.blue
        return barButton
    }

    func searchBarPosition(in countryPickerView: CountryPickerView) -> SearchBarPosition {
        return .navigationBar
    }

    func countryPickerView(_ countryPickerView: CountryPickerView, didSelectCountry country: Country) {
        self.selectedCountry = country
        let filledMobile = self.phoneNumberTextField.text ?? ""
        self.phoneNumberTextField.text = "\(self.selectedCountry.phoneCode) "+filledMobile
        self.phoneNumberTextField.becomeFirstResponder()
    }

    func preferredCountries(in countryPickerView: CountryPickerView) -> [Country]? {
        if let currentCountry = countryPickerView.getCountryByCode(AppSettings.shared.currentCountryCode){
            return [currentCountry]
        }else{
            return [countryPickerView.selectedCountry]
        }
    }

    func showPhoneCodeInList(in countryPickerView: CountryPickerView) -> Bool? {
        return true
    }


}

extension SignUpViewController:UITextFieldDelegate{
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == self.phoneNumberTextField{
            let textInTextField = textField.text ?? ""
            if textInTextField.isEmpty{
                cpv.showCountriesList(from: self)
                return false
            }else{
                return true
            }
        }
        return true
    }

    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.refreshLoginEditingFields(textField: textField)
    }

    func textFieldDidEndEditing(_ textField: UITextField) {
        self.phoneNumberButton.isSelected = false
        self.passwordButton.isSelected = false
        self.userNameButton.isSelected = false
    }

    @objc func refreshLoginEditingFields(textField: UITextField) {

        if textField == self.phoneNumberTextField{
            self.phoneNumberButton.isSelected = true
            self.passwordButton.isSelected = false
            self.userNameButton.isSelected = false
            let text = textField.text ?? ""
            if text.isEmpty{
                self.cpv.showCountriesList(from: self)
            }
        }else if textField == self.passwordTextField{
            self.phoneNumberButton.isSelected = false
            self.passwordButton.isSelected = true
            self.userNameButton.isSelected = false
        }
        else if textField == self.userNameTextField{
            self.phoneNumberButton.isSelected = false
            self.passwordButton.isSelected = false
            self.userNameButton.isSelected = true
        }
    }

    @IBAction func textFieldDidChangeTextHandler(_ textField: UITextField){
        self.refreshLoginEditingFields(textField: textField)
    }

}

