//
//  EnableTouchIDViewController.swift
//  TipNTap
//
//  Created by TecOrb on 26/02/18.
//  Copyright © 2018 Nakul Sharma. All rights reserved.
//

import UIKit
import LGSideMenuController

class EnableTouchIDViewController: UIViewController {
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var okayButton: UIButton!
    @IBOutlet weak var notNowButton: UIButton!
    @IBOutlet weak var userTouchIDButton: UIButton!
    @IBOutlet weak var buttonContainerView: UIView!
    @IBOutlet weak var buttonStackView : UIStackView!


    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = true
        self.okayButton.isHidden = true
    }

    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = true
    }

    override func viewDidLayoutSubviews() {
        self.decorateviews()
    }

    func decorateviews(){
        self.buttonContainerView.applyGradient(withColours: [appColor.gradientStart,appColor.gradientEnd], gradientOrientation: .topLeftBottomRight)
        self.okayButton.applyGradient(withColours: [appColor.gradientStart,appColor.gradientEnd], gradientOrientation: .topLeftBottomRight)
        self.notNowButton.addRightBorderWithColor(appColor.gradientStart.withAlphaComponent(0.7), width: 0.5)
        self.userTouchIDButton.addLeftBorderWithColor(appColor.gradientStart.withAlphaComponent(0.7), width: 0.5)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func onClickNotNow(_ sender: UIButton){
        AppSettings.shared.interestedInLoginWithTouchID = false
        AppSettings.shared.isLoginWithTouchIDEnable = false
        //proceed to dash board by changing some setting for user
        AppSettings.shared.proceedToDashboard()
    }
    @IBAction func onClickOkay(_ sender: UIButton){
        AppSettings.shared.proceedToDashboard()
    }

    @IBAction func onClickUseTouchID(_ sender: UIButton){
        self.titleLabel.text = "\r\nTouch ID is ready to go\r\n"
        self.messageLabel.text = ""
        AppSettings.shared.interestedInLoginWithTouchID = true
        AppSettings.shared.isLoginWithTouchIDEnable = true
        self.buttonStackView.isHidden = true
        self.okayButton.isHidden = false
    }

    
}












