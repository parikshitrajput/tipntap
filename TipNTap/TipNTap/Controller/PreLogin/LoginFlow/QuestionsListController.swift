//
//  QuestionsListController.swift
//  TipNTap
//
//  Created by TecOrb on 16/02/18.
//  Copyright © 2018 Nakul Sharma. All rights reserved.
//

import UIKit


protocol QuestionSelectionDelegate {
    func questionList(_ viewController: QuestionsListController, didSelectQuestion question : Question, andIndex index:Int)
}
class QuestionsListController: UITableViewController {
    var questionDataSource = Set<Question>()
    private var questions = Array<Question>()
    var delegate : QuestionSelectionDelegate?
    var selectingIndex: Int!
    var question : Question!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.register(UINib(nibName: "SecurityQuestionCell", bundle: nil), forCellReuseIdentifier: "SecurityQuestionCell")

        self.tableView.estimatedRowHeight = 55
        self.questions = Array(questionDataSource)
        self.clearsSelectionOnViewWillAppear = false
        self.tableView.tableFooterView = UIView(frame:CGRect.zero)
    }

    @IBAction func onClickClose(_ sender: UIBarButtonItem){
        self.dismiss(animated: true, completion: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return questionDataSource.count
    }

    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    override func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 55
    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SecurityQuestionCell", for: indexPath) as! SecurityQuestionCell
        cell.questionLabel.text = "\r\n"+"Q. "+questions[indexPath.row].text+"\r\n"
        return cell
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if self.questions.count == 0{return}
        self.question = questions[indexPath.row]
        self.delegate?.questionList(self, didSelectQuestion: question, andIndex: self.selectingIndex)
        self.dismiss(animated: true, completion: nil)
    }
}
