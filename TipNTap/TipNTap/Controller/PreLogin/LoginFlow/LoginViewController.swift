//
//  LoginViewController.swift
//  TipNTap
//
//  Created by TecOrb on 22/01/18.
//  Copyright © 2018 Nakul Sharma. All rights reserved.
//

import UIKit
import CountryPickerView
import BiometricAuthentication
import EAIntroView


class LoginViewController: UIViewController {
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var countryPickerView: UIView!
    @IBOutlet weak var introView: UIView!
    var selectecIndex: UInt = 0

    @IBOutlet weak var phoneNumberButton: UIButton!
    @IBOutlet weak var phoneNumberTextField: FloatLabelTextField!

    @IBOutlet weak var passwordButton: UIButton!
    @IBOutlet weak var passwordTextField: FloatLabelTextField!
    @IBOutlet weak var loginButton: UIButton!

    var appAction : appAction = .login
    var cpv : CountryPickerView!
    var selectedCountry: Country!
    var flagForBiomatry = true

    var titleView : NavigationTitleView!
    func setupNavigationTitle() {
        titleView = NavigationTitleView.instanceFromNib()
        titleView.frame = CGRect(x: 60, y: 0, width: self.view.frame.size.width-120, height: 44)
        titleView.shouldAttributtedMiddle = true
        self.navigationItem.titleView = self.titleView
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = false
        if AppSettings.shared.isIntroShown{
            self.navigationItem.title = AppSettings.shared.isIntroShown ? "Login" : " "
        }else{
            self.setupNavigationTitle()
        }
        self.introView.isHidden = AppSettings.shared.isIntroShown

        self.phoneNumberTextField.addTarget(self, action:#selector(refreshLoginEditingFields(textField:)), for: .editingChanged)
        self.passwordTextField.addTarget(self, action:#selector(refreshLoginEditingFields(textField:)), for: .editingChanged)
        self.phoneNumberTextField.titleFont = fonts.Raleway.regular.font(.small)
        self.passwordTextField.titleFont = fonts.Raleway.regular.font(.small)

        self.phoneNumberTextField.delegate = self
        self.passwordTextField.delegate = self

        self.countryPickerSetUp()
        self.leftBarButtonSetup()
        self.rightBarButtonSetup()
        self.containerView.setNeedsLayout()
        self.containerView.layoutIfNeeded()
        let user = User.loadSavedUser()

        if !user.countryCode.isEmpty{
            self.cpv.setCountryByPhoneCode("+"+user.countryCode)
            self.selectedCountry = self.cpv.selectedCountry
            self.phoneNumberTextField.text = "+"+user.countryCode+" "+AppSettings.shared.phoneNumber
        }
    }


    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }

    func leftBarButtonSetup(){
        self.navigationItem.leftBarButtonItem = nil
//        if AppSettings.shared.isRegistered{
//            self.navigationItem.leftBarButtonItem = nil
//        }else{
//            let leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "back_button"), style: .plain, target: self, action: #selector(onClickBackButton(_:)))
//            leftBarButtonItem.tintColor = appColor.blue
//            self.navigationItem.setLeftBarButton(leftBarButtonItem, animated: true)
//        }
    }

    func rightBarButtonSetup(){
        self.navigationItem.rightBarButtonItem = nil
    }


    @IBAction func onClickForgotPasswordButton(_ sender: UIButton){
        let forgotPasswordVC = AppStoryboard.Main.viewController(ForgotPasswordViewController.self)
        self.navigationController?.pushViewController(forgotPasswordVC, animated: true)
    }


    @IBAction func onClickBackButton(_ sender: UIBarButtonItem){
        self.navigationController?.pop(true)
    }


    func countryPickerSetUp() {
        if cpv != nil{
            cpv.removeFromSuperview()
        }
        cpv = CountryPickerView(frame: self.countryPickerView.frame)
        self.countryPickerView.addSubview(cpv)
        cpv.countryDetailsLabel.font = fonts.Raleway.medium.font(.medium)
        cpv.showPhoneCodeInView = true
        cpv.showCountryCodeInView = true
        cpv.showCountryFlagInView = false
        cpv.dataSource = self
        cpv.delegate = self
        cpv.translatesAutoresizingMaskIntoConstraints = false
        let topConstraints = NSLayoutConstraint(item: cpv, attribute: .top, relatedBy: .equal, toItem: self.countryPickerView, attribute: .top, multiplier: 1, constant: 0)
        let bottomConstraints = NSLayoutConstraint(item: cpv, attribute: .bottom, relatedBy: .equal, toItem: self.countryPickerView, attribute: .bottom, multiplier: 1, constant: 0)
        let leadingConstraints = NSLayoutConstraint(item: cpv, attribute: .leading, relatedBy: .equal, toItem: self.countryPickerView, attribute: .leading, multiplier: 1, constant: 0)
        let trailingConstraints = NSLayoutConstraint(item: cpv, attribute: .trailing, relatedBy: .equal, toItem: self.countryPickerView, attribute: .trailing, multiplier: 1, constant: 0)
        self.countryPickerView.addConstraints([topConstraints,leadingConstraints,trailingConstraints,bottomConstraints])
        //self.selectedCountry = cpv.selectedCountry
    }

    override func viewDidLayoutSubviews() {
        CommonClass.makeViewCircularWithCornerRadius(self.containerView, borderColor: appColor.gray , borderWidth: 1, cornerRadius: 3)
        self.containerView.addshadow(top: true, left: true, bottom: true, right: true, shadowRadius: 12, shadowOpacity: 0.3,shadowColor: appColor.gray)
        CommonClass.makeViewCircularWithCornerRadius(self.loginButton, borderColor: UIColor.clear, borderWidth: 0, cornerRadius: self.loginButton.frame.size.height/4)
        self.loginButton.applyGradient(withColours: [appColor.gradientStart,appColor.gradientEnd], gradientOrientation: .topLeftBottomRight)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func viewDidAppear(_ animated: Bool) {
        self.navigationItem.title = AppSettings.shared.isIntroShown ? "Login" : " "
        self.navigationController?.navigationBar.isHidden = false
        if !AppSettings.shared.isIntroShown{
            //AppSettings.shared.isIntroShown = true
            self.showIntro()
        }else{
        if flagForBiomatry{
            if BioMetricAuthenticator.canAuthenticate(){
                if AppSettings.shared.isLoginWithTouchIDEnable{
                    self.askForBiomatry()
                    //AppSettings.shared.proceedToDashboard()
                }
            }
        }
    }
    }

    @IBAction func onClickLogIn(_ sender: UIButton){
        guard let country = self.selectedCountry else {
            NKToastHelper.sharedInstance.showAlert(self, title: warningMessage.title, message: "Please enter your phone number", completionBlock: {})
            return
        }

        let countryCode = country.phoneCode

        guard let phoneNumber = phoneNumberTextField.text else {
            NKToastHelper.sharedInstance.showAlert(self, title: warningMessage.title, message: "Please enter your phone number", completionBlock: {})
            return
        }
        if !phoneNumber.contains(countryCode){
            NKToastHelper.sharedInstance.showAlert(self, title: warningMessage.title, message: "Please reenter your phone number by selecting country first", completionBlock: {})
            return
        }

        guard let password = passwordTextField.text else {
            NKToastHelper.sharedInstance.showAlert(self, title: warningMessage.title, message: "Please enter your password", completionBlock: {})
            return
        }

        let withoutCountryCode = phoneNumber.replacingOccurrences(of: countryCode, with: "")
        let actuallPhoneNumber = withoutCountryCode.removingWhitespaces()

        let validation = self.validateParams(countryCode: countryCode, phoneNumber: actuallPhoneNumber, password: password)
        if !validation.success{
            NKToastHelper.sharedInstance.showAlert(self, title: warningMessage.title, message: validation.message, completionBlock: {})
            return
        }
        //call for login from server
        self.loginUserWith(countryCode: countryCode, phoneNumber: actuallPhoneNumber, password: password)
    }

    func userShouldLogoutFirst(countryCode:String,phoneNumber:String)->Bool{
        let loggedInPhoneNumber = AppSettings.shared.phoneNumber
        let loggedInCC = AppSettings.shared.countryCode
        return !((loggedInCC == countryCode) && (loggedInPhoneNumber == phoneNumber))
    }

    func loginUserWith(countryCode:String,phoneNumber:String,password:String){
        if self.userShouldLogoutFirst(countryCode: countryCode, phoneNumber: phoneNumber){
            AppSettings.shared.resetOnFirstAppLaunch()
        }
        LoginService.sharedInstance.loginWith(phoneNumber, countryCode: countryCode, password: password) { (success, resUser, message) in
            if success{
                AppSettings.shared.countryCode = "+"+countryCode
                AppSettings.shared.phoneNumber = phoneNumber

                if let user = resUser{
                    if !user.isContactVerified{
                        LoginService.sharedInstance.updateDeviceTokeOnServer()
                        LoginService.sharedInstance.validateUser({ (success, resUser, resmessage) in
                            AppSettings.shared.hideLoader()
                            if success{
                                self.completeFlow()
                            }else{
                                NKToastHelper.sharedInstance.showAlert(self, title: warningMessage.title, message: resmessage)
                            }
                        })
                    }else{
                        self.completeFlow()
                    }
                }else{
                    NKToastHelper.sharedInstance.showErrorAlert(self, message: message)
                }
            }else{
                NKToastHelper.sharedInstance.showErrorAlert(self, message: message)
            }
        }
    }

    func completeFlow(){
        LoginService.sharedInstance.updateDeviceTokeOnServer()
        if self.shouldAskForEnableingTouchIDFlow(){
            let enableTouchIDVC = AppStoryboard.Main.viewController(EnableTouchIDViewController.self)
            self.navigationController?.navigationBar.isHidden = true
            self.navigationController?.pushViewController(enableTouchIDVC, animated: true)
        }else{
            AppSettings.shared.proceedToDashboard()
        }
    }

    func shouldAskForEnableingTouchIDFlow() -> Bool{
        var result = false
        if BioMetricAuthenticator.canAuthenticate(){
            if AppSettings.shared.interestedInLoginWithTouchID{
                if !AppSettings.shared.isLoginWithTouchIDEnable{
                    result = true
                }
            }
        }
        return result
    }
    @IBAction func moreButtonClicked(_ sender: UIBarButtonItem) {
        self.showAlertToChooseLoginOption()
    }



    @IBAction func onClickSignUP(_ sender: UIButton){
        let signUpVC = AppStoryboard.Main.viewController(SignUpViewController.self)
        self.navigationController?.pushViewController(signUpVC, animated: true)
    }


    func validateParams(countryCode:String,phoneNumber: String,password:String) -> (success:Bool,message:String){

        if countryCode.trimmingCharacters(in: .whitespaces).count == 0{
            return (false,"Please enter country code")
        }

        if phoneNumber.trimmingCharacters(in: .whitespaces).count == 0{
            return (false,"Please enter your contact")
        }
        let phoneValidation = CommonClass.validatePhoneNumber(phoneNumber.trimmingCharacters(in: .whitespaces))
        if !phoneValidation{
            return (false,"Please enter a valid contact")
        }

        if (phoneNumber.trimmingCharacters(in: .whitespaces).count < self.selectedCountry.minDigit){
            return (false,"Contact should not be less than \(self.selectedCountry.minDigit) in length")
        }
        if  (phoneNumber.trimmingCharacters(in: .whitespaces).count > self.selectedCountry.maxDigit){
            return (false,"Contact should not be more than \(self.selectedCountry.maxDigit) in length")
        }
        let passwordValidation = CommonClass.validatePassword(password)
        if !passwordValidation{
            return (false,"Please enter a valid password")
        }
        return (true,"")
    }

}


extension LoginViewController: CountryPickerViewDataSource,CountryPickerViewDelegate{
    func sectionTitleForPreferredCountries(in countryPickerView: CountryPickerView) -> String? {
        return (AppSettings.shared.currentCountryCode.count != 0) ? "Current" : nil
    }

    func showOnlyPreferredSection(in countryPickerView: CountryPickerView) -> Bool? {
        return false
    }

    func navigationTitle(in countryPickerView: CountryPickerView) -> String? {
        return "Select Country"
    }

    func closeButtonNavigationItem(in countryPickerView: CountryPickerView) -> UIBarButtonItem? {
        let barButton = UIBarButtonItem(image: #imageLiteral(resourceName: "close_icon"), style: .plain, target: nil, action: nil)
        barButton.tintColor = appColor.blue
        return barButton
    }

    func searchBarPosition(in countryPickerView: CountryPickerView) -> SearchBarPosition {
        return .navigationBar
    }


    func countryPickerView(_ countryPickerView: CountryPickerView, didSelectCountry country: Country) {
        self.flagForBiomatry = false
        self.selectedCountry = country
        let filledMobile = self.phoneNumberTextField.text ?? ""
        self.phoneNumberTextField.text = "\(self.selectedCountry.phoneCode) "+filledMobile
        self.phoneNumberTextField.becomeFirstResponder()
    }


    func preferredCountries(in countryPickerView: CountryPickerView) -> [Country]? {
        if let currentCountry = countryPickerView.getCountryByPhoneCode(AppSettings.shared.currentCountryCode){
            return [currentCountry]
        }else{
            return [countryPickerView.selectedCountry]
        }
    }

    func showPhoneCodeInList(in countryPickerView: CountryPickerView) -> Bool? {
        return true
    }


}

extension LoginViewController:UITextFieldDelegate{

    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == self.phoneNumberTextField{
            let textInTextField = textField.text ?? ""
            if textInTextField.isEmpty{
                flagForBiomatry = false
                cpv.showCountriesList(from: self)
                return false
            }else{
                return true
            }
        }
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.refreshLoginEditingFields(textField: textField)
    }

    func textFieldDidEndEditing(_ textField: UITextField) {
        self.phoneNumberButton.isSelected = false
        self.passwordButton.isSelected = false
    }

    @objc func refreshLoginEditingFields(textField: UITextField) {

        if textField == self.phoneNumberTextField{
            self.phoneNumberButton.isSelected = true
            self.passwordButton.isSelected = false
            let text = textField.text ?? ""
            if text.isEmpty{
                flagForBiomatry = false
                self.cpv.showCountriesList(from: self)
            }
        }else if textField == self.passwordTextField{
            self.phoneNumberButton.isSelected = false
            self.passwordButton.isSelected = true
        }
    }
    @IBAction func textFieldDidChangeTextHandler(_ textField: UITextField){
        self.refreshLoginEditingFields(textField: textField)
    }

}




extension LoginViewController{
    func showAlertToChooseLoginOption(){
        let actionSheet = UIAlertController(title: nil, message:nil, preferredStyle: UIDevice.isRunningOnIpad ? .alert : .actionSheet)
        let cancelAction: UIAlertAction = UIAlertAction(title: "Cancel", style: .cancel) { action -> Void in
            actionSheet.dismiss(animated: true, completion: nil)
        }
        actionSheet.addAction(cancelAction)
        let openGalleryAction: UIAlertAction = UIAlertAction(title: "Use Password", style: .default)
        { action -> Void in
            self.passwordTextField.becomeFirstResponder()
        }
        actionSheet.addAction(openGalleryAction)

        let openCameraAction: UIAlertAction = UIAlertAction(title: "Use Biomatry", style: .default)
        { action -> Void in
            self.askForBiomatry()
        }
        actionSheet.addAction(openCameraAction)
        self.present(actionSheet, animated: true, completion: nil)
    }


    func askForBiomatry(){
        // start authentication
        BioMetricAuthenticator.authenticateWithBioMetrics(reason: "", success: {
            // authentication successful
            let phoneNumber = AppSettings.shared.phoneNumber
            let countryCode = AppSettings.shared.countryCode
            let password = AppSettings.shared.passwordEncrypted
            self.loginUserWith(countryCode: countryCode, phoneNumber: phoneNumber, password: password)

        }, failure: { [weak self] (error) in
            // do nothing on canceled
            if error == .canceledByUser || error == .canceledBySystem {
                return
            }
            // show alternatives on fallback button clicked
            else if error == .fallback {
            // here we're entering username and password
                self?.phoneNumberTextField.becomeFirstResponder()
            }
            // No biometry enrolled in this device, ask user to register fingerprint or face
            else if error == .biometryNotEnrolled {
                self?.showGotoSettingsAlert(message: error.message())
            }
            // Biometry is locked out now, because there were too many failed attempts.
            // Need to enter device passcode to unlock.
            else if error == .biometryLockedout {
                self?.showPasscodeAuthentication(message: error.message())
            }
            // show error on authentication failed
            else {
                self?.showErrorAlert(message: error.message())
            }
        })
    }

    // show passcode authentication
    func showPasscodeAuthentication(message: String) {
        BioMetricAuthenticator.authenticateWithPasscode(reason: message, success: {
            // passcode authentication success
            let phoneNumber = AppSettings.shared.phoneNumber
            let countryCode = AppSettings.shared.countryCode
            let password = AppSettings.shared.passwordEncrypted
            self.loginUserWith(countryCode: countryCode, phoneNumber: phoneNumber, password: password)

        }) { (error) in
            print(error.message())
        }
    }



    func showAlert(title: String, message: String) {

        let okAction = AlertAction(title: OKTitle)
        let alertController = getAlertViewController(type: .alert, with: title, message: message, actions: [okAction], showCancel: false) { (button) in
        }
        present(alertController, animated: true, completion: nil)
    }

    func showLoginSucessAlert() {
        showAlert(title: "Success", message: "Login successful")
    }

    func showErrorAlert(message: String) {
        showAlert(title: "Error", message: message)
    }

    func showGotoSettingsAlert(message: String) {
        let settingsAction = AlertAction(title: "Settings")
        let alertController = getAlertViewController(type: .alert, with: "Error", message: message, actions: [settingsAction], showCancel: true, actionHandler: { (buttonText) in
            if buttonText == CancelTitle { return }

            // open settings
            guard let settingsUrl = URL(string: UIApplicationOpenSettingsURLString) else {
                return
            }
            if UIApplication.shared.canOpenURL(settingsUrl) {
                UIApplication.shared.open(settingsUrl, options: [:], completionHandler: nil)
            }
        })
        present(alertController, animated: true, completion: nil)
    }
}







extension LoginViewController : EAIntroDelegate{
    func showIntro() {
        //AppSettings.shared.isIntroShown = true

        let sampleDescription1 = "Want to pay without hassle, just scan the QR code of TipNTap and pay by entering amount"
        let sampleDescription2 = "To pay without QR code, just enter registered mobile number and pay"

        let sampleDescription3 = "Want to receive from TipNTap, reveal your QR code or registered mobile number"

        let page1 = EAIntroPage()
        page1.title = "Scan";
        page1.titleColor = UIColor.black
        page1.titleFont = fonts.Raleway.bold.font(.xXXLarge)
        page1.descFont = fonts.Raleway.regular.font(.medium)
        page1.desc = sampleDescription1;
        page1.descColor = UIColor.black
        page1.bgImage = #imageLiteral(resourceName: "tt1")

        let page2 = EAIntroPage()
        page2.title = "Pay";
        page2.titleColor = UIColor.black
        page2.titleFont = fonts.Raleway.bold.font(.xXXLarge)
        page2.descFont = fonts.Raleway.regular.font(.medium)
        page2.desc = sampleDescription2;
        page2.descColor = UIColor.black
        page2.bgImage = #imageLiteral(resourceName: "tt2")

        let page3 = EAIntroPage()
        page3.title = "Receive";
        page3.titleColor = UIColor.black
        page3.titleFont = fonts.Raleway.bold.font(.xXXLarge)
        page3.descFont = fonts.Raleway.regular.font(.medium)
        page3.desc = sampleDescription3;
        page3.descColor = UIColor.black
        page3.bgImage = #imageLiteral(resourceName: "tt3")

        let intro = EAIntroView(frame: self.introView.bounds, andPages: [page1,page2,page3])
        intro?.pageControl.currentPageIndicatorTintColor = appColor.blue
        intro?.pageControl.pageIndicatorTintColor = appColor.gray
        intro?.tapToNext = true
        intro?.swipeToExit = false
        intro?.skipButton.setTitle("Skip", for: .normal)
        intro?.skipButton.titleLabel?.font = fonts.Raleway.medium.font(.smallMedium)

        intro?.skipButton.setTitleColor(appColor.blue, for: .normal)
        intro?.delegate = self
        intro?.show(in: self.introView, animateDuration: 0.3, withInitialPageIndex: self.selectecIndex)
    }

    func intro(_ introView: EAIntroView!, pageEndScrolling page: EAIntroPage!, with pageIndex: UInt) {
        let title = (pageIndex == 2) ? "Get Started" : "Skip"
        introView.skipButton.setTitle(title, for: .normal)
        self.selectecIndex = pageIndex
    }

    func introDidFinish(_ introView: EAIntroView!, wasSkipped: Bool) {
        AppSettings.shared.isIntroShown = true
        self.introView.isHidden = AppSettings.shared.isIntroShown
        self.navigationItem.titleView = nil
        self.navigationItem.title = AppSettings.shared.isIntroShown ? "Login" : " "
    }

}




/*

        if AppSettings.shared.phoneNumber.count == 0{
            self.navigationItem.rightBarButtonItem = nil
            return
        }

        if !AppSettings.shared.isLoginWithTouchIDEnable && BioMetricAuthenticator.canAuthenticate() {
            self.navigationItem.rightBarButtonItem = nil
            let rightBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "navigation_button"), style: .plain, target: self, action: #selector(moreButtonClicked(_:)))
            rightBarButtonItem.tintColor = appColor.blue
            self.navigationItem.setRightBarButton(rightBarButtonItem, animated: true)
        }else{
            self.navigationItem.rightBarButtonItem = nil
        }



 */
