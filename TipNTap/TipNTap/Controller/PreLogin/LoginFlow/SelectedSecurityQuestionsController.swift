//
//  SelectedSecurityQuestionsController.swift
//  TipNTap
//
//  Created by TecOrb on 16/02/18.
//  Copyright © 2018 Nakul Sharma. All rights reserved.
//
import SwiftyJSON
import UIKit
import RSKPlaceholderTextView
import Firebase

class SelectedSecurityQuestionsController: UIViewController {
    var listOfQuestions = Set<Question>()
    var selectedQuestions = Set<Question>()
    var selectedQuestionOne : Question = Question()
    var selectedQuestionTwo : Question = Question()
    var appAction : appAction!

    var countryCode: String!
    var phoneNumber: String!
    var fullNmae:String!
    var password:String!

    @IBOutlet weak var questionOneButton : UIButton!
    @IBOutlet weak var answerOneTextField : UITextField!
    @IBOutlet weak var questionOneLabel : UILabel!
    @IBOutlet weak var questionOneContainer : UIView!

    @IBOutlet weak var questionTwoButton : UIButton!
    @IBOutlet weak var questionTwoLabel : UILabel!
    @IBOutlet weak var answerTwoTextField : UITextField!
    @IBOutlet weak var questionTwoContainer : UIView!

    @IBOutlet weak var nextButton : UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.getAllSecurityQuestionsFromServer()
        self.answerOneTextField.addTarget(self, action:#selector(textDidChanged(_:)), for: .editingChanged)
        self.answerTwoTextField.addTarget(self, action:#selector(textDidChanged(_:)), for: .editingChanged)
        self.answerOneTextField.delegate = self
        self.answerTwoTextField.delegate = self
    }

    override func viewDidLayoutSubviews() {
        self.setupViews()
    }

    func setupViews(){
        self.nextButton.applyGradient(withColours: [appColor.gradientStart,appColor.gradientEnd], gradientOrientation: .topLeftBottomRight)

        self.questionOneContainer.addshadow(top: true, left: true, bottom: true, right: true, shadowRadius: 5, shadowOpacity: 0.5,shadowColor: .gray)
        self.questionTwoContainer.addshadow(top: true, left: true, bottom: true, right: true, shadowRadius: 5, shadowOpacity: 0.5,shadowColor: .gray)
    }

    func setDefualtQuestion(selectedQuestions:Set<Question>, listOfQuestions:Set<Question>) {
        if (selectedQuestions.count == 0) && (listOfQuestions.count >= 2) {
            for index in 0..<2{
                self.selectedQuestions.insert(Array(listOfQuestions)[index])
            }
            if self.selectedQuestions.count == 2{
                let squestions = Array(self.selectedQuestions)
                self.selectedQuestionOne = squestions[0]
                self.selectedQuestionTwo = squestions[1]
                self.questionOneLabel.text = "Q. "+squestions[0].text
                self.questionTwoLabel.text = "Q. "+squestions[1].text
            }
        }else if selectedQuestions.count == 2{
            let squestions = Array(selectedQuestions)
            self.selectedQuestionOne = squestions[0]
            self.selectedQuestionTwo = squestions[1]
            self.questionOneLabel.text = "Q. "+squestions[0].text
            self.questionTwoLabel.text = "Q. "+squestions[1].text
        }
        self.answerOneTextField.placeholder = "Answer"
        self.answerTwoTextField.placeholder = "Answer"
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


    //MARK: - Actions
    @IBAction func onClickBack(_ sender: UIBarButtonItem){
        self.navigationController?.pop(true)
    }

    @IBAction func onClickNext(_ sender: UIButton){
        // send OTP on user's mobile using firebase
        let validation = self.validateQuestions()
        if !validation.result{
            NKToastHelper.sharedInstance.showAlert(self, title: warningMessage.title, message: validation.message)
            return
        }

        self.sendOTPOn(countryCode: self.countryCode, phoneNumber: self.phoneNumber)
    }

    func validateQuestions() -> (result:Bool,message:String){
        if selectedQuestionOne.answer.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty || selectedQuestionOne.answer.trimmingCharacters(in: .whitespacesAndNewlines) == ""{
            return(false,"Please write the answer of security questions")
        }
        if selectedQuestionTwo.answer.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty || selectedQuestionTwo.answer.trimmingCharacters(in: .whitespacesAndNewlines) == ""{
            return(false,"Please write the answer of security questions")
        }
        return(true,"")
    }

    @IBAction func onClickQuestionOne(_ sender: UIButton){
        self.openListOfQuestions(index: 0)
    }

    func sendOTPOn(countryCode:String,phoneNumber:String){
        let firebaseAuth = Auth.auth()
        do {
            try firebaseAuth.signOut()
        } catch let signOutError as NSError {
            NKToastHelper.sharedInstance.showAlert(self, title: warningMessage.title, message: signOutError.localizedDescription, completionBlock: {
                self.navigationController?.pop(true)
            })
        }
        AppSettings.shared.showLoader(withStatus: "Please wait..")
        let fullPhoneNumber = countryCode+phoneNumber
        PhoneAuthProvider.provider().verifyPhoneNumber(fullPhoneNumber, uiDelegate: nil) { (verificationID, error) in
            AppSettings.shared.hideLoader()
            if let error = error {
                NKToastHelper.sharedInstance.showAlert(self, title: warningMessage.title, message: error.localizedDescription, completionBlock: {})
                return
            }
            //proceed to otp varification
            guard let varificationToken = verificationID else{
                return
            }

            self.openOTPVerificationScreen(verificationID: varificationToken, fullName: self.fullNmae, contact: self.phoneNumber, countryCode: self.countryCode, password: self.password, question1: self.selectedQuestionOne, question2: self.selectedQuestionTwo, appAction: self.appAction)
        }
    }

    func openOTPVerificationScreen(verificationID:String, fullName:String, contact:String, countryCode:String, password:String, question1:Question,question2:Question,appAction:appAction){
        let otpVC = AppStoryboard.Main.viewController(OTPVerificationViewController.self)
        otpVC.verificationID = verificationID
        otpVC.fullName = fullName
        otpVC.contact = contact
        otpVC.countryCode = countryCode
        otpVC.password = password
        otpVC.question1 = question1
        otpVC.question2 = question2
        otpVC.appAction = appAction
        self.navigationController?.pushViewController(otpVC, animated: true)
    }

    func openListOfQuestions(index : Int){
        let selectedQ = Set([selectedQuestionOne,selectedQuestionTwo])
        let questionsDataSource = self.listOfQuestions.subtracting(selectedQ)
        let selectQuestionVC = AppStoryboard.Main.viewController(QuestionsListController.self)
        selectQuestionVC.selectingIndex = index
        selectQuestionVC.questionDataSource = questionsDataSource
        selectQuestionVC.delegate = self
        let nav = UINavigationController(rootViewController: selectQuestionVC)
        nav.navigationBar.barTintColor = .white
        nav.navigationBar.isTranslucent = false
        nav.navigationBar.isOpaque = true
        nav.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.black]
        nav.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        nav.navigationBar.isHidden = false
        self.navigationController?.present(nav, animated: true, completion: nil)
    }
    

    @IBAction func onClickQuestionTwo(_ sender: UIButton){
        self.openListOfQuestions(index: 1)
    }

    func getSecuritySelectedQuestionsFromServer(countryCode:String,phoneNumber:String){
        AppSettings.shared.showLoader(withStatus: "Loading..")
        LoginService.sharedInstance.getListOfSecurityQuestionsForUser(countryCode, phoneNumber: phoneNumber){ (success, questionsSet, message) in
            AppSettings.shared.hideLoader()
            if let qSet = questionsSet{
                self.selectedQuestions = qSet
                self.setDefualtQuestion(selectedQuestions: self.selectedQuestions, listOfQuestions: self.listOfQuestions)
            }else{
                self.setDefualtQuestion(selectedQuestions: self.selectedQuestions, listOfQuestions: self.listOfQuestions)
            }
        }
    }

    func getAllSecurityQuestionsFromServer() {
        AppSettings.shared.showLoader(withStatus: "Loading")
        LoginService.sharedInstance.getListOfSecurityQuestions { (success, questionsSet, message) in
            AppSettings.shared.hideLoader()
            if let qSet = questionsSet{
                self.listOfQuestions = qSet
                self.getSecuritySelectedQuestionsFromServer(countryCode: self.countryCode, phoneNumber: self.phoneNumber)
            }
        }
    }
}

extension SelectedSecurityQuestionsController: QuestionSelectionDelegate{
    func questionList(_ viewController: QuestionsListController, didSelectQuestion question: Question, andIndex index: Int) {
        if index == 0{
            guard let firstquestion = self.selectedQuestions.first else{
                return
            }
            if firstquestion != question{
                self.selectedQuestionOne = question
                self.answerOneTextField.text = nil

                guard let lastquestion = Array(self.selectedQuestions).last else{
                    return
                }

                let newSelctedArray = [question,lastquestion]
                self.selectedQuestions = Set(newSelctedArray)
                self.setDefualtQuestion(selectedQuestions: self.selectedQuestions, listOfQuestions: self.listOfQuestions)
            }else{
                self.answerOneTextField.text = firstquestion.answer
            }
        }else{
            guard let lastquestion = Array(self.selectedQuestions).last else{
                return
            }
            if lastquestion != question{
                self.selectedQuestionTwo = question
                self.answerTwoTextField.text = nil
                guard let firstquestion = self.selectedQuestions.first else{
                    return
                }
                let newSelctedArray = [firstquestion,question]
                self.selectedQuestions = Set(newSelctedArray)
                self.setDefualtQuestion(selectedQuestions: self.selectedQuestions, listOfQuestions: self.listOfQuestions)
            }else{
                self.answerTwoTextField.text = lastquestion.answer
            }

        }
    }


}


extension SelectedSecurityQuestionsController:UITextFieldDelegate{
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == answerOneTextField{
            self.selectedQuestionOne.answer = textField.text ?? ""
        }else if textField == answerTwoTextField{
            self.selectedQuestionTwo.answer = textField.text ?? ""
        }
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == answerOneTextField{
            self.selectedQuestionOne.answer = textField.text ?? ""
        }else if textField == answerTwoTextField{
            self.selectedQuestionTwo.answer = textField.text ?? ""
        }
    }

    @IBAction func textDidChanged(_ textField: UITextField){
        if textField == answerOneTextField{
            self.selectedQuestionOne.answer = textField.text ?? ""
        }else if textField == answerTwoTextField{
            self.selectedQuestionTwo.answer = textField.text ?? ""
        }
    }
}

























class Question : NSObject{
    let kID = "id"
    let kText = "question"
//    let kAnswer = "answer"

    var ID = ""
    var text: String = ""
    var answer = ""

    override init() {
        super.init()
    }

    init(dictionary:Dictionary<String,AnyObject>) {
        if let ID = dictionary[kID] as? Int{
            self.ID = "\(ID)"
        }else if let ID = dictionary[kID] as? String{
            self.ID = ID
        }

        if let _text = dictionary[kText] as? String{
            self.text = _text
        }

//        if let _answer = dictionary[kAnswer] as? String{
//            self.answer = _answer
//        }

        super.init()
    }

    static func ==(lhs:Question,rhs:Question) -> Bool{
        return lhs.ID == rhs.ID
    }

}

class QuestionParser: NSObject{
    var questions = Set<Question>()
    //var code = 0
    var errorCode = ErrorCode.failure
    var message = ""
    var token = ""

    override init() {
        super.init()
    }


    init(json: JSON) {
        if let _code = json["code"].int{
            self.errorCode = ErrorCode(rawValue: _code)
        }

        if let _message = json["message"].string{
            self.message = _message
        }

        if let _userDict = json["user"].dictionaryObject as Dictionary<String,AnyObject>?{
            if let _token = _userDict["reset_password_token"] as? String{
                self.token = _token
            }
            if let _message = _userDict["message"] as? String{
                self.message = _message
            }

        }

        if let qset = json["questions"].arrayObject as? Array<Dictionary<String,AnyObject>>{
            for ques in qset{
                let q = Question(dictionary: ques)
                self.questions.insert(q)
            }
        }
    }
}


