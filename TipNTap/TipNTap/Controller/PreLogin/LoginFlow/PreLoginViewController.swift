//
//  PreLoginViewController.swift
//  TipNTap
//
//  Created by TecOrb on 29/01/18.
//  Copyright © 2018 Nakul Sharma. All rights reserved.
//

import UIKit
import EAIntroView


class PreLoginViewController: UIViewController {
    @IBOutlet weak var introView: UIView!
    var selectecIndex: UInt = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        //self.showIntro()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewDidAppear(_ animated: Bool) {
        self.showIntro()
    }
}


extension PreLoginViewController:EAIntroDelegate{
    func showIntro() {
        let sampleDescription1 = "Scan the qrcode of TipNTapian"
        let sampleDescription3 = "Receive tip from TipNTapian"
        let sampleDescription2 = "Pay tip to TipNTapian"

        let page1 = EAIntroPage()
        page1.title = sampleDescription1//" Scan ";
        page1.titleColor = UIColor.black
        page1.titleFont = fonts.Raleway.medium.font(.medium)
        page1.descFont = fonts.Raleway.medium.font(.medium)
        page1.desc = "";
        page1.descColor = UIColor.black
        page1.bgImage = #imageLiteral(resourceName: "tt1")

        let page2 = EAIntroPage()
        page2.title = sampleDescription2;
        page2.titleColor = UIColor.black
        page2.titleFont = fonts.Raleway.medium.font(.medium)
        page2.descFont = fonts.Raleway.medium.font(.medium)
        page2.desc = "";
        page2.descColor = UIColor.black
        page2.bgImage = #imageLiteral(resourceName: "tt2")

        let page3 = EAIntroPage()
        page3.title = sampleDescription3;
        page3.titleColor = UIColor.black
        page3.titleFont = fonts.Raleway.medium.font(.medium)
        page3.descFont = fonts.Raleway.medium.font(.medium)
        page3.desc = "";
        page3.descColor = UIColor.black
        page3.bgImage = #imageLiteral(resourceName: "tt3")

        let intro = EAIntroView(frame: self.introView.bounds, andPages: [page1,page2,page3])
        intro?.pageControl.currentPageIndicatorTintColor = appColor.blue
        intro?.pageControl.pageIndicatorTintColor = appColor.gray
        intro?.tapToNext = false
        intro?.swipeToExit = false
        intro?.limitPageIndex = 2
        intro?.delegate = self
        intro?.show(in: self.introView, animateDuration: 0.3, withInitialPageIndex: self.selectecIndex)
    }
    func intro(_ introView: EAIntroView!, pageEndScrolling page: EAIntroPage!, with pageIndex: UInt) {
        self.selectecIndex = pageIndex
    }

}

