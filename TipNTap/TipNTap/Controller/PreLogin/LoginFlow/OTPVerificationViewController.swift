//
//  OTPVerificationViewController.swift
//  TipNTap
//
//  Created by TecOrb on 29/01/18.
//  Copyright © 2018 Nakul Sharma. All rights reserved.
//

import UIKit
import Firebase
enum appAction {
    case forgotPassword,login,signUp
}

class OTPVerificationViewController: UIViewController {
    @IBOutlet weak var otpView: PinCodeTextField!
    @IBOutlet weak var donebutton: UIButton!
    @IBOutlet weak var resendbutton: UIButton!
    @IBOutlet weak var sentToLabel: UILabel!
    @IBOutlet weak var didNotReceivedCodeLabel: UILabel!

    var verificationID : String!
    var otpString :String = ""
    var fullName:String?
    var contact:String!
    var countryCode:String!
    var password:String?
    var question1:Question!
    var question2:Question!
    var appAction:appAction!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.resendbutton.isEnabled = true
        self.didNotReceivedCodeLabel.alpha = 1.0
        self.resendbutton.alpha = 1.0

        self.sentToLabel.text = " (\(self.countryCode+self.contact))"
        self.otpView.keyboardType = .numberPad
        self.otpView.font = fonts.Raleway.bold.font(.xXXLarge)
        self.otpView.delegate = self
        self.toggleResend()
        self.perform(#selector(toggleResend), with: nil, afterDelay: 60)
    }
    
    @objc func toggleResend(){
        self.resendbutton.isEnabled = !self.resendbutton.isEnabled
        self.didNotReceivedCodeLabel.alpha = (self.didNotReceivedCodeLabel.alpha == 1.0) ? 0.2 : 1.0
        self.resendbutton.alpha = (self.resendbutton.alpha == 1.0) ? 0.2 : 1.0
    }

    override func viewDidLayoutSubviews() {
        self.donebutton.applyGradient(withColours: [appColor.gradientStart,appColor.gradientEnd], gradientOrientation: .topLeftBottomRight)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func onClickResend(_ sender: UIButton){
        self.otpView.text = ""
        self.sendOTPOn(countryCode: self.countryCode, phoneNumber: self.contact)
    }


    func sendOTPOn(countryCode:String,phoneNumber:String){
        AppSettings.shared.showLoader(withStatus: "Sending..")

        let firebaseAuth = Auth.auth()
        do {
            try firebaseAuth.signOut()
        } catch let signOutError as NSError {
            NKToastHelper.sharedInstance.showAlert(self, title: warningMessage.title, message: signOutError.localizedDescription, completionBlock: {
                self.navigationController?.pop(true)
            })
            return
        }
        //AppSettings.shared.showLoader(withStatus: "Sending..")
        let fullPhoneNumber = countryCode+phoneNumber
        PhoneAuthProvider.provider().verifyPhoneNumber(fullPhoneNumber, uiDelegate: nil) { (verificationID, error) in
            AppSettings.shared.hideLoader()
            if let error = error {
                NKToastHelper.sharedInstance.showAlert(self, title: warningMessage.title, message: error.localizedDescription, completionBlock: {
                    self.navigationController?.pop(true)
                })
                return
            }
            self.toggleResend()
            self.perform(#selector(self.toggleResend), with: nil, afterDelay: 60)
            guard let verificationToken = verificationID else{
                return
            }
            self.verificationID = verificationToken
        }
    }


    @IBAction func onClickDoneButton(_ sender: UIButton){
        if self.verificationID.count == 0{
            return
        }

        if let otp = otpView.text{
            if otp.count == 6{
                self.varifyOTP(otp: otp,authID: self.verificationID)
            }else{
                NKToastHelper.sharedInstance.showAlert(self, title: warningMessage.title, message: "Please enter 6 digit code")
            }
        }else{
            NKToastHelper.sharedInstance.showAlert(self, title: warningMessage.title, message: "Please enter 6 digit code")
        }
    }

    @IBAction func onClickBackButton(_ sender: UIBarButtonItem){
        self.navigationController?.pop(true)
    }

    func varifyOTP(otp:String,authID:String){
        if self.appAction == .signUp{
            guard let name = self.fullName else{
                NKToastHelper.sharedInstance.showErrorAlert(self, message: "Please enter your name")
                return
            }
            guard let myPassword = self.password else{
                NKToastHelper.sharedInstance.showErrorAlert(self, message: "Please enter your password")
                return
            }
            AppSettings.shared.showLoader(withStatus: "Validating..")
            let credential = PhoneAuthProvider.provider().credential(
                withVerificationID: authID,
                verificationCode: otp)

            Auth.auth().signIn(with: credential) { (user, error) in
                if let error = error {
                    AppSettings.shared.hideLoader()
                    NKToastHelper.sharedInstance.showAlert(self, title: warningMessage.title, message: error.localizedDescription, completionBlock: {
                        self.otpView.text = ""
                    })
                    return
                }else{
                    //create user's account
                    AppSettings.shared.updateLoader(withStatus: "Creating..")
                    self.signUpUserWith(name: name, contact: self.contact, countryCode: self.countryCode, password: myPassword, question1: self.question1, question2: self.question2)
                }
            }
        }
    }

    func signUpUserWith(name : String, contact : String, countryCode : String, password : String, question1 : Question, question2 : Question){
        LoginService.sharedInstance.registerUserWith(name, contact: contact, countryCode: countryCode, password: password, securityQuestion1: question1, securityQuestion2: question2) { (success, resUser, message) in
            AppSettings.shared.hideLoader()
            if success{
                AppSettings.shared.countryCode = countryCode
                AppSettings.shared.phoneNumber = contact
                AppSettings.shared.passwordEncrypted = password

                AppSettings.shared.isLoginWithTouchIDEnable = false
                AppSettings.shared.interestedInLoginWithTouchID = false
                LoginService.sharedInstance.validateUser({ (success, resUser, resmessage) in
                    AppSettings.shared.hideLoader()
                    if success{
                        LoginService.sharedInstance.updateDeviceTokeOnServer()
                        NKToastHelper.sharedInstance.showAlert(self, title: warningMessage.title, message: "Your account has created successfully. You can access your account by logging in", completionBlock: {
                            self.navigationController?.popToRoot(true)
                        })
                    }else{
                        NKToastHelper.sharedInstance.showAlert(self, title: warningMessage.title, message: resmessage)
                    }
                })
            }else{
                NKToastHelper.sharedInstance.showAlert(self, title: warningMessage.title, message: message)
            }


        }

    }

}


extension OTPVerificationViewController: PinCodeTextFieldDelegate{
    func textFieldValueChanged(_ textField: PinCodeTextField) {
        if let otp = otpView.text{
            if otp.count == 6{
                self.varifyOTP(otp: otp,authID: self.verificationID)
            }
        }
    }
}


