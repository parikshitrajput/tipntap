//
//  TutorialForgotPasswordViewController.swift
//  TipNTap
//
//  Created by TecOrb on 22/01/18.
//  Copyright © 2018 Nakul Sharma. All rights reserved.
//


import UIKit

class TutorialForgotPasswordViewController: UIViewController {

    @IBOutlet weak var closeButton: UIButton!

    override func viewDidLoad() {
        closeButton.transform = CGAffineTransform(rotationAngle: CGFloat(Double.pi/4))
    }

    @IBAction func closeAction(_ sender: AnyObject) {
        self.dismiss(animated: true, completion: nil)
    }

//    override func viewWillAppear(_ animated: Bool) {
//        UIApplication.shared.setStatusBarStyle(.lightContent, animated: true)
//    }
//
//    override func viewWillDisappear(_ animated: Bool) {
//        UIApplication.shared.setStatusBarStyle(.default, animated: true)
//    }
}
