//
//  UserSecurityQuestionsViewController.swift
//  TipNTap
//
//  Created by TecOrb on 13/03/18.
//  Copyright © 2018 Nakul Sharma. All rights reserved.
//

import UIKit
import SwiftyJSON
import RSKPlaceholderTextView
import Firebase

class UserSecurityQuestionsViewController: UIViewController {
    var selectedQuestionOne : Question = Question()
    var selectedQuestionTwo : Question = Question()
    var appAction : appAction!

    var countryCode: String!
    var phoneNumber: String!

    @IBOutlet weak var questionOneButton : UIButton!
    @IBOutlet weak var answerOneTextField : UITextField!
    @IBOutlet weak var questionOneLabel : UILabel!
    @IBOutlet weak var questionOneContainer : UIView!

    @IBOutlet weak var questionTwoButton : UIButton!
    @IBOutlet weak var questionTwoLabel : UILabel!
    @IBOutlet weak var answerTwoTextField : UITextField!
    @IBOutlet weak var questionTwoContainer : UIView!

    @IBOutlet weak var nextButton : UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.nextButton.isEnabled = false
        self.answerOneTextField.addTarget(self, action:#selector(textDidChanged(_:)), for: .editingChanged)
        self.answerTwoTextField.addTarget(self, action:#selector(textDidChanged(_:)), for: .editingChanged)
        self.answerOneTextField.delegate = self
        self.answerTwoTextField.delegate = self
        self.getSecuritySelectedQuestionsFromServer(countryCode: self.countryCode, phoneNumber: self.phoneNumber)
    }

    override func viewDidLayoutSubviews() {
        self.setupViews()
    }

    func setupViews(){
        self.nextButton.applyGradient(withColours: [appColor.gradientStart,appColor.gradientEnd], gradientOrientation: .topLeftBottomRight)
        CommonClass.makeViewCircularWithCornerRadius(self.nextButton, borderColor: UIColor.clear, borderWidth: 0, cornerRadius: self.nextButton.frame.size.height/4)
        self.questionOneContainer.addshadow(top: true, left: true, bottom: true, right: true, shadowRadius: 5, shadowOpacity: 0.5,shadowColor: .gray)
        self.questionTwoContainer.addshadow(top: true, left: true, bottom: true, right: true, shadowRadius: 5, shadowOpacity: 0.5,shadowColor: .gray)
    }

    func setDefualtQuestion(selectedQuestions:Set<Question>) {
        self.answerOneTextField.placeholder = "Answer"
        self.answerTwoTextField.placeholder = "Answer"

        self.nextButton.isEnabled = (selectedQuestions.count > 0)
        if selectedQuestions.count == 0{
            return
        }

        let squestions = Array(selectedQuestions)
        self.selectedQuestionOne = squestions[0]
        self.selectedQuestionTwo = squestions[1]
        self.questionOneLabel.text = "Q. "+squestions[0].text
        self.questionTwoLabel.text = "Q. "+squestions[1].text

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


    //MARK: - Actions
    @IBAction func onClickBack(_ sender: UIBarButtonItem){
        self.navigationController?.pop(true)
    }

    @IBAction func onClickNext(_ sender: UIButton){
        let validationResult = self.validateParams()
        if !validationResult.result{
            NKToastHelper.sharedInstance.showErrorAlert(self, message: validationResult.message, completionBlock: {
              //  self.getSecuritySelectedQuestionsFromServer(countryCode: self.countryCode, phoneNumber: self.phoneNumber)
            })
        }
        // just check the server reposne for selected questions and act accordingly
        self.getTokenForForgotPasswordRequest(countryCode: self.countryCode, phoneNumber: self.phoneNumber, questionOne: selectedQuestionOne, questionTwo: selectedQuestionTwo)
    }

    func validateParams() -> (result:Bool,message:String){
        if self.selectedQuestionOne.ID == ""{
            return (false,"Question 1 does not exist")
        }

        if self.selectedQuestionOne.answer == ""{
            return (false,"Please write answer of question 1.")
        }

        if self.selectedQuestionTwo.ID == ""{
            return (false,"Question 2 does not exist")
        }

        if self.selectedQuestionTwo.answer == ""{
            return (false,"Please write answer of question 2.")
        }

        return (true,"")
    }

    func getTokenForForgotPasswordRequest(countryCode:String,phoneNumber:String,questionOne:Question,questionTwo:Question){
        AppSettings.shared.showLoader(withStatus: "Please Wait..")
        LoginService.sharedInstance.getTokenForResetPassword(countryCode: countryCode, phoneNumber: phoneNumber, questionOne: questionOne, questionTwo: questionTwo) { (success, resToken, message) in
            AppSettings.shared.hideLoader()
            self.nextButton.isEnabled = true

            if success{
                if let token = resToken{
                    self.openResetPassword(token: token)
                }else{
                    NKToastHelper.sharedInstance.showAlert(self, title: warningMessage.title, message: message)
                }
            }else{
                NKToastHelper.sharedInstance.showAlert(self, title: warningMessage.title, message: message)
            }
        }
    }

    func openResetPassword(token:String){
        let resetPasswordVC = AppStoryboard.Main.viewController(ResetPasswordViewController.self)
        resetPasswordVC.token = token
        self.navigationController?.pushViewController(resetPasswordVC, animated: true)
    }

    func getSecuritySelectedQuestionsFromServer(countryCode:String,phoneNumber:String){
        AppSettings.shared.showLoader(withStatus: "Loading..")
        LoginService.sharedInstance.getListOfSecurityQuestionsForUser(countryCode, phoneNumber: phoneNumber){ (success, questionsSet, message) in
            AppSettings.shared.hideLoader()

            if let qSet = questionsSet{
                self.setDefualtQuestion(selectedQuestions: qSet)
            }else{
                NKToastHelper.sharedInstance.showErrorAlert(self, message: message, completionBlock: {

                })
            }
        }
    }

}



extension UserSecurityQuestionsViewController:UITextFieldDelegate{
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == answerOneTextField{
            self.selectedQuestionOne.answer = textField.text ?? ""
        }else if textField == answerTwoTextField{
            self.selectedQuestionTwo.answer = textField.text ?? ""
        }
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == answerOneTextField{
            self.selectedQuestionOne.answer = textField.text ?? ""
        }else if textField == answerTwoTextField{
            self.selectedQuestionTwo.answer = textField.text ?? ""
        }
    }

    @IBAction func textDidChanged(_ textField: UITextField){
        if textField == answerOneTextField{
            self.selectedQuestionOne.answer = textField.text ?? ""
        }else if textField == answerTwoTextField{
            self.selectedQuestionTwo.answer = textField.text ?? ""
        }
    }
}



