//
//  ForgotPasswordViewController.swift
//  TipNTap
//
//  Created by TecOrb on 22/01/18.
//  Copyright © 2018 Nakul Sharma. All rights reserved.
//

import UIKit
import BubbleTransition
import CountryPickerView
import Firebase

class ForgotPasswordViewController: UIViewController, UIViewControllerTransitioningDelegate {
    var appAction : appAction = .forgotPassword
    
    @IBOutlet weak var transitionButton: UIButton!
    @IBOutlet weak var proceedButton: UIButton!

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var countryPickerView: UIView!

    @IBOutlet weak var phoneNumberButton: UIButton!
    @IBOutlet weak var phoneNumberTextField: FloatLabelTextField!
    let transition = BubbleTransition()
    
    var cpv : CountryPickerView!
    var selectedCountry: Country!

    override func viewDidLoad() {
        self.phoneNumberTextField.addTarget(self, action:#selector(refreshLoginEditingFields(textField:)), for: .editingChanged)
        self.phoneNumberTextField.titleFont = fonts.Raleway.regular.font(.small)

        self.phoneNumberTextField.delegate = self
        self.countryPickerSetUp()
        self.containerView.setNeedsLayout()
        self.containerView.layoutIfNeeded()
    }

    func countryPickerSetUp() {
        if cpv != nil{
            cpv.removeFromSuperview()
        }
        cpv = CountryPickerView(frame: self.countryPickerView.frame)
        self.countryPickerView.addSubview(cpv)
        cpv.countryDetailsLabel.font = fonts.Raleway.medium.font(.medium)
        cpv.showPhoneCodeInView = true
        cpv.showCountryCodeInView = true
        cpv.showCountryFlagInView = false
        cpv.dataSource = self
        cpv.delegate = self
        cpv.translatesAutoresizingMaskIntoConstraints = false
        let topConstraints = NSLayoutConstraint(item: cpv, attribute: .top, relatedBy: .equal, toItem: self.countryPickerView, attribute: .top, multiplier: 1, constant: 0)
        let bottomConstraints = NSLayoutConstraint(item: cpv, attribute: .bottom, relatedBy: .equal, toItem: self.countryPickerView, attribute: .bottom, multiplier: 1, constant: 0)
        let leadingConstraints = NSLayoutConstraint(item: cpv, attribute: .leading, relatedBy: .equal, toItem: self.countryPickerView, attribute: .leading, multiplier: 1, constant: 0)
        let trailingConstraints = NSLayoutConstraint(item: cpv, attribute: .trailing, relatedBy: .equal, toItem: self.countryPickerView, attribute: .trailing, multiplier: 1, constant: 0)
        self.countryPickerView.addConstraints([topConstraints,leadingConstraints,trailingConstraints,bottomConstraints])
        //self.selectedCountry = cpv.selectedCountry
    }

    override func viewDidLayoutSubviews() {
        CommonClass.makeViewCircularWithCornerRadius(self.containerView, borderColor: appColor.gray , borderWidth: 1, cornerRadius: 3)
        self.containerView.addshadow(top: true, left: true, bottom: true, right: true, shadowRadius: 12, shadowOpacity: 0.3,shadowColor: appColor.gray)
        CommonClass.makeViewCircularWithCornerRadius(self.proceedButton, borderColor: UIColor.clear, borderWidth: 0, cornerRadius: self.proceedButton.frame.size.height/4)
        self.proceedButton.applyGradient(withColours: [appColor.gradientStart,appColor.gradientEnd], gradientOrientation: .topLeftBottomRight)
    }

    func sendOTPOn(countryCode:String,phoneNumber:String){
        let firebaseAuth = Auth.auth()
        do {
            try firebaseAuth.signOut()
        } catch let signOutError as NSError {
            NKToastHelper.sharedInstance.showAlert(self, title: warningMessage.title, message: signOutError.localizedDescription, completionBlock: {
                self.navigationController?.pop(true)
            })
        }
        AppSettings.shared.showLoader(withStatus: "Please wait..")
        let fullPhoneNumber = countryCode+phoneNumber
        PhoneAuthProvider.provider().verifyPhoneNumber(fullPhoneNumber, uiDelegate: nil) { (verificationID, error) in
            AppSettings.shared.hideLoader()
            if let error = error {
                NKToastHelper.sharedInstance.showAlert(self, title: warningMessage.title, message: error.localizedDescription, completionBlock: {})
                return
            }
            //proceed to otp varification
            guard let verificationToken = verificationID else{
                return
            }
            self.openVarifyPhoneNumber(verificationToken, contact: phoneNumber, countryCode: countryCode)
        }
    }

    func openVarifyPhoneNumber(_ verificationToken: String, contact:String,countryCode:String){
        let OTPVC = AppStoryboard.Main.viewController(FPOTPViewController.self)
        OTPVC.contact = contact
        OTPVC.countryCode = countryCode
        OTPVC.verificationID = verificationToken
        self.navigationController?.pushViewController(OTPVC, animated: true)
    }








    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let controller = segue.destination
        controller.transitioningDelegate = self
        controller.modalPresentationStyle = .custom
    }

    @IBAction func onClickBackButton(_ sender: UIBarButtonItem){
        self.navigationController?.pop(true)
    }

    // MARK: UIViewControllerTransitioningDelegate
    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        transition.transitionMode = .present
        transition.startingPoint = transitionButton.center
        transition.bubbleColor = UIColor(red: 226.0/255.0, green: 234.0/255.0, blue: 234.0/255.0, alpha: 1.0)
        return transition
    }

    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        transition.transitionMode = .dismiss
        transition.startingPoint = transitionButton.center
        transition.bubbleColor = UIColor(red: 226.0/255.0, green: 234.0/255.0, blue: 234.0/255.0, alpha: 1.0)
        return transition
    }

}


extension ForgotPasswordViewController{
    @IBAction func onClickProceed(_ sender: UIButton){
        guard let country = self.selectedCountry else {
            NKToastHelper.sharedInstance.showAlert(self, title: warningMessage.title, message: "Please enter your phone number", completionBlock: {})
            return
        }

        let countryCode = country.phoneCode

        guard let phoneNumber = phoneNumberTextField.text else {
            NKToastHelper.sharedInstance.showAlert(self, title: warningMessage.title, message: "Please enter your phone number", completionBlock: {})
            return
        }
        if !phoneNumber.contains(countryCode){
            NKToastHelper.sharedInstance.showAlert(self, title: warningMessage.title, message: "Please reenter your phone number by selecting country first", completionBlock: {})
            return
        }



        let withoutCountryCode = phoneNumber.replacingOccurrences(of: countryCode, with: "")
        let actuallPhoneNumber = withoutCountryCode.removingWhitespaces()

        let validation = self.validateParams(countryCode: countryCode, phoneNumber: actuallPhoneNumber)
        if !validation.success{
            NKToastHelper.sharedInstance.showAlert(self, title: warningMessage.title, message: validation.message, completionBlock: {})
            return
        }
        self.sendOTPOn(countryCode: countryCode, phoneNumber: actuallPhoneNumber)
    }

    func validateParams(countryCode:String,phoneNumber: String) -> (success:Bool,message:String){

        if countryCode.trimmingCharacters(in: .whitespaces).count == 0{
            return (false,"Please enter country code")
        }

        if phoneNumber.trimmingCharacters(in: .whitespaces).count == 0{
            return (false,"Please enter your contact")
        }
        let phoneValidation = CommonClass.validatePhoneNumber(phoneNumber.trimmingCharacters(in: .whitespaces))
        if !phoneValidation{
            return (false,"Please enter a valid contact")
        }

        if (phoneNumber.trimmingCharacters(in: .whitespaces).count < self.selectedCountry.minDigit){
            return (false,"Contact should not be less than \(self.selectedCountry.minDigit) in length")
        }
        if  (phoneNumber.trimmingCharacters(in: .whitespaces).count > self.selectedCountry.maxDigit){
            return (false,"Contact should not be more than \(self.selectedCountry.maxDigit) in length")
        }
        return (true,"")
    }


}

extension ForgotPasswordViewController: UITextFieldDelegate{

    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == self.phoneNumberTextField{
            let textInTextField = textField.text ?? ""
            if textInTextField.isEmpty{
                cpv.showCountriesList(from: self)
                return false
            }else{
                return true
            }
        }
        return true
    }

    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.refreshLoginEditingFields(textField: textField)
    }

    func textFieldDidEndEditing(_ textField: UITextField) {
        self.phoneNumberButton.isSelected = false
    }

    @objc func refreshLoginEditingFields(textField: UITextField) {

        if textField == self.phoneNumberTextField{
            self.phoneNumberButton.isSelected = true
            let text = textField.text ?? ""
            if text.isEmpty{
                self.cpv.showCountriesList(from: self)
            }
        }
    }
    @IBAction func textFieldDidChangeTextHandler(_ textField: UITextField){
        self.refreshLoginEditingFields(textField: textField)
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
}


extension ForgotPasswordViewController : CountryPickerViewDataSource,CountryPickerViewDelegate{
    func sectionTitleForPreferredCountries(in countryPickerView: CountryPickerView) -> String? {
        return (AppSettings.shared.currentCountryCode.count != 0) ? "Current" : nil
    }

    func showOnlyPreferredSection(in countryPickerView: CountryPickerView) -> Bool? {
        return false
    }

    func navigationTitle(in countryPickerView: CountryPickerView) -> String? {
        return "Select Country"
    }

    func closeButtonNavigationItem(in countryPickerView: CountryPickerView) -> UIBarButtonItem? {
        let barButton = UIBarButtonItem(image: #imageLiteral(resourceName: "close_icon"), style: .plain, target: nil, action: nil)
        barButton.tintColor = appColor.blue
        return barButton
    }

    func searchBarPosition(in countryPickerView: CountryPickerView) -> SearchBarPosition {
        return .navigationBar
    }


    func countryPickerView(_ countryPickerView: CountryPickerView, didSelectCountry country: Country) {
        self.selectedCountry = country
        let filledMobile = self.phoneNumberTextField.text ?? ""
        self.phoneNumberTextField.text = "\(self.selectedCountry.phoneCode) "+filledMobile
        self.phoneNumberTextField.becomeFirstResponder()
    }


    func preferredCountries(in countryPickerView: CountryPickerView) -> [Country]? {
        if let currentCountry = countryPickerView.getCountryByCode(AppSettings.shared.currentCountryCode){
            return [currentCountry]
        }else{
            return [countryPickerView.selectedCountry]
        }
    }

    func showPhoneCodeInList(in countryPickerView: CountryPickerView) -> Bool? {
        return true
    }


}
