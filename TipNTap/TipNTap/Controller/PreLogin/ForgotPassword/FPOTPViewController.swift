//
//  FPOTPViewController.swift
//  TipNTap
//
//  Created by TecOrb on 13/03/18.
//  Copyright © 2018 Nakul Sharma. All rights reserved.
//

import UIKit
import Firebase


class FPOTPViewController: UIViewController {
    @IBOutlet weak var otpView: PinCodeTextField!
    @IBOutlet weak var donebutton: UIButton!
    @IBOutlet weak var resendbutton: UIButton!
    @IBOutlet weak var sentToLabel: UILabel!
    @IBOutlet weak var didNotReceivedCodeLabel: UILabel!

    var verificationID : String!
    var otpString :String = ""
    var contact:String!
    var countryCode:String!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.sentToLabel.text = " (\(self.countryCode+self.contact))"
        self.otpView.keyboardType = .numberPad
        self.otpView.font = fonts.Raleway.bold.font(.xXXLarge)
        self.otpView.delegate = self
        self.toggleResend()
        self.perform(#selector(toggleResend), with: nil, afterDelay: 60)
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(1)) {
            self.otpView.becomeFirstResponder()
        }
    }

    @objc func toggleResend(){
        self.resendbutton.isEnabled = !self.resendbutton.isEnabled
        self.didNotReceivedCodeLabel.alpha = (self.didNotReceivedCodeLabel.alpha == 1.0) ? 0.2 : 1.0
        self.resendbutton.alpha = (self.resendbutton.alpha == 1.0) ? 0.2 : 1.0
    }

    override func viewDidLayoutSubviews() {
        self.donebutton.applyGradient(withColours: [appColor.gradientStart,appColor.gradientEnd], gradientOrientation: .topLeftBottomRight)
        CommonClass.makeViewCircularWithCornerRadius(self.donebutton, borderColor: UIColor.clear, borderWidth: 0, cornerRadius: self.donebutton.frame.size.height/4)

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func onClickResend(_ sender: UIButton){
        self.otpView.text = ""
        self.sendOTPOn(countryCode: self.countryCode, phoneNumber: self.contact)
    }


    func sendOTPOn(countryCode:String,phoneNumber:String){
        AppSettings.shared.showLoader(withStatus: "Sending..")

        let firebaseAuth = Auth.auth()
        do {
            try firebaseAuth.signOut()
        } catch let signOutError as NSError {
            NKToastHelper.sharedInstance.showAlert(self, title: warningMessage.title, message: signOutError.localizedDescription, completionBlock: {
                self.navigationController?.pop(true)
            })
            return
        }


        let fullPhoneNumber = countryCode+phoneNumber
        PhoneAuthProvider.provider().verifyPhoneNumber(fullPhoneNumber, uiDelegate: nil) { (verificationID, error) in
            AppSettings.shared.hideLoader()
            if let error = error {
                NKToastHelper.sharedInstance.showAlert(self, title: warningMessage.title, message: error.localizedDescription, completionBlock: {
                    self.navigationController?.pop(true)
                })
                return
            }
            self.toggleResend()
            self.perform(#selector(self.toggleResend), with: nil, afterDelay: 60)
            guard let verificationToken = verificationID else{
                return
            }
            self.verificationID = verificationToken
        }
    }


    @IBAction func onClickDoneButton(_ sender: UIButton){
        if self.verificationID.count == 0{
            return
        }

        if let otp = otpView.text{
            if otp.count == 6{
                self.varifyOTP(otp: otp,authID: self.verificationID)
            }else{
                NKToastHelper.sharedInstance.showAlert(self, title: warningMessage.title, message: "Please enter 6 digit code")
            }
        }else{
            NKToastHelper.sharedInstance.showAlert(self, title: warningMessage.title, message: "Please enter 6 digit code")
        }
    }

    @IBAction func onClickBackButton(_ sender: UIBarButtonItem){
        self.navigationController?.pop(true)
    }


    func varifyOTP(otp:String,authID:String){
            AppSettings.shared.showLoader(withStatus: "Validating..")
            let credential = PhoneAuthProvider.provider().credential(
                withVerificationID: authID,
                verificationCode: otp)

            Auth.auth().signIn(with: credential) { (user, error) in
                if let error = error {
                    AppSettings.shared.hideLoader()
                    NKToastHelper.sharedInstance.showAlert(self, title: warningMessage.title, message: error.localizedDescription, completionBlock: {
                        self.otpView.text = ""
                    })
                    return
                }else{
                    //create user's account
                    AppSettings.shared.updateLoader(withStatus: "Creating..")
                    //Go to security question list of user
                    let usqVC = AppStoryboard.Main.viewController(UserSecurityQuestionsViewController.self)
                    usqVC.countryCode = self.countryCode
                    usqVC.phoneNumber = self.contact
                    self.navigationController?.pushViewController(usqVC, animated: true)
                }
            }
        }
    }

extension FPOTPViewController: PinCodeTextFieldDelegate{
    func textFieldValueChanged(_ textField: PinCodeTextField) {
        if let otp = otpView.text{
            if otp.count == 6{
                self.varifyOTP(otp: otp,authID: self.verificationID)
            }
        }
    }
}


