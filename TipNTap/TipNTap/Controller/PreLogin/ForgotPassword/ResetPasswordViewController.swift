//
//  ResetPasswordViewController.swift
//  TipNTap
//
//  Created by TecOrb on 13/03/18.
//  Copyright © 2018 Nakul Sharma. All rights reserved.
//

import UIKit

class ResetPasswordViewController: UIViewController {

    @IBOutlet weak var confirmPasswordButton: UIButton!
    @IBOutlet weak var confirmPasswordTextField: FloatLabelTextField!

    @IBOutlet weak var passwordButton: UIButton!
    @IBOutlet weak var passwordTextField: FloatLabelTextField!
    @IBOutlet weak var resetButton: UIButton!
    @IBOutlet weak var containerView: UIView!

    var token : String!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.confirmPasswordTextField.addTarget(self, action:#selector(refreshLoginEditingFields(textField:)), for: .editingChanged)
        self.passwordTextField.addTarget(self, action:#selector(refreshLoginEditingFields(textField:)), for: .editingChanged)
        self.confirmPasswordTextField.titleFont = fonts.Raleway.regular.font(.small)
        self.passwordTextField.titleFont = fonts.Raleway.regular.font(.small)

        self.confirmPasswordTextField.delegate = self
        self.passwordTextField.delegate = self
    }

    override func viewDidLayoutSubviews() {
        CommonClass.makeViewCircularWithCornerRadius(self.containerView, borderColor: appColor.gray , borderWidth: 1, cornerRadius: 3)
        self.containerView.addshadow(top: true, left: true, bottom: true, right: true, shadowRadius: 12, shadowOpacity: 0.3,shadowColor: appColor.gray)
        CommonClass.makeViewCircularWithCornerRadius(self.resetButton, borderColor: UIColor.clear, borderWidth: 0, cornerRadius: self.resetButton.frame.size.height/4)
        self.resetButton.applyGradient(withColours: [appColor.gradientStart,appColor.gradientEnd], gradientOrientation: .topLeftBottomRight)
    }


    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func onClickBack(_ sender: UIBarButtonItem){
        self.navigationController?.pop(true)
    }

    @IBAction func onClickResetButton(_ sender: UIButton) {
        guard let password = self.passwordTextField.text else {
            NKToastHelper.sharedInstance.showErrorAlert(self, message: warningMessage.enterPassword.rawValue)
            return
        }
        guard let cPassword = self.confirmPasswordTextField.text else {
            NKToastHelper.sharedInstance.showErrorAlert(self, message: warningMessage.confirmPassword.rawValue)
            return
        }

        if password != cPassword{
            NKToastHelper.sharedInstance.showErrorAlert(self, message: warningMessage.passwordDidNotMatch.rawValue)
            return
        }
        self.resetPassword(token: self.token, password: password)
    }

    func resetPassword(token:String,password:String){
        LoginService.sharedInstance.resetPassword(newPassword: password, withToken: token) { (success, message) in
            if success{
                AppSettings.shared.passwordEncrypted = password
                NKToastHelper.sharedInstance.showAlert(self, title: warningMessage.title, message: message){
                    self.navigationController?.popToRoot(true)
                }
            }else{
                NKToastHelper.sharedInstance.showAlert(self, title: warningMessage.title, message: message)
            }
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
extension ResetPasswordViewController : UITextFieldDelegate{

    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.refreshLoginEditingFields(textField: textField)
    }

    func textFieldDidEndEditing(_ textField: UITextField) {
        self.confirmPasswordButton.isSelected = false
        self.passwordButton.isSelected = false
    }

    @objc func refreshLoginEditingFields(textField: UITextField) {

        if textField == self.confirmPasswordTextField{
            self.confirmPasswordButton.isSelected = true
            self.passwordButton.isSelected = false
        }else if textField == self.passwordTextField{
            self.confirmPasswordButton.isSelected = false
            self.passwordButton.isSelected = true
        }
    }
    @IBAction func textFieldDidChangeTextHandler(_ textField: UITextField){
        self.refreshLoginEditingFields(textField: textField)
    }

}

