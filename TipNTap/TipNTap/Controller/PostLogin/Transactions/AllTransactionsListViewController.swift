//
//  TransactionsListViewController.swift
//  TipNTap
//
//  Created by TecOrb on 14/03/18.
//  Copyright © 2018 Nakul Sharma. All rights reserved.
//

import UIKit
struct transactionColor {
    static let received = UIColor(red: 71.0/255.0, green: 180.0/255.0, blue: 127.0/255.0, alpha: 1.0)
    static let send = appColor.blue
}

class AllTransactionsListViewController: UIViewController {
    @IBOutlet weak var txnTableView: UITableView!
    var txnArray = Array<UserTransaction>()
    var user : User!
    var pageNumber = 1
    var totalPage = 1
    var recordsPerPage = 10
    var isNewDataLoading = false
    var canLoadMore = true
    let filter = TransactionFilter.all


    override func viewDidLoad() {
        super.viewDidLoad()
        self.registerCells()
        self.txnTableView.separatorColor = appColor.lightGray
        self.txnTableView.dataSource = self
        self.txnTableView.delegate = self
        self.loadTransactionFor(filter: self.filter, pageNumber: self.pageNumber, recordPerPage: self.recordsPerPage)
    }

    deinit{
        NotificationCenter.default.removeObserver(self)
    }
    func registerCells(){
        self.txnTableView.register(UINib(nibName: "NoDataCell", bundle: nil), forCellReuseIdentifier: "NoDataCell")
        self.txnTableView.register(UINib(nibName: "TransactionCell", bundle: nil), forCellReuseIdentifier: "TransactionCell")
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func loadTransactionFor(filter:TransactionFilter, pageNumber:Int, recordPerPage:Int) {
        if pageNumber > self.totalPage {
            return
        }

        if !AppSettings.isConnectedToNetwork{
            NKToastHelper.sharedInstance.showErrorAlert(self, message: warningMessage.networkIsNotConnected.rawValue)
            return
        }

        self.isNewDataLoading = true
        if pageNumber == 1{
            txnArray.removeAll()
            txnTableView.reloadData()
        }
        if self.pageNumber > 1{
            self.txnTableView.addFooterSpinner()
        }

        TransactionService.sharedInstance.getTransactions(filter: self.filter, page: pageNumber, perPage: recordsPerPage) { (success, txnResponse, resTotalPage, message) in
            self.isNewDataLoading = false
            self.totalPage = resTotalPage

            if let newtxnArray = txnResponse{
                if newtxnArray.count == 0{
                    self.pageNumber = self.pageNumber - 1
                }
                self.txnArray.append(contentsOf: newtxnArray)
                self.txnTableView.reloadData()
            }else{
                if self.pageNumber > 1{
                    self.pageNumber = self.pageNumber - 1
                }
            }
            self.txnTableView.removeFooterSpinner()
            self.txnTableView.reloadData()
        }
    }

    func handleRefresh(){//(_ refreshControl: UIRefreshControl) {
        pageNumber = 1
        self.isNewDataLoading = true
        self.txnTableView.reloadData()
        TransactionService.sharedInstance.getTransactions(filter: self.filter, page: pageNumber, perPage: recordsPerPage) { (success, txnResponse,resTotalPage, message)  in
            self.txnArray.removeAll()
            self.totalPage = resTotalPage
            //self.refreshControl.endRefreshing()
            self.isNewDataLoading = false
            if let newtxnArray = txnResponse{
                self.txnArray.append(contentsOf: newtxnArray)
            }
            self.txnTableView.removeFooterSpinner()
            self.txnTableView.reloadData()
        }
    }


}

extension AllTransactionsListViewController: UITableViewDataSource,UITableViewDelegate{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (self.txnArray.count == 0) ? 1 : self.txnArray.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return (self.txnArray.count == 0) ? 120 : 68
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if self.txnArray.count == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "NoDataCell", for: indexPath) as! NoDataCell
            cell.messageLabel.text = isNewDataLoading ? "Loading.." : "No transaction found\nTap here to refresh"
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "TransactionCell", for: indexPath) as! TransactionCell
            let txn = txnArray[indexPath.row]
            cell.txnTypeLabel.text = txn.transactionType.capitalized
            cell.txnAmountlabel.text = (txn.transactionType.lowercased() == "received") ? String(format:"$ %0.2f",txn.payout) : String(format:"$ %0.2f",txn.amount)
            cell.txnAmountlabel.textColor = txn.isTransferedToPayee ? transactionColor.received : transactionColor.send
            let message = (txn.transactionType.lowercased() == "received") ? "From \(txn.sender.name) #\(txn.sender.accountReference)" : "To \(txn.payee.name) #\(txn.payee.accountReference)"
            cell.txnMessagelabel.text = message
            DispatchQueue.main.async {
                if self.pageNumber <= self.totalPage{
                    self.loadNextBatch(indexPath: indexPath)
                }
            }
            return cell
        }
    }

    
    func loadNextBatch(indexPath:IndexPath) {
        if self.pageNumber > self.totalPage {
            self.txnTableView.tableFooterView = nil
            return
        }
        if txnArray.count >= self.recordsPerPage && canLoadMore {
            if !isNewDataLoading && ((txnArray.count - indexPath.row) <= self.recordsPerPage/2){
                if AppSettings.isConnectedToNetwork{
                    isNewDataLoading = true
                    self.pageNumber+=1
                    self.loadTransactionFor(filter: self.filter, pageNumber: self.pageNumber, recordPerPage: self.recordsPerPage)
                }
            }
        }
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if self.txnArray.count == 0{
            self.handleRefresh()
            return
        }
        let txn = self.txnArray[indexPath.row]
        self.openTransactionDetails(txn)
    }

    func openTransactionDetails(_ transaction: UserTransaction) {
        let paymentSuccessVC = AppStoryboard.Scanner.viewController(PaymentSuccessViewController.self)
        paymentSuccessVC.payment = transaction
        paymentSuccessVC.isPaying = (transaction.transactionType.lowercased() != "received")
        paymentSuccessVC.fromPassbook = true
        self.navigationController?.pushViewController(paymentSuccessVC, animated: true)
    }
}






