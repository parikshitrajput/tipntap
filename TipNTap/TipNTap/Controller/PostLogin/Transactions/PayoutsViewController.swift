//
//  PayoutsViewController.swift
//  TipNTap
//
//  Created by TecOrb on 30/05/18.
//  Copyright © 2018 Nakul Sharma. All rights reserved.
//

import UIKit

class PayoutsViewController: UIViewController {
    @IBOutlet weak var txnTableView: UITableView!
    var payoutsArray = Array<Payout>()
    var user : User!
    var pageNumber = 1
    var totalPage = 1
    var recordsPerPage = 10
    var isNewDataLoading = false
    var canLoadMore = true

    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.tintColor = appColor.blue
        refreshControl.addTarget(self, action: #selector(PayoutsViewController.handleRefresh(_:)), for: UIControlEvents.valueChanged)
        refreshControl.backgroundColor = UIColor.init(red: 245.0/255.0, green: 245.0/255.0, blue: 245.0/255.0, alpha: 1)
        return refreshControl
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.registerCells()
        self.txnTableView.addSubview(refreshControl)
        self.txnTableView.dataSource = self
        self.txnTableView.delegate = self
        self.txnTableView.backgroundView?.backgroundColor = self.txnTableView.backgroundColor
        self.txnTableView.contentInset = UIEdgeInsets(top: 5, left: 0, bottom: 5, right: 0)
        self.loadPayoutsFor(pageNumber: self.pageNumber, recordPerPage: self.recordsPerPage)
    }

    @IBAction func onClickMenuButton(_ sender: UIBarButtonItem){
        sideMenuController?.toggleLeftView(animated: true, delay: 0.0, completionHandler: nil)
    }

    @objc func handleRefresh(_ sender: UIRefreshControl){
        pageNumber = 1
        payoutsArray.removeAll()
        txnTableView.reloadData()
        self.loadPayoutsFor(pageNumber: self.pageNumber, recordPerPage: self.recordsPerPage)
    }

    deinit{
        NotificationCenter.default.removeObserver(self)
    }

    func registerCells(){
        self.txnTableView.register(UINib(nibName: "PayoutCell", bundle: nil), forCellReuseIdentifier: "PayoutCell")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func loadPayoutsFor(pageNumber:Int, recordPerPage:Int) {
        if !AppSettings.isConnectedToNetwork{
            NKToastHelper.sharedInstance.showErrorAlert(self, message: warningMessage.networkIsNotConnected.rawValue)
            return
        }

        self.isNewDataLoading = true
        if pageNumber == 1{
            payoutsArray.removeAll()
            txnTableView.reloadData()
        }
        if self.pageNumber > 1{
            self.txnTableView.addFooterSpinner()
        }

        PaymentService.sharedInstance.getPayoutsForUser(page: pageNumber, perPage: recordPerPage) { (success, payoutsResponse, resTotalPage, message) in
            if self.refreshControl.isRefreshing{self.refreshControl.endRefreshing()}
            self.isNewDataLoading = false
            self.totalPage = resTotalPage

            if let newPayoutArray = payoutsResponse{
                if newPayoutArray.count == 0{
                    self.pageNumber = self.pageNumber - 1
                }
                self.payoutsArray.append(contentsOf: newPayoutArray)
                self.txnTableView.reloadData()
            }else{
                if self.pageNumber > 1{
                    self.pageNumber = self.pageNumber - 1
                }
            }
            self.txnTableView.removeFooterSpinner()
            self.txnTableView.reloadData()
        }


    }



}

extension PayoutsViewController: UITableViewDataSource,UITableViewDelegate{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (self.payoutsArray.count == 0) ? 1 : self.payoutsArray.count
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 122
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if self.payoutsArray.count == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "NoDataCell", for: indexPath) as! NoDataCell
            cell.messageLabel.text = isNewDataLoading ? "Loading.." : "No payout found\nPull down to refresh"
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "PayoutCell", for: indexPath) as! PayoutCell
            let payout = self.payoutsArray[indexPath.row]
            cell.amountLabel.text = String(format:"$ %0.2f",payout.amount)
            cell.dateTypeLabel.text = (payout.status == .paid) ? "Paid" : "Expected"
            let dateToShow = (payout.status == .paid) ? payout.arrivalDate : payout.arrivalDate
            cell.dateLabel.text = self.formattedDateWithTimeStamp(dateToShow, format: "dd-MM-YYYY")
            cell.amountContainner.backgroundColor = (payout.status == .paid) ? appColor.green : appColor.payoutBlue
            cell.statusLabel.text = (payout.status == .paid) ? "PAID" : "INPROCESS"
            cell.statusLabel.backgroundColor = payout.status.getColor()
            cell.bankNameLabel.text = payout.bankAccount.bankName.capitalized
            cell.accountNumberLabel.text = "xxxxxx-\(payout.bankAccount.last4)"
            let payoutDate = self.formattedDateWithTimeStamp(payout.payoutDate, format: "dd-MM-YYYY")
            cell.descriptionLabel.text = "Payout initiated on \(payoutDate)"
            DispatchQueue.main.async {
                if self.pageNumber <= self.totalPage{
                    self.loadNextBatch(indexPath: indexPath)
                }
            }

            return cell
        }
    }



    func loadNextBatch(indexPath:IndexPath) {
        if self.pageNumber > self.totalPage {
            self.txnTableView.tableFooterView = nil
            return
        }

        if payoutsArray.count >= self.recordsPerPage && canLoadMore {
            if !isNewDataLoading && ((payoutsArray.count - indexPath.row) <= self.recordsPerPage/2){
                if AppSettings.isConnectedToNetwork{
                    isNewDataLoading = true
                    self.pageNumber+=1
                    self.loadPayoutsFor(pageNumber: self.pageNumber, recordPerPage: self.recordsPerPage)
                }
            }
        }
    }


    func formattedDateWithTimeStamp(_ timeStamp: Double,format:String) -> String {
        let date = Date(timeIntervalSince1970: TimeInterval(timeStamp))
        let dayTimePeriodFormatter = DateFormatter()
        dayTimePeriodFormatter.dateStyle = .medium
        dayTimePeriodFormatter.locale = Locale.current
        dayTimePeriodFormatter.timeZone = TimeZone.current
        dayTimePeriodFormatter.dateFormat = format
        let formattedDate = dayTimePeriodFormatter.string(from: date)
        return formattedDate
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        if self.payoutsArray.count == 0{
//            return
//        }
//        let payout = self.payoutsArray[indexPath.row]
//        self.openPayoutDetails(payout)
    }

//    func openPayoutDetails(_ payout: Payout) {
////        let paymentSuccessVC = AppStoryboard.Scanner.viewController(PaymentSuccessViewController.self)
////        paymentSuccessVC.payment = transaction
////        paymentSuccessVC.fromPassbook = true
////        paymentSuccessVC.isPaying = (transaction.transactionType.lowercased() != "received")
////        self.navigationController?.pushViewController(paymentSuccessVC, animated: true)
//    }
}
