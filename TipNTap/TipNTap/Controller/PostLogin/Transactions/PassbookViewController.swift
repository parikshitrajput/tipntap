//
//  MyBookingsViewController.swift
//  GPDock
//
//  Created by TecOrb on 04/08/17.
//  Copyright © 2017 Nakul Sharma. All rights reserved.
//
//cash current
//card completed

import UIKit
import MXSegmentedPager

class PassbookViewController: MXSegmentedPagerController {
    var user: User!
    let segmentHeight: CGFloat = 60
    @IBOutlet var headerView: UIView!
    @IBOutlet var accountBalanceLabel: UILabel!
    @IBOutlet var pendingAccountBalanceLabel: UILabel!
    @IBOutlet var heightOfTempHeader: NSLayoutConstraint!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.heightOfTempHeader.constant = AppSettings.shared.getNavigationDefaultHeight()
        self.user = User.loadSavedUser()
        self.setupPager()
        self.setupPassbook()
    }


    
    func setupPager(){
        segmentedPager.backgroundColor = .white
        // Parallax Header
        segmentedPager.parallaxHeader.view = headerView
        segmentedPager.parallaxHeader.mode = .fill
        let headerHeight = max(UIScreen.main.bounds.size.height*280.0/1333, (UIDevice.isIphoneX ? 200 : 200))//UIScreen.main.bounds.size.height*280.0/1333

        let navigationHeight:CGFloat = self.heightOfTempHeader.constant

        segmentedPager.parallaxHeader.height = headerHeight
        segmentedPager.parallaxHeader.minimumHeight = navigationHeight

        // Segmented Control customization
        segmentedPager.segmentedControl.selectionIndicatorLocation = .down
        segmentedPager.segmentedControl.backgroundColor = .white
        segmentedPager.segmentedControl.borderWidth = 1
        segmentedPager.segmentedControl.borderType = .bottom
        segmentedPager.segmentedControl.borderColor = appColor.lightGray


        segmentedPager.segmentedControl.titleTextAttributes = [NSAttributedStringKey.foregroundColor : UIColor.black,NSAttributedStringKey.font: fonts.Raleway.bold.font(.large)]
        segmentedPager.segmentedControl.selectedTitleTextAttributes = [NSAttributedStringKey.foregroundColor : UIColor.black,NSAttributedStringKey.font: fonts.Raleway.bold.font(.large)]
        segmentedPager.segmentedControl.selectionStyle = .fullWidthStripe
        segmentedPager.segmentedControl.selectionIndicatorColor = appColor.blue
        segmentedPager.segmentedControl.selectionIndicatorHeight = 3
    }

    func setupPassbook(){
        self.user.loadUserWallet { (success, wallet, message) in
            if success{
                if let myWallet = wallet{
                    let avlWithUSD = myWallet.available.filter({ (accountInstance) -> Bool in
                        return accountInstance.currency.lowercased() == "usd"
                    })
                    let pendingWithUSD = myWallet.pending.filter({ (accountInstance) -> Bool in
                        return accountInstance.currency.lowercased() == "usd"
                    })

                    let accountBalances = avlWithUSD.map({ (accountInstance) -> Double in
                            return accountInstance.amount
                    })

                    let pendingAccountBalances = pendingWithUSD.map({ (accountInstance) -> Double in
                        return accountInstance.amount
                    })

                    let totalAvlBalance = accountBalances.map({$0}).reduce(0, +)
                    let totalPendingBalance = pendingAccountBalances.map({$0}).reduce(0, +)
                    self.accountBalanceLabel.text = String(format:"$ %0.2f",totalAvlBalance)
                        self.pendingAccountBalanceLabel.text = String(format:"$ %0.2f",totalPendingBalance)
                }else{
                    self.accountBalanceLabel.text = "$ 0.00"
                    self.pendingAccountBalanceLabel.text = "$ 0.00"
                    NKToastHelper.sharedInstance.showErrorAlert(self, message: message)
                }
            }else{
                self.accountBalanceLabel.text = "$ 0.00"
                self.pendingAccountBalanceLabel.text = "$ 0.00"
                NKToastHelper.sharedInstance.showErrorAlert(self, message: message)
            }
        }
    }

    @IBAction func onClickMenuButton(_ sender: UIButton){
        self.navigationController?.pop(true)
//        sideMenuController?.toggleLeftView(animated: true, delay: 0.0, completionHandler: nil)
    }

    deinit {
        NotificationCenter.default.removeObserver(self)
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func heightForSegmentedControl(in segmentedPager: MXSegmentedPager) -> CGFloat {
        return 50
    }
    override func segmentedPager(_ segmentedPager: MXSegmentedPager, titleForSectionAt index: Int) -> String {
        return ["All", "Paid", "Received"][index]
    }

    override func segmentedPager(_ segmentedPager: MXSegmentedPager, didScrollWith parallaxHeader: MXParallaxHeader) {
    }
}



