//
//  PrivacyPolicyViewController.swift
//  TipNTap
//
//  Created by TecOrb on 22/05/18.
//  Copyright © 2018 Nakul Sharma. All rights reserved.
//

import UIKit

class PrivacyPolicyViewController: UIViewController,UIWebViewDelegate {
    @IBOutlet weak var webView: UIWebView!
    var isPrivacyPolicy : Bool = true
    var fromMenu: Bool = true
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupLeftNavigationBarButton(fromMenu: self.fromMenu)
        self.webView.delegate = self
        self.navigationItem.title = isPrivacyPolicy ? "Privacy Policy" :"Terms of Use"
        self.webView.delegate = self
        self.webView.scalesPageToFit = true
        if isPrivacyPolicy{
            let path = "https://app.termly.io/document/privacy-policy/c0335a91-52a1-42aa-adb0-8a681b91877d"
            self.webView.delegate = self
            if let targetUrl = URL(string: path){
                let request = URLRequest(url: targetUrl)
                self.webView.loadRequest(request)
            }else{
                NKToastHelper.sharedInstance.showErrorAlert(self, message: warningMessage.functionalityPending.rawValue)
            }

            ////
//            if let path = Bundle.main.path(forResource: "tipntapPrivacyPolicy", ofType: "docx"){
//                let targetUrl = URL(fileURLWithPath: path)
//                let request = URLRequest(url: targetUrl)
//                self.webView.loadRequest(request)
//            }else{
//                NKToastHelper.sharedInstance.showErrorAlert(self, message: warningMessage.functionalityPending.rawValue)
//            }
        }else{
            let path = "https://app.termly.io/document/terms-of-use-for-website/e87dcd89-0f14-4760-bc1e-099eefa32619"
            self.webView.delegate = self
            if let targetUrl = URL(string: path){
                let request = URLRequest(url: targetUrl)
                self.webView.loadRequest(request)
            }else{
                NKToastHelper.sharedInstance.showErrorAlert(self, message: warningMessage.functionalityPending.rawValue)
            }
        }

    }

    func setupLeftNavigationBarButton(fromMenu:Bool) {
        let leftBarButton = UIBarButtonItem(image: fromMenu ? #imageLiteral(resourceName: "menu_button") : #imageLiteral(resourceName: "close_icon"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(onClickMenuButton(_:)))
        let tintColor = UIColor(red: 51.0/255.0, green: 74.0/255.0, blue: 219.0/255.0, alpha: 1.0)
        leftBarButton.tintColor = tintColor
        self.navigationItem.setLeftBarButtonItems(nil, animated: false)
        self.navigationItem.setLeftBarButtonItems([leftBarButton], animated: false)
    }

    @IBAction func onClickMenuButton(_ sender: UIBarButtonItem) {
        if fromMenu{
            sideMenuController?.toggleLeftView(animated: true, delay: 0.0, completionHandler: nil)
        }else{
            self.dismiss(animated: true, completion: nil)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        if AppSettings.shared.isLoaderOnScreen{
            AppSettings.shared.hideLoader()
        }
    }
    func webViewDidFinishLoad(_ webView: UIWebView) {

        if AppSettings.shared.isLoaderOnScreen{
            AppSettings.shared.hideLoader()
        }
        webView.resizeWebContent()

    }
    func webViewDidStartLoad(_ webView: UIWebView) {
        if !AppSettings.shared.isLoaderOnScreen{
            AppSettings.shared.showLoader(withStatus: "Loading")
        }
    }

    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        if navigationType == UIWebViewNavigationType.linkClicked {
            UIApplication.shared.open(request.url!, options: [:], completionHandler: nil)
            return false
        }
        return true
    }
}


