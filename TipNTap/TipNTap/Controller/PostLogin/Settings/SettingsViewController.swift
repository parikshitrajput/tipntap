//
//  SettingsViewController.swift
//  TipNTap
//
//  Created by TecOrb on 30/03/18.
//  Copyright © 2018 Nakul Sharma. All rights reserved.
//

import UIKit
import BiometricAuthentication

class SettingsViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var logoutButton: UIButton!

    var imagePickerController : UIImagePickerController!
    var userImage: UIImage?
    var userName = ""
    var isEditingName : Bool = false
    let settings = [["App Lock","Notifications"],["General Settings"],["Change Photo","Mobile","Name","Password"]]
    let settingIcon = [["app_lock","notification_setting"],[""],["change_photo","mobile_number","name_setting","password"]]

    let settingsHint = [["Enable/Disable login with touch id","Enable/Disable notifications"],["Settings related to profile"],["Change Photo","Mobile","Name","Password"]]
    let placeHolders = ["Change Photo","Mobile Number","Full Name","Password"]
    var user : User!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.user = User.loadSavedUser()
        self.user.isNotificationEnabled = AppSettings.shared.isNotificationEnable
        self.imagePickerController = UIImagePickerController()
        self.imagePickerController.delegate = self
        
        self.tableView.register(UINib(nibName: "ProfileImageCell", bundle: nil), forCellReuseIdentifier: "ProfileImageCell")
        self.tableView.register(UINib(nibName: "SettingSwitchCell", bundle: nil), forCellReuseIdentifier: "SettingSwitchCell")

        self.tableView.register(UINib(nibName: "SettingHeaderCell", bundle: nil), forCellReuseIdentifier: "SettingHeaderCell")
        self.tableView.register(UINib(nibName: "SettingTextFieldCell", bundle: nil), forCellReuseIdentifier: "SettingTextFieldCell")
        self.tableView.tableFooterView = UIView(frame: .zero)
        self.tableView.reloadData()
    }

    override func viewDidLayoutSubviews() {
        self.logoutButton.applyGradient(withColours: [appColor.gradientStart,appColor.gradientEnd], gradientOrientation: .horizontal, locations: [0.0,1.0])
    }

    @IBAction func chageSwitch(_ sender : UISwitch){
        if let indexPath = sender.tableViewIndexPath(self.tableView){
            if indexPath.row == 0{
                self.toggleTouchId(value: sender.isOn)
            }else{
                if !AppSettings.isConnectedToNetwork{
                    NKToastHelper.sharedInstance.showErrorAlert(self, message: warningMessage.networkIsNotConnected.rawValue)
                    return
                }
                self.updateNotificationSettings(value: sender.isOn)
            }
        }

    }

    func toggleTouchId(value : Bool){
        if value{
            if !AppSettings.shared.isLoginWithTouchIDEnable{
                self.askForBiomatry()
            }
        }else{
            AppSettings.shared.isLoginWithTouchIDEnable = !AppSettings.shared.isLoginWithTouchIDEnable
            AppSettings.shared.interestedInLoginWithTouchID = !AppSettings.shared.isLoginWithTouchIDEnable
        }
        self.tableView.reloadData()
    }
    
    


    @IBAction func onClickEditButton(_ sender : UISwitch){
        if let indexPath = sender.tableViewIndexPath(self.tableView){
            guard let cell = self.tableView.cellForRow(at: indexPath) as? SettingTextFieldCell else{
                return
            }
            cell.textField.keyboardType = .asciiCapable
            self.isEditingName = true
            cell.editButton.isHidden = self.isEditingName
            cell.doneButton.isHidden = !self.isEditingName
            cell.textField.isUserInteractionEnabled = true
            cell.textField.becomeFirstResponder()
        }
    }

    @IBAction func onClickSetUpPasswordButton(_ sender : UISwitch){
        //
        let setupPasswordVC = AppStoryboard.Settings.viewController(SetupPasswordViewController.self)
        self.navigationController?.pushViewController(setupPasswordVC, animated: true)
    }


    @IBAction func onClickDoneButton(_ sender : UISwitch){
        if let indexPath = sender.tableViewIndexPath(self.tableView){
            guard let cell = self.tableView.cellForRow(at: indexPath) as? SettingTextFieldCell else{
                return
            }

            self.isEditingName = false
            cell.editButton.isHidden = self.isEditingName
            cell.doneButton.isHidden = !self.isEditingName
            cell.textField.isUserInteractionEnabled = false
            self.view.endEditing(true)
            if self.userName == self.user.name{return}
            if self.userName.count < 3{
                NKToastHelper.sharedInstance.showErrorAlert(self, message: "Please enter a valid name")
            }
            AppSettings.shared.showLoader(withStatus: "Updating..")
            self.user.setUserProfileOnServer(newName: userName, image: self.userImage, notification: AppSettings.shared.isNotificationEnable) { (success, resUser, message) in
                AppSettings.shared.hideLoader()
                if success{
                    if let _user = resUser{
                        self.user = _user
                        self.userName = ""
                        self.userImage = nil
                    }else{
                        NKToastHelper.sharedInstance.showErrorAlert(self, message: message)
                    }
                }else{
                    NKToastHelper.sharedInstance.showErrorAlert(self, message: message)
                }
               self.tableView.reloadData()
            }
        }
    }

    @IBAction func onClickMenu(_ sender:UIBarButtonItem){
        sideMenuController?.toggleLeftView(animated: true, delay: 0.0, completionHandler: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source



    func numberOfSections(in tableView: UITableView) -> Int {
        return self.settings.count
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.settings[section].count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "SettingSwitchCell", for: indexPath) as! SettingSwitchCell
            let icons = settingIcon[indexPath.section]
            let hints = settingsHint[indexPath.section]
            let setting = settings[indexPath.section]
            cell.settingIcon.image = UIImage(named: icons[indexPath.row])
            cell.settingHintLabel.text = hints[indexPath.row]
            cell.settingNameLabel.text = setting[indexPath.row]

            cell.settingSwitch.addTarget(self, action: #selector(chageSwitch(_:)), for: .valueChanged)
            switch indexPath.row{
            case 0:
                cell.settingSwitch.isOn = AppSettings.shared.isLoginWithTouchIDEnable
            default:
                cell.settingSwitch.isOn = AppSettings.shared.isNotificationEnable
            }
            return cell
        }else if indexPath.section == 1{
            let cell = tableView.dequeueReusableCell(withIdentifier: "SettingHeaderCell", for: indexPath) as! SettingHeaderCell
            return cell
        }else{
            let icons = settingIcon[indexPath.section]
            let hints = settings[indexPath.section]
            if indexPath.row == 0{
                let cell = tableView.dequeueReusableCell(withIdentifier: "ProfileImageCell", for: indexPath) as! ProfileImageCell
                cell.doneButton.setImage(#imageLiteral(resourceName: "done"), for: UIControlState())
                cell.editButton.setImage(#imageLiteral(resourceName: "edit_setting_button"), for: UIControlState())
                cell.editButton.addTarget(self, action: #selector(onClickEditPhoto(_:)), for:.touchUpInside)
                cell.doneButton.addTarget(self, action: #selector(onClickDonePhoto(_:)), for:.touchUpInside)
                if let image = userImage{
                    cell.profileImageView.image = image
                    cell.editButton.isHidden = true
                    cell.doneButton.isHidden = false
                }else{
                    cell.editButton.isHidden = false
                    cell.doneButton.isHidden = true
                    cell.profileImageView.setIndicatorStyle(.white)
                    cell.profileImageView.setShowActivityIndicator(true)
                    cell.profileImageView.sd_setImage(with: URL(string:self.user.profileImage) ?? URL(string:BASE_URL), placeholderImage:AppSettings.shared.userAvatarImage(username:self.user.name))
                }
                return cell
            }else{
                let cell = tableView.dequeueReusableCell(withIdentifier: "SettingTextFieldCell", for: indexPath) as! SettingTextFieldCell

                cell.settingIcon.image = UIImage(named: icons[indexPath.row])
                cell.settingHintLabel.text = hints[indexPath.row]


                cell.textField.placeholder = placeHolders[indexPath.row]
                switch indexPath.row{
                case 1:
                    cell.textField.isUserInteractionEnabled = false
                    cell.editButton.isHidden = true
                    cell.doneButton.isHidden = true
                    cell.editButton.setImage(nil, for: UIControlState())
                    let cc = "+"+self.user.countryCode
                    cell.textField.text = cc+" "+self.user.contact
                case 2:
                    cell.textField.isUserInteractionEnabled = self.isEditingName
                    cell.textField.delegate = self

                    cell.editButton.isHidden = self.isEditingName
                    cell.doneButton.isHidden = !self.isEditingName
                    cell.editButton.setImage(#imageLiteral(resourceName: "edit_setting_button"), for: UIControlState())
                    cell.doneButton.setImage(#imageLiteral(resourceName: "done"), for: UIControlState())
                    cell.textField.text = self.userName.isEmpty ? self.user.name.capitalized : self.userName
                    cell.editButton.addTarget(self, action: #selector(onClickEditButton(_:)), for:.touchUpInside)
                    cell.doneButton.addTarget(self, action: #selector(onClickDoneButton(_:)), for:.touchUpInside)

                default:
                    cell.textField.isUserInteractionEnabled = false
                    cell.editButton.setImage(#imageLiteral(resourceName: "Set Up"), for: UIControlState())
                    cell.editButton.addTarget(self, action: #selector(onClickSetUpPasswordButton(_:)), for:.touchUpInside)
                    cell.editButton.isHidden = false
                    cell.doneButton.isHidden = true
                    cell.textField.text = "**********"
                }
                return cell
            }
        }
    }


    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {

        if indexPath.section == (self.settings.count-1) && indexPath.row == 0{
            return 110
        }else{
            return 70
        }
    }

}


extension SettingsViewController{
    @IBAction func onClickOfLogout(_ sender: UIButton){
        self.askForLogout()
    }

    func updateNotificationSettings(value: Bool){
        self.user.setUserProfileOnServer(newName: nil, image: nil, notification:value) { (success, resUser, message) in
            if success{
                if let _user = resUser{
                    self.user = _user
                    AppSettings.shared.isNotificationEnable = _user.isNotificationEnabled
                }else{
                    NKToastHelper.sharedInstance.showErrorAlert(self, message: message)
                    self.tableView.reloadData()
                }
            }else{
                NKToastHelper.sharedInstance.showErrorAlert(self, message: message)
                self.tableView.reloadData()
            }
            self.tableView.reloadData()
        }
    }

    @IBAction func onClickEditPhoto(_ sender: UIButton){
        self.showAlertToChooseAttachmentOption(sender)
    }

    @IBAction func onClickDonePhoto(_ sender: UIButton){
        if let indexPath = sender.tableViewIndexPath(self.tableView){
            guard let cell = self.tableView.cellForRow(at: indexPath) as? ProfileImageCell else{
                return
            }

        AppSettings.shared.showLoader(withStatus: "Updating..")

        self.user.setUserProfileOnServer(newName: userName, image: self.userImage, notification: AppSettings.shared.isNotificationEnable) { (success, resUser, message) in
            AppSettings.shared.hideLoader()
            if success{
                if let _user = resUser{
                    self.user = _user
                    self.userName = ""
                    self.userImage = nil
                    self.isEditingName = false
                    cell.editButton.isHidden = self.isEditingName
                    cell.doneButton.isHidden = !self.isEditingName
                }else{
                    NKToastHelper.sharedInstance.showErrorAlert(self, message: message)
                }
            }else{
                NKToastHelper.sharedInstance.showErrorAlert(self, message: message)
            }
            self.tableView.reloadData()
        }
    }
    }

    func askForLogout() {

        let alert = UIAlertController(title: warningMessage.title.rawValue, message: "Do you really want to logout?", preferredStyle: .alert)
        let okayAction = UIAlertAction(title: "Yes", style: .default){(action) in
            self.logout()
            alert.dismiss(animated: true, completion: nil)
        }
        let cancelAction = UIAlertAction(title: "Nope", style: .cancel){(action) in
            alert.dismiss(animated: true, completion: nil)
        }

        alert.addAction(okayAction)
        alert.addAction(cancelAction)
        let appDelegate: AppDelegate = (UIApplication.shared.delegate as! AppDelegate)
        appDelegate.window?.rootViewController?.present(alert, animated: true, completion: nil)
    }


    func logout(){
        self.user.logOut { (success, resUser, message) in
            AppSettings.shared.resetOnLogout()
            AppSettings.shared.proceedToLandingPage()
        }
    }
    
    func askForBiomatry(){
        // start authentication
        BioMetricAuthenticator.authenticateWithBioMetrics(reason: "", success: {
            self.setTouchIDEnabled(isEnabled: true)
        }, failure: { [weak self] (error) in
            // do nothing on canceled
            if error == .canceledByUser || error == .canceledBySystem {
                self?.setTouchIDEnabled(isEnabled: false)

                return
            }
                // show alternatives on fallback button clicked
            else if error == .fallback {
                self?.setTouchIDEnabled(isEnabled: false)

                // here we're entering username and password
            }
                // No biometry enrolled in this device, ask user to register fingerprint or face
            else if error == .biometryNotEnrolled {
                self?.setTouchIDEnabled(isEnabled: false)
                self?.showGotoSettingsAlert(message: error.message())
            }
                // Biometry is locked out now, because there were too many failed attempts.
                // Need to enter device passcode to unlock.
            else if error == .biometryLockedout {
                self?.showPasscodeAuthentication(message: error.message())
            }
                // show error on authentication failed
            else {
                self?.setTouchIDEnabled(isEnabled: false)
                self?.showErrorAlert(message: error.message())
            }
        })
    }

    func setTouchIDEnabled(isEnabled: Bool){
        AppSettings.shared.isLoginWithTouchIDEnable = isEnabled
        AppSettings.shared.interestedInLoginWithTouchID = isEnabled
        self.tableView.reloadData()

    }

    // show passcode authentication
    func showPasscodeAuthentication(message: String) {
        BioMetricAuthenticator.authenticateWithPasscode(reason: message, success: {
            self.setTouchIDEnabled(isEnabled: true)
        }) { (error) in
            self.setTouchIDEnabled(isEnabled: false)
        }
    }



    func showAlert(title: String, message: String) {

        let okAction = AlertAction(title: OKTitle)
        let alertController = getAlertViewController(type: .alert, with: title, message: message, actions: [okAction], showCancel: false) { (button) in
        }
        present(alertController, animated: true, completion: nil)
    }

    func showLoginSucessAlert() {
        showAlert(title: "Success", message: "Login successful")
    }

    func showErrorAlert(message: String) {
        showAlert(title: "Error", message: message)
    }

    func showGotoSettingsAlert(message: String) {
        let settingsAction = AlertAction(title: "Settings")

        let alertController = getAlertViewController(type: .alert, with: "Error", message: message, actions: [settingsAction], showCancel: true, actionHandler: { (buttonText) in
            if buttonText == CancelTitle { return }

            // open settings
            guard let settingsUrl = URL(string: UIApplicationOpenSettingsURLString) else {
                return
            }
            if UIApplication.shared.canOpenURL(settingsUrl) {
                UIApplication.shared.open(settingsUrl, options: [:], completionHandler: nil)
            }
        })
        present(alertController, animated: true, completion: nil)
    }
}




extension SettingsViewController:UITextFieldDelegate{
    func textFieldDidBeginEditing(_ textField: UITextField) {
        guard let uname = textField.text else {
            return
        }
        self.userName = uname
    }

    func textFieldDidEndEditing(_ textField: UITextField) {
        guard let uname = textField.text else {
            return
        }
        self.userName = uname
    }

    @IBAction func textFieldDidChangeEditting(_ textField: UITextField){
            guard let uname = textField.text else {
                return
            }
            self.userName = uname
    }
}



extension SettingsViewController : UIImagePickerControllerDelegate,UINavigationControllerDelegate{
    func showAlertToChooseAttachmentOption(_ sender: UIButton){
        let actionSheet = UIAlertController(title: "Profile Image", message:"Select an image to make that profile image", preferredStyle:UIDevice.isRunningOnIpad ? .alert : .actionSheet)
        let cancelAction: UIAlertAction = UIAlertAction(title: "Cancel", style: .cancel) { action -> Void in
            actionSheet.dismiss(animated: true, completion: nil)
        }

        actionSheet.addAction(cancelAction)
        let openGalleryAction: UIAlertAction = UIAlertAction(title: "Choose from Gallery", style: .default)
        { action -> Void in
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.photoLibrary){
                self.imagePickerController.sourceType = UIImagePickerControllerSourceType.photoLibrary;
                self.imagePickerController.allowsEditing = true
                self.imagePickerController.modalPresentationStyle = UIModalPresentationStyle.currentContext
                self.present(self.imagePickerController, animated: true, completion: nil)
            }
        }
        actionSheet.addAction(openGalleryAction)

        let openCameraAction: UIAlertAction = UIAlertAction(title: "Camera", style: .default)
        { action -> Void in
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera){
                self.imagePickerController.sourceType = UIImagePickerControllerSourceType.camera;
                self.imagePickerController.allowsEditing = true
                self.imagePickerController.modalPresentationStyle = UIModalPresentationStyle.currentContext
                self.present(self.imagePickerController, animated: true, completion: nil)
            }
        }


        actionSheet.addAction(openCameraAction)
        self.present(actionSheet, animated: true, completion: nil)
    }

    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }


    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String :Any]){

        if let tempImage = info[UIImagePickerControllerEditedImage] as? UIImage{
            self.userImage = tempImage
        }else if let tempImage = info[UIImagePickerControllerOriginalImage] as? UIImage{
            self.userImage = tempImage
        }
        self.tableView.reloadData()
        picker.dismiss(animated: true,completion: nil)
    }
}









