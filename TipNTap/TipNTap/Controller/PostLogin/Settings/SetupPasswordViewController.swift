//
//  SetupPasswordViewController.swift
//  TipNTap
//
//  Created by TecOrb on 30/04/18.
//  Copyright © 2018 Nakul Sharma. All rights reserved.
//

import UIKit


class SetupPasswordViewController: UIViewController {
    @IBOutlet weak var oldPasswordTextField: UITextField!
    @IBOutlet weak var newPasswordTextField: UITextField!
    @IBOutlet weak var confirmedPasswordTextField: UITextField!
    @IBOutlet weak var confirmedPasswordSeparator: UIView!
    @IBOutlet weak var doneButton: UIButton!
    var user : User!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.user = User.loadSavedUser()
    }

    override func viewDidLayoutSubviews() {
        self.doneButton.applyGradient(withColours: [appColor.gradientStart,appColor.gradientEnd], gradientOrientation: .horizontal, locations: [0.0,1.0])
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }

    @IBAction func onClickDoneButton(_ sender: UIButton){
        //hit service to reset password using token
        guard let oldPassword = oldPasswordTextField.text else {
            NKToastHelper.sharedInstance.showErrorAlert(self, message: warningMessage.enterOldPassword.rawValue)
            return
        }

        if oldPassword.count == 0{
            NKToastHelper.sharedInstance.showErrorAlert(self, message: warningMessage.enterOldPassword.rawValue)
            return
        }
        if oldPassword.count < 6{
            NKToastHelper.sharedInstance.showErrorAlert(self, message: warningMessage.validPassword.rawValue)
            return
        }

        guard let newPassword = newPasswordTextField.text else {
            NKToastHelper.sharedInstance.showErrorAlert(self, message: warningMessage.enterNewPassword.rawValue)
            return
        }
        if newPassword.count == 0{
            NKToastHelper.sharedInstance.showErrorAlert(self, message: warningMessage.enterNewPassword.rawValue)
            return
        }
        if newPassword.count < 6{
            NKToastHelper.sharedInstance.showErrorAlert(self, message: warningMessage.validPassword.rawValue)
            return
        }

        guard let confirmPassword = confirmedPasswordTextField.text else {
            NKToastHelper.sharedInstance.showErrorAlert(self, message: warningMessage.confirmPassword.rawValue)
            return
        }

        if confirmPassword.count == 0{
            NKToastHelper.sharedInstance.showErrorAlert(self, message: warningMessage.confirmPassword.rawValue)
            return
        }

        if confirmPassword != newPassword{
            NKToastHelper.sharedInstance.showErrorAlert(self, message: warningMessage.passwordDidNotMatch.rawValue)
            return
        }

        self.updatePassword(oldPassword: oldPassword, newPassword: newPassword)
    }

    func updatePassword(oldPassword:String,newPassword:String) {
        LoginService.sharedInstance.setupPassword(oldPassword, withNewPassword: newPassword) {[newPassword](success, message) in
            if success{
                AppSettings.shared.passwordEncrypted = newPassword
                self.showSuccessAlert(message: message, title: warningMessage.title.rawValue)
            }else{
                NKToastHelper.sharedInstance.showErrorAlert(self, message: message)
            }
        }
    }


    func showSuccessAlert(message:String,title:String){
        self.confirmedPasswordTextField.text = ""
        self.newPasswordTextField.text = ""
        self.oldPasswordTextField.text = ""
        
        let alert = UIAlertController(title: title, message: warningMessage.setUpPassword.rawValue, preferredStyle: UIAlertControllerStyle.alert)
        let okayAction = UIAlertAction(title: "Okay", style: .default) { (action) in
            self.navigationController?.pop(true)
        }
        alert.addAction(okayAction)
        self.present(alert, animated: true, completion: nil)
    }


    @IBAction func onClickBackButton(_ sender: UIBarButtonItem){
        self.navigationController?.pop(true)
    }


    @IBAction func textFieldDidChangeText(_ sender: UITextField){
        if sender == self.newPasswordTextField || sender == self.confirmedPasswordTextField{
            if let cnfrmPassword = self.confirmedPasswordTextField.text{
                if let newpswrd = self.newPasswordTextField.text{
                    // self.doneButton.isEnabled = (newpswrd == cnfrmPassword)
                    self.confirmedPasswordSeparator.backgroundColor = (newpswrd == cnfrmPassword) ? UIColor.lightGray : appColor.red
                }
            }
        }
    }

}
