//
//  WriteToUsViewController.swift
//  GPDock
//
//  Created by TecOrb on 10/11/17.
//  Copyright © 2017 Nakul Sharma. All rights reserved.
//

import UIKit
import RSKPlaceholderTextView

class WriteToUsViewController: UITableViewController {
    var user : User!
    @IBOutlet var messageTextView : RSKPlaceholderTextView!
    @IBOutlet weak var scnShotbutton : UIButton!
//    @IBOutlet weak var userEmailLabel : UILabel!
    @IBOutlet weak var phoneNumberLabel : UILabel!
    @IBOutlet weak var removeScreenShotbutton : UIButton!
    @IBOutlet weak var submitButton : UIButton!

    var screenShot : UIImage?
    var imagePickerController : UIImagePickerController!
    var titleView : NavigationTitleView!
    func setupNavigationTitle() {
        titleView = NavigationTitleView.instanceFromNib()
        titleView.frame = CGRect(x: 45, y: 0, width: self.view.frame.size.width-90, height: 44)
        titleView.titleLabel.text = "Write to Us"
        self.navigationItem.titleView = self.titleView
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        //self.setupNavigationTitle()
        self.user = User.loadSavedUser()
        self.clearsSelectionOnViewWillAppear = true
//        self.userEmailLabel.text = self.user.email
        self.phoneNumberLabel.text = self.user.contact
        self.imagePickerController = UIImagePickerController()
        self.imagePickerController.delegate = self
//        if let myBooking = self.booking{
//            self.messageTextView.text = "Hi, I am facing issue with booking Id: \(myBooking.ID)"
//        }
    }

    override func viewWillLayoutSubviews() {
        CommonClass.makeViewCircular(self.scnShotbutton, borderColor: UIColor.clear, borderWidth: 0)
        CommonClass.makeViewCircular(self.removeScreenShotbutton, borderColor: UIColor.clear, borderWidth: 0)
        self.submitButton.applyGradient(withColours: [appColor.gradientStart,appColor.gradientEnd], gradientOrientation: .horizontal, locations: [0.0,1.0])
    }

    func refreshScreenShotButton() {
        if let img = self.screenShot{
            self.scnShotbutton.setImage(img, for: UIControlState())
            self.removeScreenShotbutton.isHidden = false
        }else{
            self.scnShotbutton.setImage(#imageLiteral(resourceName: "screenShot"), for: UIControlState())
            self.removeScreenShotbutton.isHidden = true
        }
    }

    @IBAction func onClickBackButton(_ sender: UIBarButtonItem) {
        self.navigationController?.pop(true)
    }

    @IBAction func onClickSubmit(_ sender: UIButton){
        if !AppSettings.isConnectedToNetwork{
            NKToastHelper.sharedInstance.showErrorAlert(self, message: warningMessage.networkIsNotConnected.rawValue)
            return
        }
        if self.messageTextView.text.trimmingCharacters(in: .whitespacesAndNewlines) == ""{
            NKToastHelper.sharedInstance.showAlert(self, title: warningMessage.title, message:warningMessage.issueFacing.rawValue)

            return
        }
        if self.messageTextView.text.isEmpty{
            NKToastHelper.sharedInstance.showAlert(self, title: warningMessage.title, message:warningMessage.issueFacing.rawValue)

            return
        }

        self.sendEmailToSupport(message: self.messageTextView.text, screenShot: self.screenShot)
    }

    @IBAction func onClickRemoveScreenShot(_ sender: UIButton){
        self.screenShot = nil
        self.refreshScreenShotButton()
    }
    @IBAction func onClickAddScreenShot(_ sender: UIButton){
        self.showAlertToChooseAttachmentOption()
    }

    func sendEmailToSupport(message: String, screenShot: UIImage?){
        AppSettings.shared.showLoader(withStatus: "Sending..")
        SupportService.sharedInstance.sendEmailToSupport(message: message, image: screenShot) {(success, resSupport,message) in
            AppSettings.shared.hideLoader()
            if success{
                if resSupport != nil{
                    self.showSuccessAlertAndOut("Your message has been received and responded to as soon as possible", title: warningMessage.title.rawValue)

                }else{
                    NKToastHelper.sharedInstance.showAlert(self, title: warningMessage.title, message:message)
                }
            }else{
                NKToastHelper.sharedInstance.showAlert(self, title: warningMessage.title, message:message)
            }
        }
    }

    func showSuccessAlertAndOut(_ message:String,title:String){
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        let okayAction = UIAlertAction(title: "Okay", style: .cancel) { (action) in
            //alert.dismiss(animated: true, completion: nil)
            self.navigationController?.popToRoot(true)
        }
//        let subview = (alert.view.subviews.first?.subviews.first?.subviews.first!)! as UIView
//        subview.backgroundColor = UIColor(red: 74.0/255.0, green: 90.0/255.0, blue: 95.0/255.0, alpha: 0.5)
//        alert.view.tintColor = UIColor.white
        alert.addAction(okayAction)
        self.present(alert, animated: true, completion: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

extension WriteToUsViewController : UIImagePickerControllerDelegate,UINavigationControllerDelegate{
    func showAlertToChooseAttachmentOption(){
        let actionSheet = UIAlertController(title: nil, message:nil, preferredStyle: .actionSheet)
        let cancelAction: UIAlertAction = UIAlertAction(title: "Cancel", style: .cancel) { action -> Void in
            actionSheet.dismiss(animated: true, completion: nil)
        }
        actionSheet.addAction(cancelAction)
        let openGalleryAction: UIAlertAction = UIAlertAction(title: "Choose from Gallery", style: .default)
        { action -> Void in
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.photoLibrary){
                self.imagePickerController.sourceType = UIImagePickerControllerSourceType.photoLibrary;
                self.imagePickerController.allowsEditing = true
                self.imagePickerController.modalPresentationStyle = UIModalPresentationStyle.currentContext
                self.present(self.imagePickerController, animated: true, completion: nil)
            }
        }
        actionSheet.addAction(openGalleryAction)

        let openCameraAction: UIAlertAction = UIAlertAction(title: "Camera", style: .default)
        { action -> Void in
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera){
                self.imagePickerController.sourceType = UIImagePickerControllerSourceType.camera;
                self.imagePickerController.allowsEditing = true
                self.imagePickerController.modalPresentationStyle = UIModalPresentationStyle.currentContext
                self.present(self.imagePickerController, animated: true, completion: nil)
            }
        }
        actionSheet.addAction(openCameraAction)
        self.present(actionSheet, animated: true, completion: nil)
    }

    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.refreshScreenShotButton()
        picker.dismiss(animated: true, completion: nil)
    }


    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String :Any]){

        if let tempImage = info[UIImagePickerControllerEditedImage] as? UIImage{
            self.screenShot = tempImage
            self.refreshScreenShotButton()
        }else if let tempImage = info[UIImagePickerControllerOriginalImage] as? UIImage{
            self.screenShot = tempImage
            self.refreshScreenShotButton()
        }
        picker.dismiss(animated: true, completion: nil)
    }


}
