//
//  SettingsNavigationController.swift
//  TipNTap
//
//  Created by TecOrb on 30/03/18.
//  Copyright © 2018 Nakul Sharma. All rights reserved.
//

import UIKit

class SettingsNavigationController: UINavigationController {
    var statusBarView : UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationBar.isOpaque = false
        navigationBar.isTranslucent = true
        navigationBar.barTintColor = .white
        navigationBar.titleTextAttributes = [NSAttributedStringKey.font: fonts.Raleway.bold.font(.xXLarge)]
    }

    override func viewWillAppear(_ animated: Bool) {
        let bounds = self.navigationBar.bounds
        self.navigationBar.setBackgroundImage(UIImage(color: .white,size:bounds.size), for: .default)
        self.navigationBar.shadowImage = UIImage()//color: .white,size:CGSize(width: bounds.size.width, height: 5))
        self.navigationBar.isTranslucent = true
        //make it cover the status bar by offseting y by -20
        //        bounds.offsetBy(dx: 0.0, dy: -20.0)
        //        bounds.size.height = bounds.height + 20.0
        let visualEffectView = UIView(frame: bounds)
        visualEffectView.backgroundColor = .white
        visualEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        //navigationBar.insertSubview(visualEffectView, at: 0)
//        let statusBar: UIView = UIApplication.shared.value(forKey: "statusBar") as! UIView
//        self.statusBarView = statusBar
//        if statusBar.responds(to: #selector(setter: UIView.backgroundColor)){
//            statusBar.backgroundColor = UIColor.white
//        }
    }

    override var shouldAutorotate : Bool {
        return false
    }

    override var prefersStatusBarHidden : Bool {
        return false
    }

    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .default
    }

    override var preferredStatusBarUpdateAnimation : UIStatusBarAnimation {
        return .none
    }

}

class PrivacyPolicyNavigationController: UINavigationController {
    var statusBarView : UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationBar.isOpaque = false
        navigationBar.isTranslucent = true
        navigationBar.barTintColor = .white
        navigationBar.titleTextAttributes = [NSAttributedStringKey.font: fonts.Raleway.bold.font(.xXLarge)]
    }

    override func viewWillAppear(_ animated: Bool) {
        let bounds = self.navigationBar.bounds
        self.navigationBar.setBackgroundImage(UIImage(color: .white,size:bounds.size), for: .default)
        self.navigationBar.shadowImage = UIImage()//color: .white,size:CGSize(width: bounds.size.width, height: 5))
        self.navigationBar.isTranslucent = true
        //make it cover the status bar by offseting y by -20
        //        bounds.offsetBy(dx: 0.0, dy: -20.0)
        //        bounds.size.height = bounds.height + 20.0
        let visualEffectView = UIView(frame: bounds)
        visualEffectView.backgroundColor = .white
        visualEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        //navigationBar.insertSubview(visualEffectView, at: 0)
//        let statusBar: UIView = UIApplication.shared.value(forKey: "statusBar") as! UIView
//        self.statusBarView = statusBar
//        if statusBar.responds(to: #selector(setter: UIView.backgroundColor)){
//            statusBar.backgroundColor = UIColor.white
//        }
    }

    override var shouldAutorotate : Bool {
        return false
    }

    override var prefersStatusBarHidden : Bool {
        return false
    }

    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .default
    }

    override var preferredStatusBarUpdateAnimation : UIStatusBarAnimation {
        return .none
    }

}



class SupportNavigationController: UINavigationController {
    var statusBarView : UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationBar.isOpaque = false
        navigationBar.isTranslucent = true
        navigationBar.barTintColor = .white
        navigationBar.titleTextAttributes = [NSAttributedStringKey.font: fonts.Raleway.bold.font(.xXLarge)]
    }
    
    override func viewWillAppear(_ animated: Bool) {
        let bounds = self.navigationBar.bounds
        self.navigationBar.setBackgroundImage(UIImage(color: .white,size:bounds.size), for: .default)
        self.navigationBar.shadowImage = UIImage()//color: .white,size:CGSize(width: bounds.size.width, height: 5))
        self.navigationBar.isTranslucent = true
        //make it cover the status bar by offseting y by -20
        //        bounds.offsetBy(dx: 0.0, dy: -20.0)
        //        bounds.size.height = bounds.height + 20.0
        let visualEffectView = UIView(frame: bounds)
        visualEffectView.backgroundColor = .white
        visualEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
       // navigationBar.insertSubview(visualEffectView, at: 0)
//        let statusBar: UIView = UIApplication.shared.value(forKey: "statusBar") as! UIView
//        self.statusBarView = statusBar
//        if statusBar.responds(to: #selector(setter: UIView.backgroundColor)){
//            statusBar.backgroundColor = UIColor.white
//        }
    }
    
    override var shouldAutorotate : Bool {
        return false
    }
    
    override var prefersStatusBarHidden : Bool {
        return false
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .default
    }
    
    override var preferredStatusBarUpdateAnimation : UIStatusBarAnimation {
        return .none
    }
    
}










