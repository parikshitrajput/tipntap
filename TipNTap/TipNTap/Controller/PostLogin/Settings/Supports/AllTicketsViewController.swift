//
//  AllTicketsViewController.swift
//  GPDock
//
//  Created by TecOrb on 08/05/18.
//  Copyright © 2018 Nakul Sharma. All rights reserved.
//

import UIKit

class AllTicketsViewController: UIViewController {
    @IBOutlet var ticketsTableView : UITableView!
    var user : User!
    var isNewDataLoading = true
    var tickets = Array<SupportQuery>()
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(AllTicketsViewController.handleRefresh(_:)), for: UIControlEvents.valueChanged)
        return refreshControl
    }()

    var titleView : NavigationTitleView!
    func setupNavigationTitle() {
        titleView = NavigationTitleView.instanceFromNib()
        titleView.frame = CGRect(x: 60, y: 0, width: self.view.frame.size.width-120, height: 44)
        titleView.titleLabel.text = "My Tickets"
        //titleView.titleButton.setTitle("My Tickets", for: .normal)
        self.navigationItem.titleView = self.titleView
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        //self.setupNavigationTitle()
        self.user = User.loadSavedUser()
        self.ticketsTableView.addSubview(self.refreshControl)

        self.ticketsTableView.register(UINib(nibName: "TicketListCell", bundle: nil), forCellReuseIdentifier: "TicketListCell")

        self.ticketsTableView.dataSource = self
        self.ticketsTableView.delegate = self
        self.getAllTickets()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }

    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        self.getAllTickets()

    }

    @IBAction func onClickBackButton(_ sender: UIBarButtonItem){
        self.navigationController?.pop(true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


    func getAllTickets(){
        self.isNewDataLoading = true
        self.ticketsTableView.reloadData()
        SupportService.sharedInstance.getAllQueriesForUser(self.user.ID) { (success, resTickets, message) in
            self.tickets.removeAll()
            self.isNewDataLoading = false
            if self.refreshControl.isRefreshing{
                self.refreshControl.endRefreshing()
            }

            if success{
                if let someTickets = resTickets{
                    self.tickets.append(contentsOf: someTickets)
                }else{
                    NKToastHelper.sharedInstance.showAlert(self, title: warningMessage.title, message: message)
                }
            }else{
                NKToastHelper.sharedInstance.showAlert(self, title: warningMessage.title, message: message)
            }
            self.ticketsTableView.reloadData()
        }
    }

}

extension AllTicketsViewController: UITableViewDataSource,UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (tickets.count > 0) ? tickets.count : 1 ;
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if self.tickets.count == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "NoDataCell", for: indexPath) as! NoDataCell
            cell.messageLabel.text = self.isNewDataLoading ? "Loading.." : "No Query found\r\nPlease pull down to refresh"
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "TicketListCell", for: indexPath) as! TicketListCell
            let ticket = self.tickets[indexPath.row]
            cell.ticketImageView.setShowActivityIndicator(true)
            cell.ticketImageView.setIndicatorStyle(.gray)
            cell.ticketImageView.sd_setImage(with: URL(string:ticket.image),placeholderImage: #imageLiteral(resourceName: "signupLogo"))
            cell.statusLabel.text = ticket.status ? "Closed" : "Opened"
            cell.statusLabel.textColor = ticket.status ? appColor.red : appColor.blue
            cell.referenceNumberLabel.text = ticket.referenceNumber
            cell.queryTextLabel.text = ticket.message
            return cell
        }
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if self.tickets.count == 0{return}
        let ticket = self.tickets[indexPath.row]
        self.openSupportTicketDetails(ticket: ticket)
    }

    func openSupportTicketDetails(ticket: SupportQuery){
        let supportTicketDetailsVC = AppStoryboard.Settings.viewController(SupportTicketViewController.self)
        supportTicketDetailsVC.ticket = ticket
        supportTicketDetailsVC.delegate = self
        self.navigationController?.pushViewController(supportTicketDetailsVC, animated: true)
    }
}

extension AllTicketsViewController: SupportTicketViewControllerDelegate{
    func ticket(_ ticket: SupportQuery, didUpdateStatusTo status: Bool) {
        self.tickets.filter { (tckt) -> Bool in
            tckt.id == ticket.id
        }.first?.status = status
        self.ticketsTableView.reloadData()
    }


}


