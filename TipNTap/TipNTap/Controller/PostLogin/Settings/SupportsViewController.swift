//
//  SupportsViewController.swift
//  GPDock
//
//  Created by TecOrb on 07/11/17.
//  Copyright © 2017 Nakul Sharma. All rights reserved.
//

import UIKit

class SupportsViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {
    var faqs = Array<FAQModel>()
    var user : User!

    var fromMenu: Bool = true
    @IBOutlet weak var supportTableView: UITableView!
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.backgroundColor = UIColor.groupTableViewBackground
        refreshControl.addTarget(self, action: #selector(SupportsViewController.handleRefresh(_:)), for: UIControlEvents.valueChanged)
        return refreshControl
    }()


    var titleView : NavigationTitleView!

    func setupNavigationTitle() {
        titleView = NavigationTitleView.instanceFromNib()
        titleView.frame = CGRect(x: 45, y: 0, width: self.view.frame.size.width-90, height: 44)
        titleView.titleLabel.text = "Support"
        self.navigationItem.titleView = self.titleView
    }

    

    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = false
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
       // self.setupNavigationTitle()
        self.user = User.loadSavedUser()
        self.supportTableView.register(UINib(nibName: "SupportTableViewCell", bundle: nil), forCellReuseIdentifier: "SupportTableViewCell")
        self.navigationController?.navigationBar.isHidden = false
        self.supportTableView.tableFooterView = UIView(frame:CGRect.zero)
        self.supportTableView.addSubview(refreshControl)
        self.supportTableView.contentInset = UIEdgeInsets(top: 4, left: 0, bottom: 4, right: 0)
        self.supportTableView.backgroundView?.backgroundColor = UIColor.groupTableViewBackground
        self.setupLeftNavigationBarButton(fromMenu: self.fromMenu)
        if fromMenu{
            self.setupRightBarButtons()
        }
        self.getFAQsFromServer()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


    deinit {
        NotificationCenter.default.removeObserver(self)
    }

    func setupRightBarButtons() {
        let viewTicketButton = UIButton(frame: CGRect(x:0, y:0, width:35, height:35))
        viewTicketButton.autoresizingMask = [.flexibleWidth,.flexibleHeight]
        viewTicketButton.setImage(#imageLiteral(resourceName: "ticket"), for: .normal)
        viewTicketButton.addTarget(self, action: #selector(viewTicketListButton(_:)), for: .touchUpInside)
        let viewTicketBarButton = UIBarButtonItem(customView: viewTicketButton)
        self.navigationItem.setRightBarButtonItems(nil, animated: false)
        self.navigationItem.setRightBarButtonItems([viewTicketBarButton], animated: false)
    }

    @IBAction func viewTicketListButton(_ sender: UIButton){
        self.openTicketsListScreen()
    }

    func openTicketsListScreen() {

            let ticketsListVC = AppStoryboard.Settings.viewController(AllTicketsViewController.self)
            self.navigationController?.pushViewController(ticketsListVC, animated: true)
       
    }

    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        if !AppSettings.isConnectedToNetwork{
            NKToastHelper.sharedInstance.showErrorAlert(self, message: warningMessage.networkIsNotConnected.rawValue)
            return
        }


        SupportService.sharedInstance.getFAQsFromServer { (success, resFAQs, message) in
            self.refreshControl.endRefreshing()
            if success{
                if let someFAQs = resFAQs{
                    self.faqs.removeAll()
                    self.faqs.append(contentsOf: someFAQs)
                    self.supportTableView.reloadData()
                }
            }
        }
    }
    func getFAQsFromServer(){
        AppSettings.shared.showLoader(withStatus: "Loading..")

        SupportService.sharedInstance.getFAQsFromServer { (success, resFAQs, message) in
            AppSettings.shared.hideLoader()
            if success{
                if let someFAQs = resFAQs{
                    self.faqs.append(contentsOf: someFAQs)
                    self.supportTableView.reloadData()
                }
            }
        }
    }

    func setupLeftNavigationBarButton(fromMenu:Bool) {
        let leftBarButton = UIBarButtonItem(image: fromMenu ? #imageLiteral(resourceName: "menu_button") : #imageLiteral(resourceName: "close_icon"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(onClickMenuButton(_:)))
        let tintColor = UIColor(red: 51.0/255.0, green: 74.0/255.0, blue: 219.0/255.0, alpha: 1.0)
        leftBarButton.tintColor = tintColor
        self.navigationItem.setLeftBarButtonItems(nil, animated: false)
        self.navigationItem.setLeftBarButtonItems([leftBarButton], animated: false)
    }
    
    @IBAction func onClickMenuButton(_ sender: UIBarButtonItem) {
        if fromMenu{
            sideMenuController?.toggleLeftView(animated: true, delay: 0.0, completionHandler: nil)
        }else{
            self.dismiss(animated: true, completion: nil)
        }
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return faqs.count + 2
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0{
            return 160
        }else if indexPath.section == faqs.count+1 {
            return 127
        }else{
            return 70
        }
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if ((indexPath.section > 0) && (indexPath.section < faqs.count+1)){
            let faqVC = AppStoryboard.Settings.viewController(FAQViewController.self)
            let faqModel = faqs[indexPath.section-1]
            faqVC.faqModel = faqModel
            self.navigationController?.pushViewController(faqVC, animated: true)
        }else{return}
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "SupportUserNameTableViewCell", for: indexPath) as! SupportUserNameTableViewCell
            cell.userNameLabel.text = "Hi " + (AppSettings.shared.isLoggedIn ? self.user.name : "Guest")
            return cell
        }else if ((indexPath.section > 0) && (indexPath.section < faqs.count+1)){
            let cell = tableView.dequeueReusableCell(withIdentifier: "SupportTableViewCell", for: indexPath) as! SupportTableViewCell
            let faqModel = faqs[indexPath.section-1]
            cell.questionTextLabel.text = faqModel.category.capitalized
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "SupportNotListedTableViewCell", for: indexPath) as! SupportNotListedTableViewCell
            cell.reachUsButton.addTarget(self, action: #selector(SupportsViewController.onClickReachUs(_:)), for: .touchUpInside)
            return cell
        }
    }

    @IBAction func onClickReachUs(_ sender: UIButton){

            self.navigateToWriteToUs()

    }

    func navigateToWriteToUs() {
        let writeToUsVC = AppStoryboard.Settings.viewController(WriteToUsViewController.self)
        self.navigationController?.pushViewController(writeToUsVC, animated: true)
    }
}
