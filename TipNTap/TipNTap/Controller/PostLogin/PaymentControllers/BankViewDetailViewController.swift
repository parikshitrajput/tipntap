//
//  BankViewDetailViewController.swift
//  TipNTap
//
//  Created by Parikshit on 10/09/18.
//  Copyright © 2018 Nakul Sharma. All rights reserved.
//

import UIKit

class BankViewDetailViewController: UIViewController,UIWebViewDelegate {
    @IBOutlet weak var webView: UIWebView!
    var url: URL!

    override func viewDidLoad() {
        super.viewDidLoad()
        let requestObj = URLRequest(url: url!)
        webView.loadRequest(requestObj)
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        if AppSettings.shared.isLoaderOnScreen {
            AppSettings.shared.hideLoader()
        }
    }
    func webViewDidFinishLoad(_ webView: UIWebView) {
        
        if AppSettings.shared.isLoaderOnScreen {
            AppSettings.shared.hideLoader()
        }
    }
    func webViewDidStartLoad(_ webView: UIWebView) {
        if AppSettings.shared.isLoaderOnScreen {
            AppSettings.shared.showLoader(withStatus: "Loading..")
        }
    }

}
