//
//  AddNewPaymentOptionViewController.swift
//  GPDock
//
//  Created by TecOrb on 01/08/17.
//  Copyright © 2017 Nakul Sharma. All rights reserved.
//

import UIKit
import Stripe


class AddNewPaymentOptionViewController: UIViewController{
    @IBOutlet weak var cardNumberTextField : BKCardNumberField!
    @IBOutlet weak var cardHolderNameTextField : UITextField!
    @IBOutlet weak var ccvTextField : UITextField!
    @IBOutlet weak var expTextField : BKCardExpiryField!
    @IBOutlet weak var containnerView : UIView!
    @IBOutlet weak var payButton : UIButton!
    @IBOutlet weak var zipCodeTextField : UITextField!

    
//    @IBOutlet weak var payableAmountLabel : UILabel!
    @IBOutlet weak var saveCardButton : UIButton!
    var isPayingForMarina = false
    var paymentNote : String?
    var couponCode : String?

    var payableAmout : Double!
    var payeeUser : User!
    
    var docInfoUser : User!

    
//    var redirectContext: STPRedirectContext?
//    var threeDSParams :STPSourceParams?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        CardIOUtilities.preloadCardIO()
        self.setupAllView()
        self.navigateToScanCard()
    }
    func setupAllView(){
       // self.payableAmountLabel.text = "Enter Card Details"//"$ "+String(format: "%0.2lf", self.payableAmout)
        let totalAmount = self.payableAmout + (self.payableAmout*self.payeeUser.adminPercentage) + self.payeeUser.paymentServiceFee
        self.payButton.setTitle("Pay $ "+String(format: "%0.2lf", totalAmount), for: .normal)
        cardNumberTextField.showsCardLogo = true
        cardNumberTextField.cardNumberFormatter.groupSeparater = " "
        self.saveCardButton.isSelected = true
        CommonClass.makeViewCircularWithCornerRadius(self.containnerView, borderColor: UIColor.groupTableViewBackground, borderWidth: 0, cornerRadius: 1)
        CommonClass.makeViewCircularWithCornerRadius(self.cardNumberTextField, borderColor: UIColor.groupTableViewBackground, borderWidth: 0, cornerRadius: 1)
        CommonClass.makeViewCircularWithCornerRadius(self.cardHolderNameTextField, borderColor: UIColor.groupTableViewBackground, borderWidth: 0, cornerRadius: 1)
        self.cardHolderNameTextField.setLeftPaddingPoints(5)

        CommonClass.makeViewCircularWithCornerRadius(self.ccvTextField, borderColor: UIColor.groupTableViewBackground, borderWidth: 0, cornerRadius: 1)
        self.ccvTextField.setLeftPaddingPoints(5)
        self.ccvTextField.setRightPaddingPoints(5)

        CommonClass.makeViewCircularWithCornerRadius(self.expTextField, borderColor: UIColor.groupTableViewBackground, borderWidth: 0, cornerRadius: 1)
        self.expTextField.setLeftPaddingPoints(5)
        
        CommonClass.makeViewCircularWithCornerRadius(self.zipCodeTextField, borderColor: UIColor.groupTableViewBackground, borderWidth: 0, cornerRadius: 1)
        self.zipCodeTextField.setLeftPaddingPoints(5)

    }

    override func viewDidLayoutSubviews() {
        self.payButton.applyGradient(withColours: [appColor.gradientStart,appColor.gradientEnd], gradientOrientation: .horizontal, locations: [0.0,1.0])
    }
    @IBAction func onToggleSaveCardForFasterCheckOut(_ sender: UIButton){
        sender.isSelected = !sender.isSelected
    }

    @IBAction func onClickBackButton(_ sender: UIBarButtonItem){
        self.navigationController?.pop(true)
    }

    @IBAction func onClickPayButton(_ sender: UIButton){
        //validate card params
        self.payButton.isEnabled = false
        self.view.endEditing(true)
        guard let cardNumber = self.cardNumberTextField.cardNumber  else {
            self.payButton.isEnabled = true
            return
        }
        if cardNumber.count < 14{
            NKToastHelper.sharedInstance.showAlert(self, title: warningMessage.title, message: warningMessage.validCardNumber.rawValue)
            self.payButton.isEnabled = true

            return
        }
        if cardHolderNameTextField.text == nil || cardHolderNameTextField.text?.count == 0{
            NKToastHelper.sharedInstance.showAlert(self, title: warningMessage.title, message: warningMessage.cardHolderName.rawValue)
            self.payButton.isEnabled = true

            return
        }
        guard let cardHolderName = self.cardHolderNameTextField.text?.trimmingCharacters(in: .whitespaces) else{
            NKToastHelper.sharedInstance.showAlert(self, title: warningMessage.title, message: warningMessage.cardHolderName.rawValue)

            self.payButton.isEnabled = true

            return
        }

        guard let brand = self.cardNumberTextField.cardNumberFormatter.cardPatternInfo.shortName  else {
            self.payButton.isEnabled = true
            NKToastHelper.sharedInstance.showAlert(self, title: warningMessage.title, message: warningMessage.cardDeclined.rawValue)
            self.payButton.isEnabled = true

            return
        }

        if ccvTextField.text == nil || ccvTextField.text!.count < 3 || ccvTextField.text!.count > 4 {
            NKToastHelper.sharedInstance.showAlert(self, title: warningMessage.title, message: warningMessage.enterCVV.rawValue)
            self.payButton.isEnabled = true

            return
        }
        guard let cvv = self.ccvTextField.text?.trimmingCharacters(in: .whitespaces) else{
            NKToastHelper.sharedInstance.showAlert(self, title: warningMessage.title, message: warningMessage.enterCVV.rawValue)
            self.payButton.isEnabled = true

            return
        }
        guard let expMonth = self.expTextField.dateComponents.month else{
            NKToastHelper.sharedInstance.showAlert(self, title: warningMessage.title, message: warningMessage.expMonth.rawValue)
            self.payButton.isEnabled = true

            return
        }
        guard let expYear = self.expTextField.dateComponents.year else{
            NKToastHelper.sharedInstance.showAlert(self, title: warningMessage.title, message: warningMessage.expYear.rawValue)
            self.payButton.isEnabled = true

            return
        }

        if zipCodeTextField.text == nil || zipCodeTextField.text!.count < 5 || zipCodeTextField.text!.count > 5{
            NKToastHelper.sharedInstance.showAlert(self, title: warningMessage.title, message: warningMessage.enterZipCode.rawValue)
            self.payButton.isEnabled = true

            return
        }

        guard let zip = self.zipCodeTextField.text?.trimmingCharacters(in: .whitespaces) else{
            NKToastHelper.sharedInstance.showAlert(self, title: warningMessage.title, message: warningMessage.validZipCode.rawValue)

            self.payButton.isEnabled = true

            return
        }
//
        let inputValidation = self.validteCardInputs(cardNumber, cardHolderName: cardHolderName, brand: brand, expMonth: expMonth, expYear: expYear, CVV: cvv, zipCode: zip)
        if !inputValidation.success{
            NKToastHelper.sharedInstance.showAlert(self, title: warningMessage.title, message: inputValidation.message)
            self.payButton.isEnabled = true
            return
        }

        let success = self.validateCardParams(cardNumber, cardHolderName: cardHolderName, expMonth: UInt(expMonth ), expYear: UInt(expYear ), CVV: cvv, zipCode: zip)
        if !success{
            NKToastHelper.sharedInstance.showAlert(self, title: warningMessage.title, message: warningMessage.cardDeclined.rawValue)
            self.payButton.isEnabled = true

            return
        }

        if !AppSettings.isConnectedToNetwork{
            NKToastHelper.sharedInstance.showErrorAlert(self, message: warningMessage.networkIsNotConnected.rawValue)
            self.payButton.isEnabled = true
            return
        }

        if docInfoUser.govDocReject {
           self.showDocumentRejectAlert()
        }else{
            navigateToUploadDocumentScreen()

        }
//        self.createTokenAndProceedPayWith(cardNumber, cardHolderName: cardHolderName, expMonth: UInt(expMonth), expYear: UInt(expYear), CVV: cvv, zipCode: zip)
 
    }
    
    func showDocumentRejectAlert(){
        let alert = UIAlertController(title: warningMessage.title.rawValue, message: "The Government ID you uploaded has been rejected by the admin.", preferredStyle: UIAlertControllerStyle.alert)
        let okayAction = UIAlertAction(title: "UPLOAD AGAIN", style: .cancel) { (action) in
            self.navigateToUploadDocumentScreen()
        }
        
        alert.addAction(okayAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    
    func navigateToUploadDocumentScreen() {
        self.payButton.isEnabled = true
        let uploadVC = AppStoryboard.Scanner.viewController(DocumentUploadViewController.self)
        uploadVC.isNewCardPayment = true
        uploadVC.delegate = self
        uploadVC.docInfoUser = self.docInfoUser
        self.navigationController?.pushViewController(uploadVC, animated: true)
    }
    
    
    
    func payAmountForNewCardAfterDocUpload(docImage:UIImage?,signImage:UIImage?) {
        self.payButton.isEnabled = false
        self.view.endEditing(true)
        guard let cardNumber = self.cardNumberTextField.cardNumber  else {
            self.payButton.isEnabled = true
            return
        }
        if cardNumber.count < 14{
            NKToastHelper.sharedInstance.showAlert(self, title: warningMessage.title, message: warningMessage.validCardNumber.rawValue)
            self.payButton.isEnabled = true
            
            return
        }
        if cardHolderNameTextField.text == nil || cardHolderNameTextField.text?.count == 0{
            NKToastHelper.sharedInstance.showAlert(self, title: warningMessage.title, message: warningMessage.cardHolderName.rawValue)
            self.payButton.isEnabled = true
            
            return
        }
        guard let cardHolderName = self.cardHolderNameTextField.text?.trimmingCharacters(in: .whitespaces) else{
            NKToastHelper.sharedInstance.showAlert(self, title: warningMessage.title, message: warningMessage.cardHolderName.rawValue)
            
            self.payButton.isEnabled = true
            
            return
        }
        
        guard let brand = self.cardNumberTextField.cardNumberFormatter.cardPatternInfo.shortName  else {
            self.payButton.isEnabled = true
            NKToastHelper.sharedInstance.showAlert(self, title: warningMessage.title, message: warningMessage.cardDeclined.rawValue)
            self.payButton.isEnabled = true
            
            return
        }
        
        if ccvTextField.text == nil || ccvTextField.text!.count < 3 || ccvTextField.text!.count > 4 {
            NKToastHelper.sharedInstance.showAlert(self, title: warningMessage.title, message: warningMessage.enterCVV.rawValue)
            self.payButton.isEnabled = true
            
            return
        }
        guard let cvv = self.ccvTextField.text?.trimmingCharacters(in: .whitespaces) else{
            NKToastHelper.sharedInstance.showAlert(self, title: warningMessage.title, message: warningMessage.enterCVV.rawValue)
            self.payButton.isEnabled = true
            
            return
        }
        guard let expMonth = self.expTextField.dateComponents.month else{
            NKToastHelper.sharedInstance.showAlert(self, title: warningMessage.title, message: warningMessage.expMonth.rawValue)
            self.payButton.isEnabled = true
            
            return
        }
        guard let expYear = self.expTextField.dateComponents.year else{
            NKToastHelper.sharedInstance.showAlert(self, title: warningMessage.title, message: warningMessage.expYear.rawValue)
            self.payButton.isEnabled = true
            
            return
        }
        
        if zipCodeTextField.text == nil || zipCodeTextField.text!.count < 5 || zipCodeTextField.text!.count > 5{
            NKToastHelper.sharedInstance.showAlert(self, title: warningMessage.title, message: warningMessage.enterZipCode.rawValue)
            self.payButton.isEnabled = true
            
            return
        }
        
        guard let zip = self.zipCodeTextField.text?.trimmingCharacters(in: .whitespaces) else{
            NKToastHelper.sharedInstance.showAlert(self, title: warningMessage.title, message: warningMessage.validZipCode.rawValue)
            
            self.payButton.isEnabled = true
            
            return
        }
        //
        let inputValidation = self.validteCardInputs(cardNumber, cardHolderName: cardHolderName, brand: brand, expMonth: expMonth, expYear: expYear, CVV: cvv, zipCode: zip)
        if !inputValidation.success{
            NKToastHelper.sharedInstance.showAlert(self, title: warningMessage.title, message: inputValidation.message)
            self.payButton.isEnabled = true
            return
        }
        
        let success = self.validateCardParams(cardNumber, cardHolderName: cardHolderName, expMonth: UInt(expMonth ), expYear: UInt(expYear ), CVV: cvv, zipCode: zip)
        if !success{
            NKToastHelper.sharedInstance.showAlert(self, title: warningMessage.title, message: warningMessage.cardDeclined.rawValue)
            self.payButton.isEnabled = true
            
            return
        }
        
        if !AppSettings.isConnectedToNetwork{
            NKToastHelper.sharedInstance.showErrorAlert(self, message: warningMessage.networkIsNotConnected.rawValue)
            self.payButton.isEnabled = true
            return
        }
        
        self.createTokenAndProceedPayWith(cardNumber, cardHolderName: cardHolderName, expMonth: UInt(expMonth), expYear: UInt(expYear), CVV: cvv, zipCode: zip, docImage: docImage, signImage: signImage)
    }
    
    
    
    
//    func createSource(_ cardNumber:String,cardHolderName:String,expMonth: UInt,expYear:UInt,CVV:String) {
//        AppSettings.shared.showLoader(withStatus: "Connecting..")
//
//        let cardParams = STPCardParams()
//        cardParams.number = cardNumber
//        if cardHolderName.count != 0{
//            cardParams.name = cardHolderName
//        }
//        cardParams.expMonth = expMonth
//        cardParams.expYear = expYear
//        cardParams.cvc = CVV
//
//
//
//        let sourceParams = STPSourceParams.cardParams(withCard: cardParams)
//        //sourceParams.usage = .reusable
//        STPAPIClient.shared().createSource(with: sourceParams) { (source, error) in
//            if error != nil
//
//            {
//                NKToastHelper.sharedInstance.showAlert(self, title: warningMessage.title, message: error)
//
//                print("Error \(String(describing: error))")
//            }
//            print("Sourceee>>>>>>>\(String(describing: source))")
//            print("cardThreeDsecure>>>\(String(describing: source?.cardDetails))")
//
//            if let s = source, s.flow == .none && s.status == .chargeable {
//                //                if s.cardDetails?.threeDSecure == STPSourceCard3DSecureStatus.required || s.cardDetails?.threeDSecure == STPSourceCard3DSecureStatus.optional
//                //                {
//                //threeDSecureRequired
//                self.threeDSParams = STPSourceParams.threeDSecureParams(
//                    withAmount: UInt(self.payableAmout * 100),
//                    currency: "USD",
//                    returnURL: "tipntapteststripe://stripe-redirect",
//                    card: s.stripeID
//                )
//
//                STPAPIClient.shared().createSource(with: self.threeDSParams!, completion: { (threeDS, threeDErr) in
//                    AppSettings.shared.hideLoader()
//
//                    if threeDErr != nil
//                    {
//                        print("threeDErr \(String(describing: threeDErr))")
//                    }
//                    else
//                    {
//                        print("AfterthreeDS >>>>>>>>>>\(String(describing: threeDS))")
//
//                        self.redirectContext = STPRedirectContext(source: threeDS!, completion: { (sourceID, clientSecret, error) in
//
//
//                            print("SourceID>>>>>>\(sourceID)")
//                            print("ClientSecret>>>>>>\(String(describing: clientSecret))")
//
//                        })
//
//
//                        let nextVC = AppStoryboard.MyCards.viewController(BankViewDetailViewController.self)
//                        nextVC.url = threeDS?.redirect?.url
//                        self.navigationController?.pushViewController(nextVC, animated: true)
//
//                         //.redirectContext?.startRedirectFlow(from: self)
//                        //self.redirectContext?.startSafariViewControllerRedirectFlow(from: self)
//                        //self.redirectContext?.startSafariAppRedirectFlow()
//                        //self.redirectContext?.startSafariViewControllerRedirectFlow(from: self)
//                    }
//
//                })
//                //}
//            }
//            else{
//                print("Does not enter")
//            }
//        }
//
//
//
//    }

    func createTokenAndProceedPayWith(_ cardNumber:String,cardHolderName:String,expMonth: UInt,expYear:UInt,CVV:String, zipCode:String,docImage:UIImage?,signImage:UIImage?){

        AppSettings.shared.showLoader(withStatus: "Connecting..")
        let cardParams = STPCardParams()
        cardParams.number = cardNumber
        if cardHolderName.count != 0{
            cardParams.name = cardHolderName
        }
        cardParams.expMonth = expMonth
        cardParams.expYear = expYear
        cardParams.cvc = CVV
        cardParams.address.postalCode = zipCode

        STPAPIClient.shared().createToken(withCard: cardParams) { (stpToken, error) in
            if error != nil{
                AppSettings.shared.hideLoader()
                NKToastHelper.sharedInstance.showAlert(self, title: warningMessage.title, message: "\(String(describing: error!.localizedDescription))")
                self.payButton.isEnabled = true
                return
            }else{
                guard let token = stpToken else{
                    AppSettings.shared.hideLoader()
                    NKToastHelper.sharedInstance.showAlert(self, title: warningMessage.title, message: warningMessage.cardDeclined.rawValue)
                    self.payButton.isEnabled = true
                    return
                }

                AppSettings.shared.updateLoader(withStatus: "Paying..")
                self.payForPayee(self.payeeUser, sourceToken: token.tokenId, paymentNotes: self.paymentNote ?? "", amount: self.payableAmout,couponCode: self.couponCode, govDoc: docImage, signImage: signImage)
            }

        }
    }


    func validteCardInputs(_ cardNumber:String,cardHolderName:String,brand:String,expMonth: Int?,expYear:Int?,CVV:String, zipCode:String) -> (success:Bool,message:String) {
        
        if cardNumber.count == 0{
            return(false,warningMessage.validCardNumber.rawValue)
        }

        if cardHolderName.count == 0{
            return(false,warningMessage.cardHolderName.rawValue)
        }

        if let expMon = expMonth{
            if (expMon < 1) || (expMon > 12){return(false,warningMessage.validExpMonth.rawValue)}
        }else{
            return(false,warningMessage.expMonth.rawValue)
        }

        if let expY = expYear{
            if (expY < Date().year){return(false,warningMessage.validExpYear.rawValue)}
        }else{
            return(false,warningMessage.expYear.rawValue)
        }

        if !brand.lowercased().contains("maestro"){
            if (CVV.count < 3) || (CVV.count > 4){return(false,warningMessage.enterCVV.rawValue)}
        }
        
        if zipCode.count == 0 {
            return(false,warningMessage.enterZipCode.rawValue)
        }
        return(true,"")
        


    }

    func validateCardParams(_ cardNumber:String,cardHolderName:String,expMonth: UInt,expYear:UInt,CVV:String, zipCode: String) -> Bool {
        let cardParams = STPCardParams()
        cardParams.number = cardNumber
        if cardHolderName.count != 0{
            cardParams.name = cardHolderName
        }
        cardParams.expMonth = expMonth
        cardParams.expYear = expYear
        cardParams.cvc = CVV
        cardParams.address.postalCode = zipCode
        let validationState = STPCardValidator.validationState(forCard: cardParams)
        return (validationState != .invalid)
    }



    func payForPayee(_ payee:User, sourceToken:String,paymentNotes:String,amount:Double,couponCode:String? = nil,govDoc:UIImage?,signImage:UIImage?){
        PaymentService.sharedInstance.payForPayeeWith(payee, sourceToken: sourceToken, amount: amount, cardID: "", message: paymentNotes,couponCode: couponCode, govDoc: govDoc, signatureImage: signImage) { (success, resPayment, message) in
            AppSettings.shared.hideLoader()
            if success{
                if let payment = resPayment{
                    //self.showPaymentSuccess(payment)
                    self.showSuccessAlertAndOut(message, title: warningMessage.title.rawValue, payment: payment)
                }else{
                    self.payButton.isEnabled = true
                    NKToastHelper.sharedInstance.showAlert(self, title: warningMessage.title, message: message)
                }
            }else{
                self.payButton.isEnabled = true
                NKToastHelper.sharedInstance.showAlert(self, title: warningMessage.title, message: message)
            }
        }
    }
    
    
    func showSuccessAlertAndOut(_ message:String,title:String,payment: UserTransaction){
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        let okayAction = UIAlertAction(title: "Okay", style: .cancel) { (action) in
            self.showPaymentSuccess(payment)

        }

        alert.addAction(okayAction)
        self.present(alert, animated: true, completion: nil)
    }


    func showPaymentSuccess(_ payment: UserTransaction){
        let paymentSuccessVC = AppStoryboard.Scanner.viewController(PaymentSuccessViewController.self)
        paymentSuccessVC.payment = payment
        paymentSuccessVC.isPaying = true
        self.navigationController?.pushViewController(paymentSuccessVC, animated: true)
    }

    func paymentSuccessViewController(viewController:PaymentSuccessViewController, didFinishPayment done: Bool) {
        viewController.dismiss(animated: false) {
            self.dismiss(animated: false, completion: nil)
        }
    }


    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func onClickScanCard(_ sender: UIButton){
        //open card scanner screen
        self.navigateToScanCard()
    }

    func navigateToScanCard(){
        let scanCardVC = AppStoryboard.Settings.viewController(ScanCardViewController.self)
        scanCardVC.delegate = self
        let nav = self.getNavigationController(with: scanCardVC)
        self.navigationController?.present(nav, animated: true, completion: nil)
    }

    func getNavigationController(with rootViewController: UIViewController) -> UINavigationController{
        let nav = UINavigationController(rootViewController: rootViewController)
        nav.navigationBar.isTranslucent = false
        nav.navigationBar.isOpaque = true
        nav.navigationBar.barTintColor = .white
        nav.navigationBar.backgroundColor = .white
        nav.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.black]
        nav.navigationController?.present(nav, animated: true, completion: nil)
        nav.navigationBar.setBackgroundImage(UIImage(), for: .default)
        nav.navigationBar.shadowImage = UIImage()
        nav.navigationBar.isTranslucent = true
        var bounds = nav.navigationBar.bounds
        // make it cover the status bar by offseting y by -20
        bounds.offsetBy(dx: 0.0, dy: -20.0)
        bounds.size.height = bounds.height + 20.0
        let visualEffectView = UIVisualEffectView(frame: bounds)
        visualEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        //nav.navigationBar.insertSubview(visualEffectView, at: 0)
        return nav
    }

}

extension AddNewPaymentOptionViewController: ScanCardViewControllerDelegate{
    func cardDidScan(viewcontroller: ScanCardViewController, withCardInfo cardInfo: CardIOCreditCardInfo) {
        if let cardNumber = cardInfo.cardNumber{
            self.cardNumberTextField.cardNumber = cardNumber
        }else{
            self.cardNumberTextField.cardNumber = ""
        }
        if let ccv = cardInfo.cvv{
            self.ccvTextField.text = ccv
        }else{
            self.ccvTextField.text = ""
        }
        if let cardholderName = cardInfo.cardholderName{
            self.cardHolderNameTextField.text = cardholderName
        }else{
            self.cardHolderNameTextField.text = ""
        }

        if let expiryMonth = cardInfo.expiryMonth as UInt?{
            if expiryMonth > 0{
                self.expTextField.dateComponents.month = Int(expiryMonth)
            }else{
                self.expTextField.text = ""
            }
        }else{
            self.expTextField.text = ""
        }
        if let expiryYear = cardInfo.expiryYear as UInt?{
            if expiryYear > 0{
                self.expTextField.dateComponents.year = Int(expiryYear)
            }else{
                self.expTextField.text = ""
            }
        }else{
            self.expTextField.text = ""
        }
        
        if let zipcCode = cardInfo.postalCode{
            self.zipCodeTextField.text = zipcCode
        }else{
            self.zipCodeTextField.text = ""
        }
    }

    func cardDidScan(viewcontroller: ScanCardViewController, isUserChooseManual wantManual: Bool) {
        if wantManual{
            self.cardNumberTextField.cardNumber = ""
            self.ccvTextField.text = ""
            self.cardHolderNameTextField.text = ""
            self.expTextField.text = ""
            self.zipCodeTextField.text = ""
        }
    }

}


extension AddNewPaymentOptionViewController: DocumentUploadViewControllerDelegate{
    func documentUploadViewController(viewController: DocumentUploadViewController, didUploadDoc document: UIImage?, didUploadSign sign: UIImage?, selectCard card: Card, selectIndexPath: IndexPath, didSuccess success: Bool) {
        
        
    }
    
    func uploadDocumentForNewCard(viewController: DocumentUploadViewController, didUploadDoc document: UIImage?, didUploadSign sign: UIImage?, didSuccess success: Bool) {
        if success{
            self.payAmountForNewCardAfterDocUpload(docImage: document, signImage: sign)
            
        }
    }
    
    
}



