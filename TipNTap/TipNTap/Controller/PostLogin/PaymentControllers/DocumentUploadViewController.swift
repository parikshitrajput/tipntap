//
//  DocumentUploadViewController.swift
//  TipNTap
//
//  Created by Parikshit on 04/03/19.
//  Copyright © 2019 Nakul Sharma. All rights reserved.
//

import UIKit

protocol DocumentUploadViewControllerDelegate {
    func documentUploadViewController(viewController: DocumentUploadViewController,didUploadDoc document:UIImage?, didUploadSign sign: UIImage?,selectCard card: Card,selectIndexPath:IndexPath ,didSuccess success: Bool)
    
    func uploadDocumentForNewCard(viewController: DocumentUploadViewController,didUploadDoc document:UIImage?, didUploadSign sign: UIImage?,didSuccess success: Bool)
}




class DocumentUploadViewController: UIViewController,YPSignatureDelegate {
    
    @IBOutlet weak var uploadDocImage: UIImageView!
    @IBOutlet var signatureImageView: YPDrawSignatureView!
    @IBOutlet var documendView: UIView!
    @IBOutlet var signatureUploadView: UIView!

    @IBOutlet weak var uploadDocumentButton: UIButton!
    @IBOutlet weak var signatureUploadButton: UIButton!
    @IBOutlet weak var canceluploadDocumentButton: UIButton!
    @IBOutlet weak var cancelSignatureUploadButton: UIButton!
    @IBOutlet weak var placeHolderImageButton: UIButton!


    var imagePickerController : UIImagePickerController!
    
    var userImage: UIImage?
    var isUploadSignature: Bool = false
    var delegate : DocumentUploadViewControllerDelegate?
    var docInfoUser : User!
    var selectCard: Card!
    var indexPath: IndexPath!
    var isNewCardPayment: Bool = false


    override func viewDidLoad() {
        super.viewDidLoad()
        signatureImageView.delegate = self
        self.imagePickerController = UIImagePickerController()
        self.imagePickerController.delegate = self
        self.checkGovDocumentCondition()
        // Do any additional setup after loading the view.
    }
    
    
    func checkGovDocumentCondition(){
        
        if docInfoUser.govDocAvailable && !docInfoUser.govDocReject{
          self.showSignatureView()
            
        }else if (docInfoUser.govDocAvailable && docInfoUser.govDocReject){
          self.showDocView()
            
        }else if(!docInfoUser.govDocAvailable && docInfoUser.govDocReject) {
            self.showDocView()

        }else{
            self.showDocView()

        }

    }
    
    
    func showDocView() {
        self.signatureUploadView.isHidden = true
        self.title = "Upload Government ID"
        self.uploadDocumentButton.setTitle("UPLOAD GOVERNMENT ID", for: .normal)
        self.canceluploadDocumentButton.isHidden = true
        self.placeHolderImageButton.isHidden = false
    }
    
    func showSignatureView() {

        self.documendView.isHidden = true
        self.title = "Signature"
        self.signatureUploadButton.setTitle("PROCEED", for: .normal)
        self.cancelSignatureUploadButton.isHidden = true
    }
    
    override func viewDidLayoutSubviews() {
        self.uploadDocumentButton.applyGradient(withColours: [appColor.gradientStart,appColor.gradientEnd], gradientOrientation: .topLeftBottomRight)
        self.signatureUploadButton.applyGradient(withColours: [appColor.gradientStart,appColor.gradientEnd], gradientOrientation: .topLeftBottomRight)
        CommonClass.makeViewCircularWithCornerRadius(self.uploadDocImage, borderColor: .groupTableViewBackground, borderWidth: 0, cornerRadius: 5)
        CommonClass.makeViewCircularWithCornerRadius(self.signatureImageView, borderColor: .groupTableViewBackground, borderWidth: 0, cornerRadius: 5)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func didStart(_ view : YPDrawSignatureView) {
        //print("Started Drawing")
        self.cancelSignatureUploadButton.isHidden = true
    }
    

    func didFinish(_ view : YPDrawSignatureView) {
        
       // print("Finished Drawing")
        self.cancelSignatureUploadButton.isHidden = false

    }

    @IBAction func onClickUploadDocumentButton(_ sender: UIButton) {
       if self.uploadDocumentButton.titleLabel?.text == "SAVE GOVERNMENT ID" {
          self.documendView.isHidden = true
          self.signatureUploadView.isHidden = false
         self.title = "Signature"
        self.signatureUploadButton.setTitle("PROCEED", for: .normal)
        self.cancelSignatureUploadButton.isHidden = true

       }else{
        self.showAlertToChooseAttachmentOption(sender)

        }

    }
    
    @IBAction func onClickUploadSignatureButton(_ sender: UIButton) {
        
        if let signatureImage = self.signatureImageView.getSignature(scale: 10) {
           if  self.isNewCardPayment {
                self.delegate?.uploadDocumentForNewCard(viewController: self, didUploadDoc: self.userImage, didUploadSign: signatureImage, didSuccess: true)
            }else{
            self.delegate?.documentUploadViewController(viewController: self, didUploadDoc: self.userImage, didUploadSign: signatureImage, selectCard: self.selectCard, selectIndexPath: self.indexPath, didSuccess: true)
            }
             self.navigationController?.pop(false)
        }else{
            NKToastHelper.sharedInstance.showErrorAlert(self, message: "Please Signature First")
        }
    }
    
    @IBAction func onClickBackButton(_ sender: UIBarButtonItem) {
        self.navigationController?.pop(true)
        
    }
    
    @IBAction func onClickCloseSignatureButton(_ sender: UIButton) {
      self.signatureImageView.clear()
      self.cancelSignatureUploadButton.isHidden = true
    }
    
    
    @IBAction func onClickCloseDocumentButton(_ sender: UIButton) {
      self.userImage = nil
      self.uploadDocImage.image = nil
      self.uploadDocumentButton.setTitle("UPLOAD GOVERNMENT ID", for: .normal)
      self.placeHolderImageButton.isHidden = false
        if userImage == nil {
            self.canceluploadDocumentButton.isHidden = true
        }else{
            self.canceluploadDocumentButton.isHidden = false
            
        }


    }
    
    
    func setImage() {
        if let img = self.userImage{
            self.uploadDocImage.image = img
            self.uploadDocumentButton.setTitle("SAVE GOVERNMENT ID", for: .normal)
            self.canceluploadDocumentButton.isHidden = false
            self.placeHolderImageButton.isHidden = true
        }else{
            self.placeHolderImageButton.isHidden = false

            //self.uploadLabel.isHidden = false
            
        }
    }

}


extension DocumentUploadViewController : UIImagePickerControllerDelegate,UINavigationControllerDelegate{
    func showAlertToChooseAttachmentOption(_ sender: UIButton){
        let actionSheet = UIAlertController(title: "Select Government ID", message:"Select an image to make that Government ID", preferredStyle:UIDevice.isRunningOnIpad ? .alert : .actionSheet)
        let cancelAction: UIAlertAction = UIAlertAction(title: "Cancel", style: .cancel) { action -> Void in
            actionSheet.dismiss(animated: true, completion: nil)
        }
        
        actionSheet.addAction(cancelAction)
        let openGalleryAction: UIAlertAction = UIAlertAction(title: "Choose from Gallery", style: .default)
        { action -> Void in
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.photoLibrary){
                self.imagePickerController.sourceType = UIImagePickerControllerSourceType.photoLibrary;
                self.imagePickerController.allowsEditing = true
                self.imagePickerController.modalPresentationStyle = UIModalPresentationStyle.currentContext
                self.present(self.imagePickerController, animated: true, completion: nil)
            }
        }
        actionSheet.addAction(openGalleryAction)
        
        let openCameraAction: UIAlertAction = UIAlertAction(title: "Camera", style: .default)
        { action -> Void in
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera){
                self.imagePickerController.sourceType = UIImagePickerControllerSourceType.camera;
                self.imagePickerController.allowsEditing = true
                self.imagePickerController.modalPresentationStyle = UIModalPresentationStyle.currentContext
                self.present(self.imagePickerController, animated: true, completion: nil)
            }
        }
        
        
        actionSheet.addAction(openCameraAction)
        self.present(actionSheet, animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String :Any]){
        
        if let tempImage = info[UIImagePickerControllerEditedImage] as? UIImage{
            self.userImage = tempImage
            self.setImage()
        }else if let tempImage = info[UIImagePickerControllerOriginalImage] as? UIImage{
            self.userImage = tempImage
            self.setImage()
        }
        picker.dismiss(animated: true,completion: nil)
    }
}


