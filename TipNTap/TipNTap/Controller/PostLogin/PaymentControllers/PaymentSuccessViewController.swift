//
//  PaymentSuccessViewController.swift
//  GPDock
//
//  Created by TecOrb on 03/11/17.
//  Copyright © 2017 Nakul Sharma. All rights reserved.
//

import UIKit
import SwiftyGif
//protocol PaymentSuccessViewControllerDelegate {
//    func paymentSuccessViewController(viewController:PaymentSuccessViewController, didFinishPayment done: Bool)
//}

class PaymentSuccessViewController: UIViewController {
    @IBOutlet weak var amountLabel : UILabel!
    @IBOutlet weak var messageLabel : UILabel!
    @IBOutlet weak var payeeLabel : UILabel!
    @IBOutlet weak var referenceLabel : UILabel!
    @IBOutlet weak var transactionIDLabel : UILabel!
    @IBOutlet weak var dateLabel : UILabel!
    @IBOutlet weak var fromToLabel : UILabel!

    @IBOutlet weak var doneGIFImageView : UIImageView!
    @IBOutlet weak var payeeContainer : UIView!
    @IBOutlet weak var moneyContainer : UIView!
    @IBOutlet weak var makeNewPaymentContainer : UIView!

    var isPaying = false
    var fromPassbook = false

    var payment : UserTransaction!
    var titleView : NavigationTitleView!
    let gifManager = SwiftyGifManager(memoryLimit:10)
    var activityVC : UIActivityViewController!

    func setupNavigationTitle() {
        titleView = NavigationTitleView.instanceFromNib()
        titleView.frame = CGRect(x: 60, y: 0, width: self.view.frame.size.width-120, height: 44)
        titleView.shouldAttributtedMiddle = true
        self.navigationItem.titleView = self.titleView
    }
    override func viewDidLayoutSubviews() {
        self.payeeContainer.addshadow(top: true, left: false, bottom: false, right: false,shadowRadius: 5)
        self.moneyContainer.addTopBorderWithColor(appColor.lightGray, width: 1)
    }


    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupNavigationTitle()

        let gifImage = UIImage(gifName: "tickanimation")
        self.doneGIFImageView.setGifImage(gifImage, manager: gifManager, loopCount: 1)


        self.messageLabel.text = (payment.transactionType.lowercased() == "received") ? "Money received" : "Money sent"
        self.fromToLabel.text = (payment.transactionType.lowercased() == "received") ? "From" : "To"

        self.amountLabel.text = (payment.transactionType.lowercased() == "received") ? String(format:"$ %0.2f",payment.payout) : String(format:"$ %0.2f",payment.amount)

        self.amountLabel.textColor = (payment.transactionType.lowercased() == "received") ? transactionColor.received : transactionColor.send

        self.payeeLabel.text = (payment.transactionType.lowercased() == "received") ? self.payment.sender.name : self.payment.payee.name
        self.referenceLabel.text = (payment.transactionType.lowercased() == "received") ? "#"+self.payment.sender.accountReference : "#"+self.payment.payee.accountReference
        self.transactionIDLabel.text = "Txn ID : \(self.payment.transactionId)"
        self.dateLabel.text = CommonClass.formattedDateWithTimeStamp(self.payment.paymentDate, format: "MMM dd, hh:mm a")
        self.makeNewPaymentContainer.alpha = isPaying ? 1.0 : 0.0
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func onClickShare(_ sender:UIBarButtonItem){
        self.makeNewPaymentContainer.isHidden = true
        guard let scrsht = UIApplication.shared.screenShot else {
            self.makeNewPaymentContainer.isHidden = false
            return
        }
        self.shareApplication(image: scrsht)
        self.makeNewPaymentContainer.isHidden = true
    }

    func shareApplication(image:UIImage){
        self.activityVC = UIActivityViewController(activityItems: [image], applicationActivities: nil)
        self.activityVC.completionWithItemsHandler = {(actType, done,response, error) in
            if done{
                if self.fromPassbook{
                    self.navigationController?.popToRoot(true)
                }else{
                    self.navigationController?.pop(true)
                }
            }
        }
        let appDelegate: AppDelegate = (UIApplication.shared.delegate as! AppDelegate)
        activityVC.popoverPresentationController?.sourceView = appDelegate.window?.rootViewController?.view
        appDelegate.window?.rootViewController?.present(activityVC, animated: true, completion: nil)
    }

    @IBAction func onClickBack(_ sender:UIBarButtonItem){
        if self.fromPassbook{
            self.navigationController?.pop(true)
        }else{
            self.navigationController?.popToRoot(true)
        }
    }

    @IBAction func onClickMakeAnotherPayment(_ sender:UIButton){
        if self.fromPassbook{
            AppSettings.shared.proceedToDashboard(true)
        }else{
            if let vcs = self.navigationController?.viewControllers{
                if let rootscvc = vcs.filter({ (scvc) -> Bool in
                    return scvc is RootScannerViewController
                }).first{
                    self.navigationController?.popToViewController(rootscvc, animated: true)
                }
            }
        }
        
//        if isPaying{
//            if let vcs = self.navigationController?.viewControllers{
//                if let rootscvc = vcs.filter({ (scvc) -> Bool in
//                    return scvc is RootScannerViewController
//                }).first{
//                    self.navigationController?.popToViewController(rootscvc, animated: true)
//                }
//            }
//        }
    }

}
