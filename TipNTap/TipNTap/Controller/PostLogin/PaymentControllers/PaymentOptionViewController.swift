//
//  PaymentOptionViewController.swift
//  GPDock
//
//  Created by TecOrb on 27/07/17.
//  Copyright © 2017 Nakul Sharma. All rights reserved.
//

import UIKit
import Stripe

class PaymentOptionViewController: UIViewController {
    @IBOutlet weak var optionTableView : UITableView!
    var user:User!
    var payeeUser: User!
    var cards = Array<Card>()
    var amount : Double!
    var paymentNote : String?
    var couponCode : String?
    var selectedIndex = -1
    var docInfoUser = User()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.user = User.loadSavedUser()
        optionTableView.dataSource = self
        optionTableView.delegate = self
        optionTableView.tableHeaderView = UIView(frame: CGRect.zero)
        self.optionTableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        self.getUserCards()
    }



    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }


    func getUserCards() -> Void {
        AppSettings.shared.showLoader(withStatus: "Fetching..")
        PaymentService.sharedInstance.getCardsForUser() { (success,resCards,message,resUser)  in
            AppSettings.shared.hideLoader()
            if let someCards = resCards{
                //print("abcd>>>\(String(describing: resUser?.paymentServiceFee))")
                self.cards.removeAll()
                self.cards.append(contentsOf: someCards)
                if let someUser = resUser {
                self.docInfoUser = someUser
                }
                self.optionTableView.reloadData()
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func onClickBackButton(_ sender: UIBarButtonItem){
        self.navigationController?.pop(true)
    }

    

    
    func navigateToUploadDocumentScreen(card:Card,indexPath:IndexPath) {
        let uploadVC = AppStoryboard.Scanner.viewController(DocumentUploadViewController.self)
        uploadVC.indexPath = indexPath
        uploadVC.selectCard = card
        uploadVC.isNewCardPayment = false
        uploadVC.docInfoUser = self.docInfoUser
        uploadVC.delegate = self
        self.navigationController?.pushViewController(uploadVC, animated: true)
    }
    
    
    
    func showDocumentRejectAlert(card:Card,indexPath:IndexPath){
        let alert = UIAlertController(title: warningMessage.title.rawValue, message: "The Government ID you uploaded has been rejected by the admin.", preferredStyle: UIAlertControllerStyle.alert)
        let okayAction = UIAlertAction(title: "UPLOAD AGAIN", style: .cancel) { (action) in
            self.navigateToUploadDocumentScreen(card: card, indexPath: indexPath)
        }
        
        alert.addAction(okayAction)
        self.present(alert, animated: true, completion: nil)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension PaymentOptionViewController: UITableViewDataSource,UITableViewDelegate{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }


    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        var height:CGFloat = 72
        if indexPath.section == 1{
            if indexPath.row == selectedIndex{
                height = 72
            }
        }
        return height
    }

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        var height:CGFloat = 12

        if section == 0{
            height = 0
        }else if section == 1{
            if cards.count == 0{height = 0}else{height = 12}
        }else if section == 2 {
            height = 12
        }
        return height
    }

    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        var height:CGFloat = 0

        if section == 0{
            height = 0
        }else if section == 1{
            if cards.count == 0{height = 0}else{height = 0}
        }else if section == 2 {
            height = 0
        }
        return height
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var rows = 0
        switch section {
        case 0:
            rows = 1
        case 1:
            rows = cards.count
        case 2:
            rows = 1
        default:
            rows = 0
        }
        return rows
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "PayableAmountDescriptionCell", for: indexPath) as! PayableAmountDescriptionCell
            cell.payableTitleLabel.text = ""
            let totalAmount = self.amount + (self.amount*self.payeeUser.adminPercentage) + self.payeeUser.paymentServiceFee
            cell.amountLabel.text = String(format: "$ %0.2lf", totalAmount)
            return cell
        }else if indexPath.section == 1{

            if selectedIndex == indexPath.row{
                let cell = tableView.dequeueReusableCell(withIdentifier: "SelectedCardTableViewCell", for: indexPath) as! SelectedCardTableViewCell
                let card = cards[indexPath.row]
                cell.cardImageView.image = UIImage(named:card.brand.lowercased()+"_")
                cell.cardNumberLabel.text = "xxxx xxxx xxxx \(card.last4)"
                cell.fundingTypeLabel.text = "Expiring \(card.expMonth)/\(card.expYear)"
                cell.doneButton.addTarget(self, action: #selector(onClickPayButton(_:)), for: .touchUpInside)
                return cell

            }else{
                let cell = tableView.dequeueReusableCell(withIdentifier: "CardTableViewCell", for: indexPath) as! CardTableViewCell
                let card = cards[indexPath.row]
                cell.cardImageView.image = UIImage(named:card.brand.lowercased()+"_")
                cell.cardNumberLabel.text = "xxxx xxxx xxxx \(card.last4)"
                let expMonth = String(format: "%02d", card.expMonth)
                let expYear = String(format: "%02d", card.expYear)
                cell.fundingTypeLabel.text = "Expiring \(expMonth)/\(expYear)"
                cell.removeButton.addTarget(self, action: #selector(onClickRemoveButton(_:)), for: .touchUpInside)

                return cell
            }

        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "NewCardTableViewCell", for: indexPath) as! NewCardTableViewCell
            return cell

        }
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 1{
            if self.selectedIndex == indexPath.row{
                self.selectedIndex = -1
            }else{
                self.selectedIndex = indexPath.row
            }
            tableView.reloadData()
        }else if indexPath.section == 2{
            self.navigateToAddNewCard()
        }
    }


    func navigateToAddNewCard(){
        let addNewPaymentOptionVC = AppStoryboard.Scanner.viewController(AddNewPaymentOptionViewController.self)
        addNewPaymentOptionVC.payableAmout = self.amount
        addNewPaymentOptionVC.paymentNote = self.paymentNote
        addNewPaymentOptionVC.payeeUser = self.payeeUser
        addNewPaymentOptionVC.couponCode = self.couponCode
        addNewPaymentOptionVC.docInfoUser = self.docInfoUser
        self.navigationController?.pushViewController(addNewPaymentOptionVC, animated: true)
    }

    @IBAction func onTextDidChanged(_ textField: UITextField){
        //self.cvvForSelectedCard = textField.text!.trimmingCharacters(in: .whitespacesAndNewlines)
    }

    @IBAction func onClickRemoveButton(_ sender: UIButton){
        if let indexPath = sender.tableViewIndexPath(self.optionTableView) as IndexPath?{
            if indexPath.row == selectedIndex{self.selectedIndex = -1}
            let card = cards[indexPath.row]
            self.askToDeleteCard(card: card, atIndexPath: indexPath)
        }
    }

    func askToDeleteCard(card : Card,atIndexPath indexPath: IndexPath) {
        let alert = UIAlertController(title: warningMessage.title.rawValue, message: "Would you really want to delete this card?", preferredStyle: .alert)
        let okayAction = UIAlertAction(title: "Delete", style: .destructive){[weak card](action) in
            alert.dismiss(animated: true, completion: nil)
            self.removeCard(card, indexPath: indexPath)
        }

        let cancelAction = UIAlertAction(title: "Nope", style: .default){(action) in
            alert.dismiss(animated: true, completion: nil)
        }
        alert.addAction(cancelAction)
        alert.addAction(okayAction)
        self.navigationController?.present(alert, animated: true, completion: nil)
    }

    func removeCard(_ mycard:Card?,indexPath: IndexPath) -> Void {
        guard let card = mycard else {
            return
        }
        AppSettings.shared.showLoader(withStatus: "Please wait..")
        PaymentService.sharedInstance.removeCard(card.ID) { (success, message) in
            AppSettings.shared.hideLoader()
            if success{
                self.cards.remove(at: indexPath.row)
                self.optionTableView.deleteRows(at: [indexPath], with: .automatic)
            }
            NKToastHelper.sharedInstance.showAlert(self, title: warningMessage.title, message: message)
        }

    }

    func confirmByPaying(indexPath:IndexPath,govDoc:UIImage?, signImage:UIImage?) {
        let card = cards[indexPath.row]
        let YY = CommonClass.sharedInstance.formattedDateWith(Date(), format: "YY")
        let MM = CommonClass.sharedInstance.formattedDateWith(Date(), format: "MM")

        let expValidation = STPCardValidator.validationState(forExpirationYear: YY, inMonth: MM)
        if expValidation != .valid{
            NKToastHelper.sharedInstance.showAlert(self, title: warningMessage.title, message: "The card was declined. Please reenter the payment details")
            return
        }

        AppSettings.shared.showLoader(withStatus: "Paying...")
        self.payForPayee(self.payeeUser, cardID: card.ID, paymentNotes: self.paymentNote ?? "", amount: self.amount, couponCode: self.couponCode, govDoc: govDoc, sign: signImage)
    }

    func payForPayee(_ payee:User,cardID:String,paymentNotes:String,amount:Double, couponCode:String? = nil,govDoc:UIImage?, sign:UIImage?){
        PaymentService.sharedInstance.payForPayeeWith(payee, sourceToken: "", amount: amount, cardID:cardID, message: paymentNotes,couponCode:couponCode, govDoc: govDoc, signatureImage: sign ) { (success, resPayment, message) in
            AppSettings.shared.hideLoader()
            if success{
                if let payment = resPayment{
                    self.showSuccessAlertAndOut(message, title: warningMessage.title.rawValue, payment: payment)

                    //self.showPaymentSuccess(payment)
                }else{
                    NKToastHelper.sharedInstance.showAlert(self, title: warningMessage.title, message: message)
                }
            }else{
                NKToastHelper.sharedInstance.showAlert(self, title: warningMessage.title, message: message)
            }
        }
    }
    
    
    func showSuccessAlertAndOut(_ message:String,title:String,payment: UserTransaction){
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        let okayAction = UIAlertAction(title: "Okay", style: .cancel) { (action) in
            self.showPaymentSuccess(payment)
            
        }
        
        alert.addAction(okayAction)
        self.present(alert, animated: true, completion: nil)
    }
    

    @IBAction func onClickPayButton(_ sender: UIButton){

        if let indexPath = sender.tableViewIndexPath(self.optionTableView){
            let card = cards[indexPath.row]
            let YY = "\(card.expYear)".suffix(2) //let last4 = a.suffix(4)
            let MM = "\(card.expMonth)"
            let expValidation = STPCardValidator.validationState(forExpirationYear: String(YY), inMonth: MM)
            if expValidation != .valid{
                NKToastHelper.sharedInstance.showAlert(self, title: warningMessage.title, message: "The card was declined. Please reenter the payment details")
                return
            }
           // self.confirmByPaying(indexPath: indexPath)
            if docInfoUser.govDocReject {
                showDocumentRejectAlert(card: card, indexPath: indexPath)
            }else{
                self.navigateToUploadDocumentScreen(card: card, indexPath: indexPath)

            }
        }
    }

}


extension PaymentOptionViewController:UITextFieldDelegate{
    func textFieldDidBeginEditing(_ textField: UITextField) {
       // self.cvvForSelectedCard = textField.text!.trimmingCharacters(in: .whitespacesAndNewlines)
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
       // self.cvvForSelectedCard = textField.text!.trimmingCharacters(in: .whitespacesAndNewlines)
    }

    func payForDirectPayment(marinaID:String,cardID:String,sourceToken:String,amounts:[Double],paymentNotes:String){
    }

    func showPaymentSuccess(_ payment: UserTransaction){
        let paymentSuccessVC = AppStoryboard.Scanner.viewController(PaymentSuccessViewController.self)
        paymentSuccessVC.payment = payment
        paymentSuccessVC.isPaying = true
        self.navigationController?.pushViewController(paymentSuccessVC, animated: true)
    }

}

extension PaymentOptionViewController: DocumentUploadViewControllerDelegate{
    func uploadDocumentForNewCard(viewController: DocumentUploadViewController, didUploadDoc document: UIImage?, didUploadSign sign: UIImage?, didSuccess success: Bool) {
        
    }
    
    func documentUploadViewController(viewController: DocumentUploadViewController, didUploadDoc document: UIImage?, didUploadSign sign: UIImage?, selectCard card: Card, selectIndexPath: IndexPath, didSuccess success: Bool) {
        if success{
            self.confirmByPaying(indexPath: selectIndexPath, govDoc: document, signImage: sign)
            
        }
    }
    
    
}









