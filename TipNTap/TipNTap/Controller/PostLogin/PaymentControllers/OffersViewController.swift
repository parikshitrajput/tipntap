//
//  OffersViewController.swift
//  TipNTap
//
//  Created by TecOrb on 23/03/18.
//  Copyright © 2018 Nakul Sharma. All rights reserved.
//

import UIKit
//import FAPagingLayout
protocol OffersViewControllerDelegate {
    func offersViewController(viewController:OffersViewController,didSelectOffer offer: Offer)
}
class OffersViewController: UIViewController {
    var offers = Array<Offer>()
    var page: Int = 1
    let perPage = 10
    var isNewDataLoading = true
    var shouldShowApply = true
    var delegate: OffersViewControllerDelegate?
    @IBOutlet var collectionView: UICollectionView!

    @IBOutlet weak var offerTitle : UILabel!
    @IBOutlet weak var offerDetails : UILabel!
    @IBOutlet weak var offerDetailsButton : UIButton!
    var isFirstTime = true
    var offerColors = [UIColor]()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.viewConfigrations()
        self.navigationItem.title = self.shouldShowApply ? "Discover Offers" : "Current Offers"
        self.offerDetailsButton.isHidden = !self.shouldShowApply
        self.loadOffers(pageNumber: self.page, recordPerPage: self.perPage)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewDidLayoutSubviews() {
        CommonClass.makeViewCircularWithCornerRadius(self.offerDetailsButton, borderColor: appColor.lightGray, borderWidth: 0, cornerRadius: 3)

        self.offerDetailsButton.applyGradient(withColours: [appColor.gradientStart,appColor.gradientEnd], gradientOrientation: .horizontal, locations: [0.0,1.0])

    }
    private func viewConfigrations() {
        let margin:CGFloat = self.view.frame.size.width/7.0
        collectionView.register(UINib(nibName: "CommonOfferCell", bundle: nil), forCellWithReuseIdentifier: "CommonOfferCell")
        collectionView.register(UINib(nibName: "NoOfferCell", bundle: nil), forCellWithReuseIdentifier: "NoOfferCell")
        collectionView.contentInset = UIEdgeInsetsMake(0, margin, 0, margin)
        collectionView.decelerationRate = UIScrollViewDecelerationRateFast
    }

    @IBAction func onClcikBack(_ sender: UIBarButtonItem){
        self.navigationController?.pop(true)
    }


    @IBAction func onClcikMoreOptions(_ sender: UIBarButtonItem){
//        self.openActionSheet()
    }

    @IBAction func onClcikAppyOffer(_ sender: UIButton){
        guard let indexpath = self.indexPathForClosestCell() else {return}
        if self.offers.count == 0{
            return
        }
        self.delegate?.offersViewController(viewController: self, didSelectOffer: self.offers[indexpath.item])
        self.navigationController?.pop(true)
    }


    func loadOffers(pageNumber:Int, recordPerPage:Int) {
        if !AppSettings.isConnectedToNetwork{
            NKToastHelper.sharedInstance.showErrorAlert(self, message: warningMessage.networkIsNotConnected.rawValue)
            return
        }

        self.isNewDataLoading = true
        if pageNumber == 1{
            offers.removeAll()
            offerColors.removeAll()
        }

        TransactionService.sharedInstance.getOffersList(pageNumber, perPage: recordPerPage) { (success, offerResponse, message) in
            self.isNewDataLoading = false
            if let newOfferArray = offerResponse{
                if newOfferArray.count == 0{
                    self.page = self.page - 1
                }
                self.offers.append(contentsOf: newOfferArray)
                self.getColors(capecity: self.offers.count)
                self.collectionView.reloadData()
            }else{
                if self.page > 1{
                    self.page = self.page - 1
                }
            }
            self.collectionView.reloadData()
        }
    }

    func getColors(capecity:Int){
        self.offerColors.removeAll()
        for _ in 0..<capecity{
            self.offerColors.append(.random())
        }
    }
}

extension OffersViewController: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{


    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return (self.offers.count == 0) ? 1 : self.offers.count
    }
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if offers.count == 0{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "NoOfferCell", for: indexPath) as! NoOfferCell
            cell.messageLabel.text = isNewDataLoading ? "Loading.." : "No offer found"
            cell.errorIcon.isHidden = self.isNewDataLoading
            return cell
        }else{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CommonOfferCell", for: indexPath) as! CommonOfferCell
            let offer = offers[indexPath.item]
            self.setupViewForItem(cell)
            cell.phoneImageView.image = (indexPath.item % 2 == 0) ? #imageLiteral(resourceName: "phone") : #imageLiteral(resourceName: "award")
            cell.sprinkalImageView.image = (indexPath.item % 2 == 0) ? #imageLiteral(resourceName: "sprinkal_offer") : #imageLiteral(resourceName: "star_offer")
            cell.backView.backgroundColor = offerColors[indexPath.item]
            if isFirstTime{
                self.isFirstTime = false
                self.offerTitle.text = offer.title
                self.offerDetails.text = offer.details
                //self.offerDetailsButton.setTitle(offer.coupanCode, for: UIControlState())
            }
            CommonClass.makeViewCircular(cell.backView, borderColor: .clear, borderWidth: 0)
            CommonClass.makeViewCircular(cell.innerView, borderColor: .clear, borderWidth: 0)

            return cell
        }
    }

    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        guard let aCell = cell as? CommonOfferCell else{
            return
        }
        self.setupViewForItem(aCell)
        aCell.setNeedsLayout()
        aCell.layoutIfNeeded()
        CommonClass.makeViewCircular(aCell.backView, borderColor: .clear, borderWidth: 0)
        CommonClass.makeViewCircular(aCell.innerView, borderColor: .clear, borderWidth: 0)

        aCell.setNeedsLayout()
        aCell.layoutIfNeeded()
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if self.offers.count == 0 || !self.shouldShowApply{
            return
        }

        self.delegate?.offersViewController(viewController: self, didSelectOffer: self.offers[indexPath.item])
        self.navigationController?.pop(true)
    }


    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        var cellSize: CGSize = collectionView.bounds.size
        cellSize.width -= collectionView.contentInset.left
        cellSize.width -= collectionView.contentInset.right
       // cellSize.height = collectionView.bounds.size.height
        return cellSize
    }

    func scrollViewDidEndScrollingAnimation(_ scrollView: UIScrollView) {
        NSObject.cancelPreviousPerformRequests(withTarget: self)
        let alpha: CGFloat = 1.0
        self.configureViewsForVisibleCells()
        UIView.animate(withDuration: 0.3) {
            self.offerTitle.alpha = alpha
            self.offerDetails.alpha = alpha
            if self.shouldShowApply{
                self.offerDetailsButton.isHidden = false
            }
        }
    }

    func indexPathForClosestCell() -> IndexPath?{
        if collectionView.visibleCells.count == 0{
            return nil
        }
        var closestCell : UICollectionViewCell = collectionView.visibleCells[0];
        for cell in collectionView!.visibleCells as [UICollectionViewCell] {
            let closestCellDelta = abs(closestCell.center.x - collectionView.bounds.size.width/2.0 - collectionView.contentOffset.x)
            let cellDelta = abs(cell.center.x - collectionView.bounds.size.width/2.0 - collectionView.contentOffset.x)
            if (cellDelta < closestCellDelta){
                closestCell = cell
            }
        }
        let indexPath = collectionView.indexPath(for: closestCell)
        return indexPath
    }


    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let alpha: CGFloat = 0.0
        UIView.animate(withDuration: 0.5) {
            self.offerTitle.alpha = alpha
            self.offerDetails.alpha = alpha
            if self.shouldShowApply{
                self.offerDetailsButton.isHidden = true
            }
        }
        NSObject.cancelPreviousPerformRequests(withTarget: scrollView)
        self.perform(#selector(self.scrollViewDidEndScrollingAnimation(_:)), with: scrollView, afterDelay: 0.3)
    }


    func configureViewsForVisibleCells(){
        for visiblecell in self.collectionView.visibleCells{
            if let cell = visiblecell as? CommonOfferCell{
                self.setupViewForItem(cell)
            }
        }
    }

    func setupViewForItem(_ cell: CommonOfferCell){
        guard let indexPath = self.indexPathForClosestCell()else{return}
        let offer = self.offers[indexPath.item]
        self.offerTitle.text = offer.title
        self.offerDetails.text = offer.details
        //self.offerDetailsButton.setTitle(offer.coupanCode, for: UIControlState())
    }

    func isCenteredIndexPath(indexPath:IndexPath)-> Bool{
        guard let centerIndexPath = self.indexPathForClosestCell() else {
            return false
        }
        return indexPath.item == centerIndexPath.item
    }
}




