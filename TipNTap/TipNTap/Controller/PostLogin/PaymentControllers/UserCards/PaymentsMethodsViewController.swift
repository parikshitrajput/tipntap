//
//  PaymentsMethodsViewController.swift
//  GPDock
//
//  Created by TecOrb on 31/10/17.
//  Copyright © 2017 Nakul Sharma. All rights reserved.
//

import UIKit
import Stripe

class PaymentsMethodsViewController: UIViewController {
    @IBOutlet weak var optionTableView : UITableView!
    var user:User!
    var payeeUser: User!
    var cards = Array<Card>()

    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(PaymentsMethodsViewController.userDidAddNewCardHandler(_:)), name: .USER_DID_ADD_NEW_CARD_NOTIFICATION, object: nil)

        optionTableView.dataSource = self
        optionTableView.delegate = self
        optionTableView.tableHeaderView = UIView(frame: CGRect.zero)
        self.optionTableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        self.getUserCards()
    }

    @objc func userDidAddNewCardHandler(_ notification: Notification) {
        if let userInfo = notification.userInfo as? Dictionary<String,Card>{
            if let aCard = userInfo["card"]{
                let duplicateCards = self.cards.filter({ (card) -> Bool in
                    return aCard.ID == card.ID
                })

                if duplicateCards.count == 0{
                    self.cards.append(aCard)
                    self.optionTableView.reloadData()
                    self.navigationController?.pop(true)
                }else{
                    NKToastHelper.sharedInstance.showAlert(self, title: warningMessage.title, message: "The card you have entered is already exists") {
                        self.navigationController?.pop(true)
                    }
                }
            }
        }
    }

    func getUserCards() -> Void {
        AppSettings.shared.showLoader(withStatus: "Fetching..")
        PaymentService.sharedInstance.getCardsForUser() { (success,resCards,message,resUser)  in
            AppSettings.shared.hideLoader()
            if let someCards = resCards{
                self.cards.removeAll()
                self.cards.append(contentsOf: someCards)
                self.optionTableView.reloadData()
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func onClickMenu(_ sender:UIBarButtonItem){
        sideMenuController?.toggleLeftView(animated: true, delay: 0.0, completionHandler: nil)
    }

}

extension PaymentsMethodsViewController: UITableViewDataSource,UITableViewDelegate{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let height:CGFloat = 72
        return height
    }

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        var height:CGFloat = 0
        if section == 0{
            height = 0
        }else{
            if cards.count == 0{height = 1}else{height = 12}
        }
        return height
    }

    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        let height:CGFloat = 0
        return height
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var rows = 0
        switch section {
        case 0:
            rows = cards.count
        case 1:
            rows = 1
        default:
            rows = 0
        }
        return rows
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "CardTableViewCell", for: indexPath) as! CardTableViewCell
            let card = cards[indexPath.row]
            cell.cardImageView.image = UIImage(named:card.brand.lowercased()+"_")
            cell.cardNumberLabel.text = "xxxx xxxx xxxx \(card.last4)"
            let expMonth = String(format: "%02d", card.expMonth)
            let expYear = String(format: "%02d", card.expYear)
            cell.fundingTypeLabel.text = "Expiring \(expMonth)/\(expYear)"
            cell.removeButton.addTarget(self, action: #selector(onClickRemoveButton(_:)), for: .touchUpInside)
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "NewCardTableViewCell", for: indexPath) as! NewCardTableViewCell
            return cell

        }
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 0{
           return
        }else if indexPath.section == 1{
            self.navigateToAddNewCard()
        }
    }


    func navigateToAddNewCard(){
        let addNewPaymentOptionVC = AppStoryboard.MyCards.viewController(AddCardViewController.self)
        self.navigationController?.pushViewController(addNewPaymentOptionVC, animated: true)
    }

    @IBAction func onClickRemoveButton(_ sender: UIButton){
        if let indexPath = sender.tableViewIndexPath(self.optionTableView) as IndexPath?{
            let card = cards[indexPath.row]
            self.askToDeleteCard(card: card, atIndexPath: indexPath)
        }
    }

    func askToDeleteCard(card : Card,atIndexPath indexPath: IndexPath) {
        let alert = UIAlertController(title: warningMessage.title.rawValue, message: "Would you really want to delete this card?", preferredStyle: .alert)
        let okayAction = UIAlertAction(title: "Delete", style: .destructive){[weak card](action) in
            alert.dismiss(animated: true, completion: nil)
            self.removeCard(card, indexPath: indexPath)
        }

        let cancelAction = UIAlertAction(title: "Nope", style: .default){(action) in
            alert.dismiss(animated: true, completion: nil)
        }
        alert.addAction(cancelAction)
        alert.addAction(okayAction)
        self.navigationController?.present(alert, animated: true, completion: nil)
    }

    func removeCard(_ mycard:Card?,indexPath: IndexPath) -> Void {
        guard let card = mycard else {
            return
        }
        AppSettings.shared.showLoader(withStatus: "Please wait..")
        PaymentService.sharedInstance.removeCard(card.ID) { (success, message) in
            AppSettings.shared.hideLoader()
            if success{
                self.cards.remove(at: indexPath.row)
                self.optionTableView.deleteRows(at: [indexPath], with: .automatic)
            }
            NKToastHelper.sharedInstance.showAlert(self, title: warningMessage.title, message: message)
        }

    }

}






