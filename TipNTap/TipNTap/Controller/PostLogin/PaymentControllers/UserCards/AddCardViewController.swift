//
//  AddCardViewController.swift
//  GPDock
//
//  Created by TecOrb on 31/10/17.
//  Copyright © 2017 Nakul Sharma. All rights reserved.
//

import UIKit
import Stripe

class AddCardViewController : UIViewController {
    @IBOutlet weak var cardNumberTextField : BKCardNumberField!
    @IBOutlet weak var cardHolderNameTextField : UITextField!
    @IBOutlet weak var ccvTextField : UITextField!
    @IBOutlet weak var expTextField : BKCardExpiryField!
    @IBOutlet weak var containnerView : UIView!
    @IBOutlet weak var payButton : UIButton!
    @IBOutlet weak var zipCodeTextField : UITextField!


    override func viewDidLoad() {
        super.viewDidLoad()
        CardIOUtilities.preloadCardIO()
        self.setupAllView()
        self.navigateToScanCard()
    }
    override func viewDidLayoutSubviews() {
        self.payButton.applyGradient(withColours: [appColor.gradientStart,appColor.gradientEnd], gradientOrientation: .horizontal, locations: [0.0,1.0])
    }
    func setupAllView(){
        cardNumberTextField.showsCardLogo = true
        cardNumberTextField.cardNumberFormatter.groupSeparater = " "
        CommonClass.makeViewCircularWithCornerRadius(self.containnerView, borderColor: UIColor.groupTableViewBackground, borderWidth: 1, cornerRadius: 2)
        CommonClass.makeViewCircularWithCornerRadius(self.cardNumberTextField, borderColor: UIColor.groupTableViewBackground, borderWidth: 1, cornerRadius: 1)
        CommonClass.makeViewCircularWithCornerRadius(self.cardHolderNameTextField, borderColor: UIColor.groupTableViewBackground, borderWidth: 1, cornerRadius: 1)
        self.cardHolderNameTextField.setLeftPaddingPoints(5)
        CommonClass.makeViewCircularWithCornerRadius(self.ccvTextField, borderColor: UIColor.groupTableViewBackground, borderWidth: 1, cornerRadius: 1)
        self.ccvTextField.setLeftPaddingPoints(5)

        CommonClass.makeViewCircularWithCornerRadius(self.expTextField, borderColor: UIColor.groupTableViewBackground, borderWidth: 1, cornerRadius: 1)
        self.expTextField.setLeftPaddingPoints(5)
        
        CommonClass.makeViewCircularWithCornerRadius(self.zipCodeTextField, borderColor: UIColor.groupTableViewBackground, borderWidth: 1, cornerRadius: 1)
        self.zipCodeTextField.setLeftPaddingPoints(5)


        CommonClass.makeViewCircularWithCornerRadius(self.payButton, borderColor: UIColor.clear, borderWidth: 0, cornerRadius: 2)
    }

    @IBAction func onClickScanCard(_ sender: UIButton){
        self.navigateToScanCard()
    }

    func navigateToScanCard(){
        let scanCardVC = AppStoryboard.Settings.viewController(ScanCardViewController.self)
        scanCardVC.delegate = self
        let nav = self.getNavigationController(with: scanCardVC)
        self.navigationController?.present(nav, animated: true, completion: nil)
    }

    func getNavigationController(with rootViewController: UIViewController) -> UINavigationController{
        let nav = UINavigationController(rootViewController: rootViewController)
        nav.navigationBar.isTranslucent = false
        nav.navigationBar.isOpaque = true
        nav.navigationBar.barTintColor = .white
        nav.navigationBar.backgroundColor = .white
        nav.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.black]
        nav.navigationController?.present(nav, animated: true, completion: nil)
        nav.navigationBar.setBackgroundImage(UIImage(), for: .default)
        nav.navigationBar.shadowImage = UIImage()
        nav.navigationBar.isTranslucent = true
        let bounds = nav.navigationBar.bounds
        // make it cover the status bar by offseting y by -20
//        bounds.offsetBy(dx: 0.0, dy: -20.0)
//        bounds.size.height = bounds.height + 20.0
        let visualEffectView = UIVisualEffectView(frame: bounds)
        visualEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
       // nav.navigationBar.insertSubview(visualEffectView, at: 0)
        return nav
    }

    @IBAction func onClickBackButton(_ sender: UIBarButtonItem){
        self.navigationController?.pop(true)
    }

    @IBAction func onClickPayButton(_ sender: UIButton){
        //validate card params
        self.payButton.isEnabled = false
        self.view.endEditing(true)
        guard let cardNumber = self.cardNumberTextField.cardNumber  else {
            self.payButton.isEnabled = true
            return
        }
        if cardNumber.count < 14{
            NKToastHelper.sharedInstance.showAlert(self, title: warningMessage.title, message: warningMessage.validCardNumber.rawValue)
            self.payButton.isEnabled = true

            return
        }
        if cardHolderNameTextField.text == nil || cardHolderNameTextField.text?.count == 0{
            NKToastHelper.sharedInstance.showAlert(self, title: warningMessage.title, message: warningMessage.cardHolderName.rawValue)
            self.payButton.isEnabled = true

            return
        }
        guard let cardHolderName = self.cardHolderNameTextField.text?.trimmingCharacters(in: .whitespaces) else{
            NKToastHelper.sharedInstance.showAlert(self, title: warningMessage.title, message: warningMessage.cardHolderName.rawValue)

            self.payButton.isEnabled = true

            return
        }

        guard let brand = self.cardNumberTextField.cardNumberFormatter.cardPatternInfo.shortName  else {
            self.payButton.isEnabled = true
            NKToastHelper.sharedInstance.showAlert(self, title: warningMessage.title, message: warningMessage.cardDeclined.rawValue)
            self.payButton.isEnabled = true

            return
        }

        if ccvTextField.text == nil || ccvTextField.text!.count < 3 || ccvTextField.text!.count > 4 {
            NKToastHelper.sharedInstance.showAlert(self, title: warningMessage.title, message: warningMessage.enterCVV.rawValue)
            self.payButton.isEnabled = true

            return
        }
        guard let cvv = self.ccvTextField.text?.trimmingCharacters(in: .whitespaces) else{
            NKToastHelper.sharedInstance.showAlert(self, title: warningMessage.title, message: warningMessage.enterCVV.rawValue)
            self.payButton.isEnabled = true

            return
        }
        guard let expMonth = self.expTextField.dateComponents.month else{
            NKToastHelper.sharedInstance.showAlert(self, title: warningMessage.title, message: warningMessage.expMonth.rawValue)
            self.payButton.isEnabled = true

            return
        }
        guard let expYear = self.expTextField.dateComponents.year else{
            NKToastHelper.sharedInstance.showAlert(self, title: warningMessage.title, message: warningMessage.expYear.rawValue)
            self.payButton.isEnabled = true

            return
        }
        
        if zipCodeTextField.text == nil || zipCodeTextField.text!.count < 5 || zipCodeTextField.text!.count > 5{
            NKToastHelper.sharedInstance.showAlert(self, title: warningMessage.title, message: warningMessage.enterZipCode.rawValue)
            self.payButton.isEnabled = true
            
            return
        }
        
        guard let zip = self.zipCodeTextField.text?.trimmingCharacters(in: .whitespaces) else{
            NKToastHelper.sharedInstance.showAlert(self, title: warningMessage.title, message: warningMessage.validZipCode.rawValue)
            
            self.payButton.isEnabled = true
            
            return
        }
        

        let inputValidation = self.validteCardInputs(cardNumber, cardHolderName: cardHolderName, brand: brand, expMonth: expMonth, expYear: expYear, CVV: cvv, zipCode: zip)
        if !inputValidation.success{
            NKToastHelper.sharedInstance.showAlert(self, title: warningMessage.title, message: inputValidation.message)
            self.payButton.isEnabled = true
            return
        }

        let success = self.validateCardParams(cardNumber, cardHolderName: cardHolderName, expMonth: UInt(expMonth ), expYear: UInt(expYear ), CVV: cvv, zipCode: zip)
        if !success{
            NKToastHelper.sharedInstance.showAlert(self, title: warningMessage.title, message: warningMessage.cardDeclined.rawValue)
            self.payButton.isEnabled = true

            return
        }
        
        if !AppSettings.isConnectedToNetwork{
            NKToastHelper.sharedInstance.showErrorAlert(self, message: warningMessage.networkIsNotConnected.rawValue)
            self.payButton.isEnabled = true
            return
        }

        self.createTokenWith(cardNumber, cardHolderName: cardHolderName, expMonth: UInt(expMonth), expYear: UInt(expYear), CVV: cvv, zipCode: zip)
    }

    func createTokenWith(_ cardNumber:String,cardHolderName:String,expMonth: UInt,expYear:UInt,CVV:String,zipCode:String){

        AppSettings.shared.showLoader(withStatus: "Creating..")
        let cardParams = STPCardParams()
        cardParams.number = cardNumber
        if cardHolderName.count != 0{
            cardParams.name = cardHolderName
        }
        cardParams.expMonth = expMonth
        cardParams.expYear = expYear
        cardParams.cvc = CVV
        cardParams.address.postalCode = zipCode

        STPAPIClient.shared().createToken(withCard: cardParams) { (stpToken, error) in
            if error != nil{
                AppSettings.shared.hideLoader()
                NKToastHelper.sharedInstance.showErrorAlert(self, message:"\(String(describing: error!.localizedDescription))" )
                self.payButton.isEnabled = true
                return
            }else{
                guard let token = stpToken else{
                    AppSettings.shared.hideLoader()
                    NKToastHelper.sharedInstance.showErrorAlert(self, message: warningMessage.cardDeclined.rawValue)
                    self.payButton.isEnabled = true
                    return
                }
                AppSettings.shared.updateLoader(withStatus: "Saving..")
                self.createCardFor(sourceToken: token.tokenId)
            }
        }
    }


    func validteCardInputs(_ cardNumber:String,cardHolderName:String,brand:String,expMonth: Int?,expYear:Int?,CVV:String,zipCode:String) -> (success:Bool,message:String) {
        if cardNumber.count == 0{
            return(false,warningMessage.validCardNumber.rawValue)
        }

        if cardHolderName.count == 0{//Please enter the card holder's name
            return(false,warningMessage.cardHolderName.rawValue)
        }

        if let expMon = expMonth{
            if (expMon < 1) || (expMon > 12){return(false,warningMessage.validExpMonth.rawValue)}
        }else{
            return(false,warningMessage.expMonth.rawValue)
        }

        if let expY = expYear{
            if (expY < Date().year){return(false,warningMessage.validExpYear.rawValue)}
        }else{
            return(false,warningMessage.expYear.rawValue)
        }

        if !brand.lowercased().contains("maestro"){
            if (CVV.count < 3) || (CVV.count > 4){return(false,warningMessage.enterCVV.rawValue)}
        }
        
        if zipCode.count == 0 {
            return(false,warningMessage.validZipCode.rawValue)
        }
        return(true,"")

    }

    func validateCardParams(_ cardNumber:String,cardHolderName:String,expMonth: UInt,expYear:UInt,CVV:String,zipCode:String) -> Bool {
        let cardParams = STPCardParams()
        cardParams.number = cardNumber
        if cardHolderName.count != 0{
            cardParams.name = cardHolderName
        }
        cardParams.expMonth = expMonth
        cardParams.expYear = expYear
        cardParams.cvc = CVV
        cardParams.address.postalCode = zipCode
        let validationState = STPCardValidator.validationState(forCard: cardParams)
        return (validationState != .invalid)
    }

    func createCardFor(sourceToken:String){
        PaymentService.sharedInstance.addCard(sourceToken) { (success, resCard,message)  in
            AppSettings.shared.hideLoader()
            if success{
                if let aCard = resCard{
                    NotificationCenter.default.post(name: .USER_DID_ADD_NEW_CARD_NOTIFICATION, object: nil, userInfo: ["card":aCard])
                }else{
                    self.payButton.isEnabled = true
                    NKToastHelper.sharedInstance.showErrorAlert(self, message: message)
                }
            }else{
                self.payButton.isEnabled = true
                NKToastHelper.sharedInstance.showErrorAlert(self, message: message)
            }
        }
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

extension AddCardViewController: ScanCardViewControllerDelegate{
    func cardDidScan(viewcontroller: ScanCardViewController, withCardInfo cardInfo: CardIOCreditCardInfo) {
        if let cardNumber = cardInfo.cardNumber{
            self.cardNumberTextField.cardNumber = cardNumber
        }else{
            self.cardNumberTextField.cardNumber = ""
        }
        if let ccv = cardInfo.cvv{
            self.ccvTextField.text = ccv
        }else{
            self.ccvTextField.text = ""
        }
        if let cardholderName = cardInfo.cardholderName{
            self.cardHolderNameTextField.text = cardholderName
        }else{
            self.cardHolderNameTextField.text = ""
        }

        if let expiryMonth = cardInfo.expiryMonth as UInt?{
            if expiryMonth > 0{
                self.expTextField.dateComponents.month = Int(expiryMonth)
            }else{
                self.expTextField.text = ""
            }
        }else{
            self.expTextField.text = ""
        }
        if let expiryYear = cardInfo.expiryYear as UInt?{
            if expiryYear > 0{
                self.expTextField.dateComponents.year = Int(expiryYear)
            }else{
                self.expTextField.text = ""
            }
        }else{
            self.expTextField.text = ""
        }
        
        if let zipcCode = cardInfo.postalCode{
            self.zipCodeTextField.text = zipcCode
        }else{
            self.zipCodeTextField.text = ""
        }
    }

    func cardDidScan(viewcontroller: ScanCardViewController, isUserChooseManual wantManual: Bool) {
        if wantManual{
            self.cardNumberTextField.cardNumber = ""
            self.ccvTextField.text = ""
            self.cardHolderNameTextField.text = ""
            self.expTextField.text = ""
            self.zipCodeTextField.text = ""

        }
    }

}


