//
//  PayViewController.swift
//  TipNTap
//
//  Created by TecOrb on 05/03/18.
//  Copyright © 2018 Nakul Sharma. All rights reserved.
//

import UIKit
import ContactsUI


let maxTrasferValue:Double = 99999.0
//let serviceFee = 0.30

class PayViewController: UIViewController {

    var user: User!
    var payeeUser: User!
    var isManually:Bool = false
    var isFilledReference = false
    var selectedOffer: Offer?
    @IBOutlet weak var mobileNumberLabel: UILabel!
    @IBOutlet weak var bnfNameLabel: UILabel!

    @IBOutlet weak var sendButton: UIButton!
    @IBOutlet weak var offerButton: UIButton!
    @IBOutlet weak var selectedOfferTableview: UITableView!

    @IBOutlet private weak var amountContainer: UIView!
    @IBOutlet private weak var amountTextField: FloatLabelTextField!
    @IBOutlet private weak var descriptionTextView: FloatLabelTextView!
    @IBOutlet weak var isAppliedLabel: UILabel!

    var toolSendButton:UIButton!
    var toolBar:UIToolbar!
    var toolBarDidSet = false
    override func viewDidLoad() {
        super.viewDidLoad()
        self.isAppliedLabel.isHidden = (self.selectedOffer == nil)
        self.selectedOfferTableview.register(UINib.init(nibName: "ApplyingOfferCell", bundle: nil), forCellReuseIdentifier: "ApplyingOfferCell")
        self.selectedOfferTableview.tableFooterView = UIView(frame: CGRect.zero)
        self.selectedOfferTableview.estimatedRowHeight = 90.0;
        self.selectedOfferTableview.rowHeight = UITableViewAutomaticDimension;
        self.selectedOfferTableview.dataSource = self
        self.selectedOfferTableview.delegate = self

        self.user = User.loadSavedUser()
        self.mobileNumberLabel.text = (!isFilledReference && isManually) ? ("+"+self.payeeUser.countryCode+self.payeeUser.contact) : self.payeeUser.accountReference
        self.bnfNameLabel.text = self.payeeUser.name
        self.amountTextField.titleFont = fonts.Raleway.regular.font(.small)
        self.descriptionTextView.titleFont = fonts.Raleway.regular.font(.small)
        self.amountTextField.delegate = self
        self.amountTextField.addTarget(self, action: #selector(textDidChagedOnEditting(_:)), for: .editingChanged)
        self.setupToolBarButton()
    }

    override func viewDidAppear(_ animated: Bool) {

    }

    override func viewDidLayoutSubviews() {
        CommonClass.makeViewCircularWithCornerRadius(self.amountContainer, borderColor: appColor.lightGray, borderWidth: 1.5, cornerRadius: 0)
        self.amountContainer.addshadow(top: true, left: false, bottom: true, right: false,shadowRadius: 10)

        self.sendButton.applyGradient(withColours: [appColor.gradientStart,appColor.gradientEnd], gradientOrientation: .topLeftBottomRight)
        CommonClass.makeViewCircularWithCornerRadius(self.offerButton, borderColor: .clear, borderWidth: 0, cornerRadius: 4)
        self.offerButton.applyGradient(withColours: [appColor.gradientStart,appColor.gradientEnd], gradientOrientation: .horizontal)

        if toolBarDidSet{
            self.toolBar.applyGradient(withColours: [appColor.gradientStart,appColor.gradientEnd], gradientOrientation: .topLeftBottomRight)
            toolBar.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 50)
        }
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


    private func setupToolBarButton() {
        toolSendButton = UIButton(type: .custom)
        toolSendButton.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width+100, height: 50)
        toolSendButton.backgroundColor = UIColor.clear
        toolSendButton.setTitleColor(.white, for: UIControlState())
        toolSendButton.setTitle("Send $0.00", for: UIControlState())
        toolSendButton.titleLabel?.font = fonts.Raleway.bold.font(.xLarge)
        toolSendButton.autoresizingMask = [.flexibleWidth]
        self.toolSendButton.addTarget(self, action: #selector(PayViewController.onClickSend(_:)), for: .touchUpInside)

        self.toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = false
        toolBar.isOpaque = true
        toolBar.tintColor = .white
        toolBar.barTintColor = .white
        toolBar.backgroundColor = .white
        toolBar.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width+100, height: 50)
        let doneButton = UIBarButtonItem(customView: self.toolSendButton)
        self.toolBar.applyGradient(withColours: [appColor.gradientStart,appColor.gradientEnd], gradientOrientation: .topLeftBottomRight)

        toolBar.setItems([doneButton], animated: true)
        toolBar.autoresizingMask = [.flexibleWidth]

        toolBar.isUserInteractionEnabled = true
        self.amountTextField.inputAccessoryView = toolBar
        self.toolBarDidSet = true
        self.toolBar.setNeedsLayout()
        self.toolBar.layoutIfNeeded()
    }

    func refreshAmountSendButtons(withAmount amount: String){
        if amount.isEmpty{
            //self.amountTextField.text = ""
            if toolBarDidSet{
                self.toolSendButton.setTitle("Send $"+"0.00", for: UIControlState())
                self.sendButton.setTitle("Send $"+"0.00", for: UIControlState())
            }else{
                self.sendButton.setTitle("Send $"+"0.00", for: UIControlState())
            }
        }else{
            if let doubleAmount = Double(amount){
                let totalAmount = doubleAmount + (doubleAmount*self.payeeUser.adminPercentage) + self.payeeUser.paymentServiceFee
                let sendingAmount = String(format:"%0.2f",totalAmount)
                if toolBarDidSet{
                    self.toolSendButton.setTitle("Send $"+sendingAmount, for: UIControlState())
                    self.sendButton.setTitle("Send $"+sendingAmount, for: UIControlState())
                }else{
                    self.sendButton.setTitle("Send $"+sendingAmount, for: UIControlState())
                }
            }
        }

    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }

    @IBAction func onClickBackButton(_ sender: UIBarButtonItem){
        self.goOut()
    }


    @IBAction func onClickContactsButton(_ sender: UIBarButtonItem){
        let contactPicker = CNContactPickerViewController()
        contactPicker.delegate = self
        self.present(contactPicker, animated: true, completion: nil)
    }

    @IBAction func onClickOffers(_ sender: UIButton){
        self.view.endEditing(true)
        let offersVC = AppStoryboard.Scanner.viewController(OffersViewController.self)
        offersVC.delegate = self
        self.navigationController?.pushViewController(offersVC, animated: true)
    }

    @IBAction func onClickSend(_ sender: UIButton){
        self.view.endEditing(true)
        if !AppSettings.isConnectedToNetwork{
            return
        }
        if let _amountPayble = self.amountTextField.text{
            guard let amount = Double(_amountPayble) else{
                NKToastHelper.sharedInstance.showAlert(self, title: warningMessage.title, message: "Please enter a valid amount")
                return
            }

            let validation = self.validateParams(amount: amount, payee: self.payeeUser)
            if !validation.result{
                NKToastHelper.sharedInstance.showAlert(self, title: warningMessage.title, message: validation.message)
                return
            }

            if let offer = self.selectedOffer{
                let minAmount = offer.minPrice
                let tipAmount = amount// - (amount*0.04)
                if tipAmount < minAmount{
                    self.showMinimumAmountForCouponCodeAlert(payee: self.payeeUser, amount: amount, message: self.descriptionTextView.text)
                }else{
                    self.payAmountTo(payee: self.payeeUser, amount: amount, message: self.descriptionTextView.text,couponcode: self.selectedOffer?.coupanCode)
                }
            }else{
                self.payAmountTo(payee: self.payeeUser, amount: amount, message: self.descriptionTextView.text)
            }
        }else{
            NKToastHelper.sharedInstance.showAlert(self, title: warningMessage.title, message: "Please enter a valid amount")
        }
    }

    func validateParams(amount:Double,payee:User) -> (result:Bool,message:String){
        if amount <= 0.0{
            return (false,"Please enter a valid amount")
        }
        if payee.ID.isEmpty{
            return (false,"Please enter a valid payee")
        }
        return (true,"")
    }

    private func payAmountTo(payee: User,amount:Double,message:String,couponcode:String? = nil) -> Void {
        
        let paymentOptionVC = AppStoryboard.Scanner.viewController(PaymentOptionViewController.self)
        paymentOptionVC.amount = amount
        paymentOptionVC.paymentNote = message
        paymentOptionVC.payeeUser = payee
        paymentOptionVC.couponCode = couponcode
        self.navigationController?.pushViewController(paymentOptionVC, animated: true)
    }
}


extension PayViewController:UITextFieldDelegate{
    @IBAction func textDidChagedOnEditting(_ textField: UITextField){
        if textField == self.amountTextField{
            if let amount = textField.text{
                self.refreshAmountSendButtons(withAmount: amount)
            }
        }
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == self.amountTextField{
            return (CommonClass.validateMaxValue(textField, maxValue: maxTrasferValue, range: range, replacementString: string) && self.validateDecimal(textField, range: range, replacementString: string))
        }
        return true
    }

    func validateDecimal(_ textField: UITextField, range: NSRange, replacementString string: String) -> Bool{
        let newString = (textField.text! as NSString).replacingCharacters(in: range, with: string)
        let decimalRegex = try! NSRegularExpression(pattern: "^\\d*\\.?\\d{0,2}$", options: [])
        let matches = decimalRegex.matches(in: newString, options: [], range: NSMakeRange(0, newString.count))
        if matches.count == 1
        {
            //self.refreshAmountSendButtons(withAmount: newString)
            return true
        }
        return false
    }


    func validateUserInput( string: String) -> Bool{
        let newString = string
        //if delete all characteres from textfield
        if(newString.isEmpty) {
            return false
        }
        //check if the string is a valid input
        let result = CommonClass.validateUserInputForPay(newString)
        return result
    }


    func findPayeeWith(countryCode: String, contactNumber: String,accountReference:String){
        self.isFilledReference = false
        self.isManually = true
        PaymentService.sharedInstance.findPayeeFor(countryCode: countryCode, contactNumber: contactNumber, accountReferenceNumber: "") { (success, resPayeeUser, message) in
            if success{
                if let _payee = resPayeeUser{
                    self.payeeUser = _payee
                    self.mobileNumberLabel.text = (!self.isFilledReference && self.isManually) ? ("+"+self.payeeUser.countryCode+self.payeeUser.contact) : self.payeeUser.accountReference
                    self.bnfNameLabel.text = self.payeeUser.name
                }else{
                    self.showInvitationAlert()
                }
            }else{
                self.showInvitationAlert()
            }
        }
    }



    private func showInvitationAlert(){
        let alert = UIAlertController(title: warningMessage.title.rawValue, message: "We didn't find a payee with given details, would you like to invite?", preferredStyle: .alert)
        let inviteAction = UIAlertAction(title: "Invite Now", style: .cancel) { (action) in
            self.inviteUser()
        }

        let letItGo = UIAlertAction(title: "Cancel", style: .default) { (action) in
            self.goOut()
        }
        alert.addAction(inviteAction)
        alert.addAction(letItGo)

        self.present(alert, animated: true, completion: nil)
    }

    private func showMinimumAmountForCouponCodeAlert(payee: User,amount:Double,message:String){
        let alertmessage = "To get the benifit of selected offer, you should tranfer a minimum amount of $\(self.selectedOffer?.minPrice ?? 0.0). Would like to proceed without applying coupon code \(self.selectedOffer?.coupanCode ?? " ")?"
        let alert = UIAlertController(title: warningMessage.title.rawValue, message: alertmessage, preferredStyle: .alert)
        let proceedAction = UIAlertAction(title: "Proceed Anyway", style: .destructive) { (action) in
            self.payAmountTo(payee: payee, amount: amount, message:message)
        }

        let editAmount = UIAlertAction(title: "Edit Amount", style: .default) { (action) in
            alert.dismiss(animated: true, completion: {
                self.amountTextField.becomeFirstResponder()
            })
        }
        alert.addAction(proceedAction)
        alert.addAction(editAmount)
        self.present(alert, animated: true, completion: nil)
    }

    func inviteUser(){
       let text = "I’m inviting you to join TipNTap, a simple and secure payments app. Here’s my code \(self.user.accountReference) - just enter it before your first transaction. Once you’ve sent your first payment, we’ll each get $1! https://g.co/tipntap/\(self.user.accountReference)"

        let activityVC = UIActivityViewController(activityItems: [text], applicationActivities: nil)
        activityVC.popoverPresentationController?.sourceView = self.view
        activityVC.completionWithItemsHandler = {(acttype, done, response, error) in
            self.goOut()
        }
        self.present(activityVC, animated: true, completion: nil)
    }

    func goOut(){
        if let vcs = self.navigationController?.viewControllers{
            if let rootscvc = vcs.filter({ (scvc) -> Bool in
                return scvc is RootScannerViewController
            }).first{
                self.navigationController?.popToViewController(rootscvc, animated: true)
            }
        }
    }
}

extension PayViewController: CNContactPickerDelegate{
    func contactPickerDidCancel(_ picker: CNContactPickerViewController) {
        picker.dismiss(animated: true, completion: nil)
    }

    func contactPicker(_ picker: CNContactPickerViewController, didSelect contactProperty: CNContactProperty) {
        picker.dismiss(animated: true, completion: nil)
        if let thePhoneNumber = contactProperty.value as? CNPhoneNumber{
            let pickedPhone = thePhoneNumber.stringValue.removingWhitespaces()
            let userInputValidation = self.validateUserInput(string: pickedPhone)
            if userInputValidation{
                self.mobileNumberLabel.text = pickedPhone
                self.bnfNameLabel.text = contactProperty.contact.givenName
                self.isFilledReference = false
                self.isManually = true
                self.findPayeeWith(countryCode: self.user.countryCode, contactNumber: pickedPhone, accountReference: "")
            }else{
                NKToastHelper.sharedInstance.showErrorAlert(self, message: "Please enter mobile number or account reference")
            }
        }else{
            return
        }
    }

}

extension PayViewController: UITableViewDataSource,UITableViewDelegate,OffersViewControllerDelegate{
    func offersViewController(viewController: OffersViewController, didSelectOffer offer: Offer) {
        self.selectedOffer = offer
        self.isAppliedLabel.isHidden = (self.selectedOffer == nil)
        self.selectedOfferTableview.reloadData()
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return (self.selectedOffer != nil) ? 1 : 0
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (self.selectedOffer != nil) ? 1 : 0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ApplyingOfferCell", for: indexPath) as! ApplyingOfferCell
        cell.offerTitleLabel.text = self.selectedOffer?.title
        cell.offerDetailsLabel.text = self.selectedOffer?.details
        cell.couponCodeLabel.text = self.selectedOffer?.coupanCode
        cell.removeButton.addTarget(self, action: #selector(PayViewController.onClickRemoveOffer(_:)), for: .touchUpInside)
        cell.setNeedsLayout()
        cell.layoutIfNeeded()
        return cell
    }

    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        guard let aCell = cell as? ApplyingOfferCell else{
            return
        }
        aCell.setNeedsLayout()
        aCell.layoutIfNeeded()
        aCell.innerContentView.addshadow(top: true, left: true, bottom: true, right: true,shadowOpacity: 0.4)
        aCell.setNeedsLayout()
        aCell.layoutIfNeeded()
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }

    @IBAction func onClickRemoveOffer(_ sender:UIButton){
        self.selectedOffer = nil
        self.isAppliedLabel.isHidden = (self.selectedOffer == nil)
        self.selectedOfferTableview.reloadData()
    }
}







extension String {
    func removingWhitespaces() -> String {
        let customCharSet = CharacterSet(charactersIn: " -()")
//        let stringAfterRemovingDashes = self.components(separatedBy: "-").joined()
        return components(separatedBy: customCharSet).joined()
    }
}


