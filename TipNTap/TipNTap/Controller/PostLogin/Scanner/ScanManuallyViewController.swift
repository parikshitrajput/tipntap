//
//  ScanManuallyViewController.swift
//  GPDock Merchant
//
//  Created by TecOrb on 22/11/17.
//  Copyright © 2017 Nakul Sharma. All rights reserved.
//

import UIKit
import  Contacts
import ContactsUI
import Firebase
import Messages
import MessageUI
class ScanManuallyViewController: UIViewController {

    var user: User!

    @IBOutlet weak var mobileNumberTextField: FloatLabelTextField!
    @IBOutlet weak var submitButton: UIButton!
    @IBOutlet private weak var mobileNumberView: UIView!

    var toolSubmitButton:UIButton!
    var toolBar:UIToolbar!
    var toolBarDidSet = false
    var isFilledReference = false

    var bnfName: String = ""
    var bnfMobileNumber:String!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.user = User.loadSavedUser()
        self.setupToolBarButton()
        self.mobileNumberTextField.delegate = self
        self.mobileNumberTextField.titleFont = fonts.Raleway.regular.font(.small)

    }

    override func viewDidAppear(_ animated: Bool) {
        if let mobileNumber = self.mobileNumberTextField.text{
            if mobileNumber.isEmpty{
                self.mobileNumberTextField.becomeFirstResponder()
            }
        }

    }

    override func viewDidLayoutSubviews() {
        CommonClass.makeViewCircularWithCornerRadius(self.mobileNumberView, borderColor: appColor.lightGray, borderWidth: 1.5, cornerRadius: 0)
        self.mobileNumberView.addshadow(top: true, left: false, bottom: true, right: false,shadowRadius: 15)
        self.submitButton.applyGradient(withColours: [appColor.gradientStart,appColor.gradientEnd], gradientOrientation: .topLeftBottomRight)

        if toolBarDidSet{
            self.toolBar.applyGradient(withColours: [appColor.gradientStart,appColor.gradientEnd], gradientOrientation: .topLeftBottomRight)
            toolBar.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 50)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


    private func setupToolBarButton() {

        toolSubmitButton = UIButton(type: .custom)
        toolSubmitButton.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width+100, height: 50)
        toolSubmitButton.backgroundColor = UIColor.clear
        toolSubmitButton.setTitleColor(.white, for: UIControlState())
        toolSubmitButton.setTitle("Proceed", for: UIControlState())
        toolSubmitButton.titleLabel?.font = fonts.Raleway.bold.font(.xLarge)
        toolSubmitButton.autoresizingMask = [.flexibleWidth]
        self.toolSubmitButton.addTarget(self, action: #selector(ScanManuallyViewController.onClickSubmit(_:)), for: .touchUpInside)

        self.toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = false
        toolBar.isOpaque = true
        toolBar.tintColor = .white
        toolBar.barTintColor = .white
        toolBar.backgroundColor = .white
        toolBar.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width+100, height: 50)
        let doneButton = UIBarButtonItem(customView: self.toolSubmitButton)
        self.toolBar.applyGradient(withColours: [appColor.gradientStart,appColor.gradientEnd], gradientOrientation: .topLeftBottomRight)

        toolBar.setItems([doneButton], animated: true)
        toolBar.autoresizingMask = [.flexibleWidth]

        toolBar.isUserInteractionEnabled = true
        self.mobileNumberTextField.inputAccessoryView = toolBar
        self.toolBarDidSet = true
        self.toolBar.setNeedsLayout()
        self.toolBar.layoutIfNeeded()
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }

    @IBAction func onClickBackButton(_ sender: UIBarButtonItem){
        self.navigationController?.pop(false)
    }

    @IBAction func onClickContactsButton(_ sender: UIBarButtonItem){
        let contactPicker = CNContactPickerViewController()
        contactPicker.delegate = self
        self.present(contactPicker, animated: true, completion: nil)
    }

    func findPayeeWith(countryCode: String, contactNumber: String,accountReference:String){
        self.isFilledReference = !accountReference.isEmpty
        PaymentService.sharedInstance.findPayeeFor(countryCode: countryCode, contactNumber: contactNumber, accountReferenceNumber: accountReference) { (success, resPayeeUser, message) in
            if success{
                if let _payee = resPayeeUser{
                    self.mobileNumberTextField.text = self.isFilledReference ? _payee.accountReference : "+"+_payee.countryCode + _payee.contact
                    self.proceedToPayNow(payeeUser: _payee)
                }else{
                    self.showInvitationAlert(!self.isFilledReference)
                }
            }else{
                self.showInvitationAlert(!self.isFilledReference)
            }
        }
    }

    func showInvitationAlert(_ isMobileNumberEntered:Bool){
        let alert = UIAlertController(title: warningMessage.title.rawValue, message: "We didn't find a payee with given details, would you like to invite?", preferredStyle: .alert)

        if isMobileNumberEntered{
            let sendSMSAction = UIAlertAction(title: "Send SMS", style: .cancel) {(action) in
                self.precessToSendSMS()
                alert.dismiss(animated: false, completion: nil)
            }
            alert.addAction(sendSMSAction)
        }else{
            let inviteAction = UIAlertAction(title: "Invite Now", style: .cancel) { (action) in
                self.inviteUser()
                alert.dismiss(animated: false, completion: nil)
            }
            alert.addAction(inviteAction)
        }

        let letItGo = UIAlertAction(title: "Cancel", style: .default) { (action) in
            self.navigationController?.pop(false)
            alert.dismiss(animated: false, completion: nil)
        }
        alert.addAction(letItGo)
        self.present(alert, animated: true, completion: nil)
    }

    func inviteUser(){
        let inviteVC = AppStoryboard.Home.viewController(ReferAndEarnViewController.self)
        inviteVC.isFromMenu = false
        self.navigationController?.pushViewController(inviteVC, animated: true)
    }


    @IBAction func onClickSubmit(_ sender: UIButton){
        self.view.endEditing(true)
        if !AppSettings.isConnectedToNetwork{
            return
        }

        if let mobileNumberString = self.mobileNumberTextField.text{
            let mobileNumber = mobileNumberString.trimmingCharacters(in: .whitespaces)
            if mobileNumber.isEmpty{
                NKToastHelper.sharedInstance.showErrorAlert(self, message: "Please enter mobile number or account reference")
                return
            }
            let isMobile = CommonClass.validatePhoneNumber(mobileNumber)
            if isMobile{
                self.findPayeeWith(countryCode: self.user.countryCode, contactNumber: mobileNumber, accountReference: "")
            }else{
                self.findPayeeWith(countryCode: "", contactNumber: "", accountReference: mobileNumber)
            }

        }
    }

    private func proceedToPayNow(payeeUser: User) -> Void {
        let payVC = AppStoryboard.Scanner.viewController(PayViewController.self)
        payVC.payeeUser = payeeUser
        payVC.isManually = true
        payVC.isFilledReference = self.isFilledReference
        self.navigationController?.pushViewController(payVC, animated: true)
    }
}


extension ScanManuallyViewController:UITextFieldDelegate{

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        return self.validateUserInput(textField, range: range, replacementString: string)
    }

    func validateUserInput(_ textField: UITextField, range: NSRange, replacementString string: String) -> Bool{

        let newString = (textField.text! as NSString).replacingCharacters(in: range, with: string)
        //if delete all characteres from textfield
        if(newString.isEmpty) {
            return true
        }
        //check if the string is a valid input
        let result = CommonClass.validateUserInputForPay(newString)
        return result
    }
}

extension ScanManuallyViewController: CNContactPickerDelegate{
    func contactPickerDidCancel(_ picker: CNContactPickerViewController) {
        picker.dismiss(animated: true, completion: nil)
    }

    func contactPicker(_ picker: CNContactPickerViewController, didSelect contactProperty: CNContactProperty) {
        self.mobileNumberTextField.text = ""
        self.bnfMobileNumber = ""
        self.bnfName = ""
        picker.dismiss(animated: true, completion: nil)
        var theContactName = contactProperty.contact.givenName
        if theContactName.isEmpty{
            theContactName = contactProperty.contact.familyName
        }

        self.bnfName = theContactName
        if let thePhoneNumber = contactProperty.value as? CNPhoneNumber{
            let pickedPhone = thePhoneNumber.stringValue.removingWhitespaces()
            self.mobileNumberTextField.text = pickedPhone
            self.bnfMobileNumber = pickedPhone
            self.findPayeeWith(countryCode: self.user.countryCode, contactNumber: pickedPhone, accountReference: "")
        }else{
            NKToastHelper.sharedInstance.showErrorAlert(self, message: "Couldn't fetch phone number")
        }
    }
}


extension ScanManuallyViewController{
    func precessToSendSMS(){
        if !AppSettings.isConnectedToNetwork{
            NKToastHelper.sharedInstance.showErrorAlert(self, message: warningMessage.networkIsNotConnected.rawValue)
            return
        }
        AppSettings.shared.showLoader(withStatus: "Please wait..")
        self.getReferralCodeFromServer()
    }

    func createShortUrl(referalCode:String,handler:@escaping (_ shortUrl: String?)->Void){
        guard let link = URL(string: (inviteLinkUrl+referalCode)) else {
            AppSettings.shared.hideLoader()
            return
        }
        let components = DynamicLinkComponents(link: link, domain: "ubg36.app.goo.gl")
        let options = DynamicLinkComponentsOptions()
        options.pathLength = .short
        components.options = options
        components.shorten(completion: { (url, warnings, error) in
            AppSettings.shared.hideLoader()
            if let error = error {
                NKToastHelper.sharedInstance.showErrorAlert(self, message:error.localizedDescription)
                return
            }
            handler(url?.absoluteString)
        })
    }


    func getReferralCodeFromServer(){
        self.user.getReferralCode { (success, resRefCode, message) in
            if success{
                guard let refCode = resRefCode else{
                    NKToastHelper.sharedInstance.showErrorAlert(self, message: message, completionBlock: {
                        self.showRetryAlert()
                    })
                    return
                }
                self.parseReferralCodeFrom(referralUrl: refCode)
            }else{
                NKToastHelper.sharedInstance.showErrorAlert(self, message: message, completionBlock: {
                    self.showRetryAlert()
                })
            }
        }
    }

    func parseReferralCodeFrom(referralUrl:String){
        let components = referralUrl.components(separatedBy: "/")
        if let refCode = components.last{
            self.createInviteLink(inviteCode: refCode)
        }
    }

    func createInviteLink(inviteCode:String){
        if !AppSettings.isConnectedToNetwork{
            NKToastHelper.sharedInstance.showErrorAlert(self, message: warningMessage.networkIsNotConnected.rawValue)
            return
        }

        AppSettings.shared.showLoader(withStatus: "Creating link..")
        self.createShortUrl(referalCode: inviteCode) { (shortUrl) in
            if let shortenUrl = shortUrl{
                let text = self.invitationText()
                let messageText = "TipNTap!"+"\r\n"+text+"\r\n"+shortenUrl
                //compose message
                guard let mobileNumber = self.mobileNumberTextField.text else{return}
                let fullMobileNumber = mobileNumber
                self.composeMessage(receipient: fullMobileNumber, text: messageText)
            }
        }
    }

    private func showRetryAlert(){
        let alert = UIAlertController(title: warningMessage.title.rawValue, message: "The operation was failed, would you like to retry?", preferredStyle: .alert)
        let retryAction = UIAlertAction(title: "Retry Now", style: .cancel) { (action) in
            alert.dismiss(animated: false, completion: nil)
            self.getReferralCodeFromServer()
        }

        let letItGo = UIAlertAction(title: "Leave It", style: .default) { (action) in
            self.navigationController?.pop(false)
        }

        alert.addAction(retryAction)
        alert.addAction(letItGo)
        self.present(alert, animated: true, completion: nil)
    }

    func invitationText()->String{
        let text =  "A simple and secure payment app by Tipntap Inc. I am sharing this invitation link. Register and download the application. Once you’ve sent your first payment we’ll each get $1."
        return text
    }


}
extension ScanManuallyViewController:MFMessageComposeViewControllerDelegate{
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        switch result {
        case .sent:
            controller.dismiss(animated: false){}
        case .failed:
            controller.dismiss(animated: false) {
                NKToastHelper.sharedInstance.showErrorAlert(nil, message: "Invitation to \(controller.recipients?.first ?? "receipeient") failed to send.")
            }
        case .cancelled:
            controller.dismiss(animated: false){}
        }
    }

    func composeMessage(receipient:String,text:String){
        if (MFMessageComposeViewController.canSendText()) {
            let controller = MFMessageComposeViewController()
            controller.body = text
//            controller.subject = "TipNTap Invitation"
            controller.recipients = [receipient]
            controller.messageComposeDelegate = self
            self.present(controller, animated: true, completion: nil)
        }else{
            NKToastHelper.sharedInstance.showErrorAlert(nil, message: "We are unable to send message right now. Please configure your device to send messages.")
        }

    }
}






