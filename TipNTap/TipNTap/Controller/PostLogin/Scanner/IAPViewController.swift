//
//  IAPViewController.swift
//  TinNTap
//
//  Created by TecOrb on 17/01/18.
//  Copyright © 2018 Nakul Sharma. All rights reserved.
//

import UIKit
import StoreKit

class IAPViewController: UIViewController {

    let productIDs = ["com.builditright.fishlocator.fishlocation"]


    var products = Array<SKProduct>()

    override func viewDidLoad() {
        super.viewDidLoad()
        NKIAPHandler.shared.fetchAvailableProducts(productIdentifiers: self.productIDs)
        NKIAPHandler.shared.avilableProductsBlock = {[weak self] (products) in
            if let someProducts = products{
                self?.products = someProducts
                //reload the list
            }
        }

        NKIAPHandler.shared.purchaseStatusBlock = {(alert,transactionID) in
            print_debug(alert.message())
        }
    }

    @IBAction func onclickpurchase(_ sender:UIButton){
        let product = self.products[0] // or any other location of the array
        NKIAPHandler.shared.purchaseProduct(product)
    }
    @IBAction func onclickRestore(_ sender:UIButton){
        NKIAPHandler.shared.restorePurchase()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
