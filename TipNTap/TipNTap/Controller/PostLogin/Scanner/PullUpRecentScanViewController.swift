//
//  PullUpRecentScanViewController.swift
//  TipNTap
//
//  Created by TecOrb on 28/02/18.
//  Copyright © 2018 Nakul Sharma. All rights reserved.
//

import UIKit
import ISHPullUp

protocol PullUpRecentScanViewControllerDelegate{
    func pullUpRecentScanViewController(_ viewController:PullUpRecentScanViewController,didSelectPayee payee:User)
}

class PullUpRecentScanViewController: UIViewController, ISHPullUpSizingDelegate, ISHPullUpStateDelegate {
    @IBOutlet private weak var handleView: ISHPullUpHandleView!
    @IBOutlet private weak var rootView: UIView!
    @IBOutlet private weak var aTableView: UITableView!
    @IBOutlet private weak var topView: UIView!

    var pullUpRecentScanViewControllerdelegate : PullUpRecentScanViewControllerDelegate?

    private var firstAppearanceCompleted = false
    weak var pullUpController: ISHPullUpViewController!
    var recentScans = Array<RecentScan>()
    var isLoading = true
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.backgroundColor = appColor.blue
        refreshControl.addTarget(self, action: #selector(PullUpRecentScanViewController.handleRefresh(_:)), for: UIControlEvents.valueChanged)
        refreshControl.backgroundColor = UIColor.white
        return refreshControl
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.registerCells()
        self.aTableView.addSubview(refreshControl)
        self.aTableView.dataSource = self
        self.aTableView.delegate = self
        self.getRecentScans()
        CommonClass.makeViewCircularWithCornerRadius(self.rootView, borderColor: UIColor.clear, borderWidth: 0, cornerRadius: 15)
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(handleTapGesture(_:)))
        topView.addGestureRecognizer(tapGesture)
        rootView.setNeedsLayout()
        rootView.layoutIfNeeded()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        firstAppearanceCompleted = true;
    }

    @objc func handleRefresh(_ sender: UIRefreshControl){
        self.getRecentScans()
    }

    @objc private  func handleTapGesture(_ gesture: UITapGestureRecognizer) {
        pullUpController.toggleState(animated: true)
    }

    // MARK: ISHPullUpSizingDelegate

    func pullUpViewController(_ pullUpViewController: ISHPullUpViewController, maximumHeightForBottomViewController bottomVC: UIViewController, maximumAvailableHeight: CGFloat) -> CGFloat {
        let totalHeight = rootView.systemLayoutSizeFitting(UILayoutFittingCompressedSize).height
        return totalHeight
    }

    func pullUpViewController(_ pullUpViewController: ISHPullUpViewController, minimumHeightForBottomViewController bottomVC: UIViewController) -> CGFloat {
        return topView.systemLayoutSizeFitting(UILayoutFittingCompressedSize).height;
    }

    func pullUpViewController(_ pullUpViewController: ISHPullUpViewController, targetHeightForBottomViewController bottomVC: UIViewController, fromCurrentHeight height: CGFloat) -> CGFloat {
        var targetedHeight :CGFloat = 0
        if handleView.state == .down{
            targetedHeight = topView.systemLayoutSizeFitting(UILayoutFittingCompressedSize).height
        }else if handleView.state == .up{
             targetedHeight = rootView.systemLayoutSizeFitting(UILayoutFittingCompressedSize).height
        }
        return targetedHeight
    }

    func pullUpViewController(_ pullUpViewController: ISHPullUpViewController, update edgeInsets: UIEdgeInsets, forBottomViewController bottomVC: UIViewController) {
        aTableView.contentInset = edgeInsets;
    }

    // MARK: ISHPullUpStateDelegate

    func pullUpViewController(_ pullUpViewController: ISHPullUpViewController, didChangeTo state: ISHPullUpState) {
        if state == .collapsed || state == .expanded{
            self.handleView.setState(ISHPullUpHandleView.handleState(for: state), animated: firstAppearanceCompleted)
        }

        UIView.animate(withDuration: 0.1) { [weak self] in
            self?.aTableView.alpha = (state == .collapsed) ? 0 : 1;
        }
    }


}


extension PullUpRecentScanViewController:UITableViewDataSource,UITableViewDelegate{

    func getRecentScans(){
        if !AppSettings.isConnectedToNetwork{
            NKToastHelper.sharedInstance.showErrorAlert(nil, message: warningMessage.networkIsNotConnected.rawValue)
            return
        }
        self.recentScans.removeAll()
        self.isLoading = true
        self.aTableView.reloadData()
        TransactionService.sharedInstance.getRecentScans { (success, resRecentScans, message) in
            self.isLoading = false
            self.refreshControl.endRefreshing()
            if success{
                if let someRecentScans = resRecentScans{
                    self.recentScans.append(contentsOf: someRecentScans)
                }
            }else{
                NKToastHelper.sharedInstance.showErrorAlert(nil, message: message)
            }
            self.aTableView.reloadData()
        }
        
    }

    func registerCells(){
        self.aTableView.register(UINib(nibName: "NoDataCell", bundle: nil), forCellReuseIdentifier: "NoDataCell")
        self.aTableView.register(UINib(nibName: "RecentScanCell", bundle: nil), forCellReuseIdentifier: "RecentScanCell")
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (self.recentScans.count==0) ? 1: self.recentScans.count;
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return  self.aTableView.frame.size.height/5//55
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if self.recentScans.count == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "NoDataCell", for: indexPath) as! NoDataCell
            cell.messageLabel.text = isLoading ? "Loading.." : "Please make your first payment"
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "RecentScanCell", for: indexPath) as! RecentScanCell
            let recentScan = self.recentScans[indexPath.row]
            cell.userImageView.setIndicatorStyle(.white)
            cell.userImageView.setShowActivityIndicator(true)
            cell.userImageView.sd_setImage(with: URL(string:recentScan.payee.profileImage) ?? URL(string:BASE_URL), placeholderImage:AppSettings.shared.userAvatarImage(username:recentScan.payee.name))

            cell.amountLabel.text = "$"+String(format:"%0.2f",recentScan.amount)
            cell.userNameLabel.text = recentScan.payee.name
            cell.setNeedsLayout()
            cell.layoutIfNeeded()
            return cell
        }
    }

    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if let aCell = cell as? RecentScanCell{
            aCell.setNeedsLayout()
            aCell.layoutIfNeeded()
            CommonClass.makeViewCircular(aCell.userImageView, borderColor: UIColor.clear, borderWidth: 0)
        }
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if self.recentScans.count != 0{
            let recentScan = self.recentScans[indexPath.row]
            self.pullUpController.toggleState(animated: true)
            self.pullUpRecentScanViewControllerdelegate?.pullUpRecentScanViewController(self, didSelectPayee: recentScan.payee)
        }
    }


}







