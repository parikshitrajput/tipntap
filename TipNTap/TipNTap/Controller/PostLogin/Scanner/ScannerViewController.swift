//
//  ScannerViewController.swift
//  GPDock Merchant
//
//  Created by TecOrb on 22/11/17.
//  Copyright © 2017 Nakul Sharma. All rights reserved.
//

import UIKit
import AVFoundation
import ISHPullUp


class ScannerViewController: UIViewController {
    var user: User!
    @IBOutlet weak var mobileNumberView: UIView!
    @IBOutlet weak var rootView: UIView!
    @IBOutlet weak var previewView: QRCodeReaderView!
    lazy var reader: QRCodeReader = QRCodeReader()

    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.user = User.loadSavedUser()
        self.startScanner()
        self.rootView.setNeedsLayout()
        self.rootView.layoutIfNeeded()
        //self.previewView.setNeedsLayout()
    }

    override func viewDidLayoutSubviews() {
        CommonClass.makeViewCircularWithCornerRadius(self.mobileNumberView, borderColor: appColor.lightGray, borderWidth: 1.5, cornerRadius: 0)
        self.mobileNumberView.addshadow(top: true, left: false, bottom: true, right: false,shadowRadius: 15)
    }

    override func viewWillAppear(_ animated: Bool) {
        self.reader.startScanning()
    }

    override func viewWillDisappear(_ animated: Bool) {
        self.reader.stopScanning()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func startScanner(){
        guard checkScanPermissions(), !reader.isRunning else { return }
        self.previewView.setupComponents(showCancelButton: false, showSwitchCameraButton: false, showTorchButton: false, showOverlayView: true, reader: reader)

        reader.startScanning()
        reader.didFindCode = { result in
            if let profileUrl = result.value as String?{
                let profileObjects = profileUrl.components(separatedBy: "/")
                if let accountReference = profileObjects.last{
                    self.findPayeeWithReferenceNumber(referenceNumber: accountReference)
                }
            }
        }
    }

    @IBAction func onClickBackButton(_ sender: UIBarButtonItem){
         self.reader.stopScanning()
        self.navigationController?.pop(true)
        
    }

    @IBAction func onClickQRCodeButton(_ sender: UIButton){
        self.reader.stopScanning()
        let manualScannerVC = AppStoryboard.Scanner.viewController(ScanManuallyViewController.self)
        self.navigationController?.pushViewController(manualScannerVC, animated: false)
    }


    func findPayeeWithReferenceNumber(referenceNumber:String){
        PaymentService.sharedInstance.findPayeeFor(countryCode: "", contactNumber: "", accountReferenceNumber: referenceNumber) { (success, resUser, message) in
            if success{
                if let _payee = resUser{
                    self.proceedToPayNow(payeeUser: _payee)
                }else{
                    self.showInvitationAlert()
                }
            }else{
                self.showInvitationAlert()
            }
        }
    }

    private func showInvitationAlert(){
        let alert = UIAlertController(title: warningMessage.title.rawValue, message: "We didn't find a payee with given details, would you like to invite?", preferredStyle: .alert)
        let inviteAction = UIAlertAction(title: "Invite Now", style: .cancel) { (action) in
            self.inviteUser()
            self.reader.startScanning()
        }

        let letItGo = UIAlertAction(title: "Cancel", style: .default) { (action) in
            self.navigationController?.pop(false)
            self.reader.startScanning()
            alert.dismiss(animated: false, completion: nil)
        }
        alert.addAction(inviteAction)
        alert.addAction(letItGo)

        self.present(alert, animated: true, completion: nil)
    }

    func inviteUser(){
        let inviteVC = AppStoryboard.Home.viewController(ReferAndEarnViewController.self)
        inviteVC.isFromMenu = false
        self.navigationController?.pushViewController(inviteVC, animated: true)
//        let text = "I’m inviting you to join TipNTap, a simple and secure payments app. Here’s my code \(self.user.accountReference) - just enter it before your first transaction. Once you’ve sent your first payment, we’ll each get $1! https://g.co/tipntap/\(self.user.accountReference)"
//
//        let activityVC = UIActivityViewController(activityItems: [text], applicationActivities: nil)
//        activityVC.popoverPresentationController?.sourceView = self.view
//        activityVC.completionWithItemsHandler = {(acttype, done, response, error) in
//            self.navigationController?.pop(true)
//        }
//        self.present(activityVC, animated: true, completion: nil)
    }

    private func proceedToPayNow(payeeUser: User) -> Void {
        let payVC = AppStoryboard.Scanner.viewController(PayViewController.self)
        payVC.payeeUser = payeeUser
        payVC.isManually = false
        payVC.isFilledReference = true
        self.navigationController?.pushViewController(payVC, animated: true)
    }

    
    private func showAlertWith(message:String,title:String){
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        let okayAction = UIAlertAction(title: "Okay", style: .default) { (action) in
            self.reader.stopScanning()
            self.navigationController?.popToRoot(true)
         
        }

        let subview = (alert.view.subviews.first?.subviews.first?.subviews.first!)! as UIView
        subview.backgroundColor = UIColor(red: 74.0/255.0, green: 90.0/255.0, blue: 95.0/255.0, alpha: 0.5)
        alert.view.tintColor = UIColor.white
        alert.addAction(okayAction)
        self.present(alert, animated: true, completion: nil)
    }

    // MARK: - Actions

    private func checkScanPermissions() -> Bool {
        do {
            return try QRCodeReader.supportsMetadataObjectTypes()
        } catch let error as NSError {
            let alert: UIAlertController?

            switch error.code {
            case -11852:
                alert = UIAlertController(title: warningMessage.title.rawValue, message: "This app is not authorized to use Back Camera.", preferredStyle: .alert)

                alert?.addAction(UIAlertAction(title: "Setting", style: .default, handler: { (_) in
                    DispatchQueue.main.async {
                        if let settingsURL = URL(string: UIApplicationOpenSettingsURLString) {
                            UIApplication.shared.open(settingsURL, options: [:], completionHandler: nil)
                        }
                    }
                }))

                alert?.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
            case -11814:
                alert = UIAlertController(title: warningMessage.title.rawValue, message: "Reader not supported by the current device", preferredStyle: .alert)
                alert?.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
            default:
                alert = nil
            }

            guard let vc = alert else { return false }

            present(vc, animated: true, completion: nil)

            return false
        }
    }
}

extension ScannerViewController:ISHPullUpContentDelegate,PullUpRecentScanViewControllerDelegate{
    // MARK: ISHPullUpContentDelegate
    func pullUpRecentScanViewController(_ viewController: PullUpRecentScanViewController, didSelectPayee payee: User) {
        if !payee.ID.isEmpty{
            self.proceedToPayNow(payeeUser: payee)
        }
    }

    func pullUpViewController(_ vc: ISHPullUpViewController, update edgeInsets: UIEdgeInsets, forContentViewController _: UIViewController) {
//        if #available(iOS 11.0, *) {
//            additionalSafeAreaInsets = edgeInsets
//            rootView.layoutMargins = .zero
//        } else {
//            // update edgeInsets
//            rootView.layoutMargins = edgeInsets
//        }
        // call layoutIfNeeded right away to participate in animations
        // this method may be called from within animation blocks
       // rootView.layoutIfNeeded()
    }
}




class RootScannerViewController: ISHPullUpViewController {

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }

    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        commonInit()
    }

    @IBAction func onClcikBack(_ sender: UIBarButtonItem){
        self.navigationController?.pop(true)
    }

    private func commonInit() {
        let scannerVC = AppStoryboard.Scanner.viewController(ScannerViewController.self)
        let recentScansVC = AppStoryboard.Scanner.viewController(PullUpRecentScanViewController.self)
        recentScansVC.pullUpRecentScanViewControllerdelegate = scannerVC
        contentViewController = scannerVC
        bottomViewController = recentScansVC
        self.snapToEnds = true
        self.snapThreshold = 0.25
        recentScansVC.pullUpController = self
        contentDelegate = scannerVC
        sizingDelegate = recentScansVC
        stateDelegate = recentScansVC
        scannerVC.view.setNeedsLayout()
    }

}


