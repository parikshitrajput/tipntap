//
//  LeftMenuViewController.swift
//  TipNTap
//
//  Created by TecOrb on 22/01/18.
//  Copyright © 2018 Nakul Sharma. All rights reserved.
//

import UIKit
import LGSideMenuController


class LeftMenuViewController: UIViewController, UITableViewDataSource,UITableViewDelegate  {

    @IBOutlet weak var tableView: UITableView!
    let leftMenuOptions = ["Dashboard","MY Payouts","My Cards","Bank Account","Settings","Refer & Earn","Privacy Policy","Support"]
    let leftMenuIcons = ["dashboard","payout","my_card","bank_account","setting","refer_earn","privacy_policy","my_support"]
    var user : User!

    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(userDidLoggedIn(_:)), name: .USER_DID_LOGGED_IN_NOTIFICATION, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(userDidLoggedIn(_:)), name: .USER_DID_UPDATE_PROFILE_NOTIFICATION, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(slideNavigationDidClose(_:)), name: .GPDockSlideNavigationControllerDidClose, object: nil)

        self.user = User.loadSavedUser()

        self.tableView.separatorColor = UIColor.clear
        self.tableView.tableFooterView = UIView(frame: .zero)
        self.tableView.contentInset = UIEdgeInsetsMake(40, 0, 0, 0)
        self.automaticallyAdjustsScrollViewInsets = false
        self.tableView.delegate = self
    }

    deinit{
        NotificationCenter.default.removeObserver(self)
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if tableView != nil{
            self.tableView.reloadData()
        }
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return leftMenuOptions.count;
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return  70
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "LeftMenuCell", for: indexPath) as! LeftMenuCell
        cell.cellIcon.image = UIImage(named:leftMenuIcons[indexPath.row])
        cell.nameLabel.text = leftMenuOptions[indexPath.row]
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath)!
        let colorView = UIView()
        colorView.backgroundColor = UIColor.white.withAlphaComponent(0.3)
        cell.selectedBackgroundView = colorView
        cell.selectedBackgroundView = nil

        guard let mainViewController = sideMenuController else {
            return
        }
        var navigationController: UINavigationController = HomeNavigationController()
        switch indexPath.row {
        case 0://Dashboard
            navigationController = AppStoryboard.Home.viewController(HomeNavigationController.self)
        case 1://MyPayout
            navigationController = AppStoryboard.Transactions.viewController(PayoutNavigationController.self)
        case 2://MyCard
            navigationController = AppStoryboard.MyCards.viewController(MyCardsNavigationController.self)
        case 3://Add Bank Account
            navigationController = AppStoryboard.BankAccount.viewController(BankAccountsNavigationViewController.self)
        case 4://Settings
            navigationController = AppStoryboard.Settings.viewController(SettingsNavigationController.self)
        case 5://refer and earn
            navigationController = AppStoryboard.Home.viewController(ReferAndEarnNavigationController.self)
        case 6://Privacy Policy
            navigationController = AppStoryboard.Settings.viewController(PrivacyPolicyNavigationController.self)
        case 7://Support
            navigationController = AppStoryboard.Settings.viewController(SupportNavigationController.self)
            for vc in navigationController.viewControllers{
                if let pvc = vc as? PrivacyPolicyViewController{
                    pvc.isPrivacyPolicy = true
                    pvc.fromMenu = true
                }
            }
        default:
            break
        }
        mainViewController.rootViewController = navigationController
        mainViewController.hideLeftView(animated: true, delay: 0.0, completionHandler: nil)

    }


    func shareApplication(){
        tableView.reloadData()
            let textToShare = ""
            let url = URL(string:WEBSITE_URL)!
            var objectsToShare = [Any]()
            objectsToShare.append(textToShare)
            objectsToShare.append(url)

            let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
            let appDelegate: AppDelegate = (UIApplication.shared.delegate as! AppDelegate)
            activityVC.popoverPresentationController?.sourceView = appDelegate.window?.rootViewController?.view
            appDelegate.window?.rootViewController?.present(activityVC, animated: true, completion: nil)

    }

    func rateTheAppApplication(){
        tableView.reloadData()

    }

    func rateApp(appId: String, completion: @escaping ((_ success: Bool)->())) {
        guard let url = URL(string : "https://itunes.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?pageNumber=0&sortOrdering=1&type=Purple+Software&mt=8&id=" + appId) else {
            completion(false)
            return
        }
        guard #available(iOS 10, *) else {
            completion(UIApplication.shared.openURL(url))
            return
        }
        UIApplication.shared.open(url, options: [:], completionHandler: completion)
    }


    func askForLogout() {

//        let alert = UIAlertController(title: warningMessage.alertTitle.rawValue, message: "Do you really want to logout?", preferredStyle: .alert)
//        let okayAction = UIAlertAction(title: "Yes", style: .default){(action) in
//            alert.dismiss(animated: true, completion: nil)
//            self.logout()
//        }
//        let cancelAction = UIAlertAction(title: "Nope", style: .cancel){(action) in
//            SlideNavigationController.sharedInstance().closeMenu {
//                self.tableView.reloadData()
//            }
//            alert.dismiss(animated: true, completion: nil)
//        }
//
//        alert.addAction(okayAction)
//        alert.addAction(cancelAction)
//        let appDelegate: AppDelegate = (UIApplication.shared.delegate as! AppDelegate)
//        appDelegate.window?.rootViewController?.present(alert, animated: true, completion: nil)
    }

    @objc func userDidLoggedIn(_ notification:Notification) {
        if let userInfo = notification.userInfo{
            if let lUser = userInfo["user"] as? User{
                self.user = lUser
                self.tableView.reloadData()
            }
        }
    }

    @objc func slideNavigationDidClose(_ notification:Notification) {
        if self.tableView != nil{
            self.tableView.reloadData()
        }
    }

    func logout() {
        /*
            if !AppSettings.isConnectedToNetwork{
                let appDelegate: AppDelegate = (UIApplication.shared.delegate as! AppDelegate)
                //showAlertWith(viewController:appDelegate.window!.rootViewController!, message: warningMessage.networkIsNotConnected.rawValue, title: warningMessage.alertTitle.rawValue)
                self.tableView.reloadData()
                return
            }
            User.logOut({ (success, resUser, message) in
                if success{
                    AppSettings.shared.isLoggedIn = false
                    let user = User()
                    AppSettings.shared.resetOnLogout()
                    let appDelegate: AppDelegate = (UIApplication.shared.delegate as! AppDelegate)
                    let menu = AppStoryboard.Home.viewController(LeftMenuViewController.self)
                    let homeNav = AppStoryboard.Home.viewController(HomeNavigationController.self)
                    //let sideMenu =
                    //appDelegate.window?.rootViewController = nav
                    UIApplication.shared.applicationIconBadgeNumber = 0
//                    showAlertWith(viewController:appDelegate.window!.rootViewController!, message: "You have logged out successfully", title: warningMessage.alertTitle.rawValue)
                }else{
                    //let appDelegate: AppDelegate = (UIApplication.shared.delegate as! AppDelegate)
                   // showAlertWith(viewController:appDelegate.window!.rootViewController!, message: message, title: warningMessage.alertTitle.rawValue)
                }
            })
         */

    }

}






class LeftMenuCell: UITableViewCell {
    @IBOutlet weak var cellIcon :UIImageView!
    @IBOutlet weak var nameLabel :UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()

        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
