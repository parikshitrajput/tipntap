//
//  TouchIDSettingNavigationController.swift
//  TipNTap
//
//  Created by TecOrb on 15/03/18.
//  Copyright © 2018 Nakul Sharma. All rights reserved.
//

import UIKit

class TouchIDSettingNavigationController: UINavigationController {

    override func viewDidLoad() {
        super.viewDidLoad()
        navigationBar.isOpaque = false
        navigationBar.isTranslucent = true
        navigationBar.barTintColor = .white
        navigationBar.titleTextAttributes = [NSAttributedStringKey.font: fonts.Raleway.bold.font(.xXLarge)]
    }

    override func viewWillAppear(_ animated: Bool) {
        self.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationBar.shadowImage = UIImage()
        self.navigationBar.isTranslucent = true

        /*
        var bounds = self.navigationBar.bounds
        // make it cover the status bar by offseting y by -20
        bounds.offsetBy(dx: 0.0, dy: -20.0)
        bounds.size.height = bounds.height + 20.0
        let visualEffectView = UIVisualEffectView(frame: bounds)
        visualEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        navigationBar.insertSubview(visualEffectView, at: 0)
         */
    }

    override var shouldAutorotate : Bool {
        return false
    }

    override var prefersStatusBarHidden : Bool {
        return false
    }

    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .default
    }

    override var preferredStatusBarUpdateAnimation : UIStatusBarAnimation {
        return .none
    }

}

