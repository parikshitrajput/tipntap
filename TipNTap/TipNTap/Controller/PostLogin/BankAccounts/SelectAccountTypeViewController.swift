//
//  SelectAccountTypeViewController.swift
//  TipNTap
//
//  Created by TecOrb on 21/03/18.
//  Copyright © 2018 Nakul Sharma. All rights reserved.
//

import UIKit
protocol SelectAccountTypeViewControllerDelegate {
    func selectAccountTypeViewController(viewController: SelectAccountTypeViewController,didSelectAccountType accountType: AccountType)
}

class SelectAccountTypeViewController: UIViewController {
    let arrayOfLines = ["Select \"Business\" if you are a business partner of TipNTap","Select \"Individual\" if you are an individual user of TipNTap"]

    @IBOutlet weak var hintLabel: UILabel!
    @IBOutlet weak var containner: UIView!

    var delegate: SelectAccountTypeViewControllerDelegate?

    override func viewDidLoad() {
        super.viewDidLoad()
        var hint = ""
        for value in arrayOfLines {
            hint = hint  + " • " + value + "\n\n"
        }
        self.hintLabel.text = hint
    }

    override func viewDidLayoutSubviews() {
        CommonClass.makeViewCircularWithCornerRadius(self.containner, borderColor: appColor.gradientEnd, borderWidth: 1, cornerRadius: 3)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func onClickClose(_ sender: UIButton){
        self.dismiss(animated: true, completion: nil)
    }

    @IBAction func onClickBusiness(_ sender: UIButton){
        delegate?.selectAccountTypeViewController(viewController: self, didSelectAccountType: .business)
    }

    @IBAction func onClickIndividual(_ sender: UIButton){
        delegate?.selectAccountTypeViewController(viewController: self, didSelectAccountType: .individual)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
