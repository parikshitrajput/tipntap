//
//  AddBankAccountWithoutKYC.swift
//  TipNTap
//
//  Created by TecOrb on 22/03/18.
//  Copyright © 2018 Nakul Sharma. All rights reserved.
//

import UIKit
import Stripe

protocol AddBankAccountWithoutKYCDelegate{
    func addBankAccountViewController(viewController:AddBankAccountWithoutKYC,didAddAccount account:BankAccount)
}

class AddBankAccountWithoutKYC: UIViewController {
    let placeHolderArray = ["Account Holder Name", "Routing Number", "Account Number"]
    var data = ["", "", ""]
    var accountType : AccountType = .individual
    var delegate: AddBankAccountWithoutKYCDelegate?

    @IBOutlet weak var accountTypeButton: UIButton!
    @IBOutlet weak var addAccountButton: UIButton!
    @IBOutlet weak var detailsTableView: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.registerCells()
        self.accountTypeButton.setTitle("Account Type : "+self.accountType.rawValue.capitalized, for: UIControlState())
        self.detailsTableView.dataSource = self
        self.detailsTableView.delegate = self
        
    }

    func registerCells(){
        self.detailsTableView.register(UINib(nibName: "FloatTextFieldCell", bundle: nil), forCellReuseIdentifier: "FloatTextFieldCell")
    }

    override func viewDidLayoutSubviews() {
        self.addAccountButton.applyGradient(withColours: [appColor.gradientStart,appColor.gradientEnd], gradientOrientation: .horizontal)
    }

    @IBAction func onClickBackButton(_ sender: UIBarButtonItem){
        self.navigationController?.pop(true)
    }
    @IBAction func onClickAddAccount(_ button:UIButton){

        let inputParams = self.getParamsFromData()
        let validation = self.validateUserInputs(accountHolderName: inputParams.accountHolderName, routingNumber: inputParams.routingNumber, accountNumber: inputParams.accountNumber)
        if !validation.result{
            NKToastHelper.sharedInstance.showErrorAlert(self, message: validation.message)
            return
        }
        let accountParams = STPBankAccountParams()
        accountParams.accountHolderName = inputParams.accountHolderName
        accountParams.routingNumber = inputParams.routingNumber
        accountParams.accountNumber = inputParams.accountNumber
        accountParams.currency = "usd"
        accountParams.country = "US"
        accountParams.accountHolderType = (self.accountType == .business) ? .company : .individual
        if !AppSettings.isConnectedToNetwork{
            NKToastHelper.sharedInstance.showErrorAlert(self, message: warningMessage.networkIsNotConnected.rawValue)
            return
        }
        self.createAndProceedToAddBankAccount(accountParams: accountParams)
    }

    func createAndProceedToAddBankAccount(accountParams:STPBankAccountParams){
        AppSettings.shared.showLoader(withStatus: "Creating token..")
        STPAPIClient.shared().createToken(withBankAccount: accountParams) { (resToken, error) in
            if error != nil{
                AppSettings.shared.hideLoader()
                NKToastHelper.sharedInstance.showErrorAlert(self, message: error?.localizedDescription ?? "Stripe error")
                return
            }else{
                guard let token = resToken else{
                    AppSettings.shared.hideLoader()
                    NKToastHelper.sharedInstance.showErrorAlert(self, message: error?.localizedDescription ?? "Stripe error")
                    return
                }
                AppSettings.shared.updateLoader(withStatus: "Requesting..")
                self.addAccountWith(token: token.tokenId)
            }
        }
    }



    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func getParamsFromData() -> (accountHolderName:String, routingNumber:String, accountNumber:String) {
        var accountHolderName = ""
        var accountNumber = ""
        var routingNumber = ""

        for (index,input) in self.data.enumerated(){
            switch index{
            case 0:
                accountHolderName = input
            case 1:
                routingNumber = input
            case 2:
                accountNumber = input
            default:
                break
            }
        }

        return (accountHolderName,routingNumber,accountNumber)
    }


    func validateUserInputs(accountHolderName:String, routingNumber:String, accountNumber:String) -> (result:Bool,message:String) {
        if accountHolderName.isEmpty{
            return (false,"Please enter account holder name")
        }
        if routingNumber.isEmpty{
            return (false,"Please enter routing number")
        }
        if routingNumber.count != rountingNumberDigit{
            return (false,"Rouing number should be of \(rountingNumberDigit) characters in length")
        }

        if accountNumber.isEmpty{
            return (false,"Please enter routing number")
        }
        if accountNumber.count != accountNumberDigit{
            return (false,"Account number should be of \(accountNumberDigit) characters in length")
        }
        return (true,"")
    }

    func addAccountWith(token:String){
        AppSettings.shared.updateLoader(withStatus: "Requesting..")
        PaymentService.sharedInstance.addBankAccountWithoutKYC(token) { (success, resBankAccount, message) in
            AppSettings.shared.hideLoader()
            if success{
                if let account = resBankAccount{
                    self.delegate?.addBankAccountViewController(viewController: self, didAddAccount: account)
                    self.navigationController?.pop(true)
                }else{
                    NKToastHelper.sharedInstance.showErrorAlert(self, message:message)
                }
            }else{
                NKToastHelper.sharedInstance.showErrorAlert(self, message:message)
            }
        }

    }
}


extension AddBankAccountWithoutKYC: UITableViewDataSource,UITableViewDelegate{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.data.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 65
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "FloatTextFieldCell", for: indexPath) as! FloatTextFieldCell
        cell.floatTextField.delegate = self
        cell.floatTextField.addTarget(self, action: #selector(textdidChange(_:)), for: .editingChanged)
        self.configureKeyboard(cell.floatTextField, indexPath: indexPath)
        cell.floatTextField.placeholder = placeHolderArray[indexPath.row]
        cell.floatTextField.text = data[indexPath.row]
        return cell
    }


    func configureKeyboard(_ textField:FloatLabelTextField, indexPath :IndexPath){
            switch indexPath.row{
            case 0://account holder name
                textField.keyboardType = .asciiCapable
                textField.autocorrectionType = .default
                textField.autocapitalizationType = .words
            case 1,2://routing number
                textField.keyboardType = .numberPad
                textField.autocorrectionType = .no
            default:
                break
            }
    }

}

extension AddBankAccountWithoutKYC: UITextFieldDelegate{
    //MARK:- UITextFieldDelegate Methods
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if let indexPath = textField.tableViewIndexPath(self.detailsTableView) as IndexPath?{
            data[indexPath.row] = textField.text ?? ""
        }
    }

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if let indexPath = textField.tableViewIndexPath(self.detailsTableView) as IndexPath?{
            if indexPath.row == 1{//rounting number validation
                return CommonClass.validateMaxLength(textField, maxLength: rountingNumberDigit, range: range, replacementString: string)
            }else if indexPath.row == 2{//account number validation
                return CommonClass.validateMaxLength(textField, maxLength: accountNumberDigit, range: range, replacementString: string)
            }
        }
        return true
    }

    func textFieldDidEndEditing(_ textField: UITextField) {
        if let indexPath = textField.tableViewIndexPath(self.detailsTableView) as IndexPath?{
            data[indexPath.row] = textField.text ?? ""
        }
    }

    @IBAction func textdidChange(_ textField: UITextField){
        if let indexPath = textField.tableViewIndexPath(self.detailsTableView) as IndexPath?{
            data[indexPath.row] = textField.text ?? ""
        }
    }

}








