//
//  BankAccountsListViewController.swift
//  TipNTap
//
//  Created by TecOrb on 16/03/18.
//  Copyright © 2018 Nakul Sharma. All rights reserved.
//

import UIKit

class BankAccountsListViewController: UIViewController {
    @IBOutlet weak var accountsTableView : UITableView!
    var accounts = Array<BankAccount>()
    var isFromMenu = true
    var user : User!


    override func viewDidLoad() {
        super.viewDidLoad()
        self.user = User.loadSavedUser()
        self.registerCells()
        accountsTableView.dataSource = self
        accountsTableView.delegate = self
        accountsTableView.tableHeaderView = UIView(frame: CGRect.zero)
        accountsTableView.tableFooterView = UIView(frame: CGRect.zero)

        self.accountsTableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        self.getUserBankAccounts()
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func onClickMenu(_ sender:UIBarButtonItem){
        if isFromMenu{
            sideMenuController?.toggleLeftView(animated: true, delay: 0.0, completionHandler: nil)
        }else{
            self.navigationController?.pop(true)
        }
    }



    deinit{
        NotificationCenter.default.removeObserver(self)
    }


    func registerCells(){
        self.accountsTableView.register(UINib(nibName: "NoDataCell", bundle: nil), forCellReuseIdentifier: "NoDataCell")
        self.accountsTableView.register(UINib(nibName: "BankAccountCell", bundle: nil), forCellReuseIdentifier: "BankAccountCell")
        self.accountsTableView.register(UINib(nibName: "AddNewAccountCell", bundle: nil), forCellReuseIdentifier: "AddNewAccountCell")

    }

    func getUserBankAccounts() -> Void {
        AppSettings.shared.showLoader(withStatus: "Fetching..")
        PaymentService.sharedInstance.getBankAccountForUser { (success,resAccounts,message)  in
            AppSettings.shared.hideLoader()
            if let someAccounts = resAccounts{
                self.accounts.append(contentsOf: someAccounts)
            }
            self.accountsTableView.reloadData()
        }
    }

    

}

extension BankAccountsListViewController: UITableViewDataSource,UITableViewDelegate{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }


    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let height:CGFloat = 72
        return height
    }

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        var height:CGFloat = 1
        if section == 0{
            height = 0
        }
        return height
    }

    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var rows = 0
        switch section {
        case 0:
            rows = accounts.count
        case 1:
            rows = 1
        default:
            rows = 0
        }
        return rows
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "BankAccountCell", for: indexPath) as! BankAccountCell
            let bankAccount = accounts[indexPath.row]
            cell.accountNumberLabel.text = bankAccount.accountHolderName+"-"+bankAccount.last4
            cell.branchNameLabel.text = bankAccount.bankName
            cell.deleteButton.isHidden = false
            cell.defaultButton.isSelected = bankAccount.isDefault
            cell.defaultButton.addTarget(self, action: #selector(onClickDefaultButton(_:)), for: .touchUpInside)
            cell.deleteButton.addTarget(self, action: #selector(onClickRemoveButton(_:)), for: .touchUpInside)
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "AddNewAccountCell", for: indexPath) as! AddNewAccountCell
            return cell
        }
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 1{
            self.checkForKYC()
        }else{
            return
        }
    }


    func checkForKYC(){
        AppSettings.shared.showLoader(withStatus: "Please wait")
        self.user.getKYC { (success, resUserKYC, message) in
            AppSettings.shared.hideLoader()
            if success{
                if let userKYC = resUserKYC{
                    if userKYC.isUserDetailSent && !userKYC.kycStatus{
                        let myMessage = "Your KYC is under process. Please try after some time."
                        NKToastHelper.sharedInstance.showErrorAlert(self, message: myMessage)
                    }else if !userKYC.isUserDetailSent && !userKYC.kycStatus{
                        self.openAccountTypeSelectionVC()
                    }else if userKYC.isUserDetailSent && userKYC.kycStatus{
                        self.navigateToAddNewAccountWithoutKYC(accountType: userKYC.accountType)
                    }
                }else{
                    NKToastHelper.sharedInstance.showErrorAlert(self, message: message)
                }
            }else{
                NKToastHelper.sharedInstance.showErrorAlert(self, message: message)
            }
        }
    }

    func navigateToAddNewAccount(accountType:AccountType){
        let addNewAccount = AppStoryboard.BankAccount.viewController(AddBankAccountViewController.self)
        addNewAccount.delegate = self
        addNewAccount.accountType = accountType
        self.navigationController?.pushViewController(addNewAccount, animated: true)
    }

    func navigateToAddNewAccountWithoutKYC(accountType:AccountType){
        let addNewAccount = AppStoryboard.BankAccount.viewController(AddBankAccountWithoutKYC.self)
        addNewAccount.delegate = self
        addNewAccount.accountType = accountType
        self.navigationController?.pushViewController(addNewAccount, animated: true)
    }

    func openAccountTypeSelectionVC(){
        let selectionVC = AppStoryboard.BankAccount.viewController(SelectAccountTypeViewController.self)
        selectionVC.delegate = self
        let nav = UINavigationController(rootViewController: selectionVC)
        nav.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        nav.navigationBar.isHidden = true
        self.present(nav, animated: false, completion: nil)
    }


    func viewAccountDetails(account: BankAccount){
        
    }

    @IBAction func onClickDefaultButton(_ sender: UIButton){
        if let indexPath = sender.tableViewIndexPath(self.accountsTableView) as IndexPath?{
            let account = self.accounts[indexPath.row]
            if account.isDefault{
                return
            }
            self.askToMakeDefaultAccount(account: account, atIndexPath: indexPath)
        }
    }


    @IBAction func onClickRemoveButton(_ sender: UIButton){
        if let indexPath = sender.tableViewIndexPath(self.accountsTableView) as IndexPath?{
            let account = self.accounts[indexPath.row]
            self.askToDeleteAccount(account: account, atIndexPath: indexPath)
        }
    }

    func askToDeleteAccount(account : BankAccount,atIndexPath indexPath: IndexPath) {
        let alert = UIAlertController(title: warningMessage.title.rawValue, message: "Would you really want to delete this bank account?", preferredStyle: .alert)
        let okayAction = UIAlertAction(title: "Delete", style: .destructive){[weak account](action) in
            alert.dismiss(animated: true, completion: nil)
            self.removeAccount(account, indexPath: indexPath)
        }

        let cancelAction = UIAlertAction(title: "Nope", style: .default){(action) in
            alert.dismiss(animated: true, completion: nil)
        }
        alert.addAction(cancelAction)
        alert.addAction(okayAction)
        self.navigationController?.present(alert, animated: true, completion: nil)
    }

    func askToMakeDefaultAccount(account : BankAccount,atIndexPath indexPath: IndexPath) {
        let alert = UIAlertController(title: warningMessage.title.rawValue, message: "Would you really want to make this bank account as default?", preferredStyle: .alert)
        let okayAction = UIAlertAction(title: "Yes", style: .default){[weak account](action) in
            alert.dismiss(animated: true, completion: nil)
            self.makeAccountDefault(account, indexPath: indexPath)
        }

        let cancelAction = UIAlertAction(title: "Nope", style: .cancel){(action) in
            alert.dismiss(animated: true, completion: nil)
        }
        alert.addAction(cancelAction)
        alert.addAction(okayAction)
        self.navigationController?.present(alert, animated: true, completion: nil)
    }


    func makeAccountDefault(_ myAccount:BankAccount?,indexPath: IndexPath) -> Void {
        guard let account = myAccount else {
            return
        }
        AppSettings.shared.showLoader(withStatus: "Please wait..")
        PaymentService.sharedInstance.makeBankAccountDefault(account.accountId) { (success, resAccount,message) in
            AppSettings.shared.hideLoader()
            if success{
                if let defaultAccount = resAccount{
                    self.accounts.remove(at: indexPath.row)
                    for account in self.accounts{
                        account.isDefault = false
                    }
                    self.accounts.insert(defaultAccount, at: 0)
                    self.accountsTableView.reloadData()
                }

            }
//            NKToastHelper.sharedInstance.showAlert(self, title: warningMessage.title, message: message)
        }
    }
    func removeAccount(_ myAccount:BankAccount?,indexPath: IndexPath) -> Void {
        guard let account = myAccount else {
            return
        }
        AppSettings.shared.showLoader(withStatus: "Please wait..")
        PaymentService.sharedInstance.removeAccount(account.accountId) { (success, resAccount,message) in
            AppSettings.shared.hideLoader()
            if success{
                self.accounts.remove(at: indexPath.row)
                self.accountsTableView.deleteRows(at: [indexPath], with: .automatic)
            }
            NKToastHelper.sharedInstance.showAlert(self, title: warningMessage.title, message: message)
        }
    }

}

extension BankAccountsListViewController: AddBankAccountViewControllerDelegate,SelectAccountTypeViewControllerDelegate,AddBankAccountWithoutKYCDelegate{
    func addBankAccountViewController(viewController: AddBankAccountViewController, didAddAccount account: BankAccount) {
        self.accounts.append(account)
        self.accountsTableView.reloadData()
    }

    func addBankAccountViewController(viewController: AddBankAccountWithoutKYC, didAddAccount account: BankAccount) {
        self.accounts.append(account)
        self.accountsTableView.reloadData()
    }

    func selectAccountTypeViewController(viewController: SelectAccountTypeViewController, didSelectAccountType accountType: AccountType) {
        viewController.dismiss(animated: true, completion: nil)
        self.navigateToAddNewAccount(accountType: accountType)
    }
}
