//
//  AddBankAccountViewController.swift
//  TipNTap
//
//  Created by TecOrb on 19/03/18.
//  Copyright © 2018 Nakul Sharma. All rights reserved.
//

import UIKit
import Stripe

enum AccountType:String{
    case individual
    case business
}
enum AccountAction:String{
    case update
    case create
    case view
}

protocol AddBankAccountViewControllerDelegate{
    func addBankAccountViewController(viewController:AddBankAccountViewController,didAddAccount account:BankAccount)
}

class AddBankAccountViewController: UIViewController {
    let headerTitles = ["Personal Details","Address","Upload Document","Business Details"]

    var documentImage : UIImage?
    var imagePickerController : UIImagePickerController!

    enum keys : String, CodingKey {
        case address = "legal_entity_address_line1"
        case city = "legal_entity_address_city"
        case zipcode = "legal_entity_address_postal_code"
        case state = "legal_entity_address_state"
        case day = "legal_entity_dob_day"
        case month = "legal_entity_dob_month"
        case year = "legal_entity_dob_year"
        case fistName = "legal_entity_first_name"
        case lastName = "legal_entity_last_name"
        case ssnLast4 = "legal_entity_ssn_last_4"
        case ssn = "legal_entity_personal_id_number"
        case businessName = "legal_entity_business_name"
        case taxId = "legal_entity_business_tax_id"
        case document = "legal_entity_verification_document"
        case accountType = "legal_entity_account_type"

    }

    let dataPlaceHolder = [
        "Personal Details":[
            "Account holder name",
            "Routing Number",
            "Account Number",
            "Date of birth",
            "SSN"
        ],
        "Address":[
            "Address",
            "Zipcode",
            "City",
            "State"
        ],
        "Upload Document":[
            ""
        ],
        "Business Details":[
            "Business Name",
            "Tax Id"
        ]
    ]

    var data = [
        "Personal Details":[
            "",
            "",
            "",
            "",
            ""
        ],
        "Address":[
            "",
            "",
            "",
            ""
        ],
        "Upload Document":[
            ""
        ],
        "Business Details":[
            "",
            ""
        ]
    ]

    @IBOutlet weak var detailsTableView: UITableView!
    @IBOutlet weak var accountTypeContainer: UIView!
    @IBOutlet weak var addAccountButton: UIButton!

    @IBOutlet weak var businessButton: UIButton!
    @IBOutlet weak var individualButton: UIButton!
    var datePicker : UIDatePicker!
    var selectedDate: Date!
    var doneBar: UIToolbar!
    var accountType: AccountType = AccountType.individual
    var delegate: AddBankAccountViewControllerDelegate?
    override func viewDidLoad() {
        super.viewDidLoad()
        self.imagePickerController = UIImagePickerController()
        self.imagePickerController.delegate = self
        self.selectedDate = Date.today()
        self.configureAccountSelection(accountType: self.accountType)
        self.initializeDatePicker()
        self.initializeToolBar()

        self.registerCells()
        self.detailsTableView.contentInset = UIEdgeInsetsMake(0, 0, 55, 0)
        self.detailsTableView.dataSource = self
        self.detailsTableView.delegate = self
        self.detailsTableView.reloadData()
    }

    func initializeDatePicker(){
        self.datePicker = UIDatePicker()
        self.datePicker.locale = Locale.current
        self.datePicker.timeZone = TimeZone.current
        self.datePicker.maximumDate = Date.yesterday()
        datePicker.setDate(self.selectedDate, animated: false)
        datePicker.datePickerMode = .date
        datePicker.addTarget(self, action: #selector(handleDatePicker(_:)), for: .valueChanged)
    }

    func initializeToolBar(){
        func setUpDatePicker() {
            self.doneBar = UIToolbar(frame: CGRect(x:0, y:0, width:self.view.frame.size.width, height:44))
            doneBar.barStyle = .default
            let spacer = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
            let doneButton = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(onClickDone(_:)))
            let cancelButton = UIBarButtonItem(title: "Cancel", style: .done, target: self, action: #selector(onClickCancel(_:)))
            doneButton.tintColor = appColor.blue
            cancelButton.tintColor = appColor.blue
            doneBar.setItems([cancelButton,spacer,doneButton], animated: false)
            doneBar.backgroundColor = UIColor.white
            doneBar.tintColor = UIColor.white
        }
    }

    @IBAction func onClickBackButton(_ sender: UIBarButtonItem){
        self.navigationController?.pop(true)
    }

    @IBAction func onClickDone(_ sender: UIBarButtonItem){
        self.selectedDate = self.datePicker.date
        self.view.endEditing(true)
        self.detailsTableView.reloadData()
    }

    @IBAction func onClickCancel(_ sender: UIBarButtonItem){
        self.view.endEditing(true)
    }

    @objc func handleDatePicker(_ sender: UIDatePicker){
        self.selectedDate = sender.date

        let indexPath = IndexPath(row: 3, section: 0)
        let key = headerTitles[indexPath.section]
        if var dataArray = data[key]{
            dataArray[indexPath.row] = self.formattedDateFromDate(self.selectedDate)
            self.data.updateValue(dataArray, forKey: key)
        }
        if let cell = detailsTableView.cellForRow(at: indexPath) as? FloatTextFieldCell{
            cell.floatTextField.text = self.formattedDateFromDate(self.selectedDate)
        }
    }

     func formattedDateFromDate(_ date:Date) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale.autoupdatingCurrent
        dateFormatter.timeZone = TimeZone.autoupdatingCurrent
        dateFormatter.defaultDate = date
        dateFormatter.dateFormat = "dd MMM YYYY"
        let da = dateFormatter.string(from: date)
        return da
    }

    func dateParams(_ date:Date) -> (day:String,month:String,year:String){
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale.autoupdatingCurrent
        dateFormatter.timeZone = TimeZone.autoupdatingCurrent
        dateFormatter.defaultDate = date
        dateFormatter.dateFormat = "dd"
        let day = dateFormatter.string(from: date)
        dateFormatter.dateFormat = "MM"
        let month = dateFormatter.string(from: date)
        dateFormatter.dateFormat = "YYYY"
        let year = dateFormatter.string(from: date)
        return (day,month,year)
    }

    func nameParams(from name:String) -> (firstName:String,lastName:String){
        var firstName = ""
        var lastName = ""

        let fullName = name.components(separatedBy: " ")
        if let fname = fullName.first{
            firstName = fname
        }

        if fullName.count > 1{
            var lName = ""
            for i in 1..<fullName.count{
                if lName == ""{
                    lName = fullName[i]
                }else{
                    lName = lName + " " + fullName[i]
                }
            }
            lastName = (lName.count == 0) ? "_" : lName
        }

        lastName = (lastName.count == 0) ? "_" : lastName

        return(firstName,lastName)
    }

    override func viewDidLayoutSubviews() {
//        self.accountTypeContainer.backgroundColor = .white
        self.addAccountButton.applyGradient(withColours: [appColor.gradientStart,appColor.gradientEnd], gradientOrientation: .horizontal)
    }

    func configureAccountSelection(accountType:AccountType){
        let selectionTitleColor = UIColor.white
        let deSelectedTitleColor = UIColor.white.withAlphaComponent(0.5)
        self.businessButton.setTitleColor((accountType == .business) ? selectionTitleColor : deSelectedTitleColor , for: UIControlState())
        self.individualButton.setTitleColor((accountType == .individual) ? selectionTitleColor : deSelectedTitleColor , for: UIControlState())
        self.detailsTableView.reloadData()
    }





    func registerCells(){
        self.detailsTableView.register(UINib(nibName: "FloatingTextViewCell", bundle: nil), forCellReuseIdentifier: "FloatingTextViewCell")
        self.detailsTableView.register(UINib(nibName: "UploadDocCell", bundle: nil), forCellReuseIdentifier: "UploadDocCell")
        self.detailsTableView.register(UINib(nibName: "FloatTextFieldCell", bundle: nil), forCellReuseIdentifier: "FloatTextFieldCell")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func onClickBusiness(_ button:UIButton){
        self.accountType = .business
        self.configureAccountSelection(accountType: self.accountType)
    }

    @IBAction func onClickIndividual(_ button:UIButton){
        self.accountType = .individual
        self.configureAccountSelection(accountType: self.accountType)
    }
    @IBAction func onClickAddAccount(_ button:UIButton){
        let userInputs = self.getParamsFromData()
        if !userInputs.result{
            return
        }

        guard let verificationParams = userInputs.params as Dictionary<String,String>? else{
            return
        }

        guard let accountParams = userInputs.accountParams else{
            return
        }
        guard let docImage = self.documentImage else{
            NKToastHelper.sharedInstance.showErrorAlert(self, message: "Please upload a verification document")
            return
        }

        AppSettings.shared.showLoader(withStatus: "Creating token..")
        STPAPIClient.shared().createToken(withBankAccount: accountParams) { (resToken, error) in
            if error != nil{
                AppSettings.shared.hideLoader()
                NKToastHelper.sharedInstance.showErrorAlert(self, message: error?.localizedDescription ?? "Stripe error")
                return
            }else{
                guard let token = resToken else{
                    AppSettings.shared.hideLoader()
                    NKToastHelper.sharedInstance.showErrorAlert(self, message: error?.localizedDescription ?? "Stripe error")
                    return
                }
                AppSettings.shared.updateLoader(withStatus: "Requesting..")
                self.addAccountWith(token: token.tokenId, verificationParams: verificationParams, documentImage: docImage)
            }
        }
    }
}

extension AddBankAccountViewController{

    func addAccountWith(token:String,verificationParams:Dictionary<String,String>,documentImage: UIImage){
        PaymentService.sharedInstance.addBankAccountWith(token, verificationParams: verificationParams, document: documentImage) { (success, resBankAccount, message) in
            AppSettings.shared.hideLoader()
            if success{
                if let account = resBankAccount{
                    self.delegate?.addBankAccountViewController(viewController: self, didAddAccount: account)
                    self.navigationController?.pop(true)
                }else{
                    NKToastHelper.sharedInstance.showErrorAlert(self, message:message)
                }
            }else{
                NKToastHelper.sharedInstance.showErrorAlert(self, message:message)
            }
        }

    }


}
extension AddBankAccountViewController:UITableViewDataSource,UITableViewDelegate{

    func numberOfSections(in tableView: UITableView) -> Int {
        return (self.accountType == .individual) ? 3 : self.headerTitles.count
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let dataArray = self.dataPlaceHolder[self.headerTitles[section]]
        return dataArray?.count ?? 0
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return ((indexPath.section == 1) && (indexPath.row == 0)) ? 100 : 65
    }

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return self.headerViewFor(section: section)
    }

    func headerViewFor(section:Int) -> UIView?{
        if accountType == .individual{
            if section == 0{
                let header = IndividualPersonalDetailHeader.instanceFromNib()
                header.frame = CGRect(x:0,y:0,width:self.view.frame.size.width,height:40)
                return header
            }else{return nil}
        }else{
            if section == 0{
                let header = BusinessPersonalDetailHeader.instanceFromNib()
                header.frame = CGRect(x:0,y:0,width:self.view.frame.size.width,height:50)
                return header
            }else if section == 3{
                let header = BusinessDetailHeader.instanceFromNib()
                header.frame = CGRect(x:0,y:0,width:self.view.frame.size.width,height:50)
                return header
            }else{return nil}
        }
    }

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if accountType == .individual{
            return (section == 0) ? 40 : 0
        }else{
            return (section == 0 || section == 3) ? 50 : 0
        }
    }

    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return headerTitles[section]
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 2{
            self.showAlertToChooseAttachmentOption()
        }else{
            return
        }
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 1 && indexPath.row == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "FloatingTextViewCell", for: indexPath) as! FloatingTextViewCell
            cell.floatTextView.delegate = self
            if let phArray = self.dataPlaceHolder[self.headerTitles[indexPath.section]]{
                cell.floatTextView.hint = phArray[indexPath.row]
            }
            if let dataArray = self.data[self.headerTitles[indexPath.section]]{
                cell.floatTextView.text = dataArray[indexPath.row]
            }
            cell.floatTextView.keyboardType = .asciiCapable
            cell.floatTextView.autocorrectionType = .default
            cell.floatTextView.autocapitalizationType = .words
            return cell
        }else if indexPath.section == 2 && indexPath.row == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "UploadDocCell", for: indexPath) as! UploadDocCell
            cell.documentImage.image = self.documentImage
            return cell

        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "FloatTextFieldCell", for: indexPath) as! FloatTextFieldCell
            self.configureKeyboard(cell, indexPath: indexPath)

            cell.floatTextField.delegate = self
            cell.floatTextField.addTarget(self, action: #selector(textdidChange(_:)), for: .editingChanged)

            if let phArray = self.dataPlaceHolder[self.headerTitles[indexPath.section]]{
                cell.floatTextField.placeholder = phArray[indexPath.row]
            }
            if let dataArray = self.data[self.headerTitles[indexPath.section]]{
                cell.floatTextField.text = dataArray[indexPath.row]
            }
            return cell
        }
    }

    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if let indexPath = textField.tableViewIndexPath(self.detailsTableView) as IndexPath?{
            if let cell = self.detailsTableView.cellForRow(at: indexPath) as? FloatTextFieldCell{
                self.configureKeyboard(cell, indexPath: indexPath)
            }
        }
        return true
    }

    func configureKeyboard(_ cell:FloatTextFieldCell, indexPath :IndexPath){
        let textField: FloatLabelTextField = cell.floatTextField
        if indexPath.section == 0{
            switch indexPath.row{
            case 0://account holder name
                textField.inputView = nil
                textField.inputAccessoryView = nil
                textField.keyboardType = .asciiCapable
                textField.autocorrectionType = .default
                textField.autocapitalizationType = .words
            case 1,2://routing number
                textField.inputView = nil
                textField.inputAccessoryView = nil
                textField.keyboardType = .numberPad
                textField.autocorrectionType = .no
            case 3://Dob
                textField.keyboardType = .default
                textField.autocorrectionType = .no
                textField.inputView = self.datePicker
                textField.inputAccessoryView = self.doneBar
                break;
            case 4:
                textField.keyboardType = .phonePad
                textField.inputView = nil
                textField.inputAccessoryView = nil
                textField.autocorrectionType = .no
            default:
                textField.inputView = nil
                textField.inputAccessoryView = nil
                textField.keyboardType = .asciiCapable
                textField.autocorrectionType = .default
                textField.autocapitalizationType = .words
            }
        }else if indexPath.section == 1{
            switch indexPath.row{
            case 1:
                textField.inputView = nil
                textField.inputAccessoryView = nil
                textField.keyboardType = .numberPad
                textField.autocorrectionType = .no
            case 2,3:
                textField.inputView = nil
                textField.inputAccessoryView = nil
                textField.keyboardType = .asciiCapable
                textField.autocorrectionType = .default
                textField.autocapitalizationType = .words
            default:
                textField.inputView = nil
                textField.inputAccessoryView = nil
                textField.keyboardType = .asciiCapable
                textField.autocorrectionType = .default
                textField.autocapitalizationType = .words
            }
        }else if indexPath.section == 3{//Business details
            switch indexPath.row{
            case 1://business name
                textField.inputView = nil
                textField.inputAccessoryView = nil
                textField.keyboardType = .asciiCapable
                textField.autocorrectionType = .default
                textField.autocapitalizationType = .words
            case 2://tax id
                textField.inputView = nil
                textField.inputAccessoryView = nil
                textField.keyboardType = .asciiCapable
                textField.autocorrectionType = .no
                textField.autocapitalizationType = .none
            default:
                textField.inputView = nil
                textField.inputAccessoryView = nil
                textField.keyboardType = .asciiCapable
                textField.autocorrectionType = .default
                textField.autocapitalizationType = .words
            }
        }else{
            textField.inputView = nil
            textField.inputAccessoryView = nil
            textField.keyboardType = .asciiCapable
            textField.autocorrectionType = .default
            textField.autocapitalizationType = .words
        }
    }
}



extension AddBankAccountViewController: UITextFieldDelegate,UITextViewDelegate{
    //MARK:- UITextFieldDelegate Methods
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if let indexPath = textField.tableViewIndexPath(self.detailsTableView) as IndexPath?{
            let key = headerTitles[indexPath.section]
            if var darray = data[key]{
                darray[indexPath.row] = textField.text ?? ""
                self.data.updateValue(darray, forKey: key)
            }
        }
    }

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if let indexPath = textField.tableViewIndexPath(self.detailsTableView) as IndexPath?{
            if indexPath.section == 0 && indexPath.row == 1{//rounting number validation
                return CommonClass.validateMaxLength(textField, maxLength: rountingNumberDigit, range: range, replacementString: string)
            }else if indexPath.section == 0 && indexPath.row == 2{//account number validation
                return CommonClass.validateMaxLength(textField, maxLength: accountNumberDigit, range: range, replacementString: string)
            }else if indexPath.section == 0 && indexPath.row == 4{//ssn validation
                return CommonClass.validateMaxLength(textField, maxLength: ssnDigit, range: range, replacementString: string)
            }
        }
        return true
    }

    func textFieldDidEndEditing(_ textField: UITextField) {
        if let indexPath = textField.tableViewIndexPath(self.detailsTableView) as IndexPath?{
            let key = headerTitles[indexPath.section]
            if var darray = data[key]{
                darray[indexPath.row] = textField.text ?? ""
                self.data.updateValue(darray, forKey: key)
            }
        }
        self.detailsTableView.reloadData()
    }

    @IBAction func textdidChange(_ textField: UITextField){
        if let indexPath = textField.tableViewIndexPath(self.detailsTableView) as IndexPath?{
            let key = headerTitles[indexPath.section]
            if var darray = data[key]{
                darray[indexPath.row] = textField.text ?? ""
                self.data.updateValue(darray, forKey: key)
            }
        }
    }

    //MARK:- UITextViewDelegate Methods
    func textViewDidBeginEditing(_ textView: UITextView) {
        if let indexPath = textView.tableViewIndexPath(self.detailsTableView) as IndexPath?{
            let key = headerTitles[indexPath.section]
            if var darray = data[key]{
                darray[indexPath.row] = textView.text
                self.data.updateValue(darray, forKey: key)
            }
        }
    }
    func textViewDidChange(_ textView: UITextView) {
        if let indexPath = textView.tableViewIndexPath(self.detailsTableView) as IndexPath?{
            let key = headerTitles[indexPath.section]
            if var darray = data[key]{
                darray[indexPath.row] = textView.text
                self.data.updateValue(darray, forKey: key)
            }
        }
    }

    func textViewDidEndEditing(_ textView: UITextView) {
        if let indexPath = textView.tableViewIndexPath(self.detailsTableView) as IndexPath?{
            let key = headerTitles[indexPath.section]
            if var darray = data[key]{
                darray[indexPath.row] = textView.text
                self.data.updateValue(darray, forKey: key)
            }
        }
        self.detailsTableView.reloadData()
    }

}

//let accountParams = STPBankAccountParams()
//accountParams.accountNumber = accountNumber
//accountParams.accountHolderName = accountHolderName
//accountParams.routingNumber = routingNumber
//accountParams.country = country
//accountParams.currency = currrency
//accountParams.accountHolderType =

extension AddBankAccountViewController{
    func getParamsFromData() -> (params:Dictionary<String,String>, accountParams : STPBankAccountParams?,result:Bool){

        guard let personalDetails = self.data[headerTitles[0]]else{
            return ([:],nil,false)
        }
        var params = Dictionary<String,String>()
        let accountParams = STPBankAccountParams()
        accountParams.accountHolderType = (self.accountType == .business) ? .company : .individual

        let accountHolderName = personalDetails[0].trimmingCharacters(in: .whitespaces)
        let routingNumber = personalDetails[1].trimmingCharacters(in: .whitespaces)
        let accountNumber = personalDetails[2].trimmingCharacters(in: .whitespaces)
        let dob = personalDetails[3].trimmingCharacters(in: .whitespaces)
        let ssn = personalDetails[4].trimmingCharacters(in: .whitespaces)

        let personalDetailsVerification = self.verifyPersonalDetails(accountHodlerName: accountHolderName, routingNumber: routingNumber, accountNumber: accountNumber, dob: dob, ssn: ssn)

        if !personalDetailsVerification.result{
            NKToastHelper.sharedInstance.showErrorAlert(self, message: personalDetailsVerification.message)
            return ([:],nil,false)
        }else{
            accountParams.accountHolderName = accountHolderName
            accountParams.accountNumber = accountNumber
            accountParams.routingNumber = routingNumber
            accountParams.currency = "usd"
            accountParams.country = "US"

            let nameParams = self.nameParams(from: accountHolderName)
            params.updateValue(nameParams.firstName, forKey: keys.fistName.stringValue)

            params.updateValue(nameParams.lastName, forKey: keys.lastName.stringValue)
            params.updateValue((self.accountType == .individual) ? "individual" : "company", forKey: keys.accountType.stringValue)

            let dob = self.dateParams(self.selectedDate)
            params.updateValue(dob.day, forKey: keys.day.stringValue)
            params.updateValue(dob.month, forKey: keys.month.stringValue)
            params.updateValue(dob.year, forKey: keys.year.stringValue)
            params.updateValue(String(ssn.suffix(4)), forKey: keys.ssnLast4.stringValue)
            params.updateValue(ssn, forKey: keys.ssn.stringValue)

        }

        guard let addressDetails = self.data[headerTitles[1]]else{
            return ([:],nil,false)
        }

        let line1 = addressDetails[0].trimmingCharacters(in: .whitespaces)
        let zipcode = addressDetails[1].trimmingCharacters(in: .whitespaces)
        let city = addressDetails[2].trimmingCharacters(in: .whitespaces)
        let state = addressDetails[3].trimmingCharacters(in: .whitespaces)
        let addressVerification = self.verifyAddress(address: line1, zipcode: zipcode, city: city, state: state)
        if !addressVerification.result{
            NKToastHelper.sharedInstance.showErrorAlert(self, message: addressVerification.message)
            return ([:],nil,false)
        }else{
            params.updateValue(line1, forKey: keys.address.stringValue)
            params.updateValue(zipcode, forKey: keys.zipcode.stringValue)
            params.updateValue(city, forKey: keys.city.stringValue)
            params.updateValue(state, forKey: keys.state.stringValue)
        }

        if self.accountType == .business{
            guard let businessDetails = self.data[headerTitles[3]]else{
                return ([:],nil,false)
            }
            let businessName = businessDetails[0].trimmingCharacters(in: .whitespaces)
            let taxId = businessDetails[1].trimmingCharacters(in: .whitespaces)
            let businessDetailsVerification = self.verifyBusinessDetails(businessName: businessName, taxID: taxId)

            if !businessDetailsVerification.result{
                NKToastHelper.sharedInstance.showErrorAlert(self, message: businessDetailsVerification.message)
                return ([:],nil,false)
            }else{
                params.updateValue(businessName, forKey: keys.businessName.stringValue)
                params.updateValue(taxId, forKey: keys.taxId.stringValue)
            }
        }

        return (params,accountParams,true)
    }

    func verifyPersonalDetails(accountHodlerName:String, routingNumber:String, accountNumber:String, dob:String, ssn:String) -> (result:Bool,message:String){
        if accountHodlerName.isEmpty{
            return (false,"Please enter account holder name")
        }
        if routingNumber.isEmpty{
            return (false,"Please enter routing number")
        }
        if routingNumber.count != rountingNumberDigit{
            return (false,"Rounting number should be of \(rountingNumberDigit) characters")
        }
        if accountNumber.isEmpty{
            return (false,"Please enter account number")
        }
        if accountNumber.count != accountNumberDigit{
            return (false,"Account number should be of \(accountNumberDigit) characters")
        }
        if dob.isEmpty{
            return (false,"Please enter date of birth")
        }

        if ssn.isEmpty{
            return (false,"Please enter SSN")
        }
        return (true,"")
    }

    func verifyBusinessDetails(businessName:String,taxID:String) -> (result:Bool,message:String){
        if businessName.isEmpty{
            return (false,"Please enter business name")
        }

        if taxID.isEmpty{
            return (false,"Please enter tax id")
        }

        return(true,"")
    }

    func verifyAddress(address:String,zipcode:String,city:String,state:String)-> (result:Bool,message:String){
        if address.isEmpty{
            return (false,"Please enter address")
        }

        if zipcode.isEmpty{
            return (false,"Please enter zipcode")
        }

        if city.isEmpty{
            return (false,"Please enter city")
        }

        if state.isEmpty{
            return (false,"Please enter state")
        }

        return(true,"")
    }

}


extension AddBankAccountViewController : UIImagePickerControllerDelegate,UINavigationControllerDelegate{
    func showAlertToChooseAttachmentOption(){
        let actionSheet = UIAlertController(title: "Select Option", message:"To take a picture of legal document", preferredStyle: UIDevice.isRunningOnIpad ? .alert : .actionSheet)
        let cancelAction: UIAlertAction = UIAlertAction(title: "Cancel", style: .cancel) { action -> Void in
            actionSheet.dismiss(animated: true, completion: nil)
        }

        actionSheet.addAction(cancelAction)
        let openGalleryAction: UIAlertAction = UIAlertAction(title: "Choose from Gallery", style: .default)
        { action -> Void in
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.photoLibrary){
                self.imagePickerController.sourceType = UIImagePickerControllerSourceType.photoLibrary;
                self.imagePickerController.allowsEditing = false
                self.imagePickerController.modalPresentationStyle = UIModalPresentationStyle.currentContext
                self.present(self.imagePickerController, animated: true, completion: nil)
            }
        }
        actionSheet.addAction(openGalleryAction)

        let openCameraAction: UIAlertAction = UIAlertAction(title: "Camera", style: .default)
        { action -> Void in
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera){
                self.imagePickerController.sourceType = UIImagePickerControllerSourceType.camera;
                self.imagePickerController.allowsEditing = false
                self.imagePickerController.modalPresentationStyle = UIModalPresentationStyle.currentContext
                self.present(self.imagePickerController, animated: true, completion: nil)
            }
        }
        actionSheet.addAction(openCameraAction)
        self.present(actionSheet, animated: true, completion: nil)
    }

    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }


    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String :Any]){

        if let tempImage = info[UIImagePickerControllerEditedImage] as? UIImage{
            self.documentImage = tempImage
        }else if let tempImage = info[UIImagePickerControllerOriginalImage] as? UIImage{
            self.documentImage = tempImage
        }
        self.detailsTableView.reloadData()
        picker.dismiss(animated: true) {}
    }
}




