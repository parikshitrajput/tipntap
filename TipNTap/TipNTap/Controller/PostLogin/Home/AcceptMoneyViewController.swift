//
//  AcceptMoneyViewController.swift
//  TipNTap
//
//  Created by TecOrb on 02/03/18.
//  Copyright © 2018 Nakul Sharma. All rights reserved.
//

import UIKit
import SDWebImage

class AcceptMoneyViewController: UIViewController {
    var user: User!

    @IBOutlet weak var payeeNameLabel : UILabel!
    @IBOutlet weak var hintLabel : UILabel!
    @IBOutlet weak var phoneNumberLabel : UILabel!
    @IBOutlet weak var qrCodeImage : UIImageView!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.user = User.loadSavedUser()
        self.setupUserInfo()
    }

    func setupUserInfo(){
        self.qrCodeImage.setIndicatorStyle(.white)
        self.qrCodeImage.setShowActivityIndicator(true)
        self.qrCodeImage.sd_setImage(with: URL(string:user.qrImage) ?? URL(string:BASE_URL))
        self.payeeNameLabel.text = self.user.name
        self.hintLabel.text =  "Enter Mobile/Reference Number"
        self.phoneNumberLabel.text = ("+"+self.user.countryCode+self.user.contact)+"/"+self.user.accountReference
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func onClcikBack(_ sender: UIBarButtonItem){
        self.navigationController?.pop(true)
    }

    @IBAction func onClcikMoreOptions(_ sender: UIBarButtonItem){
        self.openActionSheet()
    }

    func openActionSheet(){
        let actionSheet = UIAlertController(title: "Select Options", message: "", preferredStyle: UIDevice.isRunningOnIpad ? .alert : .actionSheet)
        actionSheet.view.tintColor = appColor.blue
        actionSheet.view.backgroundColor = appColor.boxGray
        CommonClass.makeViewCircularWithCornerRadius(actionSheet.view, borderColor:appColor.blue, borderWidth: 0, cornerRadius: 15)
        //share and save to gallery
        let shareAction = UIAlertAction(title: "Share", style: .default) { (action) in
            actionSheet.dismiss(animated: true, completion: nil)
            self.shareQRCode()
        }

        let saveToGalleryAction = UIAlertAction(title: "Save to Gallery", style: .default) { (action) in
            actionSheet.dismiss(animated: true, completion: nil)
            self.saveToGallary()
        }

        let cancel = UIAlertAction(title: "Cancel", style: .cancel) { (action) in
            actionSheet.dismiss(animated: true, completion: nil)
        }

        actionSheet.addAction(shareAction)
        actionSheet.addAction(saveToGalleryAction)
        actionSheet.addAction(cancel)

        self.present(actionSheet, animated: true, completion: nil)
    }

    func shareQRCode(){
        let userNameToshare = self.user.name
        let referenceNumber = self.user.accountReference
        guard let imageURL = URL(string: self.user.qrImage)else{
            return
        }

        if let image = SDImageCache.shared().imageFromMemoryCache(forKey: imageURL.absoluteString) {
            let textToShare = userNameToshare+" "+referenceNumber+"\r\n"+self.user.qrImage
            let activityVC = UIActivityViewController(activityItems: [image,textToShare], applicationActivities: nil)
            activityVC.popoverPresentationController?.sourceView = self.view
            self.present(activityVC, animated: true, completion: nil)
        }
    }


//    func shareOnInstagram(){
//        let image = UIImage(named: "imageToShare")
//        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as String
//        let checkValidation = FileManager.default
//        let getImagePath = paths.appending("image.igo")
//        try!  checkValidation.removeItem(atPath: getImagePath)
//        let imageData =  UIImageJPEGRepresentation(image!, 1.0)
//        try! imageData?.write(to: URL.init(fileURLWithPath: getImagePath), options: .atomicWrite)
//        var documentController : UIDocumentInteractionController!
//        documentController = UIDocumentInteractionController.init(url: URL.init(fileURLWithPath: getImagePath))
//        documentController.uti = "com.instagram.exclusivegram"
//        documentController.presentOptionsMenu(from:self.view.frame, in: self.view, animated: true)
//    }

    func saveToGallary(){
        if let image = self.qrCodeImage.image{
            UIImageWriteToSavedPhotosAlbum(image, self, #selector(AcceptMoneyViewController.image(_:didFinishSavingWithError:contextInfo:)), nil)
        }else{
            NKToastHelper.sharedInstance.showErrorAlert(self, message: "Image couldn't be downloaded")
        }
    }

    @objc func image(_ image: UIImage, didFinishSavingWithError error: NSError?, contextInfo: UnsafeRawPointer) {
        if let err = error {
            NKToastHelper.sharedInstance.showErrorAlert(self, message: err.localizedDescription)
        } else {
            NKToastHelper.sharedInstance.showErrorAlert(self, message: "QRCode image has been save to photo library")
        }
    }

}
