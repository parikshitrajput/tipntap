//
//  HomeViewController.swift
//  TipNTap
//
//  Created by TecOrb on 22/01/18.
//  Copyright © 2018 Nakul Sharma. All rights reserved.
//

import UIKit
import LGSideMenuController

class HomeViewController: UIViewController {
    @IBOutlet weak var cardImageView: UIImageView!
    @IBOutlet weak var cardShadowView: UIView!

    @IBOutlet weak var scanAndPayConatainer: UIView!
    @IBOutlet weak var receiveMoneyConatainer: UIView!
    @IBOutlet weak var lastTransferConatainer: UIView!

    var titleView : NavigationTitleView!
    var isForcingToPayment = false
    func setupNavigationTitle() {
        titleView = NavigationTitleView.instanceFromNib()
        titleView.frame = CGRect(x: 60, y: 0, width: self.view.frame.size.width-120, height: 44)
        titleView.shouldAttributtedMiddle = true
        self.navigationItem.titleView = self.titleView
    }
    override func viewDidLayoutSubviews() {
        self.decorateCardBells()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupNavigationTitle()
        setUpRightBarButton()
        self.view.backgroundColor = isBlueTheme ? appColor.splash : UIColor.white
        self.decorateCardBells()
        self.view.setNeedsLayout()
        self.view.layoutIfNeeded()
        if self.isForcingToPayment{
            self.scanAndPay(false)
        }
    }





    @IBAction func onClickMenu(_ sender:UIBarButtonItem){
        sideMenuController?.toggleLeftView(animated: true, delay: 0.0, completionHandler: nil)
    }

    func setUpRightBarButton(){
        let notificationButton = UIButton(type: .system)
        notificationButton.frame = CGRect(x:0, y:0, width:35, height:35)
        notificationButton.tintColor = isBlueTheme ? .white : appColor.blue
        notificationButton.autoresizingMask = [.flexibleWidth,.flexibleHeight]
        notificationButton.setImage(#imageLiteral(resourceName: "offer_icon_action"), for: .normal)
        notificationButton.addTarget(self, action: #selector(onClickOffersButton(_:)), for: .touchUpInside)
        let rightBarButton = UIBarButtonItem(customView: notificationButton)
        self.navigationItem.setRightBarButtonItems(nil, animated: false)
        self.navigationItem.setRightBarButtonItems([rightBarButton], animated: false)
        self.navigationItem.leftBarButtonItem?.tintColor = isBlueTheme ? .white : appColor.blue
    }

    func decorateCardBells(){
        self.cardImageView.addshadow(top: true, left: true, bottom: true, right: true,shadowRadius: 15, shadowColor:.lightGray)
        CommonClass.makeViewCircularWithCornerRadius(self.cardShadowView, borderColor: appColor.lightGray, borderWidth: 0, cornerRadius: 15)
        self.cardShadowView.addshadow(top: true, left: true, bottom: true, right: true,shadowRadius: 15, shadowColor:.lightGray)

        CommonClass.makeViewCircularWithCornerRadius(self.scanAndPayConatainer, borderColor: appColor.lightGray, borderWidth: 1.5, cornerRadius: 15)
        CommonClass.makeViewCircularWithCornerRadius(self.receiveMoneyConatainer, borderColor: appColor.lightGray, borderWidth: 1.5, cornerRadius: 15)
        CommonClass.makeViewCircularWithCornerRadius(self.lastTransferConatainer, borderColor: appColor.lightGray, borderWidth: 1.5, cornerRadius: 15)
    }

    @IBAction func onClickOffersButton(_ sender: UIButton){
        let offersVC = AppStoryboard.Scanner.viewController(OffersViewController.self)
        offersVC.shouldShowApply = false
        self.navigationController?.pushViewController(offersVC, animated: true)
    }

    @IBAction func onClickReceiveMoneyButton(_ sender: UIButton){
        let receiveMoneyVC = AppStoryboard.Home.viewController(AcceptMoneyViewController.self)
        self.navigationController?.pushViewController(receiveMoneyVC, animated: true)
    }
    @IBAction func onClickScanAndPayButton(_ sender: UIButton){
        self.scanAndPay(true)
    }
  
    @objc func scanAndPay(_ animated: Bool){
        let scanAndPayVC = AppStoryboard.Scanner.viewController(RootScannerViewController.self)
        self.navigationController?.pushViewController(scanAndPayVC, animated: animated)
    }
    @IBAction func onClickLastTransferButton(_ sender: UIButton){
        let passBookVC = AppStoryboard.Transactions.viewController(PassbookViewController.self)
        self.navigationController?.pushViewController(passBookVC, animated: true)
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
