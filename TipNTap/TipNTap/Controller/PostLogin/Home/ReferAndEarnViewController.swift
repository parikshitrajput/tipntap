//
//  ReferAndEarnViewController.swift
//  TipNTap
//
//  Created by TecOrb on 24/03/18.
//  Copyright © 2018 Nakul Sharma. All rights reserved.
//

import UIKit
import Firebase
class ReferAndEarnViewController: UIViewController {
    var isFromMenu = true
    @IBOutlet weak var inviteCodeLabel : UILabel!
    @IBOutlet weak var tapToCopyLabel : UILabel!
    @IBOutlet weak var messageLabel : UILabel!

    @IBOutlet weak var copyActionButton : UIButton!
    @IBOutlet weak var inviteButton : UIButton!
    var activityVC : UIActivityViewController!
    var user : User!
    var inviteCode:String!
    var inviteLink:String?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.inviteCodeLabel.text = " "
        self.tapToCopyLabel.text = " "
        self.messageLabel.text = " "
        self.inviteButton.setTitle("", for: UIControlState())
        self.hideViews(hide: true)
        self.setupLeftBarButtonItem(isFromMenu: self.isFromMenu)
        self.user = User.loadSavedUser()
        self.getReferralCodeFromServer()
    }

    func setupLeftBarButtonItem(isFromMenu:Bool){
        let leftBarButtonItem = UIBarButtonItem(image: isFromMenu ? #imageLiteral(resourceName: "menu_button") : #imageLiteral(resourceName: "back_button"), style: .plain, target: self, action: #selector(onClickMenu(_:)))
        leftBarButtonItem.tintColor = appColor.blue
        self.navigationItem.leftBarButtonItem = leftBarButtonItem
    }

    func hideViews(hide:Bool){
        self.inviteCodeLabel.isHidden = hide
        self.tapToCopyLabel.isHidden = hide
        self.messageLabel.isHidden = hide
        self.copyActionButton.isHidden = hide
    }

    func getReferralCodeFromServer(){
        self.user.getReferralCode { (success, resRefCode, message) in
            if success{
                guard let refCode = resRefCode else{
                    NKToastHelper.sharedInstance.showErrorAlert(self, message: message, completionBlock: {
                        self.showRetryAlert()
                    })
                    return
                }
                self.parseReferralCodeFrom(referralUrl: refCode)
            }else{
                NKToastHelper.sharedInstance.showErrorAlert(self, message: message, completionBlock: {
                    self.showRetryAlert()
                })
            }
        }
    }
    private func showRetryAlert(){
        let alert = UIAlertController(title: warningMessage.title.rawValue, message: "The operation was failed, would you like to retry?", preferredStyle: .alert)
        let retryAction = UIAlertAction(title: "Retry Now", style: .cancel) { (action) in
            alert.dismiss(animated: false, completion: nil)
            self.getReferralCodeFromServer()
        }

        let letItGo = UIAlertAction(title: "Leave It", style: .default) { (action) in
            self.navigationController?.pop(false)
        }

        alert.addAction(retryAction)
        alert.addAction(letItGo)

        self.present(alert, animated: true, completion: nil)
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func viewDidLayoutSubviews() {
        self.copyActionButton.addDashedLineBorder(color: UIColor.white, pattern: [2,1])
    }

    func parseReferralCodeFrom(referralUrl:String){
        let components = referralUrl.components(separatedBy: "/")
        if let refCode = components.last{
            self.inviteCode = refCode
            self.setupCodeInView(inviteCode: refCode)
            self.createInviteLink(inviteCode: refCode)
        }
    }

    func createInviteLink(inviteCode:String){
        if !AppSettings.isConnectedToNetwork{
            NKToastHelper.sharedInstance.showErrorAlert(self, message: warningMessage.networkIsNotConnected.rawValue)
            return
        }

        AppSettings.shared.showLoader(withStatus: "Please wait..")
        self.createShortUrl(referalCode: inviteCode) { (shortUrl) in
            if let shortenUrl = shortUrl{
                self.inviteLink = shortenUrl
            }
        }
    }

    func setupCodeInView(inviteCode:String){
        self.inviteCodeLabel.text = inviteCode
        self.tapToCopyLabel.text = "Tap to copy link"
        self.messageLabel.text = self.invitationTextWithCode(invitationCode: inviteCode)
        let title = inviteCode.isEmpty ? "Reload Invitation Code" : "INVITE"
        self.inviteButton.setTitle(title, for: UIControlState())
        self.hideViews(hide: false)
        self.view.setNeedsUpdateConstraints()
        self.view.updateConstraintsIfNeeded()
    }

    func invitationTextWithCode(invitationCode: String)->String{
       let text =  "A simple and secure payment app by Tipntap Inc. I am sharing this invitation link. Register and download the application. Once you’ve sent your first payment we’ll each get $1."
        return text
    }

    @IBAction func onClickMenu(_ sender:UIBarButtonItem){
        if isFromMenu{
            sideMenuController?.toggleLeftView(animated: true, delay: 0.0, completionHandler: nil)
        }else{
            self.navigationController?.popToRoot(true)
        }
    }

    @IBAction func onClickCopy(_ sender: UIButton){
        if let invitationLink = self.inviteLink{
            UIPasteboard.general.string = invitationLink
            AppSettings.shared.showSuccess(withStatus: "Copied")
        }else{
            if !AppSettings.isConnectedToNetwork{
                NKToastHelper.sharedInstance.showErrorAlert(self, message: warningMessage.networkIsNotConnected.rawValue)
                return
            }

            AppSettings.shared.showLoader(withStatus: "Please wait..")
            self.createShortUrl(referalCode: inviteCode) { (shortUrl) in
                if let shortenUrl = shortUrl{
                    UIPasteboard.general.string = shortenUrl
                    AppSettings.shared.showSuccess(withStatus: "Copied")
                }
            }
        }

    }

    @IBAction func onClickInvite(_ sender: UIButton){
        if let invitationLink = self.inviteLink{
            let text = self.invitationTextWithCode(invitationCode: self.inviteCode)
            self.shareApplication(text: text, shortUrl: invitationLink)
        }else{
            if !AppSettings.isConnectedToNetwork{
                NKToastHelper.sharedInstance.showErrorAlert(self, message: warningMessage.networkIsNotConnected.rawValue)
                return
            }

            AppSettings.shared.showLoader(withStatus: "Please wait..")
            self.createShortUrl(referalCode: inviteCode) { (shortUrl) in

                if let shortenUrl = shortUrl{
                    let text = self.invitationTextWithCode(invitationCode: self.inviteCode)
                    self.shareApplication(text: text, shortUrl: shortenUrl)
                }
            }
        }
    }

    func createShortUrl(referalCode:String,handler:@escaping (_ shortUrl: String?)->Void){
        guard let link = URL(string: (inviteLinkUrl+referalCode)) else {
            AppSettings.shared.hideLoader()
            return
        }
        let components = DynamicLinkComponents(link: link, domain: "ubg36.app.goo.gl")
        let options = DynamicLinkComponentsOptions()
        options.pathLength = .short
        components.options = options
        components.shorten(completion: { (url, warnings, error) in
            AppSettings.shared.hideLoader()
            if let error = error {
                NKToastHelper.sharedInstance.showErrorAlert(self, message:error.localizedDescription)
                return
            }
            handler(url?.absoluteString)
        })
    }
    


    func shareApplication(text:String,shortUrl:String){
        var objectsToShare = [Any]()
        objectsToShare.append("TipNTap!"+"\r\n"+text+"\r\n"+shortUrl)
        self.activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
        self.activityVC.completionWithItemsHandler = {(actType, done,response, error) in
            self.navigationController?.popToRoot(true)
        }

        let appDelegate: AppDelegate = (UIApplication.shared.delegate as! AppDelegate)
        activityVC.setValue("Invite friends to TipNTap", forKey: "subject")
        activityVC.popoverPresentationController?.sourceView = appDelegate.window?.rootViewController?.view
        appDelegate.window?.rootViewController?.present(activityVC, animated: true, completion: nil)
    }

}
