//
//  AppDelegate.swift
//  TinNTap
//
//  Created by TecOrb on 16/01/18.
//  Copyright © 2018 Nakul Sharma. All rights reserved.
//

import UIKit
import CoreData
import IQKeyboardManagerSwift
import UserNotifications
import Alamofire
import Stripe
import AVFoundation
import Firebase
import FirebaseMessaging
import FirebaseInstanceID
import Fabric
import Crashlytics
import SwiftyJSON

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate,UNUserNotificationCenterDelegate {

    var window: UIWindow?

    var bgTask = UIBackgroundTaskInvalid
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        NotificationCenter.default.removeObserver(self)

        Fabric.with([STPAPIClient.self, Crashlytics.self])
        FirebaseApp.configure()
        STPPaymentConfiguration.shared().publishableKey = stripeKey
        self.registerForRemoteNotification()
        IQKeyboardManager.sharedManager().enable = true
        IQKeyboardManager.sharedManager().disabledToolbarClasses = [SupportTicketViewController.self]
        self.logCrashlyticsUser()
        
        if AppSettings.shared.firstLaunchAfterReset{
            AppSettings.shared.resetOnFirstAppLaunch()
        }

        UIApplication.shared.applicationIconBadgeNumber = 0
        if let token = Messaging.messaging().fcmToken{
            kUserDefaults.set(token, forKey: kDeviceToken)
        }
        LoginService.sharedInstance.updateDeviceTokeOnServer()

        self.openLandingPage()
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
         self.bgTask = application.beginBackgroundTask {
            application.endBackgroundTask(self.bgTask)
            self.bgTask = UIBackgroundTaskInvalid
            AppSettings.shared.isLoggedIn = false
            kUserDefaults.synchronize()
        }

        DispatchQueue.global().async {
            self.bgTask = UIBackgroundTaskInvalid
            application.endBackgroundTask(self.bgTask)
        }
    }


    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        AppSettings.shared.isLoggedIn = false
        kUserDefaults.synchronize()
        self.saveContext()
    }

    // MARK: - Core Data stack

    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
        */
        let container = NSPersistentContainer(name: "TipNTap")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                 
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()

    // MARK: - Core Data Saving support

    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    

    func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool {
        var result = false
        if url.scheme == SELF_URL_SCHEME || url.scheme == SELF_IDENTIFIER{
            result = true
        }
        return result
    }
    
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        var result = false
        if url.scheme == SELF_URL_SCHEME || url.scheme == SELF_IDENTIFIER{
            result = true
        }
        return result
    }
    
    
    
    func application(_ application: UIApplication, continue userActivity: NSUserActivity, restorationHandler: @escaping ([Any]?) -> Void) -> Bool {
        return true
    }
    
}

extension AppDelegate : MessagingDelegate{
    func logCrashlyticsUser() {
        if AppSettings.shared.isLoggedIn{
            let user = User.loadSavedUser()
            Crashlytics.sharedInstance().setUserEmail(user.email)
            Crashlytics.sharedInstance().setUserIdentifier(user.ID)
            Crashlytics.sharedInstance().setUserName(user.name)
        }

    }

    func messaging(_ messaging: Messaging, didRefreshRegistrationToken fcmToken: String) {
        kUserDefaults.set(fcmToken, forKey: kDeviceToken)
        LoginService.sharedInstance.updateDeviceTokeOnServer()
    }
    func messaging(_ messaging: Messaging, didReceive remoteMessage: MessagingRemoteMessage) {
        if let message = remoteMessage.appData as? Dictionary<String,AnyObject> {
            print_debug("Message ID: \(message)")
        }
    }
}

/*=============================NOTIFICATION HANDLING==============================*/

extension AppDelegate{
    func application(_ application: UIApplication,
                     didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        Messaging.messaging().apnsToken = deviceToken
        let deviceTokenString = deviceToken.reduce("", {$0 + String(format: "%02X", $1)})
        kUserDefaults.set(deviceTokenString, forKey: kDeviceToken)
        if let token = Messaging.messaging().fcmToken{
            kUserDefaults.set(token, forKey: kDeviceToken)
        }else{
            print_debug("already registered to firebase with token : \(kUserDefaults.value(forKey: kDeviceToken) ?? "--")")
        }
    }


    func registerForRemoteNotification() {
        if #available(iOS 10.0, *) {
            let center  = UNUserNotificationCenter.current()
            center.delegate = self
            center.requestAuthorization(options: [.sound, .alert, .badge]) { (granted, error) in
                if error == nil{
                    DispatchQueue.main.async {
                        Messaging.messaging().delegate = self
                        UIApplication.shared.registerForRemoteNotifications()
                    }
                }
            }
        }
        else {
            UIApplication.shared.registerUserNotificationSettings(UIUserNotificationSettings(types: [.sound, .alert, .badge], categories: nil))
            Messaging.messaging().delegate = self
            UIApplication.shared.registerForRemoteNotifications()
        }

    }



    @objc func tokenRefreshNotification(_ notification: NSNotification) {
        if let refreshedToken = Messaging.messaging().fcmToken{
            kUserDefaults.set(refreshedToken, forKey: kDeviceToken)
            LoginService.sharedInstance.updateDeviceTokeOnServer()
        }

    }


    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print_debug("Device token for push notifications: FAIL -- ")
        print_debug(error.localizedDescription)
    }

    //Called when a notification is delivered to a foreground app.

    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void)
    {

        //NotificationCenter.default.post(name: .REFRESH_NOTIFICATION_LIST_NOTIFICATION, object: nil)
        completionHandler([UNNotificationPresentationOptions.alert,UNNotificationPresentationOptions.sound,UNNotificationPresentationOptions.badge])
    }

    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        if let userInfo = response.notification.request.content.userInfo as? Dictionary<String,AnyObject>{
            ///
            if let notificationType = userInfo[kNotificationType] as? String{
                //print_debug("notification type: \(notificationType) and userInfo: \(userInfo)")
                let json = JSON.init(userInfo)
                print_debug("nitification json: \r\n \(json)")
                
                if notificationType == NotificationType.bulkNotification.rawValue{
                    
                    if let appsInfo = userInfo ["aps"] as? Dictionary<String,AnyObject> {
                        if let alertInfo = appsInfo["alert"] as? Dictionary<String,AnyObject>{
                            var tempBody = ""
                            if let body = alertInfo["body"] as? String {
                                tempBody = body
                                
                            }
                            NKToastHelper.sharedInstance.showErrorAlert(nil, message:tempBody)
                            
                        }
                        
                    }
                    
                }else if (notificationType == NotificationType.singleNotification.rawValue){

                    if let appsInfo = userInfo ["aps"] as? Dictionary<String,AnyObject> {
                        if let alertInfo = appsInfo["alert"] as? Dictionary<String,AnyObject>{
                            var tempBody = ""
                            if let body = alertInfo["body"] as? String {
                                tempBody = body
                                
                            }
                            NKToastHelper.sharedInstance.showErrorAlert(nil, message:tempBody)
                            
                        }
                        
                        
                    }

                    
                }else{
                    //print_debug(userInfo)
                    if AppSettings.shared.isLoggedIn{
                        AppSettings.shared.proceedToPassbook()
                    }
                }
            }
            ////

//            self.application(UIApplication.shared, didReceiveRemoteNotification: userInfo)
        }
    }

    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        if let userInfoDict = userInfo as? Dictionary<String,AnyObject>{
            print_debug(userInfoDict)
        }
        completionHandler(UIBackgroundFetchResult.newData)
    }
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any]) {
        if let userInfoDict = userInfo as? Dictionary<String,AnyObject>{
            print_debug(userInfoDict)
        }
        UIApplication.shared.applicationIconBadgeNumber = 0
        if ( application.applicationState == .inactive || application.applicationState == .background)
        {
        }
    }
}
/*=======================END OF NOTIFICATION HANDLING=========================*/


/*========================LANDING PAGE HANDLING======================*/

extension AppDelegate{
    func openLandingPage() {
        let preLoginNavigationVC = PreLoginNavigationController()
        self.window?.rootViewController = preLoginNavigationVC
    }
}



//nitification json:
//
//{
//    "google.c.a.e" : "1",
//    "time_to_live" : "108",
//    "notification_type" : "single_notification",
//    "gcm.message_id" : "0:1536761009108911%4c9cc8a54c9cc8a5",
//    "delay_while_idle" : "true",
//    "aps" : {
//        "mutable-content" : "1",
//        "alert" : {
//            "body" : "Test",
//            "title" : "Test"
//        }
//    },
//    "collapse_key" : "updated_state"
//}

