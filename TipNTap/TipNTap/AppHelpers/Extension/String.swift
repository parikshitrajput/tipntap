//
//  StringExtension.swift
//  GPDock
//
//  Created by TecOrb on 08/06/17.
//  Copyright © 2017 Nakul Sharma. All rights reserved.
//
//



import Foundation

extension String {
    var last4:String{
        let last4 = String(self.suffix(4))
        return last4
    }
//    static func className(aClass: AnyClass) -> String {
//        return NSStringFromClass(aClass).componentsSeparated(by: ".").last!
//    }
//    
//    func substring(from: Int) -> String {
//        return self.substring(from: self.startIndex.advancedBy(from))
//    }
//    
//    var length: Int {
//        return self.characters.count
//    }
}
