//
//  AppSettings.swift
//  TipNTap
//
//  Created by TecOrb on 29/01/18.
//  Copyright © 2018 Nakul Sharma. All rights reserved.
//
import CoreFoundation
import UIKit
import SwiftyJSON
import SVProgressHUD
import SystemConfiguration
import Alamofire
import BiometricAuthentication
import CoreTelephony
import LGSideMenuController

class AppSettings {

    static let shared = AppSettings()
    fileprivate init() {}

    func prepareHeader(withAuth:Bool) -> Dictionary<String,String>{
        let accept = "application/json"
        let currentVersion = UIApplication.appVersion()//+"."+UIApplication.appBuild()
        var header = Dictionary<String,String>()
        if withAuth{
            let user = User.loadSavedUser()
            print("UserToken<<<<" + user.userToken)
            let userToken = user.userToken
            header.updateValue(userToken, forKey: "accessToken")
        }

        header.updateValue(currentVersion, forKey: "currentVersion")
        header.updateValue("ios", forKey: "currentDevice")
        header.updateValue(accept, forKey: "Accept")
        return header
    }

    class var isConnectedToNetwork: Bool {
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout<sockaddr_in>.size)
        zeroAddress.sin_family = sa_family_t(AF_INET)

        guard let defaultRouteReachability = withUnsafePointer(to: &zeroAddress, {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {
                SCNetworkReachabilityCreateWithAddress(nil, $0)
            }
        }) else {

            return false
        }
        var flags: SCNetworkReachabilityFlags = []
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability, &flags) {
            return false
        }
        let isReachable = flags.contains(.reachable)
        let needsConnection = flags.contains(.connectionRequired)
        return (isReachable && !needsConnection)
    }


    var shouldAskToSetTouchID:Bool{
        return BioMetricAuthenticator.canAuthenticate() && self.interestedInLoginWithTouchID && (!self.isLoginWithTouchIDEnable)
    }

    var shouldAskToLoginWithTouchID:Bool{
        return BioMetricAuthenticator.canAuthenticate() && self.isLoginWithTouchIDEnable
    }

    var isLoggedIn: Bool{
        get {
            var result = false
            if let r = kUserDefaults.bool(forKey:kIsLoggedIN) as Bool?{
                result = r
            }
            return result
        }

        set(newIsLoggedIn){
            kUserDefaults.set(newIsLoggedIn, forKey: kIsLoggedIN)
        }
    }

    var isRegistered: Bool{
        get {
            var result = false
            if let r = kUserDefaults.bool(forKey:kIsRegistered) as Bool?{
                result = r
            }
            return result
        }
        set(newIsRegistered){
            kUserDefaults.set(newIsRegistered, forKey: kIsRegistered)
        }
    }

    var loginCount: Int{
        get {
            var result = 0
            if let r = kUserDefaults.integer(forKey:kLoginCount) as Int?{
                result = r
            }
            return result
        }

        set(newLoginCount){
            kUserDefaults.set(newLoginCount, forKey: kLoginCount)
        }
    }

    var isFirstTimeLogin: Bool{
        get {
            var result = false
            if let r = kUserDefaults.bool(forKey:kIsFirstTimeLogin) as Bool?{
                result = r
            }
            return result
        }
        set(newIsFirstTimeLogin){
            kUserDefaults.set(newIsFirstTimeLogin, forKey: kIsFirstTimeLogin)
        }
    }

    var interestedInLoginWithTouchID: Bool{
        get {
            var result = false
            if let r = kUserDefaults.bool(forKey:kInterestedInLoginWithTouchID) as Bool?{
                result = r
            }
            return result
        }
        set(newInterestedInLoginWithTouchID){
            kUserDefaults.set(newInterestedInLoginWithTouchID, forKey: kInterestedInLoginWithTouchID)
        }
    }

    var isLoginWithTouchIDEnable: Bool{
        get {
            var result = false
            if let r = kUserDefaults.bool(forKey:kIsLoginWithTouchIDEnable) as Bool?{
                result = r
            }
            return result
        }
        set(newLoginWithTouchIDEnable){
            kUserDefaults.set(newLoginWithTouchIDEnable, forKey: kIsLoginWithTouchIDEnable)
        }
    }
    var isNotificationEnable: Bool{
        get {
            var result = false
            if let r = kUserDefaults.bool(forKey:kIsNotificationsEnable) as Bool?{
                result = r
            }
            return result
        }
        set(newIsNotificationEnable){
            kUserDefaults.set(newIsNotificationEnable, forKey: kIsNotificationsEnable)
        }
    }

    var isIntroShown: Bool{
        get {
            var result = false
            if let r = kUserDefaults.bool(forKey:kIsIntroShown) as Bool?{
                result = r
            }
            return result
        }
        set(newIsIntroShown){
            kUserDefaults.set(newIsIntroShown, forKey: kIsIntroShown)
        }
    }

    var currentCountryCode:String{
        let networkInfo = CTTelephonyNetworkInfo()
        if let carrier = networkInfo.subscriberCellularProvider{
            let countryCode = carrier.isoCountryCode
            return countryCode ?? "US"
        }else{
            return Locale.autoupdatingCurrent.regionCode ?? "US"
        }
    }

    var previousRatedVersion : String {
        get {
            var result = ""
            if let r = kUserDefaults.value(forKey:kPreviousRatedVersion) as? String{
                result = r
            }
            return result
        }
        set(newpreviousRatedVersion){
            kUserDefaults.set(newpreviousRatedVersion, forKey: kPreviousRatedVersion)
        }
    }

    var firstLaunchAfterReset : Bool {
        get {
            var result = true
            if let r = kUserDefaults.value(forKey:kFirstLaunchAfterReset) as? String{
                if r.isEmpty || r.count == 0{
                    result = true
                }else{
                    result = false
                }
            }else{
                result = true
            }
            return result
        }
    }




    var shouldShowRatingPopUp : Bool {
        get {
            var result = true

            if let r = kUserDefaults.bool(forKey:kShouldShowRatingPopUp) as Bool?{
                result = r
            }
            return result
        }
        set(newShouldShowRatingPopUp){
            kUserDefaults.set(newShouldShowRatingPopUp, forKey: kShouldShowRatingPopUp)
        }
    }

//cedential

    var passwordEncrypted : String {
        get {
            var result = ""
            if let r = kUserDefaults.value(forKey:kPasswordEncrypted) as? String{
                result = r
            }
            return result
        }
        set(newPasswordEncrypted){

            kUserDefaults.set(newPasswordEncrypted, forKey: kPasswordEncrypted)
        }
    }

    var phoneNumber : String {
        get {
            var result = ""
            if let r = kUserDefaults.value(forKey:kPhoneNumber) as? String{
                result = r
            }
            return result
        }
        set(newPhoneNumber){
            kUserDefaults.set(newPhoneNumber, forKey: kPhoneNumber)
        }
    }


    var countryCode : String {
        get {
            var result = ""
            if let r = kUserDefaults.value(forKey:kCountryCode) as? String{
                result = r
            }
            return result
        }
        set(newCountryCode){
            kUserDefaults.set(newCountryCode, forKey: kCountryCode)
        }
    }




    func resetOnFirstAppLaunch(){
        self.isLoginWithTouchIDEnable = false
        self.interestedInLoginWithTouchID = true
        self.isLoggedIn = false
        self.isRegistered = false
        self.isNotificationEnable = false
       // self.isIntroShown = false
        kUserDefaults.set("\(Date.timeIntervalSinceReferenceDate)", forKey:kFirstLaunchAfterReset)
    }

    func resetOnLogout() {
        self.isLoggedIn = false
        self.isFirstTimeLogin = false
        self.isIntroShown = true
        //self.isLoginWithTouchIDEnable = false
        User().saveUserJSON(JSON.init(Data()))
    }

    func proceedToDashboard(_ isForcingToPayment:Bool = false){
        let homeVC = AppStoryboard.Home.viewController(HomeNavigationController.self)
        for vc in homeVC.viewControllers{
            if let homeVC = vc as? HomeViewController{
                homeVC.isForcingToPayment = isForcingToPayment
            }
        }
        let leftMenuVC = AppStoryboard.Home.viewController(LeftMenuViewController.self)
        let sideMenuVC = LGSideMenuController(rootViewController: homeVC, leftViewController: leftMenuVC, rightViewController: nil)
        sideMenuVC.leftViewPresentationStyle = .scaleFromBig
        //sideMenuVC.swipeGestureArea = .borders
        sideMenuVC.leftViewSwipeGestureRange = LGSideMenuSwipeGestureRangeMake(0, 0)
        let screenSize = UIScreen.main.bounds.size.width
        sideMenuVC.leftViewWidth = screenSize*3/5
        sideMenuVC.rootViewLayerBorderColor = .white
        sideMenuVC.rootViewLayerShadowRadius = 15.0
        sideMenuVC.leftViewBackgroundImage = UIImage(color: .white, size: UIScreen.main.bounds.size) // #imageLiteral(resourceName: "signup_background")
        sideMenuVC.leftViewBackgroundColor = UIColor.white.withAlphaComponent(0.95)
        sideMenuVC.rootViewCoverColorForLeftView = UIColor.white.withAlphaComponent(0.05)
        (UIApplication.shared.delegate as! AppDelegate).window?.rootViewController = sideMenuVC
    }


    func proceedToPassbook(){
        let homeVC = AppStoryboard.Home.viewController(HomeNavigationController.self)
        let leftMenuVC = AppStoryboard.Home.viewController(LeftMenuViewController.self)
        let sideMenuVC = LGSideMenuController(rootViewController: homeVC, leftViewController: leftMenuVC, rightViewController: nil)
        sideMenuVC.leftViewPresentationStyle = .scaleFromBig
        //sideMenuVC.swipeGestureArea = .borders
        sideMenuVC.leftViewSwipeGestureRange = LGSideMenuSwipeGestureRangeMake(0, 0)
        let screenSize = UIScreen.main.bounds.size.width
        sideMenuVC.leftViewWidth = screenSize*3/5
        sideMenuVC.rootViewLayerBorderColor = .white
        sideMenuVC.rootViewLayerShadowRadius = 15.0
        sideMenuVC.leftViewBackgroundImage = UIImage(color: .white, size: UIScreen.main.bounds.size) // #imageLiteral(resourceName: "signup_background")
        
        sideMenuVC.leftViewBackgroundColor = UIColor.white.withAlphaComponent(0.95)
        sideMenuVC.rootViewCoverColorForLeftView = UIColor.white.withAlphaComponent(0.05)
        (UIApplication.shared.delegate as! AppDelegate).window?.rootViewController = sideMenuVC
        DispatchQueue.main.asyncAfter(deadline: DispatchTime(uptimeNanoseconds: 10000)) {
            let passBookVC = AppStoryboard.Transactions.viewController(PassbookViewController.self)
            homeVC.pushViewController(passBookVC, animated: true)
        }
    }
    
    
    


    func showSessionExpireAndProceedToLandingPage(_ message:String? = nil){
        NKToastHelper.sharedInstance.showErrorAlert(nil, message: message ?? warningMessage.sessionExpired.rawValue, completionBlock: {
            let appDelegate = (UIApplication.shared.delegate as! AppDelegate)
            appDelegate.openLandingPage()
        })
    }

    func showForceUpdateAlert(){
        let alert = UIAlertController(title: warningMessage.title.rawValue, message: warningMessage.updateVersion.rawValue, preferredStyle: .alert)
        let updateAction = UIAlertAction(title: "Update Now", style: .cancel) {[alert] (action) in
            if let url = URL(string: "itms-apps://itunes.apple.com/app/id\(appID)"),
                UIApplication.shared.canOpenURL(url)
            {
                UIApplication.shared.open(url, options: [:], completionHandler: {[alert] (done) in
                    alert.dismiss(animated: false, completion: nil)
                })
            }
        }
        alert.addAction(updateAction)
        let toastShowingVC = (UIApplication.shared.delegate as! AppDelegate).window?.rootViewController
        toastShowingVC?.present(alert, animated: true, completion: nil)
    }


    func proceedToLandingPage(){
        let appDelegate = (UIApplication.shared.delegate as! AppDelegate)
        appDelegate.openLandingPage()
    }



    //=====Loader showing methods==========//
    func showLoader()
    {
        SVProgressHUD.setDefaultMaskType(.black)
        SVProgressHUD.setForegroundColor(appColor.blue)
        SVProgressHUD.setMinimumSize(loaderSize)
        SVProgressHUD.show()
    }


    var isLoaderOnScreen: Bool
    {
        return SVProgressHUD.isVisible()
    }

    func showError(withStatus status: String)
    {
        SVProgressHUD.setForegroundColor(appColor.blue)
        SVProgressHUD.setMinimumSize(loaderSize)
        SVProgressHUD.setImageViewSize(CGSize(width: 28, height: 28))
        SVProgressHUD.showError(withStatus: status)
    }
    func showSuccess(withStatus status: String)
    {
        SVProgressHUD.setForegroundColor(appColor.blue)
        SVProgressHUD.setMinimumSize(loaderSize)
        SVProgressHUD.setImageViewSize(CGSize(width: 28, height: 28))
        SVProgressHUD.showSuccess(withStatus: status)
        SVProgressHUD.dismiss(withDelay: 0.5)
    }


    func showLoader(withStatus status: String)
    {
        SVProgressHUD.setDefaultMaskType(.clear)
        SVProgressHUD.setDefaultAnimationType(.native)
        SVProgressHUD.setBackgroundLayerColor(UIColor.white.withAlphaComponent(0.95))
        SVProgressHUD.setBackgroundColor(UIColor.white.withAlphaComponent(0.95))
        SVProgressHUD.setForegroundColor(appColor.blue)
        SVProgressHUD.setMinimumSize(loaderSize)
        SVProgressHUD.show(withStatus: status)
        SVProgressHUD.setImageViewSize(loaderSize)
    }

    func updateLoader(withStatus status: String)
    {
        SVProgressHUD.setStatus(status)
        SVProgressHUD.setMinimumSize(loaderSize)
    }

    func hideLoader()
    {
        if SVProgressHUD.isVisible(){
            SVProgressHUD.dismiss()
        }
    }

    func clearAllPendingRequests(){
        Alamofire.SessionManager.default.session.getAllTasks { tasks in
            tasks.forEach { $0.cancel()
            }
        }
    }

    func clearLastPendingRequests(){
        Alamofire.SessionManager.default.session.getAllTasks { tasks in
            if let lastTask = tasks.last{
                lastTask.cancel()
            }
        }
    }

//====To open location setting of the application
//    func openLocationSetting(){
//        if let url = URL(string: "App-Prefs:root=LOCATION_SERVICES") {
//            UIApplication.shared.open(url, completionHandler: .none)
//        }
//    }


    func getNavigationDefaultHeight() -> CGFloat{
        if UIDevice.isIphoneX{
            return 105
        }else{
            return 64
        }
    }
    //==========User Avatar Image
    func userAvatarImage(username:String) -> UIImage{
        let configuration = LetterAvatarBuilderConfiguration()

        configuration.username = (username.trimmingCharacters(in: .whitespaces).count == 0) ? "NA" : username
        configuration.lettersColor = UIColor.white
        configuration.backgroundColors = [appColor.blue,appColor.blue,appColor.blue,appColor.blue,appColor.blue,appColor.blue,appColor.blue,appColor.blue,appColor.blue,appColor.blue,appColor.blue,appColor.blue]
        return UIImage.makeLetterAvatar(withConfiguration: configuration) ?? UIImage(named:"change_photo")!
    }

//==========Keys=========//
    let kIsLoggedIN = "is_logged_in"
    let kIsRegistered = "isRegistered"
    let kIsFirstTimeLogin = "isFirstTimeLogin"
    let kLoginCount = "loginCount"
    let kIsAuthourisedForSession = "isAuthourisedForSession"
    let kShouldShowRatingPopUp = "shouldShowRatingPopUp"
    let kPreviousRatedVersion = "previousRatedVersion"
    let kPasswordEncrypted = "passwordEncrypted"
    let kPhoneNumber = "phoneNumber"
    let kCountryCode = "countryCode"
    let kInterestedInLoginWithTouchID = "interestedInLoginWithTouchID"
    let kIsLoginWithTouchIDEnable = "isLoginWithTouchIDEnable"
    let kIsNotificationsEnable = "isNotificationsEnable"
    let kIsIntroShown = "isIntroShown"
//==========End of Keys=========//
}




