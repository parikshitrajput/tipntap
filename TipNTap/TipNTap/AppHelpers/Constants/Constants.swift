//
//  Constants.swift
//  TinNTap
//
//  Created by TecOrb on 16/01/18.
//  Copyright © 2018 Nakul Sharma. All rights reserved.
//

import UIKit
import Firebase
import Toast

//to set the environment
let isDebugEnabled = false
//to set logs
let isLogEnabled = false



//to set blue theme
let isBlueTheme = false


struct appColor{
    static let gradientStart = UIColor(red: 44.0/255.0, green: 190.0/255.0, blue: 167.0/255.0, alpha: 1.0)
    static let gradientEnd = UIColor(red: 54.0/255.0, green: 72.0/255.0, blue: 216.0/255.0, alpha: 1.0)
    static let blue = UIColor.color(r: 51, g: 74, b: 219)
    static let gray = UIColor.color(r: 169, g: 167, b: 168)
    static let boxGray = UIColor.color(r: 148, g: 148, b: 148)
    static let green = UIColor.color(r: 40, g: 175, b: 152)
    static let splash = UIColor.color(r: 42, g: 121, b: 195)
    static let payoutRed = UIColor.color(r: 248, g: 24, b: 73)
    static let payoutBlue = UIColor.color(r: 69, g: 111, b: 245)
    static let lightGray = UIColor.groupTableViewBackground
    static let red = UIColor.color(r:253, g:49, b:26)
}



struct gradientTextColor{
    static let start = UIColor(red: 35.0/255.0, green: 97.0/255.0, blue: 248.0/255.0, alpha: 1.0)
    static let middle = UIColor(red: 42.0/255.0, green: 65.0/255.0, blue: 228.0/255.0, alpha: 1.0)
    static let end = UIColor(red: 55.0/255.0, green: 32.0/255.0, blue: 208.0/255.0, alpha: 1.0)
}



let warningMessageShowingDuration = 1.25

extension UIColor{
    class func color(r:CGFloat,g:CGFloat,b:CGFloat,alpha:CGFloat? = nil) -> UIColor{
        if let alp = alpha{
            return UIColor(red: r/255.0, green: g/255.0, blue: b/255.0, alpha: alp)
        }else{
            return UIColor(red: r/255.0, green: g/255.0, blue: b/255.0, alpha: 1.0)
        }
    }
}


/*============== NOTIFICATIONS ==================*/
extension NSNotification.Name {
    public static let GPDockSlideNavigationControllerDidClose = NSNotification.Name("SlideNavigationControllerDidClose")
    public static let USER_DID_LOGGED_IN_NOTIFICATION = NSNotification.Name("UserDidLoggedInNotification")
    public static let REFRESH_NOTIFICATION_LIST_NOTIFICATION = NSNotification.Name("RefreshNotificationListNotification")

    public static let USER_DID_UPDATE_PROFILE_NOTIFICATION = NSNotification.Name("UserDidUpdateProfileNotification")

    public static let USER_DID_ADD_NEW_CARD_NOTIFICATION = NSNotification.Name("UserDidAddNewCardNotification")
    public static let pushPayment = NSNotification.Name("push_payment")
    public static let pushReferral = NSNotification.Name("push_referral")

    public static let FIRInstanceIDTokenRefreshNotification = NSNotification.Name.MessagingRegistrationTokenRefreshed
    public static let USER_PAID_AT_MARINA_NOTIFICATION = NSNotification.Name("UserPaidAtMarinaNotification")
    public static let USER_FAVORATES_MARINA_NOTIFICATION = NSNotification.Name("UserFavoratesMarinaNotification")
    public static let USER_DID_ADD_BOAT_NOTIFICATION = NSNotification.Name("UserDidAddNotification")

}



let kNotificationType = "notification_type"
enum NotificationType:String{
    case bookingCreatedByCustomerNotification = "customer_create_booking"
    case bookingCreatedByBusinessNotification = "business_create_booking"
    case bookingCancelledByBusinessNotification = "booking_cancel_by_business"
    case bookingCancelledByCustomerNotification = "booking_cancel_by_customer"
    case bookingCompletedByNotification = "booking_complete_by_business"
    case singleNotification = "single_notification"
    case bulkNotification = "bulk_notification"
}



let kUserDefaults = UserDefaults.standard

/*========== SOME GLOBAL VARIABLE FOR USERS ==============*/




let kDeviceToken = "DeviceToken"
let kFirstLaunchAfterReset = "firstLaunchAfterReset"

let kSupportEmail = "info@tipntap.com"
let kReportUsEmail = "info@tipntap.com"

let tollFreeNumber = "1-888-202-3625"
let appID = "1403342924"
//let adminCommission = 0.04

let rountingNumberDigit = 9
let accountNumberDigit = 12
let ssnDigit = 9


enum ErrorCode:Int{
    case success
    case failure
    case forceUpdate
    case sessionExpire
    case temporaryBlock

    init(rawValue:Int) {
        if rawValue == 102{
            self = .forceUpdate
        }else if (rawValue == 345) || (rawValue == 406) {
            self = .sessionExpire
        }else if ((rawValue >= 200) && (rawValue < 300)){
            self = .success
        }else if rawValue == 302 {
           self = .temporaryBlock
        }else{
            self = .failure
        }
    }
}


enum PayoutStatus:String{
    case paid
    case pending

    func getColor() -> UIColor{
        if self == .paid{
            return appColor.green
        }else{
            return appColor.payoutRed
        }
    }
    
    init(rawValue:String) {
        if rawValue.lowercased() == "paid"{
            self = .paid
        }else{
            self = .pending
        }
    }
}



/*================== API URLs ====================================*/
//https://www.tipntap.com/
//http://developers.tipntap.com/

let inviteLinkUrl = isDebugEnabled ? "http://developers.tipntap.com/referral/" : "https://www.tipntap.com/referral/"
let appLink = isDebugEnabled ? "http://developers.tipntap.com/" : "https://www.tipntap.com/"
let WEBSITE_URL = isDebugEnabled ? "http://developers.tipntap.com/" : "https://www.tipntap.com/"
let apiVersion = "1"

let BASE_URL = isDebugEnabled ? "http://developers.tipntap.com/api/v\(apiVersion)/" : "https://www.tipntap.com/api/v\(apiVersion)/"


enum api: String {
    case base
    case website
    case aboutUs
    case termsAndConditions
    case faq
    case login
    case logout
    case signup
    case varifyUser
    case socialAuth
    case editProfile
    case updateDeviceToken
    case forgotPassword
    case otpVarification
    case resetPassword
    case updatePassword
    case listOfSecurityQuestions
    case userSelectedSecurityQuestions
    case updateSecurityQuestions
    case checkSession
    case verifyContact
    case checkUserStatus
    case addCard
    case listOfCard
    case setCardAsDefault
    case removeCard
    case findPayee
    case payToUser
    case paymentSummary
    case addBankAccount
    case bankAccountList
    case makeBankAccountDefault
    case removeBankAccount
    case recentScans
    case userWallet
    case checkForKYC
    case offersList
    case referralCode
    case notificationToggle
    case userSettings
    case userPayouts
    case allSupportsTickets
    case addCommentToSupport
    case writeToUS
    case allFaqs

    func url() -> String {
        switch self {
        case .base: return BASE_URL
        case .website: return WEBSITE_URL
        case .aboutUs: return "\(WEBSITE_URL)mobile/about"
        case .termsAndConditions: return "\(WEBSITE_URL)terms-service"
        case .faq : return "\(BASE_URL)faqs/role/customer"
        case .login: return "\(BASE_URL)users/sign_in"
        case .logout: return "\(BASE_URL)logout"
        case .signup: return "\(BASE_URL)users"
        case .varifyUser: return "\(BASE_URL)user/verified"
        case .socialAuth: return "\(BASE_URL)user/social/login"
        case .editProfile: return "\(BASE_URL)update/profile"
        case .updateDeviceToken: return "\(BASE_URL)update/device"
        case .forgotPassword: return "\(BASE_URL)password/recovery"
        case .otpVarification: return "\(BASE_URL)user/otp/verification"
        case .resetPassword: return "\(BASE_URL)user/password/reset"
        case .updatePassword: return "\(BASE_URL)password/update"
        case .listOfSecurityQuestions : return "\(BASE_URL)security/questions"
        case .userSelectedSecurityQuestions : return "\(BASE_URL)customer/questions"
        case .updateSecurityQuestions : return "\(BASE_URL)security/questions/update"
        case .checkSession : return "\(BASE_URL)check/session"
        case .verifyContact : return "\(BASE_URL)user/verified"
        case .checkUserStatus : return "\(BASE_URL)user/status"
        case .addCard : return "\(BASE_URL)cards/new"
        case .listOfCard : return "\(BASE_URL)cards/list"
        case .setCardAsDefault : return "\(BASE_URL)card/default/add"
        case .removeCard : return "\(BASE_URL)card/remove"
        case .findPayee : return "\(BASE_URL)find/payee"
        case .payToUser : return "\(BASE_URL)user/payment"
        case .paymentSummary : return "\(BASE_URL)user/payment/summary"
        case .addBankAccount : return "\(BASE_URL)user/bank/account"
        case .bankAccountList  : return "\(BASE_URL)bank/accounts/list"
        case .makeBankAccountDefault : return "\(BASE_URL)user/default/account"
        case .removeBankAccount : return "\(BASE_URL)user/bank/account/remove"
        case .recentScans: return "\(BASE_URL)user/recent/scan"
        case .userWallet: return "\(BASE_URL)user/wallet/money"
        case .checkForKYC: return "\(BASE_URL)user/kyc/details"
        case .offersList: return "\(BASE_URL)available/offers"
        case .referralCode: return "\(BASE_URL)generate/referral"
        case .notificationToggle : return "\(BASE_URL)user/update/setting"
        case .userSettings: return "\(BASE_URL)payment/profile"
        case .userPayouts: return "\(BASE_URL)payout/list"
        case .allSupportsTickets: return "\(BASE_URL)supports"
        case .addCommentToSupport: return "\(BASE_URL)new/support/reply"
        case .writeToUS: return "\(BASE_URL)user/supports"
        case .allFaqs: return "\(BASE_URL)all/faqs"

        }
    }
}



/*================== SOCIAL LOGIN TYPE ====================================*/
enum SocialLoginType: String {
    case facebook = "Facebook"
    case google = "Google"
}

/*======================== CONSTANT MESSAGES ==================================*/
enum warningMessage : String{
    case updateVersion = "You are using a version of TipNTap that\'s no longer supported.Please upgrade your app to the newest app version to use TipNTap. Thanks!"
    case title = "Important Message"
    case setUpPassword = "Your password has updated successfully"
    case invalidPassword = "Please enter a valid password"
    case invalidPhoneNumber = "Please enter a valid phone number"
    case invalidFirstName = "Please enter a valid first name"
    case invalidLastName = "Please enter a valid last name"
    case invalidEmailAddress = "Please enter a valid email address"

    case emailCanNotBeEmpty = "Email address cann't be empty."
    case restYourPassword = "An email was sent to you to rest your password"
    case changePassword = "Your password has been changed successfully"
    case logoutMsg = "You've been logged out successfully"
    case networkIsNotConnected = "Network is not connected"
    case functionalityPending = "Under Development. Please ignore it"
    case enterPassword = "Please enter your password"
    case validPassword = "Please enter a valid password. Passwords should be 6-20 characters long."

    case enterOldPassword = "Please enter your current password"
    case validOldPassword = "Please enter a valid current password. Passwords should be 6-20 characters long."
    case enterNewPassword = "Please enter your new password"
    case validNewPassword = "Please enter a valid new password. Passwords should be 6-20 characters long."
    case confirmPassword = "Please confirm your password"
    case passwordDidNotMatch = "Please enter matching passwords"
    case cardDeclined = "The card was declined. Please reenter the payment details"
    case enterCVV = "Please enter the CVV"
    case enterValidCVV = "Please enter a valid CVV"
    case cardHolderName = "Please enter the card holder's name"
    case expMonth = "Please enter the exp. month"
    case expYear = "Please enter the exp. year"
    case validExpMonth = "Please enter a valid exp. month"
    case validExpYear = "Please enter a valid exp. year"
    case validCardNumber = "Please enter a valid card number"
    case enterZipCode = "Please enter the zipCode"
    case validZipCode = "Please enter a valid zipCode"
    case cardNumber = "Please enter the card number"
    case sessionExpired = "Your session has expired, Please login again"
    case issueFacing = "Please enter the issue you are facing"
    case messageSendToAdmin = "Your message has been received and responded to as soon as possible"
    case temporaryBlock = "you are temporary block"
}


/*============== SOCIAL MEDIA URL SCHEMES ==================*/

let SELF_URL_SCHEME = "com.tipntap.TipNTap"
let SELF_IDENTIFIER = "tipntap"
let stripeKey =  "pk_live_h8RVmJcCFCNJU7bzQMst2Pzq"   //isDebugEnabled ? "pk_test_MjWgkWsPEh9DWDRwIDT0f3lX" : "pk_live_h8RVmJcCFCNJU7bzQMst2Pzq" 
let GOOGLE_URL_SCHEME = "com.googleusercontent.apps.800415999166-7nibigrg10e6drmqa4qtgak4fkovinap"
let GOOGLE_API_KEY = "AIzaSyCBixW9vWCYJIb-UNauR3QHnIyTnQ96Vgs"
let kGoogleClientId = "800415999166-7nibigrg10e6drmqa4qtgak4fkovinap.apps.googleusercontent.com"

let STRIPE_URL_SCHEME = "tipntapteststripe"

/*============== PRINTING IN DEBUG MODE ==================*/
func print_debug <T>(_ object : T){
    if isLogEnabled{
        print(object)
    }
}

func print_log <T>(_ object : T){
//    NSLog("\(object)")
}



enum fontSize : CGFloat {
    case small = 12.0
    case smallMedium = 14.0
    case medium = 15.0
    case large = 17.0
    case xLarge = 18.0
    case xXLarge = 20.0
    case xXXLarge = 32.0
}

enum fonts {
    enum Raleway : String {
        case regular = "Raleway-Regular"
        case semiBold = "Raleway-Semibold"
        case medium = "Raleway-Medium"
        case bold = "Raleway-Bold"
        case light = "Raleway-Light"

        func font(_ size : fontSize) -> UIFont {
            return UIFont(name: self.rawValue, size: size.rawValue) ?? UIFont.systemFont(ofSize: size.rawValue)
        }
    }
}

//uses of fonts
//let fo = fonts.OpenSans.regular.font(.xXLarge)


/*==============================================*/






