//
//  SupportModels.swift
//  GPDock
//
//  Created by TecOrb on 08/05/18.
//  Copyright © 2018 Nakul Sharma. All rights reserved.
//

import UIKit
import SwiftyJSON

/*
 {
 "id": 8,
 "image": "",
 "message": "Check My issues",
 "reference_no": "U27R0670",
 "status": false,
 "support_chats": []
 }
 */

class SupportQuery: NSObject {
    enum keys:String, CodingKey{
        case id = "id"
        case image = "img_url"
        case message = "message"
        case referenceNumber = "reference_no"
        case status = "status"
        case supportChats = "support_chats"
    }

    var id = ""
    var image = ""
    var message = ""
    var referenceNumber = ""
    var status = true
    var supportChats = Array<SupportMessage>()

    override init(){
        super.init()
    }

    init(dict: Dictionary<String,AnyObject>){
        if let id = dict[keys.id.stringValue] as? Int{
            self.id = "\(id)"
        }else if let id = dict[keys.id.stringValue] as? String{
            self.id = id
        }

        if let imageUrl = dict[keys.image.stringValue] as? String{
            self.image = imageUrl
        }
        if let message = dict[keys.message.stringValue] as? String{
            self.message = message
        }
        if let referenceNumber = dict[keys.referenceNumber.stringValue] as? String{
            self.referenceNumber = referenceNumber
        }

        if let status = dict[keys.status.stringValue] as? Bool{
            self.status = status
        }

        if let supportChats = dict[keys.supportChats.stringValue] as? Array<Dictionary<String,AnyObject>>{
            for chat in supportChats{
                let message = SupportMessage(dict: chat)
                self.supportChats.append(message)
            }
        }
        super.init()
    }
}


/*
 {
 "id": 8,
 "reply": "Great",
 "reply_by": "admin"
 }
 */

enum ReplyBy {
    case you
    case admin
}

class SupportMessage: NSObject {
    enum keys:String, CodingKey{
        case id = "id"
        case reply = "reply"
        case replyBy = "reply_by"
    }

    var id = ""
    var reply = ""
    var replyBy : ReplyBy = .you

    override init(){
        super.init()
    }
    init(dict:Dictionary<String,AnyObject>) {
        if let id = dict[keys.id.stringValue] as? Int{
            self.id = "\(id)"
        }else if let id = dict[keys.id.stringValue] as? String{
            self.id = id
        }
        if let reply = dict[keys.reply.stringValue] as? String{
            self.reply = reply
        }

        if let replyBy = dict[keys.replyBy.stringValue] as? String{
            self.replyBy = (replyBy == "admin") ? .admin : .you
        }
        super.init()
    }
}


class SupportTicketParser: NSObject {
    enum keys:String, CodingKey{
        case code = "code"
        case message = "message"
        case supports = "supports"
        case support = "support"
        case comment = "chat"
        case error = "errors"

    }

    var code =  ErrorCode.failure
    var message = ""
    var support = SupportQuery()
    var supports = Array<SupportQuery>()
    var comment = SupportMessage()
    init(json:JSON) {
        if let code = json[keys.code.stringValue].int{
            self.code = ErrorCode(rawValue: code)
        }

        if let message = json[keys.message.stringValue].string{
            self.message = message
        } else if let message = json[keys.error.stringValue].string{
            self.message = message
        }
        if let sQuery = json[keys.support.stringValue].dictionaryObject as Dictionary<String,AnyObject>?{
            self.support = SupportQuery(dict: sQuery)
        }
        if let comment = json[keys.comment.stringValue].dictionaryObject as Dictionary<String,AnyObject>?{
            self.comment = SupportMessage(dict: comment)
        }

        if let supportQueries = json[keys.supports.stringValue].arrayObject as? Array<Dictionary<String,AnyObject>>{
            for query in supportQueries{
                let sq = SupportQuery(dict: query)
                self.supports.append(sq)
            }
        }


    }
}





class SupportParser: NSObject {
    let kCode = "code"//: 200,
    let kMessage = "message"//: "success! Marina Reviews",
    let kSupport = "support"
    let kErrors = "errors"

    
    var code = ErrorCode.failure
    var message: String = ""
    var support = Support()
    
    override init() {
        super.init()
    }
    
    init(json: JSON) {
        if let _code = json[kCode].int as Int?{self.code = ErrorCode(rawValue: _code)}
        
       // if let _message = json[kMessage].string as String?{self.message = _message}
        //
        if let _rMessage = json[kMessage].string as String?{
            self.message = _rMessage
        } else if let _error = json[kErrors].string as String?{
            self.message = _error
            
        }
        if let _supportDic = json[kSupport].dictionaryObject as Dictionary<String,AnyObject>?{
            self.support = Support(dictionary:_supportDic)
        }
        
        super.init()
    }
}

class Support: NSObject {
    let kID = "id"//: 200,
    let kReferenceNo = "reference_no"//: "success! Marina Reviews",
    let kStatus = "status"
    let kUser = "user"
    //let kCreatedAt = "created_at"
    
    var ID: String = ""
    var referenceNumber: String = ""
    var status:Bool = false
    var user = User()
    
    override init() {
        super.init()
    }
    
    init(dictionary:Dictionary<String,AnyObject>) {
        
        if let _Id = dictionary[kID] as? NSInteger{
            self.ID = "\(_Id)"
        }else if let _Id = dictionary[kID] as? String{
            self.ID = _Id
        }
        
        if let _refNo = dictionary[kReferenceNo] as? String{
            self.referenceNumber = _refNo
        }
        
        if let _status = dictionary[kStatus] as? Bool{
            self.status = _status
        }
        
        if let _userDict = dictionary[kUser] as? Dictionary<String,AnyObject>{
            self.user = User(dictionary: _userDict)
        }
        
        super.init()
    }
    
}

