//
//  Offer.swift
//  TipNTap
//
//  Created by TecOrb on 26/03/18.
//  Copyright © 2018 Nakul Sharma. All rights reserved.
//

import UIKit
import SwiftyJSON

/*
 "id": 3,
 "min_price": 100,
 "offer_price": 10,
 "coupan_code": "TIP10",
 "image": "https://res.cloudinary.com/dchv9szij/image/upload/v1521727773/offers/offer_3.jpg",
 "title": "Tip $100  & Get $10 CASHBACK",
 "description": "On the Every Payment of minimum $100 you will get $10 CASHBACK instantly into your wallet. Offer is available for limited number of customers. Pay Fast & Get FAST. T&C Apply.",
 "expire_at": 0
 */

class Offer: NSObject {
    enum keys: String, CodingKey {
        case id = "id"
        case minPrice = "min_price"
        case offerPrice = "offer_price"
        case coupanCode = "coupan_code"
        case image = "image"
        case title = "title"
        case details = "description"
        case expireAt = "expire_at"

    }

//values
    var id = ""
    var minPrice : Double = 0.0
    var offerPrice : Double = 0.0
    var coupanCode = ""
    var image = ""
    var title = ""
    var details = ""
    var expireAt : TimeInterval = 0

    override init() {
        super.init()
    }
    init(dictionary:Dictionary<String,AnyObject>) {
        if let _id = dictionary[keys.id.stringValue] as? String{
            self.id = _id
        }else if let _id = dictionary[keys.id.stringValue] as? Int{
            self.id = "\(_id)"
        }
        if let _minPrice = dictionary[keys.minPrice.stringValue] as? Double{
            self.minPrice = _minPrice
        }
        if let _offerPrice = dictionary[keys.offerPrice.stringValue] as? Double{
            self.offerPrice = _offerPrice
        }
        if let _coupanCode = dictionary[keys.coupanCode.stringValue] as? String{
            self.coupanCode = _coupanCode
        }
        if let _image = dictionary[keys.image.stringValue] as? String{
            self.image = _image
        }
        if let _title = dictionary[keys.title.stringValue] as? String{
            self.title = _title
        }
        if let _details = dictionary[keys.details.stringValue] as? String{
            self.details = _details
        }

        if let _expAt = dictionary[keys.expireAt.stringValue] as? Double{
            self.expireAt = _expAt
        }
        super.init()
    }
}

class OfferParser : NSObject{
    enum keys:String,CodingKey {
        case code = "code"
        case message = "errors"
        case offers = "offers"
        case offer = "offer"
    }


    var errorCode :ErrorCode = .failure
    var message = ""
    var offers = [Offer]()
    var offer = Offer()

    override init() {
        super.init()
    }

    init(json: JSON) {
        if let _rCode = json[keys.code.stringValue].int as Int?{
            self.errorCode = ErrorCode(rawValue: _rCode)
        }
        if let _rMessage = json[keys.message.stringValue].string as String?{
            self.message = _rMessage
        }

        if let _OfferDict = json[keys.offer.stringValue].dictionaryObject as [String:AnyObject]?{
            self.offer = Offer(dictionary: _OfferDict)
        }

        if let _OfferArr  = json[keys.offers.stringValue].arrayObject as? [[String: AnyObject]]{
            self.offers.removeAll()
            for ofr in _OfferArr{
                let _offer = Offer(dictionary: ofr)
                self.offers.append(_offer)
            }
        }

        super.init()
    }

}











