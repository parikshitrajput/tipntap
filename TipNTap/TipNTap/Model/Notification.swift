//
//  Notification.swift
//  TipNTap
//
//  Created by TecOrb on 27/02/18.
//  Copyright © 2018 Nakul Sharma. All rights reserved.
//

import UIKit
import SwiftyJSON


class NotificationModel: NSObject {
    //keys
    let kID = "id"

    //value
    var ID = ""

    override init() {
        super.init()
    }
    
    init(dictionary : [String:AnyObject]){
        if let userId = dictionary[kID] as? Int{
            self.ID = "\(userId)"
        }else if let userId = dictionary[kID] as? String{
            self.ID = userId
        }
        super.init()
    }


    init(json : JSON){
        if let userId = json[kID].int{
            self.ID = "\(userId)"
        }else if let userId = json[kID].string{
            self.ID = userId
        }

        super.init()
    }
}


class NotificationParser: NSObject {
    let kResponseCode = "code"
    let kResponseMessage = "errors"
    let kNotifications = "notifications"
    let kNotification = "notification"

    var errorCode = ErrorCode.failure
    var message = ""
    var notifications = [NotificationModel]()
    var notification = NotificationModel()

    override init() {
        super.init()
    }

    init(json: JSON) {
        if let _rCode = json[kResponseCode].int as Int?{
            self.errorCode = ErrorCode(rawValue: _rCode)
        }
        if let _rMessage = json[kResponseMessage].string as String?{
            self.message = _rMessage
        }

        if let notificationDict = json[kNotification].dictionaryObject as [String:AnyObject]?{
            self.notification = NotificationModel(dictionary: notificationDict)
        }

        if let ntfcs  = json[kNotifications].arrayObject as? [[String: AnyObject]]
        {
            self.notifications.removeAll()
            for n in ntfcs{
                let _notification = NotificationModel(dictionary: n)
                self.notifications.append(_notification)
            }
        }
        super.init()
    }

}
