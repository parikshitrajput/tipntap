//
//  BankAccount.swift
//  TipNTap
//
//  Created by TecOrb on 16/03/18.
//  Copyright © 2018 Nakul Sharma. All rights reserved.
//

import UIKit
import SwiftyJSON
/*
{
    "code": 200,
    "account": {
        "id": 1,
        "account_id": "ba_1C5wmlH50CKQebF31Nwj41UI",
        "account_holder_name": "Benjamin Johnson",
        "account_type": "individual",
        "bank_name": "STRIPE TEST BANK",
        "country": "US",
        "currency": "usd",
        "last4": "6789",
        "user_id": 11,
        "default_bank_account": true
    }
}
 */

class BankAccount: NSObject {

    enum keys: String, CodingKey {
        case id = "id"
        case accountId = "account_id"
        case accountHolderName = "account_holder_name"
        case accountType = "account_type"
        case bankName = "bank_name"
        case country = "country"
        case currency = "currency"
        case last4 = "last4"
        case userId = "user_id"
        case isDefault = "default_bank_account"
    }

    var id = ""
    var accountId = ""
    var accountHolderName = ""
    var accountType = ""
    var bankName = ""
    var country = ""
    var currency = ""
    var last4 = ""
    var userId = ""
    var isDefault = false

    override init() {
        super.init()
    }

    init(dictionary:Dictionary<String,AnyObject>) {
        if let _id = dictionary[keys.id.stringValue] as? Int{
            self.id = "\(_id)"
        }else if let _id = dictionary[keys.id.stringValue] as? String{
            self.id = _id
        }

        if let _accid = dictionary[keys.accountId.stringValue] as? String{
            self.accountId = _accid
        }

        if let _accountHolderName = dictionary[keys.accountHolderName.stringValue] as? String{
            self.accountHolderName = _accountHolderName
        }

        if let _accountType = dictionary[keys.accountType.stringValue] as? String{
            self.accountType = _accountType
        }
        if let _bankName = dictionary[keys.bankName.stringValue] as? String{
            self.bankName = _bankName
        }
        if let _country = dictionary[keys.country.stringValue] as? String{
            self.country = _country
        }
        if let _currency = dictionary[keys.currency.stringValue] as? String{
            self.currency = _currency
        }
        if let _last4 = dictionary[keys.last4.stringValue] as? String{
            self.last4 = _last4
        }
        if let _uid = dictionary[keys.userId.stringValue] as? Int{
            self.userId = "\(_uid)"
        }else if let _uid = dictionary[keys.userId.stringValue] as? String{
            self.userId = _uid
        }

        if let _isDefault = dictionary[keys.isDefault.stringValue] as? Bool{
            self.isDefault = _isDefault
        }
        super.init()
    }


    func setDefault(handler:@escaping (_ success:Bool,_ bankAccount: BankAccount?,_ message:String) -> Void){
        PaymentService.sharedInstance.makeBankAccountDefault(self.accountId) { (success, bankAccount, message) in
            handler(success, bankAccount, message)
        }
    }

}

class BankAccountParser: NSObject {
    enum keys: String, CodingKey {
        case code = "code"
        case message = "message"
        case errors = "errors"
        case accounts = "accounts"
        case account = "account"
    }


    var errorCode = ErrorCode.failure
    var message = ""
    var accounts = Array<BankAccount>()
    var account = BankAccount()

    override init() {
        super.init()
    }
    init(json:JSON) {
        if let _code = json[keys.code.stringValue].int{
            self.errorCode = ErrorCode(rawValue: _code)
        }
        
        if let _message = json[keys.message.stringValue].string{
            self.message = _message
        }else if let _message = json[keys.errors.stringValue].string{
            self.message = _message
        }

        if let _bankAccount = json[keys.account.stringValue].dictionaryObject as Dictionary<String,AnyObject>?{
            self.account = BankAccount(dictionary: _bankAccount)
        }

        if let _bankAccounts = json[keys.accounts.stringValue].arrayObject as? Array<Dictionary<String,AnyObject>>{
            self.accounts.removeAll()
            for _bankAccount in _bankAccounts{
                let ac = BankAccount(dictionary: _bankAccount)
                self.accounts.append(ac)
            }
        }

        super.init()
    }


}


