//
//  UserTransaction.swift
//  TipNTap
//
//  Created by TecOrb on 28/02/18.
//  Copyright © 2018 Nakul Sharma. All rights reserved.
//

import UIKit
import SwiftyJSON
/*
 "payout": 10400,
 "is_transfered_to_payee": true,
 "description": "",
 "amount": 10400,
 "offer_code": null,
 "transaction_id": "ch_1CAJmBCprMbHqL4SFZsuJy1H",
 "transfer_remark": "",
 "contact_pay": true,
 "id": 146,
 "offer_received": false,
 "offer_applied": false,
 "payment_date": 1522164717000,
 "transaction_type": "Send",
 "admin_percentage": 0,
 "offer_price": 0
 */

class UserTransaction: NSObject {
    //keys
    enum keys :String,CodingKey{
        case payout = "payout"
        case isTransferedToPayee = "is_transfered_to_payee"
        case description = "description"
        case amount = "amount"
        case offerCode = "offer_code"
        case transactionId = "transaction_id"//: "ch_1CAJmBCprMbHqL4SFZsuJy1H",
        case transferRemark = "transfer_remark"
        case isPayUsingContactNumber = "contact_pay"
        case id = "id"
        case isOfferReceived = "offer_received"//: false,
        case isOfferApplied = "offer_applied"//: false,
        case paymentDate = "payment_date"//: 1522164717000,
        case transactionType = "transaction_type"//: "Send",
        case adminPercentage = "admin_percentage"//: 0,
        case offerPrice = "offer_price"//: 0
        case payee = "payee"
        case sender = "user"
    }

    //values
    var id = ""
    var transactionDescription = ""
    var offerCode = ""
    var transactionId = ""//: "ch_1CAJmBCprMbHqL4SFZsuJy1H",
    var transferRemark = ""
    var transactionType = ""//: "Send",

    var isTransferedToPayee = false
    var isPayUsingContactNumber = false
    var isOfferReceived = false
    var isOfferApplied = false

    var paymentDate: Double = 0
    var adminPercentage = 0.0
    var offerPrice = 0.0
    var payout = 0.0
    var amount = 0.0

    var payee: User = User()
    var sender: User = User()


    override init() {
        super.init()
    }

    init(dictionary : [String:AnyObject]){
        if let _Id = dictionary[keys.id.stringValue] as? Int{
            self.id = "\(_Id)"
        }else if let _Id = dictionary[keys.id.stringValue] as? String{
            self.id = _Id
        }

        if let trDesc = dictionary[keys.description.stringValue] as? String{
            self.transactionDescription = trDesc
        }
        if let _offerCode = dictionary[keys.offerCode.stringValue] as? String{
            self.offerCode = _offerCode
        }
        if let _transactionId = dictionary[keys.transactionId.stringValue] as? String{
            self.transactionId = _transactionId
        }
        if let _transferRemark = dictionary[keys.transferRemark.stringValue] as? String{
            self.transferRemark = _transferRemark
        }
        if let _transactionType = dictionary[keys.transactionType.stringValue] as? String{
            self.transactionType = _transactionType
        }

        if let _isTransferedToPayee = dictionary[keys.isTransferedToPayee.stringValue] as? Bool{
            self.isTransferedToPayee = _isTransferedToPayee
        }
        if let _isPayUsingContactNumber = dictionary[keys.isPayUsingContactNumber.stringValue] as? Bool{
            self.isPayUsingContactNumber = _isPayUsingContactNumber
        }
        if let _isOfferReceived = dictionary[keys.isOfferReceived.stringValue] as? Bool{
            self.isOfferReceived = _isOfferReceived
        }
        if let _isOfferApplied = dictionary[keys.isOfferApplied.stringValue] as? Bool{
            self.isOfferApplied = _isOfferApplied
        }
        if let _paymentDate = dictionary[keys.paymentDate.stringValue] as? Double{
            self.paymentDate = _paymentDate
        }else if let _paymentDate = dictionary[keys.paymentDate.stringValue] as? String{
            self.paymentDate = Double(_paymentDate) ?? 0
        }

        if let _adminPercentage = dictionary[keys.adminPercentage.stringValue] as? Double{
            self.adminPercentage = _adminPercentage/100.0
        }else if let _adminPercentage = dictionary[keys.adminPercentage.stringValue] as? String{
            self.adminPercentage = (Double(_adminPercentage) ?? 0)/100.0
        }

        if let _offerPrice = dictionary[keys.offerPrice.stringValue] as? Double{
            self.offerPrice = _offerPrice
        }else if let _offerPrice = dictionary[keys.offerPrice.stringValue] as? String{
            self.offerPrice = Double(_offerPrice) ?? 0
        }

        if let _payout = dictionary[keys.payout.stringValue] as? Double{
            self.payout = _payout/100.0
        }else if let _payout = dictionary[keys.payout.stringValue] as? String{
            self.offerPrice = (Double(_payout) ?? 0)/100.0
        }

        if let _amount = dictionary[keys.amount.stringValue] as? Double{
            self.amount = _amount/100
        }else if let _amount = dictionary[keys.amount.stringValue] as? String{
            self.amount = (Double(_amount) ?? 0)/100
        }

        if let _payeeDict = dictionary[keys.payee.stringValue] as? Dictionary<String,AnyObject>{
            self.payee = User(dictionary: _payeeDict)
        }

        if let _senderDict = dictionary[keys.sender.stringValue] as? Dictionary<String,AnyObject>{
            self.sender = User(dictionary: _senderDict)
        }

        super.init()
    }


    init(json : JSON){
        if let _Id = json[keys.id.stringValue].int {
            self.id = "\(_Id)"
        }else if let _Id = json[keys.id.stringValue].string{
            self.id = _Id
        }

        if let trDesc = json[keys.description.stringValue].string{
            self.transactionDescription = trDesc
        }
        if let _offerCode = json[keys.offerCode.stringValue].string{
            self.offerCode = _offerCode
        }
        if let _transactionId = json[keys.transactionId.stringValue].string{
            self.transactionId = _transactionId
        }
        if let _transferRemark = json[keys.transferRemark.stringValue].string{
            self.transferRemark = _transferRemark
        }
        if let _transactionType = json[keys.transactionType.stringValue].string{
            self.transactionType = _transactionType
        }

        if let _isTransferedToPayee = json[keys.isTransferedToPayee.stringValue].bool{
            self.isTransferedToPayee = _isTransferedToPayee
        }
        if let _isPayUsingContactNumber = json[keys.isPayUsingContactNumber.stringValue].bool{
            self.isPayUsingContactNumber = _isPayUsingContactNumber
        }
        if let _isOfferReceived = json[keys.isOfferReceived.stringValue].bool{
            self.isOfferReceived = _isOfferReceived
        }
        if let _isOfferApplied = json[keys.isOfferApplied.stringValue].bool{
            self.isOfferApplied = _isOfferApplied
        }
        if let _paymentDate = json[keys.paymentDate.stringValue].double{
            self.paymentDate = _paymentDate
        }else if let _paymentDate = json[keys.paymentDate.stringValue].string{
            self.paymentDate = Double(_paymentDate) ?? 0
        }

        if let _adminPercentage = json[keys.adminPercentage.stringValue].double{
            self.adminPercentage = _adminPercentage/100
        }else if let _adminPercentage = json[keys.adminPercentage.stringValue].string{
            self.adminPercentage = (Double(_adminPercentage) ?? 0)/100
        }

        if let _offerPrice = json[keys.offerPrice.stringValue].double{
            self.offerPrice = _offerPrice
        }else if let _offerPrice = json[keys.offerPrice.stringValue].string{
            self.offerPrice = Double(_offerPrice) ?? 0
        }

        if let _payout = json[keys.payout.stringValue].double{
            self.payout = _payout/100
        }else if let _payout = json[keys.payout.stringValue].string{
            self.offerPrice = (Double(_payout) ?? 0)/100
        }

        if let _amount = json[keys.amount.stringValue].double{
            self.amount = _amount/100
        }else if let _amount = json[keys.amount.stringValue].string{
            self.amount = (Double(_amount) ?? 0)/100
        }

        if let _payeeDict = json[keys.payee.stringValue].dictionaryObject as Dictionary<String,AnyObject>?{
            self.payee = User(dictionary: _payeeDict)
        }

        if let _senderDict = json[keys.sender.stringValue].dictionaryObject as Dictionary<String,AnyObject>?{
            self.sender = User(dictionary: _senderDict)
        }

        super.init()
    }
}

class UserTransactionParser: NSObject{
    let kResponseCode = "code"
    let kResponseMessage = "errors"
    let kTransactions = "payments"
    let kTransaction = "transaction"
    let kPayment = "payment"
    let kTotalPage = "total_pages"
    let ksuccessMessage = "message"

    var errorCode = ErrorCode.failure
    var message = ""
    var transactions = [UserTransaction]()
    var transaction = UserTransaction()
    var totalPage = 0
    var successMessage = ""

    override init() {
        super.init()
    }

    init(json: JSON) {
        if let _rCode = json[kResponseCode].int as Int?{
            self.errorCode = ErrorCode(rawValue: _rCode)
        }
        if let _totalPage = json[kTotalPage].int as Int?{
            self.totalPage = _totalPage
        }

        if let _rMessage = json[kResponseMessage].string as String?{
            self.message = _rMessage
        }
        
        if let _rMessage = json[ksuccessMessage].string as String?{
            self.successMessage = _rMessage
        }

        if let transactionDict = json[kTransaction].dictionaryObject as [String:AnyObject]?{
            self.transaction = UserTransaction(dictionary: transactionDict)
        }else if let transactionDict = json[kPayment].dictionaryObject as [String:AnyObject]?{
            self.transaction = UserTransaction(dictionary: transactionDict)
        }

        if let _transactions  = json[kTransactions].arrayObject as? [[String: AnyObject]]
        {
            self.transactions.removeAll()
            for t in _transactions{
                let _transaction = UserTransaction(dictionary:t)
                self.transactions.append(_transaction)
            }
        }
    }

}








//RECENT SCANS
class RecentScanParser: NSObject{
    let kResponseCode = "code"
    let kResponseMessage = "message"
    let kRecentScans = "users"
    let kRecentScan = "user"

    var errorCode = ErrorCode.failure
    var message = ""
    var recentScans = Array<RecentScan>()
    var recentScan = RecentScan()

    override init() {
        super.init()
    }

    init(json: JSON) {
        if let _rCode = json[kResponseCode].int{
            self.errorCode = ErrorCode(rawValue: _rCode)
        }
        if let _rMessage = json[kResponseMessage].string as String?{
            self.message = _rMessage
        }

        if let transactionDict = json[kRecentScan].dictionaryObject as [String:AnyObject]?{
            self.recentScan = RecentScan(dictionary: transactionDict)
        }

        if let _scans  = json[kRecentScans].arrayObject as? [[String: AnyObject]]
        {
            self.recentScans.removeAll()
            for t in _scans{
                let _scan = RecentScan(dictionary:t)
                self.recentScans.append(_scan)
            }
        }
    }

}




class RecentScan: NSObject {
    //keys
    enum keys: String,CodingKey{
        case amount = "amount"
        case payee = "payee"
        case adminCommission = "admin_percentage"
    }
    //value
    var amount = 0.0
    var adminCommission = 0.0
    var payee = User()

    override init() {
        super.init()
    }

    init(dictionary : [String:AnyObject]){
        if let _amount = dictionary[keys.amount.stringValue] as? Double{
            self.amount = _amount/100
        }else if let _amount = dictionary[keys.amount.stringValue] as? String{
            self.amount = (Double(_amount) ?? 0)/100
        }

        if let _adminCommission = dictionary[keys.adminCommission.stringValue] as? Double{
            self.adminCommission = _adminCommission/100
        }else if let _adminCommission = dictionary[keys.adminCommission.stringValue] as? String{
            self.adminCommission = (Double(_adminCommission) ?? 0)/100
        }

        if let _payeeDict = dictionary[keys.payee.stringValue] as? Dictionary<String,AnyObject>{
            self.payee = User(dictionary: _payeeDict)
        }
        super.init()
//        self.payee.adminPercentage = self.adminCommission
    }


    init(json : JSON){
        if let _amount = json[keys.amount.stringValue].double{
            self.amount = _amount/100
        }else if let _amount = json[keys.amount.stringValue].string{
            self.amount = (Double(_amount) ?? 0)/100
        }

        if let _adminCommission = json[keys.adminCommission.stringValue].double{
            self.adminCommission = _adminCommission/100
        }else if let _adminCommission = json[keys.adminCommission.stringValue].string{
            self.adminCommission = (Double(_adminCommission) ?? 0)/100
        }

        if let _payeeDict = json[keys.payee.stringValue].dictionaryObject as Dictionary<String,AnyObject>?{
            self.payee = User(dictionary: _payeeDict)
        }
        super.init()
//        self.payee.adminPercentage = self.adminCommission
    }
}
