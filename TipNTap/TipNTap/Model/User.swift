//
//  User.swift
//  MyLaundryApp
//
//  Created by TecOrb on 15/12/16.
//  Copyright © 2016 Nakul Sharma. All rights reserved.
//

import UIKit
import SwiftyJSON
/*
"user": {
    "id": 5,
    "full_name": "Jai Rajput",
    "contact": "7834821711",
    "country_code": "92",
    "api_key": "iqK3TAss2Rd9axBZN8aA",
    "is_contact_verified": false,
    "account_reference": "JG7Q5",
    "qr_image": "https://res.cloudinary.com/dchv9szij/image/upload/v1519277310/account/JG7Q5_ba8kh8.png",
    "remark": null,
    "last_active_at": "2018-02-22T05:28:29.000Z",
    "contact_public": true,
    "image": null
}*/

class User: NSObject {
    //key
    let kID = "id"
    let kFullName = "full_name"
    let kEmail = "email"
    let kContact = "contact"
    let kCountryCode = "country_code"
    let kUserToken = "api_key"
    let kIsContactVerified = "is_contact_verified"
    let kAccountReference = "account_reference"
    let kQRImage = "qr_image"
    let kRemark = "remark"
    let kLastActiveAt = "last_active_at"
    let kContactPublic = "contact_public"
    let kProfileImage = "image"
    let kAdminPercentage = "admin_percentage"
    let kServiceFee = "service_fee"
    let kBusinessAdminPercentage = "business_admin_percentage"
    let kNotification = "notify"
    let kGovDocApproved = "gov_doc_approve"
    let kGovDocReject = "gov_doc_reject"
    let kGovDocAvailable = "gov_doc_available"

    //properties
    var ID = ""
    var name = ""
    var email = ""
    var contact = ""
    var countryCode = ""
    var userToken = ""
    var isContactVerified = false
    var accountReference = ""
    var qrImage = ""
    var remark = ""
    var lastActiveAt = ""
    var isPublic = true
    var profileImage = ""
    var adminPercentage = 0.0
    var businessAdminPercentage = 0.0
    var paymentServiceFee = 0.0
    var isNotificationEnabled = false
    var govDocApproved:Bool = false
    var govDocReject: Bool = false
    var govDocAvailable:Bool = false

    override init() {
        super.init()
    }

    init(json : JSON){
        if let userId = json[kID].int{
            self.ID = "\(userId)"
        }else if let userId = json[kID].string{
            self.ID = userId
        }

        if let _fullName = json[kFullName].string {
            self.name = _fullName
        }
        
        if let userEmail = json[kEmail].string {
            self.email = userEmail
        }

        if let userContact = json[kContact].string{
            self.contact = userContact
        }
        if let _cc = json[kCountryCode].string{
            self.countryCode = _cc
        }
        if let _accessToken = json[kUserToken].string{
            self.userToken = _accessToken
        }
        if let _isContactVarified = json[kIsContactVerified].bool{
            self.isContactVerified = _isContactVarified
        }

        if let _isNotificationEnabled = json[kNotification].bool{
            self.isNotificationEnabled = _isNotificationEnabled
            AppSettings.shared.isNotificationEnable = _isNotificationEnabled
        }

        if let _accountReference = json[kAccountReference].string{
            self.accountReference = _accountReference
        }

        if let _qrImage = json[kQRImage].string{
            self.qrImage = _qrImage
        }

        if let _remark = json[kRemark].string{
            self.remark = _remark
        }

        if let _lastActiveAt = json[kLastActiveAt].string{
            self.lastActiveAt = _lastActiveAt
        }
        if let _isPublic = json[kContactPublic].bool{
            self.isPublic = _isPublic
        }
        if let userProfileImage = json[kProfileImage].string{
            self.profileImage = userProfileImage
        }

        if let _adminCommission = json[kAdminPercentage].int{
            self.adminPercentage = Double(_adminCommission)/100.0
        }else if let _adminCommission = json[kAdminPercentage].double{
            self.adminPercentage = _adminCommission/100.0
        }else if let _adminCommission = json[kAdminPercentage].string{
            self.adminPercentage = (Double(_adminCommission) ?? 0.0)/100.0
        }

        if let _paymentServiceFee = json[kServiceFee].int{
            self.paymentServiceFee = Double(_paymentServiceFee)/100.0
        }else if let _paymentServiceFee = json[kServiceFee].double{
            self.paymentServiceFee = _paymentServiceFee/100.0
        }else if let _paymentServiceFee = json[kServiceFee].string{
            self.paymentServiceFee = (Double(_paymentServiceFee) ?? 0.0)/100.0
        }


        if let _businessCommission = json[kBusinessAdminPercentage].int{
            self.businessAdminPercentage = Double(_businessCommission)/100.0
        }else if let _businessCommission = json[kBusinessAdminPercentage].double{
            self.businessAdminPercentage = _businessCommission/100.0
        }else if let _businessCommission = json[kBusinessAdminPercentage].string{
            self.businessAdminPercentage = (Double(_businessCommission) ?? 0.0)/100.0
        }
        
        if let _isGovApproved = json[kGovDocApproved].bool{
            self.govDocApproved = _isGovApproved
        }
        
        if let _isGovReject = json[kGovDocReject].bool{
            self.govDocReject = _isGovReject
        }
        if let _isGovDocAvailable = json[kGovDocAvailable].bool{
            self.govDocAvailable = _isGovDocAvailable
        }
        super.init()
    }


    init(dictionary : [String:AnyObject]){
        if let userId = dictionary[kID] as? Int{
            self.ID = "\(userId)"
        }else if let userId = dictionary[kID] as? String{
            self.ID = userId
        }

        if let _fullName = dictionary[kFullName] as? String {
            self.name = _fullName
        }

        if let userEmail = dictionary[kEmail] as? String {
            self.email = userEmail
        }

        if let userContact = dictionary[kContact] as? String{
            self.contact = userContact
        }
        if let _cc = dictionary[kCountryCode] as? String{
            self.countryCode = _cc
        }
        if let _accessToken = dictionary[kUserToken] as? String{
            self.userToken = _accessToken
        }
        if let _isContactVarified = dictionary[kIsContactVerified] as? Bool{
            self.isContactVerified = _isContactVarified
        }
        if let _isNotificationEnabled = dictionary[kNotification] as? Bool{
            self.isNotificationEnabled = _isNotificationEnabled
            AppSettings.shared.isNotificationEnable = _isNotificationEnabled
        }
        if let _accountReference = dictionary[kAccountReference] as? String{
            self.accountReference = _accountReference
        }
        if let _qrImage = dictionary[kQRImage] as? String{
            self.qrImage = _qrImage
        }
        if let _remark = dictionary[kRemark] as? String{
            self.remark = _remark
        }
        if let _lastActiveAt = dictionary[kLastActiveAt] as? String{
            self.lastActiveAt = _lastActiveAt
        }
        if let _isPublic = dictionary[kContactPublic] as? Bool{
            self.isPublic = _isPublic
        }
        if let userProfileImage = dictionary[kProfileImage] as? String{
            self.profileImage = userProfileImage
        }
        

        if let _adminCommission = dictionary[kAdminPercentage] as? Int{
            self.adminPercentage = Double(_adminCommission)/100.0
        }else if let _adminCommission = dictionary[kAdminPercentage] as? Double{
            self.adminPercentage = _adminCommission/100.0
        }else if let _adminCommission = dictionary[kAdminPercentage] as? String{
            self.adminPercentage = (Double(_adminCommission) ?? 0.0)/100.0
        }

        if let _paymentServiceFee = dictionary[kServiceFee] as? Int{
            self.paymentServiceFee = Double(_paymentServiceFee)/100.0
        }else if let _paymentServiceFee = dictionary[kServiceFee] as? Double{
            self.paymentServiceFee = _paymentServiceFee/100.0
        }else if let _paymentServiceFee = dictionary[kServiceFee] as? String{
            self.paymentServiceFee = (Double(_paymentServiceFee) ?? 0.0)/100.0
        }


        if let _businessCommission = dictionary[kBusinessAdminPercentage] as? Int{
            self.businessAdminPercentage = Double(_businessCommission)/100.0
        }else if let _businessCommission = dictionary[kBusinessAdminPercentage] as? Double{
            self.businessAdminPercentage = _businessCommission/100.0
        }else if let _businessCommission = dictionary[kBusinessAdminPercentage] as? String{
            self.businessAdminPercentage = (Double(_businessCommission) ?? 0.0)/100.0
        }
        
        if let _govApproved = dictionary[kGovDocApproved] as? Bool{
            self.govDocApproved = _govApproved
        }
        if let _govReject = dictionary[kGovDocReject] as? Bool{
            self.govDocReject = _govReject
        }
        if let _isGovDocAvailable = dictionary[kGovDocAvailable] as? Bool{
            self.govDocAvailable = _isGovDocAvailable
        }

        super.init()
    }


    func saveUserJSON(_ json:JSON) {
        if let userInfo = json["user"].dictionaryObject as [String:AnyObject]?{
            let documentPath = NSHomeDirectory() + "/Documents/"
            do {
                let data = try JSON(userInfo).rawData(options: [.prettyPrinted])
                let path = documentPath + "user"
                try data.write(to: URL(fileURLWithPath: path), options: .atomic)
            }catch{
                print_debug("error in saving userinfo")
            }
            UserDefaults.standard.synchronize()
        }
    }


    class func loadSavedUser() -> User {
        let documentPath = NSHomeDirectory() + "/Documents/"
        let path = documentPath + "user"
        var data = Data()
        var json : JSON
        do{
            data = try Data(contentsOf: URL(fileURLWithPath: path))
            json = try JSON(data: data)
        }catch{
            json = JSON.init(data)
            print_debug("error in getting userinfo")
        }

        let user = User(json: json)
        return user
    }

    func logOut(_ completionBlock:@escaping (_ success:Bool,_ user:User?,_ message:String) -> Void){
        LoginService.sharedInstance.logOut{ (success, user, message) in
            completionBlock(success, user, message)
        }
    }


    func loadUserWallet(handler:@escaping (_ success:Bool,_ userWallet: Wallet?, _ message:String)->Void){
        TransactionService.sharedInstance.getUserWallet { (success, wallet, message) in
            handler(success, wallet, message)
        }
    }

    
    func getKYC(handler:@escaping (_ success:Bool,_ userKYC :UserKYC?, _ message:String)->Void){
        LoginService.sharedInstance.checkForUserKYC { (success, isKYCDone, message) in
            handler(success, isKYCDone, message)
        }
    }

    func getNotificationSetting(handler:@escaping (_ success:Bool,_ userProfile: User?, _ message:String)->Void){
        LoginService.sharedInstance.getNotificationSetting { (success, profile, message) in
            handler(success, profile, message)
        }
    }

    func setUserProfileOnServer(newName:String?,image:UIImage?,notification:Bool?,handler:@escaping (_ success:Bool,_ userProfile: User?, _ message:String)->Void){
        LoginService.sharedInstance.setProfileOnServer(newName: newName, image: image, notification: notification) { (success, resUser, message) in
            handler(success, resUser, message)
        }
    }

    func toggleNotificationSetting(handler:@escaping (_ success:Bool,_ isNotificationEnabled: Bool?, _ message:String)->Void){
        LoginService.sharedInstance.updateNotificationSettings { (success, status, message) in
            handler(success, status, message)
        }
    }

    func getReferralCode(handler:@escaping (_ success:Bool,_ referralCode :String?, _ message:String)->Void){
        LoginService.sharedInstance.getReferralCode { (success, referralCode, message) in
            handler(success, referralCode, message)
        }
    }
}

class UserKYC: NSObject{
//{
//    "code": 200,
//    "kyc_status": "unverified",
//    "type": "individual"
//    }
    let kCode = "code"
    let kKYC = "kyc_status"
    let kType = "type"
    let kMessage = "errors"
    let kIsUserDetailsSent = "is_user_details_send"
    let kKYCState = "status"
//    "status": "unverified",
//    "is_user_details_send": false

    var errorCode : ErrorCode = .failure
    var kycStatus = false
    var accountType : AccountType = .individual
    var message = ""
    var isUserDetailSent = false
    var kycState = ""

    override init() {
        super.init()
    }
    init(json: JSON) {
        if let _rCode = json[kCode].int as Int?{
            self.errorCode = ErrorCode(rawValue: _rCode)
        }

        if let _rMessage = json[kMessage].string as String?{
            self.message = _rMessage
        }else if let _rMessage = json["response_message"].string as String?{
            self.message = _rMessage
        }else if let _rMessage = json["message"].string as String?{
            self.message = _rMessage
        }

        if let _isKYCDone = json[kKYC].string{
            self.kycStatus = (_isKYCDone.lowercased() == "unverified") ? false : true
        }else if let _isKYCDone = json[kKYC].bool{
            self.kycStatus = _isKYCDone
        }


        if let _accType = json[kType].string{
            self.accountType = (_accType.lowercased() == "individual") ? .individual : .business
        }

        if let _kycState = json[kKYCState].string{
            self.kycState = _kycState
        }

        if let _isKYCDetailsSent = json[kIsUserDetailsSent].bool{
            self.isUserDetailSent = _isKYCDetailsSent
        }


    }

}


class UserParser: NSObject {
    let kResponseCode = "code"
    let kResponseMessage = "errors"
    let kUsers = "users"
    let kUser = "user"
    let kKYC = "kyc"
    let kReferralCode = "referral_url"
    let kNotificationSettings = "current_stage"
    var errorCode = ErrorCode.failure
    var message = ""
    var users = [User]()
    var user = User()
    var referralCode = ""
    var notificationEnabled = false
    override init() {
        super.init()
    }

    init(json: JSON) {
        if let _rCode = json[kResponseCode].int as Int?{
            self.errorCode = ErrorCode(rawValue: _rCode)
        }

        if let _rMessage = json[kResponseMessage].string as String?{
            self.message = _rMessage
        }else if let _rMessage = json["response_message"].string as String?{
            self.message = _rMessage
        }else if let _rMessage = json["message"].string as String?{
            self.message = _rMessage
        }

        if let _notificationEnabled = json[kNotificationSettings].bool as Bool?{
            self.notificationEnabled = _notificationEnabled
            AppSettings.shared.isNotificationEnable = _notificationEnabled
        }
        
        if let userDict = json[kUser].dictionaryObject as [String:AnyObject]?{
            self.user = User(dictionary: userDict)
        }
        
        if let _referralCode = json[kReferralCode].string as String?{
            self.referralCode = _referralCode
        }

        if let usrs  = json[kUsers].arrayObject as? [[String: AnyObject]]
        {
            users.removeAll()
            for u in usrs{
                let usr = User(dictionary: u)
                users.append(usr)
            }
        }
    }
    
}

class OTPValidationParser: NSObject {
    let kResponseCode = "code"
    let kResponseMessage = "errors"
    var errorCode = ErrorCode.failure
    var message = ""
    var isVarified = false
    var token = ""
    override init() {
        super.init()
    }

    init(json: JSON) {
        if let _rCode = json[kResponseCode].int{
            self.errorCode = ErrorCode(rawValue: _rCode)
        }
        if let _rMessage = json[kResponseMessage].string as String?{
            self.message = _rMessage
        }else if let _rMessage = json["message"].string as String?{
            self.message = _rMessage
        }

        if let resultDic = json["result"].dictionaryObject as [String:AnyObject]?{
            if let _varified = resultDic["is_verified"] as? Bool{
                self.isVarified = _varified
            }
            if let _token = resultDic["reset_password_token"] as? String{
                self.token = _token
            }

        }


    }

}

/*
{
    "code": 200,
    "message": "success! User badges count",
    "badge_counts": {
        "notifications": 4,
        "show_rate_popup": false
    }
}

 "ratings": {
 "rated_at_app_store": true,
 "rated_at_play_store": false
 }
*/

class BadgeCountParser: NSObject {
    let kResponseCode = "code"
    let kResponseMessage = "errors"

    var errorCode = ErrorCode.failure

    var message = ""
    var shouldShowRatingPopup = false
    var didRatedOnAppStore = false
    var badgesCount: Int = 0
    override init() {
        super.init()
    }

    init(json: JSON) {
        if let _rCode = json[kResponseCode].int as Int?{
            self.errorCode = ErrorCode(rawValue: _rCode)
        }
        if let _rMessage = json[kResponseMessage].string as String?{
            self.message = _rMessage
        }

        if let resultDic = json["badge_counts"].dictionaryObject as [String:AnyObject]?{
            if let _shouldRatingPopUp = resultDic["show_rate_popup"] as? Bool{
                self.shouldShowRatingPopup = _shouldRatingPopUp
                self.didRatedOnAppStore = !_shouldRatingPopUp
            }

            if let _notifications = resultDic["notifications"] as? Int{
                self.badgesCount = _notifications
            }
        }

        if let resultOfRatedOnAppStore = json["ratings"].dictionaryObject as [String:AnyObject]?{
            if let _didRatedOnAppStore = resultOfRatedOnAppStore["rated_at_app_store"] as? Bool{
                self.didRatedOnAppStore = _didRatedOnAppStore
                self.shouldShowRatingPopup = !_didRatedOnAppStore
            }
        }


    }

}



