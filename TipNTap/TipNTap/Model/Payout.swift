//
//  Payout.swift
//  TipNTap
//
//  Created by TecOrb on 30/05/18.
//  Copyright © 2018 Nakul Sharma. All rights reserved.
//

import UIKit
import SwiftyJSON
/*
 "id": 1,
 "amount": "1000",
 "arrival_date": "1527552000",
 "balance_transaction": "txn_1CWgNDJIQtPsgaOeLnVAWuTb",
 "payout_at": "1527494795",
 "currency": "usd",
 "description": "STRIPE TRANSFER",
 "status": "paid",
 "payout_id": "po_1CWgNDJIQtPsgaOe7JESlTz6",
 "bank_account"
 */

class Payout: NSObject {
    enum keys: String, CodingKey {
        case id = "id"
        case balanceTransaction = "balance_transaction"
        case amount = "amount"
        case payoutDate = "payout_at"
        case arrivalDate = "arrival_date"
        case currency = "currency"
        case description = "description"
        case status = "status"
        case payoutID = "payout_id"
        case bankAccount = "bank_account"
    }

    // values
    var id = ""
    var balanceTransaction = ""
    var amount = 0.0
    var payoutDate: Double = 0.0
    var arrivalDate:Double = 0.0
    var currency = ""
    var payoutDescription = ""
    var status: PayoutStatus = PayoutStatus.paid
    var payoutID = ""
    var bankAccount = BankAccount()


    override init() {super.init()}
    init(dictionary:Dictionary<String,AnyObject>) {
        if let id = dictionary[keys.id.stringValue] as? String{
            self.id = id
        }else if let id = dictionary[keys.id.stringValue] as? Int{
            self.id = "\(id)"
        }

        if let balanceTransaction = dictionary[keys.balanceTransaction.stringValue] as? String{
            self.balanceTransaction = balanceTransaction
        }

        if let amount = dictionary[keys.amount.stringValue] as? Double{
            self.amount = amount/100
        }else if let amount = dictionary[keys.amount.stringValue] as? String{
            self.amount = (Double(amount) ?? 0)/100
        }

        if let payoutDate = dictionary[keys.payoutDate.stringValue] as? Double{
            self.payoutDate = payoutDate
        }else if let payoutDate = dictionary[keys.payoutDate.stringValue] as? String{
            self.payoutDate = Double(payoutDate) ?? 0
        }

        if let arrivalDate = dictionary[keys.arrivalDate.stringValue] as? Double{
            self.arrivalDate = arrivalDate
        }else if let arrivalDate = dictionary[keys.arrivalDate.stringValue] as? String{
            self.arrivalDate = Double(arrivalDate) ?? 0
        }

        if let currency = dictionary[keys.currency.stringValue] as? String{
            self.currency = currency
        }
        if let payoutDescription = dictionary[keys.description.stringValue] as? String{
            self.payoutDescription = payoutDescription
        }
        if let status = dictionary[keys.status.stringValue] as? String{
            self.status = PayoutStatus(rawValue: status)
        }

        if let payoutID = dictionary[keys.payoutID.stringValue] as? String{
            self.payoutID = payoutID
        }

        if let bankAccountDict = dictionary[keys.bankAccount.stringValue] as? Dictionary<String,AnyObject>{
            self.bankAccount = BankAccount(dictionary: bankAccountDict)
        }

    }
}

class PayoutParser: NSObject {
    enum keys: String, CodingKey {
        case code = "code"
        case message = "message"
        case payouts = "payouts"
        case totalPage = "totalPage"

    }
    //values
    var errorCode = ErrorCode.failure
    var message = ""
    var payouts = Array<Payout>()
    var totalPage = 0

    init(json: JSON) {
        if let _code = json[keys.code.stringValue].int{
            self.errorCode = ErrorCode(rawValue: _code)
        }

        if let totalPage = json[keys.totalPage.stringValue].int{
            self.totalPage = totalPage
        }
        
        if let message = json[keys.message.stringValue].string{
            self.message = message
        }
        if let payoutsArray = json[keys.payouts.stringValue].arrayObject as? Array<Dictionary<String,AnyObject>>{
            self.payouts.removeAll()
            for payoutDict in payoutsArray{
                let payout = Payout(dictionary: payoutDict)
                self.payouts.append(payout)
            }
        }
    }
}
