//
//  WalletParser.swift
//  TipNTap
//
//  Created by TecOrb on 15/03/18.
//  Copyright © 2018 Nakul Sharma. All rights reserved.
//



import UIKit
import Foundation
import SwiftyJSON

class WalletParser : NSObject {
    var wallet = Wallet()
    var message = ""
    let kCode = "code"
    let kAccount = "account"
    let kErrors = "errors"
    let kMessage = "message"
    var errorCode: ErrorCode = .failure
    override init() {
        super.init()
    }

    init(json:JSON){
        if let _code = json[kCode].int{
            self.errorCode = ErrorCode(rawValue:_code)
        }
        if let _msg = json[kMessage].string{
            self.message = _msg
        }else if let _msg = json[kErrors].string{
            self.message = _msg
        }
        if let _wallet = json[kAccount].dictionaryObject as Dictionary<String,AnyObject>?{
            self.wallet = Wallet(dictionary: _wallet)
        }

        super.init()
    }



}




class Wallet : NSObject {
    var object : String = ""
    var available = Array<AccountInstance>()
    var isLive = false
    var pending = Array<AccountInstance>()

    enum keys: String, CodingKey {
        case object = "object"
        case available = "available"
        case livemode = "livemode"
        case pending = "pending"
    }

    override init() {
        super.init()
    }

    init(dictionary:Dictionary<String,AnyObject>) {
        if let _object = dictionary[keys.object.stringValue] as? String{
            self.object = _object
        }

        if let _isLiveMode = dictionary[keys.object.stringValue] as? Bool{
            self.isLive = _isLiveMode
        }

        if let _avl = dictionary[keys.available.stringValue] as? Array<Dictionary<String,AnyObject>>{
            self.available.removeAll()
            for _ac in _avl{
                let actInst = AccountInstance(dictionary: _ac)
                self.available.append(actInst)
            }
        }

        if let _pnd = dictionary[keys.pending.stringValue] as? Array<Dictionary<String,AnyObject>>{
            self.pending.removeAll()
            for _pn in _pnd{
                let actInst = AccountInstance(dictionary: _pn)
                self.pending.append(actInst)
            }
        }

        super.init()
    }




}




class AccountInstance : NSObject {
    var currency : String = ""
    var amount : Double = 0.00 //in USD

    enum keys: String, CodingKey {
        case currency = "currency"
        case amount = "amount"
    }

    override init() {
        super.init()
    }

    init(dictionary:Dictionary<String,AnyObject>) {
        if let _currency = dictionary[keys.currency.stringValue] as? String{
            self.currency = _currency
        }
        if let _amount = dictionary[keys.amount.stringValue] as? Double{
            self.amount = _amount/100.0 //cent to dollars
        }else if let _amount = dictionary[keys.amount.stringValue] as? Int{
            self.amount = Double(_amount)/100.0 //cent to dollars
        }
        super.init()
    }
}










