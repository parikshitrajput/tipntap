//
//  LoginService.swift
//  Bloom Trade
//
//  Created by TecOrb on 05/09/16.
//  Copyright © 2016 Nakul Sharma. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class LoginService {
    static let sharedInstance = LoginService()
    fileprivate init() {}

    func updateDeviceTokeOnServer(){

        /*
         "device_type": "ios",
         "device_id": "xxdfsdafzxsdfsd2342erwerqw423423wrqwer3423442"
         */
            var params = Dictionary<String,String>()
            let user = User.loadSavedUser()
            params.updateValue(user.ID, forKey: "user_id")
            if user.ID.trimmingCharacters(in: .whitespaces).count == 0{return}
            params.updateValue("ios", forKey: "device_type")
            if let deviceID = kUserDefaults.value(forKey: kDeviceToken) as? String {
                params.updateValue(deviceID, forKey: "device_id")
            }else{
                params.updateValue("--", forKey: "device_id")
            }
            var shouldUpdate :Bool = true
            for (_, value) in params{
                if value == "" || value == "--"{
                    shouldUpdate = false
                    break
                }
            }
            if !shouldUpdate{
                return
            }
        
            let head =  AppSettings.shared.prepareHeader(withAuth: true)
            print_debug("hitting \(api.updateDeviceToken.url()) with param \(params) and headers :\(head)")
            Alamofire.request(api.updateDeviceToken.url(),method: .post ,parameters: params, headers:head).responseJSON { response in
                switch response.result {
                case .success:
                    if let value = response.result.value {
                        let json = JSON(value)
                        print_debug("update device token json is:\n\(json)")
                        if let responseCode = json["code"].int{
                            if responseCode == 345{
                                AppSettings.shared.proceedToLandingPage()
                            }else if responseCode == 102{

                            }
                        }
                    }
                case .failure(let error):
                    print_debug(error.localizedDescription)
                   // self.retryUpdateDeviceTokeOnServer()
                }
            }

    }


   private func retryUpdateDeviceTokeOnServer() {
        self.updateDeviceTokeOnServer()
    }


    func getTokenForResetPassword(countryCode:String, phoneNumber:String, questionOne:Question, questionTwo:Question, completionBlock: @escaping (_ success:Bool,_ token: String?,_ message: String) -> Void){
        let head =  AppSettings.shared.prepareHeader(withAuth: false)


        var params = Dictionary<String,Any>()
        params.updateValue(phoneNumber, forKey: "contact")
        let cc = countryCode.replacingOccurrences(of: "+", with: "")
        params.updateValue(cc, forKey: "country_code")

        var securityQuestionParam1 = Dictionary<String,String>()
        securityQuestionParam1.updateValue(questionOne.ID, forKey: "question_id")
        securityQuestionParam1.updateValue(questionOne.answer, forKey: "answer")
        params.updateValue(securityQuestionParam1, forKey: "security_question1")

        var securityQuestionParam2 = Dictionary<String,String>()
        securityQuestionParam2.updateValue(questionTwo.ID, forKey: "question_id")
        securityQuestionParam2.updateValue(questionTwo.answer, forKey: "answer")
        params.updateValue(securityQuestionParam2, forKey: "security_question2")

        print_debug("hitting \(api.forgotPassword.url()) and headers :\(head)")

        Alamofire.request(api.forgotPassword.url(), method:.post, parameters:params, headers:head).responseJSON { response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print_debug("questions json is:\n\(json)")
                    let questionsParser = QuestionParser(json: json)
                    completionBlock((questionsParser.errorCode == .success), questionsParser.token, questionsParser.message)
                }else{
                    completionBlock(false,nil,response.result.error?.localizedDescription ?? "Some thing went wrong")
                }
            case .failure(let error):
                completionBlock(false,nil,error.localizedDescription)
            }
        }
    }

    func getListOfSecurityQuestions(_ completionBlock: @escaping (_ success:Bool,_ questions: Set<Question>?,_ message: String) -> Void){
        let head =  AppSettings.shared.prepareHeader(withAuth: false)
        print_debug("hitting \(api.listOfSecurityQuestions.url()) and headers :\(head)")
        Alamofire.request(api.listOfSecurityQuestions.url(),method: .get, headers:head).responseJSON { response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print_debug("questions json is:\n\(json)")
                    let questionsParser = QuestionParser(json: json)
                    completionBlock((questionsParser.errorCode == .success), questionsParser.questions, questionsParser.message)
                }else{
                    completionBlock(false,nil,response.result.error?.localizedDescription ?? "Some thing went wrong")
                }
            case .failure(let error):
                completionBlock(false,nil,error.localizedDescription)
            }
        }
    }


    func getListOfSecurityQuestionsForUser(_ countryCode: String, phoneNumber: String , completionBlock: @escaping (_ success:Bool,_ questions: Set<Question>?,_ message: String) -> Void){
        let head =  AppSettings.shared.prepareHeader(withAuth: false)
        var params = Dictionary<String,String>()
        let cc = countryCode.replacingOccurrences(of: "+", with: "")
        params.updateValue(cc.removingWhitespaces(), forKey: "country_code")
        params.updateValue(phoneNumber, forKey: "contact")

        print_debug("hitting \(api.userSelectedSecurityQuestions.url()) with param \(params) and headers :\(head)")
        Alamofire.request(api.userSelectedSecurityQuestions.url(),method: .post, parameters: params, headers:head).responseJSON { response in
                AppSettings.shared.hideLoader()

            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print_debug("user questions json is:\n\(json)")
                    let questionsParser = QuestionParser(json: json)
                    completionBlock((questionsParser.errorCode == .success), questionsParser.questions, questionsParser.message)
                }else{
                    completionBlock(false,nil,response.result.error?.localizedDescription ?? "Some thing went wrong")
                }
            case .failure(let error):
                completionBlock(false,nil,error.localizedDescription)
            }
        }
    }

    func logOut(_ completionBlock:@escaping (_ success:Bool,_ user: User?,_ message: String) -> Void){
            let head =  AppSettings.shared.prepareHeader(withAuth: true)
            print_debug("hitting \(api.logout.url()) with and headers :\(head)")
            AppSettings.shared.showLoader(withStatus: "Logging Out..")
            Alamofire.request(api.logout.url(),method: .get, headers:head).responseJSON { response in
                if AppSettings.shared.isLoaderOnScreen{
                    AppSettings.shared.hideLoader()
                }
                switch response.result {
                case .success:
                    if let value = response.result.value {
                        let json = JSON(value)
                        print_debug("log out json is:\n\(json)")
                        let userParser = UserParser(json: json)
                        completionBlock((userParser.errorCode == .success), userParser.user, userParser.message)
                    }else{
                        completionBlock(false,nil,response.result.error?.localizedDescription ?? "Some thing went wrong")
                    }
                case .failure(let error):
                    completionBlock(false,nil,error.localizedDescription)
                }
            }
    }


    func loginWith(_ contact:String,countryCode:String,password:String,completionBlock:@escaping (_ success:Bool,_ user: User?,_ message: String) -> Void){
        let cc = countryCode.replacingOccurrences(of: "+", with: "")
        let params = ["contact":contact,"password":password,"country_code":cc]
//        params.updateValue("ios", forKey: "device_type")
//        if let deviceID = kUserDefaults.value(forKey: kDeviceToken) as? String {
//            params.updateValue(deviceID, forKey: "device_id")
//        }else{
//            params.updateValue("--", forKey: "device_id")
//        }

        let head =  AppSettings.shared.prepareHeader(withAuth: false)
        print_debug("hitting \(api.login.url()) with param \(params) and headers :\(head)")
        AppSettings.shared.showLoader(withStatus: "Authenticating..")
        Alamofire.request(api.login.url(),method: .post ,parameters: ["session":params], headers:head).responseJSON {[password, contact,countryCode] response in
            AppSettings.shared.hideLoader()
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print_debug("sign in json is:\n\(json)")
                    let userParser = UserParser(json: json)
                    if userParser.errorCode == .success{
                        AppSettings.shared.passwordEncrypted = password
                        AppSettings.shared.phoneNumber = contact
                        AppSettings.shared.countryCode = countryCode
                        userParser.user.saveUserJSON(json)
                        AppSettings.shared.isLoggedIn = true
                        AppSettings.shared.isRegistered = true
                        completionBlock((userParser.errorCode == .success),userParser.user,userParser.message)
                    }else if userParser.errorCode == .forceUpdate{
                        AppSettings.shared.showForceUpdateAlert()
                    }else if userParser.errorCode == .sessionExpire{
                        AppSettings.shared.showSessionExpireAndProceedToLandingPage()
                    }else{
                        completionBlock((userParser.errorCode == .success),userParser.user,userParser.message)
                    }
                }else{
                    completionBlock(false,nil,response.result.error?.localizedDescription ?? "Some thing went wrong")
                }
            case .failure(let error):
                completionBlock(false,nil,error.localizedDescription)
            }
        }
    }





    func forgotPasswordForEmail(_ email: String,completionBlock:@escaping (_ success:Bool, _ message:String)-> Void){
        let head =  AppSettings.shared.prepareHeader(withAuth: false)

        let param = ["email":email,"role":"customer"]
        AppSettings.shared.showLoader(withStatus: "Please wait..")
        Alamofire.request(api.forgotPassword.url(), method: .post, parameters: param,  headers: head).responseJSON { response in
            if AppSettings.shared.isLoaderOnScreen{
                AppSettings.shared.hideLoader()
            }

            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print_debug("forgotpassword json is:\n\(json)")
                    if let responseCode = json["code"].int as Int?
                    {
                        if responseCode == 200{
                            completionBlock(true,json["message"].string ?? "")
                        }
                        else{
                            completionBlock(false,json["message"].string ?? response.result.error?.localizedDescription ?? "Something went wrong!")
                        }
                    }else{
                        completionBlock(false,json["message"].string ?? response.result.error?.localizedDescription ?? "Something went wrong!")
                    }
                }else{
                    completionBlock(false,response.result.error?.localizedDescription ?? "Something went wrong!")
                }
            case .failure(let error):
                completionBlock(false,error.localizedDescription)
            }
        }

    }


    func varifyOTPWithEmail(_ email: String,OTP:String,completionBlock:@escaping (_ otpValidation: OTPValidationParser?,_ message:String)-> Void){

        let param = ["email":email,"role":"customer","otp":OTP]
        AppSettings.shared.showLoader(withStatus: "Varifying..")
        let head =  AppSettings.shared.prepareHeader(withAuth: false)

        Alamofire.request(api.otpVarification.url(), method: .post, parameters: param,  headers: head).responseJSON { response in
            if AppSettings.shared.isLoaderOnScreen{
                AppSettings.shared.hideLoader()
            }
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print_debug("varify OTP json is:\n\(json)")
                    let otpValidation = OTPValidationParser(json:json)
                    completionBlock(otpValidation,otpValidation.message)
                }else{
                    completionBlock(nil,response.result.error?.localizedDescription ?? "Something went wrong!")
                }
            case .failure(let error):
                completionBlock(nil,error.localizedDescription)
            }
        }
    }

    func resetPassword(newPassword password : String, withToken token: String,completionBlock:@escaping (_ success: Bool,_ message:String)-> Void){

        let param = ["password":password]
        var head = AppSettings.shared.prepareHeader(withAuth: false)
        head.updateValue(token, forKey: "passwordToken")
        AppSettings.shared.showLoader(withStatus: "Resetting..")
        Alamofire.request(api.resetPassword.url(), method: .post, parameters: param,  headers: head).responseJSON { response in
            AppSettings.shared.hideLoader()

            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print_debug("Reset Password json is:\n\(json)")
                    let otpValidation = OTPValidationParser(json:json)
                    completionBlock((otpValidation.errorCode == .success),otpValidation.message)
                }else{
                    completionBlock(false,response.result.error?.localizedDescription ?? "Something went wrong!")
                }
            case .failure(let error):
                completionBlock(false,error.localizedDescription)
            }
        }

    }

    func setupPassword(_ oldPassword : String, withNewPassword newPassword: String,completionBlock:@escaping (_ success: Bool,_ message:String)-> Void){
        var param = Dictionary<String,String>()
        param.updateValue(oldPassword, forKey: "old_password")
        param.updateValue(newPassword, forKey: "password")
        let head = AppSettings.shared.prepareHeader(withAuth: true)
        AppSettings.shared.showLoader(withStatus: "Setting up..")
        Alamofire.request(api.updatePassword.url(), method: .post, parameters: param,  headers: head).responseJSON { response in
            AppSettings.shared.hideLoader()
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print_debug("setup Password json is:\n\(json)")
                    let otpValidation = OTPValidationParser(json:json)
                    if otpValidation.errorCode == .sessionExpire{
                        AppSettings.shared.hideLoader()
                        AppSettings.shared.showSessionExpireAndProceedToLandingPage()
                    }else{
                        completionBlock((otpValidation.errorCode == .success),otpValidation.message)
                    }
                }else{
                    completionBlock(false,response.result.error?.localizedDescription ?? "Something went wrong!")
                }
            case .failure(let error):
                completionBlock(false,error.localizedDescription)
            }
        }

    }

    func checkUserStatus(_ countryCode: String, phoneNumber:String, fullName:String, completionBlock:@escaping (_ isExist:Bool, _ message: String) -> Void){
        var params = Dictionary<String,String>()
        params.updateValue(countryCode.replacingOccurrences(of: "+", with: ""), forKey: "country_code")
        params.updateValue(phoneNumber, forKey: "contact")

        let head =  AppSettings.shared.prepareHeader(withAuth: false)
        print_debug("hitting \(api.checkUserStatus.url()) with param \(params) and headers :\(head)")

        AppSettings.shared.showLoader(withStatus: "Checking..")
        Alamofire.request(api.checkUserStatus.url(),method: .post,parameters:params, headers:head).responseJSON { response in
            AppSettings.shared.hideLoader()
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print_debug("status json is:\n\(json)")
                    let userParser = UserParser(json: json)
                    if userParser.errorCode == .forceUpdate{
                        AppSettings.shared.showForceUpdateAlert()
                    }else{
                        completionBlock((userParser.errorCode == .success),userParser.message)
                    }
                }else{
                    completionBlock(true,response.result.error?.localizedDescription ?? "Some thing went wrong")
                }
            case .failure(let error):
                completionBlock(true,error.localizedDescription)
            }
        }
    }

    func checkForUserKYC(handler:@escaping (_ success:Bool,_ userKYC : UserKYC?, _ message:String)->Void){
        let head =  AppSettings.shared.prepareHeader(withAuth: true)
        print_debug("hitting \(api.checkForKYC.url()) and headers :\(head)")
        Alamofire.request(api.checkForKYC.url(),method: .get, headers:head).responseJSON { response in
            AppSettings.shared.hideLoader()
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print_debug("verify json is:\n\(json)")
                    let kyc = UserKYC(json: json)
                    if kyc.errorCode == .sessionExpire{
                        AppSettings.shared.showSessionExpireAndProceedToLandingPage()
                    }else if kyc.errorCode == .forceUpdate{
                        AppSettings.shared.showForceUpdateAlert()
                    }else if kyc.errorCode == .success{
                        handler((kyc.errorCode == .success),kyc,kyc.message)
                    }else{
                        handler(false,nil,response.result.error?.localizedDescription ?? "Some thing went wrong")
                    }
                }
            case .failure(let error):
                handler(false,nil,error.localizedDescription)
            }
        }
    }

    func validateUser(_ completionBlock:@escaping (_ success:Bool,_ user: User?,_ message: String) -> Void){
        let head =  AppSettings.shared.prepareHeader(withAuth: true)
        print_debug("hitting \(api.varifyUser.url()) and headers :\(head)")
        AppSettings.shared.showLoader(withStatus: "Verifying..")
        Alamofire.request(api.varifyUser.url(),method: .get, headers:head).responseJSON { response in
            AppSettings.shared.hideLoader()
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print_debug("verify json is:\n\(json)")
                    let userParser = UserParser(json: json)
                    if userParser.errorCode == .success{
                        userParser.user.saveUserJSON(json)
                    }
                    if userParser.errorCode == .forceUpdate{
                        AppSettings.shared.showForceUpdateAlert()
                    }else if userParser.errorCode == .sessionExpire{
                        AppSettings.shared.showSessionExpireAndProceedToLandingPage()
                    }else{
                        completionBlock((userParser.errorCode == .success),userParser.user,userParser.message)
                    }
                }else{
                    completionBlock(false,nil,response.result.error?.localizedDescription ?? "Some thing went wrong")
                }
            case .failure(let error):
                completionBlock(false,nil,error.localizedDescription)
            }
        }
    }

    func getReferralCode(_ completionBlock:@escaping (_ success:Bool,_ referralCode: String?,_ message: String) -> Void){
        let head =  AppSettings.shared.prepareHeader(withAuth: true)
        print_debug("hitting \(api.referralCode.url()) and headers :\(head)")
        AppSettings.shared.showLoader(withStatus: "Please wait..")
        Alamofire.request(api.referralCode.url(),method: .get, headers:head).responseJSON { response in
            AppSettings.shared.hideLoader()
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print_debug("referral code json is:\n\(json)")
                    let userParser = UserParser(json: json)
                    if userParser.errorCode == .forceUpdate{
                        AppSettings.shared.showForceUpdateAlert()
                    }else if userParser.errorCode == .sessionExpire{
                        AppSettings.shared.showSessionExpireAndProceedToLandingPage()
                    }else{
                        completionBlock((userParser.errorCode == .success),userParser.referralCode,userParser.message)
                    }
                }else{
                    completionBlock(false,nil,response.result.error?.localizedDescription ?? "Some thing went wrong")
                }
            case .failure(let error):
                completionBlock(false,nil,error.localizedDescription)
            }
        }
    }


    func registerUserWith(_ name:String,contact:String,countryCode:String,password: String,securityQuestion1:Question,securityQuestion2:Question,completionBlock:@escaping (_ success:Bool,_ user: User?,_ message: String) -> Void){
        var params = Dictionary<String,Any>()
        params.updateValue(name, forKey: "full_name")
        params.updateValue(password, forKey: "password")
        params.updateValue(contact, forKey: "contact")
        let cc = countryCode.replacingOccurrences(of: "+", with: "")
        params.updateValue(cc, forKey: "country_code")

        var securityQuestionParam1 = Dictionary<String,String>()
        securityQuestionParam1.updateValue(securityQuestion1.ID, forKey: "question_id")
        securityQuestionParam1.updateValue(securityQuestion1.answer, forKey: "answer")
        params.updateValue(securityQuestionParam1, forKey: "security_question1")

        var securityQuestionParam2 = Dictionary<String,String>()
        securityQuestionParam2.updateValue(securityQuestion2.ID, forKey: "question_id")
        securityQuestionParam2.updateValue(securityQuestion2.answer, forKey: "answer")
        params.updateValue(securityQuestionParam2, forKey: "security_question2")

//        params.updateValue("ios", forKey: "device_type")
//        if let deviceID = kUserDefaults.value(forKey: kDeviceToken) as? String {
//            params.updateValue(deviceID, forKey: "device_id")
//        }else{
//            params.updateValue("--", forKey: "device_id")
//        }

        let head =  AppSettings.shared.prepareHeader(withAuth: false)
        print_debug("hitting \(api.signup.url()) with param \(params) and headers :\(head)")

        Alamofire.request(api.signup.url(), method: .post, parameters:["registration":params], headers: head).responseJSON {[password, contact,countryCode] response in
            AppSettings.shared.hideLoader()
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print_debug("sign up json is:\n\(json)")
                    let userParser = UserParser(json: json)
                    if userParser.errorCode == .success{
                        AppSettings.shared.isRegistered = true
                        AppSettings.shared.passwordEncrypted = password
                        AppSettings.shared.phoneNumber = contact
                        AppSettings.shared.countryCode = countryCode
                        userParser.user.saveUserJSON(json)
                        AppSettings.shared.isRegistered = true
                    }
                    completionBlock((userParser.errorCode == .success),userParser.user,userParser.message)
                }else{
                    completionBlock(false,nil,response.result.error?.localizedDescription ?? "Some thing went wrong")
                }
            case .failure(let error):
                completionBlock(false,nil,error.localizedDescription)
            }
        }
    }


    func updateUserWith(_ userID:String,name:String,email:String,mobile:String,profileImage: UIImage?,completionBlock:@escaping (_ success:Bool,_ user: User?,_ message: String) -> Void){
        var params = Dictionary<String,String >()
        params.updateValue(userID, forKey: "user_id")
        let fullName = name.components(separatedBy: " ")
        if let fname = fullName.first{
            params.updateValue(fname, forKey: "fname")
        }

        if fullName.count > 1{
            var lName = ""
            for i in 1..<fullName.count{
                if lName == ""{
                    lName = fullName[i]
                }else{
                    lName = lName + " " + fullName[i]
                }
            }
            params.updateValue(lName, forKey: "lname")
        }
        params.updateValue(email, forKey: "email")
        params.updateValue("customer", forKey: "role")
        params.updateValue(mobile, forKey: "contact")

        let head =  AppSettings.shared.prepareHeader(withAuth: true)
        print_debug("hitting \(api.editProfile.url()) with param \(params) and headers :\(head)")
        Alamofire.upload(
            multipartFormData: { multipartFormData in
                if let pimage = profileImage{
                    if let data = UIImageJPEGRepresentation(pimage, 0.9) as Data?{
                        multipartFormData.append(data, withName: "image_data", fileName: "image_data", mimeType: "image/jpg")
                    }
                }

                for (key, value) in params {
                    multipartFormData.append((value).data(using: .utf8)!, withName: key)
                }
        },
            to: api.editProfile.url(),method:.post,
            headers: head,
            encodingCompletion: { encodingResult in
                switch encodingResult {
                case .success(let upload, _, _):
                    upload.responseJSON { response in
                        AppSettings.shared.hideLoader()
                        switch response.result {
                        case .success:
                            if let value = response.result.value {
                                let json = JSON(value)
                                print_debug("update profile json is:\n\(json)")
                                let userParser = UserParser(json: json)
                                if userParser.errorCode == .success{
                                    userParser.user.saveUserJSON(json)
                                    AppSettings.shared.isLoggedIn = true
                                    completionBlock((userParser.errorCode == .success), userParser.user, userParser.message)

                                }else if userParser.errorCode == .sessionExpire{
                                    AppSettings.shared.showSessionExpireAndProceedToLandingPage()
                                }else if userParser.errorCode == .forceUpdate{
                                    AppSettings.shared.showForceUpdateAlert()
                                }else{
                                    completionBlock((userParser.errorCode == .success), userParser.user, userParser.message)

                                }

                            }else{
                                completionBlock(false,nil,response.result.error?.localizedDescription ?? "Some thing went wrong")
                            }
                        case .failure(let error):
                            completionBlock(false,nil,error.localizedDescription)
                        }
                    }
                case .failure(let encodingError):
                    completionBlock(false,nil,encodingError.localizedDescription)
                }
        }
        )

    }


    func loginWithSocialAuth(_ email:String,socialID:String,name:String,providerType:SocialLoginType,imageUrl:String,completionBlock:@escaping (_ success:Bool,_ user: User?,_ message: String) -> Void){
        var params = Dictionary<String,String >()
        params.updateValue(email, forKey: "email")
        params.updateValue(providerType.rawValue, forKey: "social_media")
        params.updateValue(socialID, forKey: "social_id")
        params.updateValue(imageUrl, forKey: "image_url")
        let fullName = name.components(separatedBy: " ")
        if let fname = fullName.first{
            params.updateValue(fname, forKey: "fname")
        }

        if fullName.count > 1{
            var lName = ""
            for i in 1..<fullName.count{
                if lName == ""{
                    lName = fullName[i]
                }else{
                    lName = lName + " " + fullName[i]
                }
            }
            params.updateValue(lName, forKey: "lname")

        }


        params.updateValue("customer", forKey: "role")
        if let deviceID = kUserDefaults.value(forKey: kDeviceToken) as? String {
            params.updateValue(deviceID, forKey: "device_id")
        }else{
            params.updateValue("--", forKey: "device_id")
        }
        params.updateValue("ios", forKey: "device_type")

        let head =  AppSettings.shared.prepareHeader(withAuth: false)

        print_debug("hitting \(api.socialAuth.url()) with param \(params) and headers :\(head)")
        Alamofire.request(api.socialAuth.url(),method: .post ,parameters: params,headers:head).responseJSON { response in
            AppSettings.shared.hideLoader()
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print_debug("sign in json is:\n\(json)")
                    let userParser = UserParser(json: json)
                    if userParser.errorCode == .success{
                        userParser.user.saveUserJSON(json)
                        AppSettings.shared.isLoggedIn = true
                        completionBlock(true,userParser.user,userParser.message)
                    }else if userParser.errorCode == .sessionExpire{
                        AppSettings.shared.showSessionExpireAndProceedToLandingPage()
                    }else if userParser.errorCode == .forceUpdate{
                        AppSettings.shared.showForceUpdateAlert()
                    }else{
                        completionBlock((userParser.errorCode == .success),nil,userParser.message)
                    }
                }else{
                    completionBlock(false,nil,response.result.error?.localizedDescription ?? "Some thing went wrong")
                }
            case .failure(let error):
                completionBlock(false,nil,error.localizedDescription)
            }
        }
    }





    func getNotificationSetting(handler:@escaping (_ success:Bool,_ userSetting :User?, _ message:String)->Void){
        let head =  AppSettings.shared.prepareHeader(withAuth: true)
        print_debug("hitting \(api.userSettings.url()) and headers :\(head)")
        Alamofire.request(api.userSettings.url(),method: .get, headers:head).responseJSON { response in
            AppSettings.shared.hideLoader()
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print_debug("verify json is:\n\(json)")
                    let userPaser = UserParser(json: json)
                    if userPaser.errorCode == .sessionExpire{
                        AppSettings.shared.showSessionExpireAndProceedToLandingPage()
                    }else if userPaser.errorCode == .forceUpdate{
                        AppSettings.shared.showForceUpdateAlert()
                    }else if userPaser.errorCode == .success{
                        handler(userPaser.errorCode == .success,userPaser.user,userPaser.message)
                    }else{
                        handler(false,nil,response.result.error?.localizedDescription ?? "Some thing went wrong")
                    }
                }
            case .failure(let error):
                handler(false,nil,error.localizedDescription)
            }
        }
    }


    func setProfileOnServer(newName:String?,image: UIImage?,notification:Bool?, handler: @escaping (_ success:Bool,_ userSetting :User?, _ message:String)->Void){
        let head =  AppSettings.shared.prepareHeader(withAuth: true)
        var param = Dictionary<String,String>()
        if let name = newName{
            if name.trimmingCharacters(in: .whitespaces).count != 0{
                param.updateValue(name, forKey: "full_name")
            }
        }
        if let shouldNotify = notification{param.updateValue(shouldNotify ? "true" : "false", forKey: "notify")}
        if let uimage = image{
            if let imageString = uimage.base64(format: .jpeg(0.5)){
                param.updateValue(imageString, forKey: "image")
            }
        }

        print_debug("hitting \(api.editProfile.url()) and headers :\(head)")
        Alamofire.request(api.editProfile.url(),method: .post, parameters:param, headers:head).responseJSON { response in
            AppSettings.shared.hideLoader()
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print_debug("update profile json is:\n\(json)")
                    let userParser = UserParser(json: json)
                    if userParser.errorCode == .sessionExpire{
                        AppSettings.shared.showSessionExpireAndProceedToLandingPage()
                    }else if userParser.errorCode == .forceUpdate{
                        AppSettings.shared.showForceUpdateAlert()
                    }else if userParser.errorCode == .success{
                        userParser.user.saveUserJSON(json)
                        handler(userParser.errorCode == .success,userParser.user,userParser.message)
                    }else{
                        handler(false,nil,response.result.error?.localizedDescription ?? "Some thing went wrong")
                    }
                }
            case .failure(let error):
                handler(false,nil,error.localizedDescription)
            }
        }
    }

    func updateNotificationSettings(_ completionBlock:@escaping (_ success:Bool,_ status:Bool?,_ message: String) -> Void){
        let head =  AppSettings.shared.prepareHeader(withAuth: true)
        print_debug("hitting \(api.notificationToggle.url()) with and headers :\(head)")
        AppSettings.shared.showLoader(withStatus: "Please wait..")
        Alamofire.request(api.notificationToggle.url(),method: .get, headers:head).responseJSON { response in
            AppSettings.shared.hideLoader()
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print_debug("notification settings json is: \n\(json)")
                    let userParser = UserParser(json: json)
                    completionBlock((userParser.errorCode == .success), userParser.notificationEnabled, userParser.message)
                }else{
                    completionBlock(false,nil,response.result.error?.localizedDescription ?? "Some thing went wrong")
                }
            case .failure(let error):
                completionBlock(false,nil,error.localizedDescription)
            }
        }
    }

}





