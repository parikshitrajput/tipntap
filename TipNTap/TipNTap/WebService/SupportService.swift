//
//  SupportService.swift
//  GPDock
//
//  Created by TecOrb on 07/05/18.
//  Copyright © 2018 Nakul Sharma. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class SupportService {
    static let sharedInstance = SupportService()
    fileprivate init() {}


    func getAllQueriesForUser(_ userID:String,completionBlock:@escaping (_ success:Bool,_ supportTickets:Array<SupportQuery>?, _ message:String) -> Void){
        //var params = Dictionary<String,String>()
       // params.updateValue(userID, forKey: "user_id")
        let head = AppSettings.shared.prepareHeader(withAuth: true)
        print_debug("hitting \(api.allSupportsTickets.url()) and headers :\(head)")
        Alamofire.request(api.allSupportsTickets.url(),method: .get, headers:head).responseJSON { response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print_debug("tickets json is:\n\(json)")
                    let supportParser = SupportTicketParser(json: json)
                    if supportParser.code == .sessionExpire{
                        AppSettings.shared.hideLoader()
                        AppSettings.shared.showSessionExpireAndProceedToLandingPage()
                    }else if supportParser.code == .forceUpdate{
                        AppSettings.shared.hideLoader()
                        AppSettings.shared.showForceUpdateAlert()
                    }else{
                        completionBlock((supportParser.code == .success),supportParser.supports,supportParser.message)
                    }
                }else{
                    completionBlock(false, nil,"Oops! Something wrong went")
                }
            case .failure(let error):
                completionBlock(false, nil, error.localizedDescription)
            }
        }
    }


    func getDetailsForSupportWith(_ id:String,completionBlock:@escaping (_ success:Bool,_ supportTicket: SupportQuery?, _ message:String) -> Void){
        let url = api.allSupportsTickets.url()+"/\(id)"
        let head = AppSettings.shared.prepareHeader(withAuth: true)
        print_debug("hitting \(url) and headers :\(head)")
        Alamofire.request(url,method: .get, headers:head).responseJSON { response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print_debug("ticket details json is:\n\(json)")
                    let supportParser = SupportTicketParser(json: json)
                    if supportParser.code == .sessionExpire{
                        AppSettings.shared.hideLoader()
                        AppSettings.shared.showSessionExpireAndProceedToLandingPage()
                    }else if supportParser.code == .forceUpdate{
                        AppSettings.shared.hideLoader()
                        AppSettings.shared.showForceUpdateAlert()
                    }else{
                        completionBlock((supportParser.code == .success),supportParser.support,supportParser.message)
                    }
                }else{
                    completionBlock(false, nil,"Oops! Something wrong went")
                }
            case .failure(let error):
                completionBlock(false, nil, error.localizedDescription)
            }
        }
    }


    func addCommentForSupportQuery(_ userID:String, supportID:String, message: String, completionBlock:@escaping (_ success:Bool,_ comment:SupportMessage?, _ message:String) -> Void){
        var params = Dictionary<String,String>()
        params.updateValue(userID, forKey: "user_id")
        params.updateValue(supportID, forKey: "support_id")
        params.updateValue(message, forKey: "message")

        let head = AppSettings.shared.prepareHeader(withAuth: true)
        print_debug("hitting \(api.addCommentToSupport.url()) with param \(params) and headers :\(head)")
        Alamofire.request(api.addCommentToSupport.url(),method: .post ,parameters: params, headers:head).responseJSON { response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print_debug("comment json is:\n\(json)")
                    let supportParser = SupportTicketParser(json: json)
                    if supportParser.code == .sessionExpire{
                        AppSettings.shared.hideLoader()
                        AppSettings.shared.showSessionExpireAndProceedToLandingPage()
                    }else if supportParser.code == .forceUpdate{
                        AppSettings.shared.hideLoader()
                        AppSettings.shared.showForceUpdateAlert()
                    }else{
                        completionBlock((supportParser.code == .success),supportParser.comment,supportParser.message)
                    }
                }else{
                    completionBlock(false, nil,response.result.error?.localizedDescription ?? "Oops! Something wrong went")
                }
            case .failure(let error):
                completionBlock(false, nil, error.localizedDescription)
            }
        }
    }
    
    
    func getFAQsFromServer(completionBlock:@escaping (_ success:Bool, _ faqModels:Array<FAQModel>?, _ message:String) -> Void) {
        let head = AppSettings.shared.prepareHeader(withAuth: true)
        print_debug("hitting and headers :\(head)")
        Alamofire.request(api.allFaqs.url(),method: .post , headers:head).responseJSON { response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print_debug("faqs json: \(json)")
                    let faqParser = FAQParser(json: json)
                    if faqParser.code == .sessionExpire{
                        AppSettings.shared.hideLoader()
                        AppSettings.shared.showSessionExpireAndProceedToLandingPage()
                    }else if faqParser.code == .forceUpdate{
                        AppSettings.shared.hideLoader()
                        AppSettings.shared.showForceUpdateAlert()
                    }else{
                        completionBlock((faqParser.code == .success),faqParser.faqs,faqParser.message)
                    }
                }else{
                    completionBlock(false,nil,response.error?.localizedDescription ?? "Something went wrong")
                }
            case .failure(let error):
                completionBlock(false,nil,error.localizedDescription)
            }
        }
        
    }
    
    
    
    
//    func sendEmailToSupport(message:String ,completionBlock:@escaping (_ success:Bool,_ support:Support? ,_ message:String)-> Void){
//        let head = AppSettings.shared.prepareHeader(withAuth: true)
//        let params = ["message":message]
//
//        print_debug("hitting \(api.writeToUS.url()) with param \(params) and headers :\(head)")
//        Alamofire.request(api.writeToUS.url(),method: .post ,parameters: params, headers:head).responseJSON { response in
//            switch response.result {
//            case .success:
//                if let value = response.result.value {
//                    let json = JSON(value)
//                    print_debug("faqs json: \(json)")
//                    let supportParser = SupportParser(json: json)
//                    if supportParser.code == .sessionExpire{
//                        AppSettings.shared.hideLoader()
//                        AppSettings.shared.showSessionExpireAndProceedToLandingPage()
//                    }else if supportParser.code == .forceUpdate{
//                        AppSettings.shared.hideLoader()
//                        AppSettings.shared.showForceUpdateAlert()
//                    }else{
//                        completionBlock((supportParser.code == .success),supportParser.support,supportParser.message)
//                    }
//                }else{
//                    completionBlock(false,nil,response.result.error?.localizedDescription ?? "Some thing went wrong")
//                }
//            case .failure(let error):
//                completionBlock(false,nil,error.localizedDescription)
//            }
//        }
//
//    }

    
    
//
    func sendEmailToSupport(message:String,image: UIImage?,completionBlock:@escaping (_ success:Bool,_ support:Support? ,_ message:String)-> Void){
        let head = AppSettings.shared.prepareHeader(withAuth: true)
        let params = ["message":message]

        Alamofire.upload(
            multipartFormData: { multipartFormData in
                if let pimage = image{
                    if let data = UIImageJPEGRepresentation(pimage, 0.7) as Data?{
                        multipartFormData.append(data, withName: "image", fileName: "image", mimeType: "image/jpg")
                    }
                }

                for (key, value) in params {
                    multipartFormData.append((value).data(using: .utf8)!, withName: key)
                }
        },
            to: api.writeToUS.url(),method:.post,
            headers: head,
            encodingCompletion: { encodingResult in
                switch encodingResult {
                case .success(let upload, _, _):
                    upload.responseJSON { response in
                        switch response.result {
                        case .success:
                            if let value = response.result.value {
                                let json = JSON(value)
                                print_debug("faqs json: \(json)")
                                let supportParser = SupportParser(json: json)
                                if supportParser.code == .sessionExpire{
                                    AppSettings.shared.hideLoader()
                                    AppSettings.shared.showSessionExpireAndProceedToLandingPage()
                                }else if supportParser.code == .forceUpdate{
                                    AppSettings.shared.hideLoader()
                                    AppSettings.shared.showForceUpdateAlert()
                                }else{
                                    completionBlock((supportParser.code == .success),supportParser.support,supportParser.message)
                                }
                            }else{
                                completionBlock(false,nil,response.result.error?.localizedDescription ?? "Some thing went wrong")
                            }
                        case .failure(let error):
                            completionBlock(false,nil,error.localizedDescription)
                        }
                    }
                case .failure(let encodingError):
                    completionBlock(false,nil,encodingError.localizedDescription)
                }
        }
        )

    }

}
