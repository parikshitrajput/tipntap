//
//  TransactionService.swift
//  GPDock
//
//  Created by TecOrb on 08/02/18.
//  Copyright © 2018 Nakul Sharma. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire

enum TransactionFilter:String {
    case all = "all"
    case paid = "paid"
    case received = "received"
}


class TransactionService {
    static let sharedInstance = TransactionService()
    fileprivate init() {}

    func getRecentScans(_ completionBlock:@escaping (_ success:Bool, _ recentScans: Array<RecentScan>?,_ message:String) -> Void){
        let params = ["page":"1","per_page":"5"]
        let head =  AppSettings.shared.prepareHeader(withAuth: true)
        print_debug("hitting \(api.recentScans.url()) with headers :\(head)")
        Alamofire.request(api.recentScans.url(),method: .post, parameters:params, headers:head).responseJSON { response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print_debug("recent scans json is:\n\(json)")
                    let recentScanParser = RecentScanParser(json: json)
                    if recentScanParser.errorCode == .sessionExpire{
                        AppSettings.shared.hideLoader()
                        AppSettings.shared.showSessionExpireAndProceedToLandingPage()
                    }else if recentScanParser.errorCode == .forceUpdate{
                        AppSettings.shared.hideLoader()
                        AppSettings.shared.showForceUpdateAlert()
                    }else{
                        completionBlock(recentScanParser.errorCode == .success, recentScanParser.recentScans, recentScanParser.message)
                    }
                }else{
                    completionBlock(false,nil,response.error?.localizedDescription ?? "Some thing went wrong")
                }
            case .failure(let error):
                completionBlock(false,nil,error.localizedDescription)
            }
        }
    }

    func getTransactions(filter:TransactionFilter,page:Int,perPage:Int,completionBlock:@escaping (_ success:Bool,Array<UserTransaction>?,_ totalPage : Int, _ message:String) -> Void){
        let head = AppSettings.shared.prepareHeader(withAuth: true)
        var params = Dictionary<String,String>()
        params.updateValue(filter.rawValue, forKey: "keyword")
        params.updateValue("\(page)", forKey: "page")
        params.updateValue("\(perPage)", forKey: "per_page")

        print_debug("hitting \(api.paymentSummary.url()) with param \(params) and headers :\(head)")
        Alamofire.request(api.paymentSummary.url(),method: .post ,parameters: params, headers:head).responseJSON { response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print_debug("transactions json is:\n\(json)")
                    let txnParser = UserTransactionParser(json: json)
                    if txnParser.errorCode == .sessionExpire{
                        AppSettings.shared.hideLoader()
                        AppSettings.shared.showSessionExpireAndProceedToLandingPage()
                    }else if txnParser.errorCode == .forceUpdate{
                        AppSettings.shared.hideLoader()
                        AppSettings.shared.showForceUpdateAlert()
                    }else{
                        completionBlock(txnParser.errorCode == .success, txnParser.transactions, txnParser.totalPage, txnParser.message)
                    }
                }else{
                    completionBlock(false,nil,0,response.error?.localizedDescription ?? "Some thing went wrong")
                }
            case .failure(let error):
                completionBlock(false,nil,0,error.localizedDescription)
            }
        }

    }

    func getUserWallet(completionBlock:@escaping (_ success:Bool,_ userWallet: Wallet?, _ message:String)->Void){
        let head = AppSettings.shared.prepareHeader(withAuth: true)
        print_debug("hitting \(api.userWallet.url()) and headers :\(head)")
        Alamofire.request(api.userWallet.url(),method: .get, headers:head).responseJSON { response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print_debug("wallet scans json is:\n\(json)")
                    let walletParser = WalletParser(json: json)
                    if walletParser.errorCode == .sessionExpire{
                        AppSettings.shared.hideLoader()
                        AppSettings.shared.showSessionExpireAndProceedToLandingPage()
                    }else if walletParser.errorCode == .forceUpdate{
                        AppSettings.shared.hideLoader()
                        AppSettings.shared.showForceUpdateAlert()
                    } else{
                        completionBlock(walletParser.errorCode == .success, walletParser.wallet, walletParser.message)
                    }
                }else{
                    completionBlock(false,nil,response.error?.localizedDescription ?? "Some thing went wrong")
                }
            case .failure(let error):
                completionBlock(false,nil,error.localizedDescription)
            }
        }
    }

    func getOffersList(_ page:Int, perPage:Int,completionBlock:@escaping (_ success:Bool,Array<Offer>?,_ message:String) -> Void){
        let head = AppSettings.shared.prepareHeader(withAuth: true)
        var params = Dictionary<String,String>()
        params.updateValue("\(page)", forKey: "page")
        params.updateValue("\(perPage)", forKey: "per_page")

        print_debug("hitting \(api.offersList.url()) with param \(params) and headers :\(head)")
        Alamofire.request(api.offersList.url(),method: .post ,parameters: params, headers:head).responseJSON { response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print_debug("offers json is:\n\(json)")
                    let offerParser = OfferParser(json: json)
                    if offerParser.errorCode == .sessionExpire{
                        AppSettings.shared.hideLoader()
                        AppSettings.shared.showSessionExpireAndProceedToLandingPage()
                    }else if offerParser.errorCode == .forceUpdate{
                        AppSettings.shared.hideLoader()
                        AppSettings.shared.showForceUpdateAlert()
                    }else{
                        completionBlock(offerParser.errorCode == .success, offerParser.offers, offerParser.message)
                    }
                }else{
                    completionBlock(false,nil,response.error?.localizedDescription ?? "Some thing went wrong")
                }
            case .failure(let error):
                completionBlock(false,nil,error.localizedDescription)
            }
        }

    }


    

}
