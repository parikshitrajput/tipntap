//
//  PaymentService.swift
//  GPDock
//
//  Created by TecOrb on 01/08/17.
//  Copyright © 2017 Nakul Sharma. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import Stripe
/*
{
    "code": 200,
    "message": "success! pay at marina",
    "pay": {
        "id": 65,
        "amount": 5500,
        "transaction_id": "ch_1BJyYSHCw2A9l24gl3e808AQ",
        "created_at": "2017-11-03T06:21:25.000Z",
        "pay_for_grocery": 20,
        "pay_for_pumpout": 10,
        "pay_for_gasoline": 0,
        "pay_for_others": 25,
        "pay_note": "ddd",
        "paid_amount_in_doller": 55
    }
}
*/
class PaymentParser: NSObject {

    var errorCode = ErrorCode.failure
    var responseMessage = ""
    var payment = Payment()

    override init() {
        super.init()
    }

    init(json: JSON) {
        if let _code = json["code"].int as Int?{
            self.errorCode = ErrorCode(rawValue: _code)
        }

        if let _msg = json["response_message"].string as String?{
            self.responseMessage = _msg
        }else if let _msg = json["errors"].string{
            self.responseMessage = _msg
        }

        if let _payment = json["pay"].dictionaryObject as Dictionary<String,AnyObject>?{
            self.payment = Payment(params: _payment)
        }

    }
}


class Payment:NSObject{
    /*
    "id": 65,
    "amount": 5500,
    "transaction_id": "ch_1BJyYSHCw2A9l24gl3e808AQ",
    "created_at": "2017-11-03T06:21:25.000Z",
    "pay_for_grocery": 20,
    "pay_for_pumpout": 10,
    "pay_for_gasoline": 0,
    "pay_for_others": 25,
    "pay_note": "ddd",
    "paid_amount_in_doller": 55
 */
    let kID = "id"
    let kAmount = "service_amount"
    let kTransactionID = "transaction_id"
    let kCustomerTransactionID = "customer_transaction_id"
    let kCreatedAt = "created_at"
    let kPayForGrocery = "pay_for_grocery"
    let kPayForPumpout = "pay_for_pumpout"
    let kPayForGasoline = "pay_for_gasoline"
    let kPayForOthers = "pay_for_others"
    let kPayNote = "pay_note"
    let kAccountNumber = "account_number"


    var ID = ""
    var transactionID = ""
    var CustomerTransactionID = ""
    var createdAt = ""
    var payNote = ""
    var accountNumber = ""
    var amount : Double = 0.0
    var payForGrocery : Double = 0.0
    var payForPumpout : Double = 0.0
    var payForGasoline : Double = 0.0
    var payForOthers : Double = 0.0

    override init() {
        super.init()
    }

    init(params:Dictionary<String,AnyObject>) {

        if let _ID = params[kID] as? Int{
            self.ID = "\(_ID)"
        }else if let _ID = params[kID] as? String{
            self.ID = _ID
        }

        if let _trID = params[kTransactionID] as? String{
            self.transactionID = _trID
        }
        if let _ctrID = params[kCustomerTransactionID] as? String{
            self.CustomerTransactionID = _ctrID
        }

        if let _createdAt = params[kCreatedAt] as? String{
            self.createdAt = _createdAt
        }

        if let _payNote = params[kPayNote] as? String{
            self.payNote = _payNote
        }

        if let _amount = params[kAmount] as? Double{
            self.amount = _amount
        }else if let _amount = params[kAmount] as? String{
            self.amount = Double(_amount) ?? 0.0
        }
        if let _accountNumber = params[kAccountNumber] as? Double{
            self.accountNumber = "\(_accountNumber)"
        }else if let _accountNumber = params[kAccountNumber] as? String{
            self.accountNumber = _accountNumber
        }

        if let _amountPaid = params[kAmount] as? Double{
            self.amount = _amountPaid
        }else if let _amountPaid = params[kAmount] as? String{
            self.amount = Double(_amountPaid) ?? 0.0
        }

        if let _payForGrocery = params[kPayForGrocery] as? Double{
            self.payForGrocery = _payForGrocery
        }else if let _payForGrocery = params[kPayForGrocery] as? String{
            self.amount = Double(_payForGrocery) ?? 0.0
        }

        if let _payForPumpout = params[kPayForPumpout] as? Double{
            self.payForPumpout = _payForPumpout
        }else if let _payForPumpout = params[kPayForPumpout] as? String{
            self.payForPumpout = Double(_payForPumpout) ?? 0.0
        }

        if let _payForGasoline = params[kPayForGasoline] as? Double{
            self.payForGasoline = _payForGasoline
        }else if let _payForGasoline = params[kPayForGasoline] as? String{
            self.payForGasoline = Double(_payForGasoline) ?? 0.0
        }

        if let _payForOthers = params[kPayForOthers] as? Double{
            self.payForOthers = _payForOthers
        }else if let _payForOthers = params[kPayForOthers] as? String{
            self.payForOthers = Double(_payForOthers) ?? 0.0
        }
        super.init()
    }
}


class CardParser: NSObject {

    var errorCode = ErrorCode.failure
    var responseMessage = ""
    var cards = Array<Card>()
    var card = Card()
    var users = [User]()
    var user = User()
    let kUsers = "users"
    let kUser = "user"
    
    override init() {
        super.init()
    }

    init(json: JSON) {
        if let _code = json["code"].int{
            self.errorCode = ErrorCode(rawValue: _code)
        }

        if let _msg = json["message"].string as String?{
            self.responseMessage = _msg
        }

        if let _cards = json["cards"].arrayObject as? Array<Dictionary<String,AnyObject>>{
            for _cardDict in _cards{
                let card = Card(params: _cardDict)
                self.cards.append(card)
            }
        }

        if let _card = json["card"].dictionaryObject as Dictionary<String,AnyObject>?{
             self.card = Card(params: _card)
        }
        
        if let userDict = json[kUser].dictionaryObject as [String:AnyObject]?{
            self.user = User(dictionary: userDict)
        }

        if let usrs  = json[kUsers].arrayObject as? [[String: AnyObject]]
        {
            users.removeAll()
            for u in usrs{
                let usr = User(dictionary: u)
                users.append(usr)
            }
        }

    }
}

class Card:NSObject{
/*
    "id": 3,
    "card_token": "card_1AlxIOHCw2A9l24g1yf1va48",
    "exp_month": "1",
    "exp_year": "2020",
    "last4": "4242",
    "brand": "Visa",
    "funding_type": "credit",
    "is_default": true
    */
    let kID = "id"
    let kCardToken = "card_token"
    let kExpMonth = "exp_month"
    let kExpYear = "exp_year"
    let kLast4 = "last4"
    let kBrand = "brand"
    let kFundingType = "funding_type"
    let kIsDefault = "is_default"


    var ID = ""
    var cardToken : String = ""
    var expMonth: UInt = 0
    var expYear : UInt = 0
    var last4 : String = ""
    var brand : String = ""
    var fundingType :String = ""
    var isDefault : Bool = false

    override init() {
        super.init()
    }

    init(params:Dictionary<String,AnyObject>) {

        if let _ID = params[kID] as? Int{
            self.ID = "\(_ID)"
        }else if let _ID = params[kID] as? String{
            self.ID = _ID
        }

        if let _token = params[kCardToken] as? Int{
            self.cardToken = "\(_token)"
        }else if let _token = params[kCardToken] as? String{
            self.cardToken = _token
        }

        if let _expMonth = params[kExpMonth] as? UInt{
            self.expMonth = _expMonth
        }else if let _expMonth = params[kExpMonth] as? String{
            self.expMonth = UInt(_expMonth) ?? 0
        }

        if let _expYear = params[kExpYear] as? UInt{
            self.expYear = _expYear
        }else if let _expYear = params[kExpYear] as? String{
            self.expYear = UInt(_expYear) ?? 0
        }

        if let _last4 = params[kLast4] as? String{
            self.last4 = _last4
        }
        if let _brand = params[kBrand] as? String{
            self.brand = _brand
        }
        if let _fundingType = params[kFundingType] as? String{
            self.fundingType = _fundingType
        }
        if let _isDefault = params[kIsDefault] as? Bool{
            self.isDefault = _isDefault
        }
        super.init()
    }
}

class PaymentService {
    static let sharedInstance = PaymentService()
    fileprivate init() {}
    private func makeFullMobileNumber(cc:String,mobileNumber:String) -> String{
        var fullMobile = ""
        if mobileNumber.contains("+"){
            fullMobile = mobileNumber
        }else{
            fullMobile = cc+mobileNumber
        }
        return fullMobile
    }

    func findPayeeFor(countryCode:String,contactNumber:String,accountReferenceNumber:String,completionBlock: @escaping (_ success:Bool, _ user: User?, _ message: String) -> Void){
        var params = Dictionary<String,String>()
        let fullMobile = self.makeFullMobileNumber(cc: countryCode, mobileNumber: contactNumber).replacingOccurrences(of: "+", with: "")
        params.updateValue(fullMobile, forKey: "full_contact")
        params.updateValue(accountReferenceNumber, forKey: "account_reference")
        let header = AppSettings.shared.prepareHeader(withAuth: true)
        print_debug("Hitting \(api.findPayee.url()) with params: \(params) and header :\(header)")
        Alamofire.request(api.findPayee.url(),method: .post ,parameters: params, headers:header).responseJSON {response in
            AppSettings.shared.hideLoader()
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print_debug("find payee json is:\n\(json)")
                    let userParser = UserParser(json: json)
                    if userParser.errorCode == .sessionExpire{
                        AppSettings.shared.hideLoader()
                        AppSettings.shared.showSessionExpireAndProceedToLandingPage()
                    }else{
                        completionBlock((userParser.errorCode == .success),userParser.user,userParser.message)
                    }
                }else{
                    completionBlock(false,nil,response.result.error?.localizedDescription ?? "Some thing went wrong")
                }
            case .failure(let error):
                completionBlock(false,nil,error.localizedDescription)
            }
        }
    }



    func getCardsForUser(completionBlock:@escaping (_ success:Bool,_ cards: Array<Card>?,_ message:String,_ user: User?) -> Void){
        let head =  AppSettings.shared.prepareHeader(withAuth: true)

        print_debug("hitting \(api.listOfCard.url()) with and headers :\(head)")
        Alamofire.request(api.listOfCard.url(),method: .get, headers:head).responseJSON { response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print_debug("cards json is:\n\(json)")
                    let cardParser = CardParser(json: json)
                    if cardParser.errorCode == .sessionExpire{
                        AppSettings.shared.hideLoader()
                        AppSettings.shared.showSessionExpireAndProceedToLandingPage()
                    }else if cardParser.errorCode == .forceUpdate{
                        AppSettings.shared.hideLoader()
                        AppSettings.shared.showForceUpdateAlert()
                    } else{
                        if cardParser.errorCode == .success{
                            completionBlock(true,cardParser.cards,cardParser.responseMessage, cardParser.user)
                        }else{
                            completionBlock(false,nil,cardParser.responseMessage, nil)
                        }
                    }
                }else{
                    completionBlock(false,nil,response.result.error?.localizedDescription ?? "Some thing went wrong", nil)
                }
            case .failure(let error):
                completionBlock(false,nil,error.localizedDescription, nil)
            }
        }
    }


    /*
     "source_token":"",
     "payee_id":"1",
     "amount":"10000",
     "card_id":"1",
     "description":""
     */
    func payForPayeeWith(_ payee:User, sourceToken:String,amount:Double, cardID:String, message:String, couponCode:String? = nil, govDoc:UIImage?, signatureImage:UIImage? ,completionBlock: @escaping (_ success:Bool, _ payment:UserTransaction?,_ message:String)->Void){
        var params = Dictionary<String,String>()
        params.updateValue(cardID, forKey: "card_id")
        params.updateValue(sourceToken, forKey: "source_token")
        params.updateValue(message, forKey: "description")
        params.updateValue(payee.ID, forKey: "payee_id")
        let totalAmount = amount + (amount*payee.adminPercentage)
        let amountInCent = totalAmount*100
        let amountInCentStr = String(format:"%0.0f",amountInCent)
        params.updateValue(amountInCentStr, forKey: "amount")
        if let offerCode = couponCode{
            params.updateValue(offerCode, forKey: "coupan_code")
        }
        if let doc = govDoc{
            if let imageString = doc.base64(format: .jpeg(0.5)){
                params.updateValue(imageString, forKey: "gov_doc")
            }
        }
        
        if let sign = signatureImage{
            if let imageString = sign.base64(format: .jpeg(0.5)){
                params.updateValue(imageString, forKey: "signature")
            }
        }

        let head = AppSettings.shared.prepareHeader(withAuth: true)
        Alamofire.request(api.payToUser.url(),method: .post ,parameters: params, headers:head).responseJSON { response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print_debug("payment json is:\n\(json)")

                    let paymentParser = UserTransactionParser(json: json)
                    if paymentParser.errorCode == .success{
                        completionBlock(true,paymentParser.transaction,paymentParser.successMessage)
                    }else if paymentParser.errorCode == .sessionExpire{
                        AppSettings.shared.hideLoader()
                        AppSettings.shared.showSessionExpireAndProceedToLandingPage(paymentParser.message)
                    }else if paymentParser.errorCode == .temporaryBlock {
                        AppSettings.shared.hideLoader()

                        NKToastHelper.sharedInstance.showAlert(nil, title: warningMessage.title, message: paymentParser.message ) {
                            AppSettings.shared.proceedToDashboard()
                        }
                    }else if paymentParser.errorCode == .forceUpdate{
                        AppSettings.shared.hideLoader()
                        AppSettings.shared.showForceUpdateAlert()
                    }else{
                        completionBlock(false,nil,paymentParser.message)
                    }
                }else{
                    completionBlock(false,nil,"Opps!, Some thing went wrong")
                }
            case .failure(let error):
                completionBlock(false,nil,"Oops!\r\n\(error.localizedDescription)")
            }
        }
    }


    func removeCard(_ cardID:String,completionBlock:@escaping (_ success:Bool,_ message: String) -> Void){

        let head =  AppSettings.shared.prepareHeader(withAuth: true)
        let params = ["card_id":cardID]
        print_debug("hitting \(api.removeCard.url()) and headers :\(head)")

        Alamofire.request(api.removeCard.url(),method: .post, parameters: params,headers:head).responseJSON { response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print_debug("remove card json is:\n\(json)")
                    let cardParser = CardParser(json: json)
                    if cardParser.errorCode == .sessionExpire{
                        AppSettings.shared.hideLoader()
                        AppSettings.shared.showSessionExpireAndProceedToLandingPage()
                    }else if cardParser.errorCode == .forceUpdate{
                        AppSettings.shared.hideLoader()
                        AppSettings.shared.showForceUpdateAlert()
                    }else{
                        completionBlock(cardParser.errorCode == .success,cardParser.responseMessage)
                    }
                }else{
                    completionBlock(false,"Oops! Something went wrong")
                }
            case .failure(let error):
                completionBlock(false,"Oops!\r\n\(error.localizedDescription)")
            }
        }
    }



    func getBankAccountForUser(completionBlock:@escaping (_ success:Bool,_ bankAccounts: Array<BankAccount>?,_ message:String) -> Void){
        let head =  AppSettings.shared.prepareHeader(withAuth: true)

        print_debug("hitting \(api.bankAccountList.url()) with and headers :\(head)")
        Alamofire.request(api.bankAccountList.url(),method: .get, headers:head).responseJSON { response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print_debug("back account list json is:\n\(json)")
                    let accountsParser = BankAccountParser(json: json)
                    if accountsParser.errorCode == .sessionExpire{
                        AppSettings.shared.hideLoader()
                        AppSettings.shared.showSessionExpireAndProceedToLandingPage()
                    }else if accountsParser.errorCode == .forceUpdate{
                        AppSettings.shared.hideLoader()
                        AppSettings.shared.showForceUpdateAlert()
                    }else{
                        completionBlock(accountsParser.errorCode == .success, accountsParser.accounts, accountsParser.message)
                    }
                }else{
                    completionBlock(false,nil,response.result.error?.localizedDescription ?? "Some thing went wrong")
                }
            case .failure(let error):
                completionBlock(false,nil,error.localizedDescription)
            }
        }
    }

    func addBankAccountWithoutKYC(_ sourceToken: String, completionBlock:@escaping (_ success:Bool,_ bankAccount: BankAccount?,_ message:String) -> Void){
        let head =  AppSettings.shared.prepareHeader(withAuth: true)
        var params = Dictionary<String,String>()
        params.updateValue(sourceToken, forKey: "source_token")
        Alamofire.upload(
            multipartFormData: { multipartFormData in
                for (key, value) in params {
                    multipartFormData.append((value).data(using: .utf8)!, withName: key)
                }
        },
            to: api.addBankAccount.url(),method:.post,
            headers: head,
            encodingCompletion: { encodingResult in
                switch encodingResult {
                case .success(let upload, _, _):
                    upload.responseJSON { response in
                        switch response.result {
                        case .success:
                            if let value = response.result.value {
                                let json = JSON(value)
                                print_debug("back account json is:\n\(json)")
                                let accountsParser = BankAccountParser(json: json)
                                if accountsParser.errorCode == .sessionExpire{
                                    AppSettings.shared.hideLoader()
                                    AppSettings.shared.showSessionExpireAndProceedToLandingPage()
                                }else if accountsParser.errorCode == .forceUpdate{
                                    AppSettings.shared.hideLoader()
                                    AppSettings.shared.showForceUpdateAlert()
                                }else{
                                    completionBlock(accountsParser.errorCode == .success, accountsParser.account, accountsParser.message)
                                }
                            }else{
                                completionBlock(false,nil,response.result.error?.localizedDescription ?? "Some thing went wrong")
                            }
                        case .failure(let error):
                            completionBlock(false,nil,error.localizedDescription)
                        }
                    }
                case .failure(let encodingError):
                    completionBlock(false,nil,encodingError.localizedDescription)
                }
        }
        )
    }

    func addBankAccountWith(_ sourcetoken : String, verificationParams:Dictionary<String,String>?, document:UIImage?, completionBlock:@escaping (_ success:Bool,_ bankAccount: BankAccount?,_ message:String) -> Void){
        let head =  AppSettings.shared.prepareHeader(withAuth: true)
        var params = Dictionary<String,String>()
        if let kycParams = verificationParams{
            for (key, value) in kycParams{
                params.updateValue(value, forKey: key)
            }
        }
        params.updateValue(sourcetoken, forKey: "source_token")

        Alamofire.upload(
            multipartFormData: { multipartFormData in
                if let pimage = document{
                    if let resizedImage = pimage.resizedTo1MB(){
                        if let data = UIImageJPEGRepresentation(resizedImage, 1.0) as Data?{
                            multipartFormData.append(data, withName: "legal_entity_verification_document", fileName: "legal_entity_verification_document", mimeType: "image/jpg")
                        }
                    }
                }

                for (key, value) in params {
                    multipartFormData.append((value).data(using: .utf8)!, withName: key)
                }
        },
            to: api.addBankAccount.url(),method:.post,
            headers: head,
            encodingCompletion: { encodingResult in
                switch encodingResult {
                case .success(let upload, _, _):
                    upload.uploadProgress(closure: { (progress) in
                        AppSettings.shared.updateLoader(withStatus: "Uploading..")// \(progress.fractionCompleted)")
                    })
                    upload.responseJSON { response in
                        AppSettings.shared.hideLoader()
                        switch response.result {
                        case .success:
                            if let value = response.result.value {
                                let json = JSON(value)
                                print_debug("back account json is:\n\(json)")
                                let accountsParser = BankAccountParser(json: json)
                                if accountsParser.errorCode == .sessionExpire{
                                    AppSettings.shared.hideLoader()
                                    AppSettings.shared.showSessionExpireAndProceedToLandingPage()
                                }else if accountsParser.errorCode == .forceUpdate{
                                    AppSettings.shared.hideLoader()
                                    AppSettings.shared.showForceUpdateAlert()
                                }else{
                                    completionBlock(accountsParser.errorCode == .success, accountsParser.account, accountsParser.message)
                                }
                            }else{
                                completionBlock(false,nil,response.result.error?.localizedDescription ?? "Some thing went wrong")
                            }
                        case .failure(let error):
                            completionBlock(false,nil,error.localizedDescription)
                        }
                    }
                case .failure(let encodingError):
                    completionBlock(false,nil,encodingError.localizedDescription)
                }
        }
        )
    }



    func makeBankAccountDefault(_ accountId : String, completionBlock:@escaping (_ success:Bool,_ bankAccount: BankAccount?,_ message:String) -> Void){
        let head =  AppSettings.shared.prepareHeader(withAuth: true)
        let params = ["account_id": accountId]
        print_debug("hitting \(api.makeBankAccountDefault.url()) with and headers :\(head)")

        Alamofire.request(api.makeBankAccountDefault.url(),method: .post, parameters: params, headers:head).responseJSON { response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print_debug("deafult account json is:\n\(json)")
                    let accountsParser = BankAccountParser(json: json)
                    if accountsParser.errorCode == .sessionExpire{
                        AppSettings.shared.hideLoader()
                        AppSettings.shared.showSessionExpireAndProceedToLandingPage()
                    }else if accountsParser.errorCode == .forceUpdate{
                        AppSettings.shared.hideLoader()
                        AppSettings.shared.showForceUpdateAlert()
                    }else{
                        completionBlock(accountsParser.errorCode == .success, accountsParser.account, accountsParser.message)
                    }
                }else{
                    completionBlock(false,nil,response.result.error?.localizedDescription ?? "Some thing went wrong")
                }
            case .failure(let error):
                completionBlock(false,nil,error.localizedDescription)
            }
        }
    }

    func removeAccount(_ accountId : String, completionBlock:@escaping (_ success:Bool,_ bankAccount: BankAccount?,_ message:String) -> Void){
        let head =  AppSettings.shared.prepareHeader(withAuth: true)
        let params = ["account_id": accountId]
        print_debug("hitting \(api.removeBankAccount.url()) with and headers :\(head)")

        Alamofire.request(api.removeBankAccount.url(),method: .post, parameters: params, headers:head).responseJSON { response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print_debug("deafult account json is:\n\(json)")
                    let accountsParser = BankAccountParser(json: json)
                    if accountsParser.errorCode == .sessionExpire{
                        AppSettings.shared.hideLoader()
                        AppSettings.shared.showSessionExpireAndProceedToLandingPage()
                    }else if accountsParser.errorCode == .forceUpdate{
                        AppSettings.shared.hideLoader()
                        AppSettings.shared.showForceUpdateAlert()
                    }else{
                        completionBlock(accountsParser.errorCode == .success, accountsParser.account, accountsParser.message)
                    }
                }else{
                    completionBlock(false,nil,response.result.error?.localizedDescription ?? "Some thing went wrong")
                }
            case .failure(let error):
                completionBlock(false,nil,error.localizedDescription)
            }
        }
    }


    func addCard(_ sourceToken : String, completionBlock:@escaping (_ success:Bool,_ card: Card?,_ message:String) -> Void){
        let head =  AppSettings.shared.prepareHeader(withAuth: true)
        let params = ["source_token": sourceToken]
        print_debug("hitting \(api.addCard.url()) with and headers :\(head)")

        Alamofire.request(api.addCard.url(),method: .post, parameters: params, headers:head).responseJSON { response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print_debug("deafult account json is:\n\(json)")
                    let cardParser = CardParser(json: json)
                    if cardParser.errorCode == .sessionExpire{
                        AppSettings.shared.hideLoader()
                        AppSettings.shared.showSessionExpireAndProceedToLandingPage()
                    }else if cardParser.errorCode == .forceUpdate{
                        AppSettings.shared.hideLoader()
                        AppSettings.shared.showForceUpdateAlert()
                    }else{
                        completionBlock(cardParser.errorCode == .success, cardParser.card, cardParser.responseMessage)
                    }
                }else{
                    completionBlock(false,nil,response.result.error?.localizedDescription ?? "Some thing went wrong")
                }
            case .failure(let error):
                completionBlock(false,nil,error.localizedDescription)
            }
        }
    }



    //to access payouts of user

    func getPayoutsForUser(page:Int, perPage:Int, completionBlock:@escaping (_ success:Bool,_ payouts: Array<Payout>?, _ totalPages: Int,_ message:String) -> Void){
        var param = Dictionary<String,String>()
        param.updateValue("\(page)", forKey: "page")
        param.updateValue("\(perPage)", forKey: "per_page")

        let head =  AppSettings.shared.prepareHeader(withAuth: true)

        print_debug("hitting \(api.userPayouts.url()) with and headers :\(head)")
        Alamofire.request(api.userPayouts.url(),method: .post,parameters:param, headers:head).responseJSON { response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print_debug("payouts json is:\n\(json)")
                    let payoutParser = PayoutParser(json: json)
                    if payoutParser.errorCode == .sessionExpire{
                        AppSettings.shared.hideLoader()
                        AppSettings.shared.showSessionExpireAndProceedToLandingPage()
                    }else if payoutParser.errorCode == .forceUpdate{
                        AppSettings.shared.hideLoader()
                        AppSettings.shared.showForceUpdateAlert()
                    } else{
                        if payoutParser.errorCode == .success{
                            completionBlock(true,payoutParser.payouts,payoutParser.totalPage,payoutParser.message)
                        }else{
                            completionBlock(false,nil,payoutParser.totalPage,payoutParser.message)
                        }
                    }
                }else{
                    completionBlock(false,nil,0,response.result.error?.localizedDescription ?? "Some thing went wrong")
                }
            case .failure(let error):
                completionBlock(false,nil,0,error.localizedDescription)
            }
        }
    }


}
