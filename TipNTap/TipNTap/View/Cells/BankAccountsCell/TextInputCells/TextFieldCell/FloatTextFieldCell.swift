//
//  FloatTextFieldCell.swift
//  TipNTap
//
//  Created by TecOrb on 19/03/18.
//  Copyright © 2018 Nakul Sharma. All rights reserved.
//

import UIKit

class FloatTextFieldCell: UITableViewCell {
    @IBOutlet weak var floatTextField : FloatLabelTextField!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.floatTextField.titleFont = fonts.Raleway.regular.font(.small)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
