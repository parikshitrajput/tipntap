//
//  FloatingTextViewCell.swift
//  TipNTap
//
//  Created by TecOrb on 19/03/18.
//  Copyright © 2018 Nakul Sharma. All rights reserved.
//

import UIKit

class FloatingTextViewCell: UITableViewCell {
    @IBOutlet weak var floatTextView : FloatLabelTextView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.floatTextView.titleFont = fonts.Raleway.regular.font(.small)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
