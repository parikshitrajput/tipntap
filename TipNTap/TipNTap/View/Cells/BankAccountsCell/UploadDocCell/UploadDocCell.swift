//
//  UploadDocCell.swift
//  TipNTap
//
//  Created by TecOrb on 22/03/18.
//  Copyright © 2018 Nakul Sharma. All rights reserved.
//

import UIKit

class UploadDocCell: UITableViewCell {
    @IBOutlet weak var documentImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
