//
//  BankAccountCell.swift
//  TipNTap
//
//  Created by TecOrb on 16/03/18.
//  Copyright © 2018 Nakul Sharma. All rights reserved.
//

import UIKit

class BankAccountCell: UITableViewCell {
    @IBOutlet weak var accountNumberLabel : UILabel!
    @IBOutlet weak var branchNameLabel : UILabel!
    @IBOutlet weak var deleteButton : UIButton!
    @IBOutlet weak var defaultButton : UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
