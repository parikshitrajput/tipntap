//
//  PayoutCell.swift
//  TipNTap
//
//  Created by TecOrb on 30/05/18.
//  Copyright © 2018 Nakul Sharma. All rights reserved.
//

import UIKit

class PayoutCell: UITableViewCell {
    @IBOutlet weak var containner : UIView!
    @IBOutlet weak var amountContainner : UIView!
    @IBOutlet weak var dateTypeLabel : UILabel!
    @IBOutlet weak var dateLabel : UILabel!
    @IBOutlet weak var amountLabel : UILabel!
    @IBOutlet weak var statusLabel : NKCustomLabel!
    @IBOutlet weak var bankNameLabel : UILabel!
    @IBOutlet weak var accountNumberLabel : UILabel!
    @IBOutlet weak var descriptionLabel : UILabel!


    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func layoutSubviews() {
        CommonClass.makeViewCircularWithCornerRadius(self.containner, borderColor: UIColor.clear, borderWidth: 0, cornerRadius: 4)
        CommonClass.makeViewCircularWithCornerRadius(self.statusLabel, borderColor: UIColor.clear, borderWidth: 0, cornerRadius: 2)

    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
