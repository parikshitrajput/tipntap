//
//  CommonOfferCell.swift
//  TipNTap
//
//  Created by TecOrb on 26/03/18.
//  Copyright © 2018 Nakul Sharma. All rights reserved.
//

import UIKit

class CommonOfferCell: UICollectionViewCell {
    @IBOutlet weak var backView : UIView!
    @IBOutlet weak var innerView : UIView!

    @IBOutlet weak var sprinkalImageView : UIImageView!
    @IBOutlet weak var phoneImageView : UIImageView!
    

    override var bounds: CGRect{
        didSet{
            self.contentView.frame = bounds
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func layoutSubviews() {
        CommonClass.makeViewCircular(self.backView, borderColor: .clear, borderWidth: 0)
        CommonClass.makeViewCircular(self.innerView, borderColor: .clear, borderWidth: 0)

    }
}
