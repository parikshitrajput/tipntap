//
//  NoOfferCell.swift
//  TipNTap
//
//  Created by TecOrb on 26/03/18.
//  Copyright © 2018 Nakul Sharma. All rights reserved.
//

import UIKit

class NoOfferCell: UICollectionViewCell {
    @IBOutlet weak var messageLabel : UILabel!
    @IBOutlet weak var errorIcon : UIImageView!

    override var bounds: CGRect{
        didSet{
            self.contentView.frame = bounds
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
