//
//  ApplyingOfferCell.swift
//  TipNTap
//
//  Created by TecOrb on 27/03/18.
//  Copyright © 2018 Nakul Sharma. All rights reserved.
//

import UIKit

class ApplyingOfferCell: UITableViewCell {
    @IBOutlet weak var offerTitleLabel : UILabel!
    @IBOutlet weak var offerDetailsLabel : UILabel!
    @IBOutlet weak var couponCodeLabel : UILabel!
    @IBOutlet weak var removeButton : UIButton!
    @IBOutlet weak var innerContentView : UIView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func layoutSubviews() {
        CommonClass.makeViewCircularWithCornerRadius(self.couponCodeLabel, borderColor: .clear, borderWidth: 0, cornerRadius: 3)
        CommonClass.makeViewCircular(self.removeButton, borderColor: .clear, borderWidth: 0)
        super.layoutSubviews()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
