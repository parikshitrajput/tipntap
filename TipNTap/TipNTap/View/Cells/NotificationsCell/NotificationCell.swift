//
//  NotificationCell.swift
//  TipNTap
//
//  Created by TecOrb on 27/02/18.
//  Copyright © 2018 Nakul Sharma. All rights reserved.
//

import UIKit

class NotificationCell: UITableViewCell {
    @IBOutlet weak var cellIcon : UIImageView!
    @IBOutlet weak var transactionTypeIcon : UIImageView!
    @IBOutlet weak var messageLabel : UILabel!
    @IBOutlet weak var fromLabel : UILabel!
    @IBOutlet weak var dateLabel : UILabel!
    @IBOutlet weak var amountLabel : UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    override func layoutSubviews() {
        CommonClass.makeViewCircular(self.cellIcon, borderColor: UIColor.clear, borderWidth: 0)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
}
