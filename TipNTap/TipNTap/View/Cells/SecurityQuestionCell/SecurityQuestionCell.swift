//
//  SecurityQuestionCell.swift
//  TipNTap
//
//  Created by TecOrb on 21/02/18.
//  Copyright © 2018 Nakul Sharma. All rights reserved.
//

import UIKit

class SecurityQuestionCell: UITableViewCell {
    @IBOutlet weak var questionLabel : UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
