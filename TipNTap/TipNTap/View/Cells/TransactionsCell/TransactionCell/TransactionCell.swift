//
//  TransactionCell.swift
//  TipNTap
//
//  Created by TecOrb on 14/03/18.
//  Copyright © 2018 Nakul Sharma. All rights reserved.
//

import UIKit

class TransactionCell: UITableViewCell {
    @IBOutlet weak var txnTypeLabel: UILabel!
    @IBOutlet weak var txnMessagelabel: UILabel!
    @IBOutlet weak var txnAmountlabel: UILabel!
    @IBOutlet weak var txnDateLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
