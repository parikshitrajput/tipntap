//
//  RecentScanCell.swift
//  TipNTap
//
//  Created by TecOrb on 28/02/18.
//  Copyright © 2018 Nakul Sharma. All rights reserved.
//

import UIKit

class RecentScanCell: UITableViewCell {
    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var userNameLabel : UILabel!
    @IBOutlet weak var amountLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func layoutSubviews() {
        CommonClass.makeViewCircular(self.userImageView, borderColor: UIColor.clear, borderWidth: 0)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
