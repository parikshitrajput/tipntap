//
//  SettingTextFieldCell.swift
//  TipNTap
//
//  Created by TecOrb on 31/03/18.
//  Copyright © 2018 Nakul Sharma. All rights reserved.
//

import UIKit

class SettingTextFieldCell: UITableViewCell {
    @IBOutlet weak var settingIcon : UIImageView!
    @IBOutlet weak var settingHintLabel : UILabel!
    @IBOutlet weak var editButton : UIButton!
    @IBOutlet weak var doneButton : UIButton!
    @IBOutlet weak var textField : UITextField!


    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func layoutSubviews() {
        CommonClass.makeViewCircularWithCornerRadius(self.editButton, borderColor: appColor.blue, borderWidth: 0, cornerRadius: 3)

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
