//
//  SettingSwitchCell.swift
//  TipNTap
//
//  Created by TecOrb on 31/03/18.
//  Copyright © 2018 Nakul Sharma. All rights reserved.
//

import UIKit

class SettingSwitchCell: UITableViewCell {
    @IBOutlet weak var settingIcon : UIImageView!
    @IBOutlet weak var settingSwitch: UISwitch!
    @IBOutlet weak var settingHintLabel : UILabel!
    @IBOutlet weak var settingNameLabel : UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    override func layoutSubviews() {
        CommonClass.makeViewCircularWithCornerRadius(self.settingSwitch, borderColor: appColor.blue, borderWidth: 0, cornerRadius: 15)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
