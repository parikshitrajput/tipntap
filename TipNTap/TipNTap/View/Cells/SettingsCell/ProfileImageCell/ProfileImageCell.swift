//
//  ProfileImageCell.swift
//  TipNTap
//
//  Created by TecOrb on 02/04/18.
//  Copyright © 2018 Nakul Sharma. All rights reserved.
//

import UIKit

class ProfileImageCell: UITableViewCell {
    @IBOutlet weak var profileImageView : UIImageView!
    @IBOutlet weak var editButton : UIButton!
    @IBOutlet weak var doneButton : UIButton!
    @IBOutlet weak var changePhotoLabel : UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
