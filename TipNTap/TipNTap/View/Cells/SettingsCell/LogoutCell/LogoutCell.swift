//
//  LogoutCell.swift
//  TipNTap
//
//  Created by TecOrb on 02/04/18.
//  Copyright © 2018 Nakul Sharma. All rights reserved.
//

import UIKit

class LogoutCell: UITableViewCell {
    @IBOutlet weak var logoutButton: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
}
