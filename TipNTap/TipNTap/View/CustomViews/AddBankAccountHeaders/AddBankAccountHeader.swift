//
//  AddBankAccountHeader.swift
//  TipNTap
//
//  Created by TecOrb on 19/03/18.
//  Copyright © 2018 Nakul Sharma. All rights reserved.
//

import UIKit
class BusinessPersonalDetailHeader: UIView {
    class func instanceFromNib() -> BusinessPersonalDetailHeader {
        return UINib(nibName: "AddBankAccountHeader", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! BusinessPersonalDetailHeader
    }

}


class IndividualPersonalDetailHeader: UIView {
    class func instanceFromNib() -> IndividualPersonalDetailHeader {
        return UINib(nibName: "AddBankAccountHeader", bundle: nil).instantiate(withOwner: nil, options: nil)[1] as! IndividualPersonalDetailHeader
    }

}

class BusinessDetailHeader: UIView {
    class func instanceFromNib() -> BusinessDetailHeader {
        return UINib(nibName: "AddBankAccountHeader", bundle: nil).instantiate(withOwner: nil, options: nil)[2] as! BusinessDetailHeader
    }

}
