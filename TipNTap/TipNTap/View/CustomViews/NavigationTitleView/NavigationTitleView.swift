//
//  NavigationTitleView.swift
//  GPDock
//
//  Created by TecOrb on 29/11/17.
//  Copyright © 2017 Nakul Sharma. All rights reserved.
//

import UIKit

class NavigationTitleView: UIView {
    @IBOutlet weak var titleLabel :NKCustomLabel!
    //let gradient = CAGradientLayer()
    var shouldAttributtedMiddle : Bool = false{
        didSet{
            self.setNeedsLayout()
            self.layoutIfNeeded()
        }
    }


    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
//    override func draw(_ rect: CGRect) {
//        // Drawing code
//    }


    class func instanceFromNib() -> NavigationTitleView {
        return UINib(nibName: "NavigationTitleView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! NavigationTitleView
    }

    override func layoutSubviews() {
        if shouldAttributtedMiddle{
            self.titleLabel.attributedText = self.attributedTitle()
        }
    }



    private func attributedTitle() -> NSAttributedString{
        let title = NSMutableAttributedString(string: "TipNTap")
        
        if isBlueTheme{
            title.addAttributes([NSAttributedStringKey.foregroundColor : UIColor.white,NSAttributedStringKey.font:fonts.Raleway.bold.font(fontSize.xXLarge)], range: NSRange.init(location: 0, length: 3))
            title.addAttributes([NSAttributedStringKey.foregroundColor : UIColor.orange,NSAttributedStringKey.font:fonts.Raleway.bold.font(fontSize.xXLarge)], range: NSRange.init(location: 3, length: 1))
            title.addAttributes([NSAttributedStringKey.foregroundColor : UIColor.white,NSAttributedStringKey.font:fonts.Raleway.bold.font(fontSize.xXLarge)], range: NSRange.init(location: 4, length: 3))
        }else{
            title.addAttributes([NSAttributedStringKey.foregroundColor : appColor.blue,NSAttributedStringKey.font:fonts.Raleway.bold.font(fontSize.xXLarge)], range: NSRange.init(location: 0, length: 3))
            title.addAttributes([NSAttributedStringKey.foregroundColor : appColor.red,NSAttributedStringKey.font:fonts.Raleway.bold.font(fontSize.xXLarge)], range: NSRange.init(location: 3, length: 1))
            title.addAttributes([NSAttributedStringKey.foregroundColor : appColor.blue,NSAttributedStringKey.font:fonts.Raleway.bold.font(fontSize.xXLarge)], range: NSRange.init(location: 4, length: 3))
        }
        return title
    }
}
